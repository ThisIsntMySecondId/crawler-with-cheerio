//? Info Plz manually preprocess relative url to absolute url and vice versa
//Todo: You need to go inside the blog to get details
const config = {
    name: 'fredkschott',
    mainUrl: 'http://fredkschott.com/',
    startUrl: 'http://fredkschott.com/post/',
    postWrapper: '.blog-post-list li',
    details: {
        title: {
            type: 'text',
            location: 'a.post-title',
            postprocessing: null,
        },
        postLink: {
            type: 'url',
            location: 'a.post-title',
            postprocessing: null,
        },
    },
    nextButton: '',
    storeContent: true,
    contentLocation: '.blog-post',
    contentLink: 'postLink',
    get contentSaveLocation() {
        return `./${this.name}-content/`;
    },
    saveLocation: 'post11.csv',
    get csvHeaders() {
        return Object.entries(config.details).map(x => x[0]).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

module.exports = config;

//TOdo: Three Types text, image, url
//Todo: Post Processing
//Todo: work with next button a little bit and add post processing in next button
//Todo: give resonable names to properties of config (not post, details etc.)