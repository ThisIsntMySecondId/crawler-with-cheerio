const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const writeStream = fs.createWriteStream('post3.csv');

// Write Headers
writeStream.write(`Sr.No.,Title,Link,Author,Date,Tags,Contents \n`);

const mainLink = "https://stackabuse.com";

// const url = 'https://stackabuse.com/tag/node/';
const url = 'https://stackabuse.com/tag/node/page/7/';

const getHtmlData = (url) => {
    return new Promise((resolve, reject) => {
        request(url, (err, response, html) => {
            if (err) {
                console.log("cant access the url: ", url);
                reject(err);
            }

            if (!err && response.statusCode == 200) {
                // console.log("found html");
                resolve(html);
            }
        });
    });
};

const parseHtml = (html) => {
    const $ = cheerio.load(html);
    let data = {};
    let posts = [];

    const heading = $('.post');
    heading.each((i, el) => {
        let post = {};
        post.id = i;
        post.title = $(el).find('.post-head .post-title a').text();
        post.url = $(el).find('.post-head .post-title a').attr('href');
        post.author = $(el).find('.post-head .post-meta .author a').text();
        post.date = $(el).find('.post-head .post-meta .date').text().replace(/,/, '');
        post.tags = $(el).find('.post-footer .tag-list').text().trim();
        posts.push(post);
    });

    let nextpath = $('nav.pagination a.older-posts').attr('href');
    if (!!nextpath) data.nextLink = mainLink + nextpath;
    else data.nextLink = "";
    data.posts = posts;
    // console.log("data is produced");
    return Promise.resolve(data);
};

function getPostContent(link, title, file_name) {
    request(`https://stackabuse.com${link}`, (err, response, html) => {
        if (!err && response.statusCode == 200) {
            const $ = cheerio.load(html);
            const content = $('section article.post .post-content');
            // console.log(content.html());
            fs.writeFile(`./content/${file_name}-content.html`, content.html(), (err) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log(`Wrote contents of Post ${title} in file ${file_name} successfully`);
                    return;
                }
            });
        }
    });
}

const getAllData = async (startUrl) => {
    nextLink = startUrl;
    number_of_posts = 0;
    while (nextLink) {
        // console.log("jii");
        // console.log(nextLink);
        let ddd = await getHtmlData(nextLink).then(parseHtml);
        // console.log(ddd)
        nextLink = ddd.nextLink;
        // console.log(`${ddd.posts}`);
        ddd.posts.forEach(post => {
            // (console.log(post.title));
            // (console.log(post.url));
            // Write Row in CSV file
            // writeStream.write(`${title},${url},${author},${date},\"${tags}\" \n`);
            // console.log(`${post.url}`);
            getPostContent(post.url, post.title, `Post ${++number_of_posts}`);
            writeStream.write(`${number_of_posts},\"${post.title}\",${post.url},${post.author},${post.date},\"${post.tags}\",./content/${number_of_posts}-content.html \n`);
        });
    }

}

getAllData(url);