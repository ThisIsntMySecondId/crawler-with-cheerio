const config = {
    mainUrl: 'https://www.labnol.org/',
    startUrl: 'https://www.labnol.org/',
    post: '.feature.feature-home',
    details: {
        title: '.feature__body .home__title',
        postLink: '.feature feature-home a',
        date: '.feature__body .home__date',
    },
    nextButton: '.col-sm-12.col-lg-7.col-md-7 a.btn.btn--primary',
    saveLocation: 'post6.csv',
    csvHeaders: function () {
        return Object.keys(this.details).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    },
};

// console.log(Object.entries(config.details)[1][1]);

module.exports = config;

// console.log(config.startUrl.toString());