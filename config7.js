//? Info Plz manually preprocess relative url to absolute url and vice versa

const config = {
    name: 'thrillophilia',
    mainUrl: 'http://www.thrillophilia.com/blog/',
    startUrl: 'http://www.thrillophilia.com/blog/',
    postWrapper: '.isobrick.isotope-item',
    details: {
        title: {
            type: 'text',
            location: '.isobrick-inner .thumbovertext h2.title a',
            postprocessing: null,
        },
        postLink: {
            type: 'url',
            location: '.isobrick-inner a.brick-thumb-link',
            postprocessing: null,
        },
        date: {
            type: 'text',
            location: '.isobrick-inner .thumboverdate',
            postprocessing: null,
        },
        image: {
            type: 'image',
            location: '.isobrick-inner img',
            postprocessing: null,
        },
    },
    nextButton: '.pagination a.older-posts',
    storeContent: false,
    contentLocation: '',
    contentLink: 'postLink',
    get contentSaveLocation() {
        return `./${this.name}-content/`;
    },
    saveLocation: 'post12.csv',
    get csvHeaders() {
        return Object.entries(config.details).map(x => x[0]).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

module.exports = config;

//TOdo: Three Types text, image, url
//Todo: Post Processing
//Todo: work with next button a little bit and add post processing in next button
//Todo: give resonable names to properties of config (not post, details etc.)