//? Info Plz manually preprocess relative url to absolute url and vice versa
//Note: Title is compulsory and is used as file name for storing post content
const config = {
    name: 'labnol',
    mainUrl: 'https://www.labnol.org/',
    startUrl: 'https://www.labnol.org/',
    postWrapper: '.feature.feature-home',
    details: {
        title: {
            type: 'text',
            location: '.feature__body h2.home__title',
            postprocessing: null,
        },
        postLink: {
            type: 'url',
            location: '.feature.feature-home a',
            postprocessing: null,
        },
        date: {
            type: 'text',
            location: '.feature__body .home__date',
            postprocessing: null,
        },
        image: {
            type: 'image',
            location: '.feature.feature-home a .feature__img',
            postprocessing: null,
            data: 'src',
        },
    },
    nextButton: '.col-sm-12.col-lg-7.col-md-7 a.btn.btn--primary',
    storeContent: true,
    contentLocation: 'article',
    contentLink: 'postLink',
    get contentSaveLocation() {
        return `./${this.name}-content/`;
    },
    saveLocation: 'post8.csv',
    get csvHeaders() {
        return Object.entries(config.details).map(x => x[0]).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

module.exports = config;

//TOdo: Three Types text, image, url
//Todo: Post Processing
//Todo: work with next button a little bit and add post processing in next button
//Todo: give resonable names to properties of config (not post, details etc.)