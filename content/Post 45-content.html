
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>By default, Node.js is fairly secure by itself. Although, there are definitely things you have to watch out for. If your Node web-app starts to get more and more popular, for example, you&apos;ll need to be thinking more and more about security to ensure that you&apos;re keeping your users&apos; data safe.</p>
<p>After seeing some questions about Node.js security around the web in the last few weeks, I figured it would be helpful to write up a short guide on what you can do to secure your apps.</p>
<p>Many, if not all, of the suggestions here are really easy to follow and implement, and are mostly specific to Node itself or its modules. So I won&apos;t be covering things like a encryption or <a href="https://stackabuse.com/implementing-user-authentication-the-right-way/">user authentication</a>, which is a bit out of scope of this article. Many of the tips here will be focused on Node web frameworks, since those are typically the most vulnerable to attack.</p>
<h3 id="dontruncodewithsudo">Don&apos;t Run Code with Sudo</h3>
<p>This happens way more than you think, and it&apos;s dangerous. Since it gives root permissions, running code with sudo can make annoying problems go away, like writing to a directory that the user doesn&apos;t own. But that&apos;s just the easy way out, and these shortcuts bring up a mess of other problems you shouldn&apos;t ignore.</p>
<p>Instead, find the root cause of the problem and figure out a way to get around it without compromising the whole system.</p>
<p>So, for example, if you need to open port 80 for a web service but can&apos;t since you&apos;re not running under root, you should instead use a proxy like <a href="https://en.wikipedia.org/wiki/Nginx">Nginx</a> to forward the requests from port 80 to whatever other port your service is actually running on.</p>
<p>If you run under root and your application is taken over by attackers, they can then do whatever they want with your system <em>and</em> your data. This is the worst-case scenario that you&apos;re trying to protect yourself from.</p>
<h3 id="avoidevalatallcosts">Avoid <code>eval</code> at all Costs</h3>
<p>Okay, I&apos;ll admit it, at times it can be tempting to make your code more dynamic by letting it execute arbitrary JavaScript using <code>eval</code>, but believe me, this is a bad idea.</p>
<p>Some people even try to use it when they get lazy with parsing user input. After all, the V8 JavaScript engine is really good at parsing things like simple math operations, so it would be tempting to use that to your advantage:</p>
<pre><code class="language-javascript">var result = eval(&apos;(13 + (2 * 23.249) / 0.981)&apos;);
// result = 60.398572884811415
</code></pre>
<p>There are just too many ways this can come back to bite you. Unless you&apos;re an expert and know how to protect yourself from all of the <a rel="nofollow" target="_blank" href="https://aw-snap.info/articles/js-examples.php">different kinds of malicious JavaScript</a> code, just steer clear of this.</p>
<p>Here is a simple example of the exploit:</p>
<pre><code class="language-javascript">var userInput = req.body.userInput;    // User entered &apos;process.exit()&apos;
var answer = eval(userInput);          // App quits here
</code></pre>
<p>Running this code will shut down your app, causing a denial of service (DOS) to your users.</p>
<h3 id="addremovehttpheaders">Add/Remove HTTP Headers</h3>
<p>There are quite a few HTTP headers out there that can both help you and hurt you. Using the right ones in the right way is the tricky part.</p>
<p>Express, by default, adds the <code>X-Powered-By: Express</code> header, which really does nothing but tell potential attackers what web framework you&apos;re using, and therefore how to exploit it based on publicly-known vulnerabilities. The more information they have about your technology stack, the more ways they&apos;ll be able to attack it.</p>
<p>That&apos;s where <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/helmet">helmet</a> comes in to play. Helmet is a small module for Node that helps secure Express/Connect apps by adding/removing various HTTP headers.</p>
<p>You can do anything from enabling <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/hsts">HSTS</a> to preventing <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/frameguard">click-jacking attacks</a>. These are things that take little to no work on your part, but they can make a world of difference. So if you&apos;re building an Express app, this should be a no-brainer (and really, for any web service you should do this).</p>
<h3 id="usescanningutilitieslikeretirejs">Use Scanning Utilities like Retire.js</h3>
<p>Not all programmers are security experts, and while you should do your best to stay up-to-date on common exploits like XSS or SQL injection, it&apos;s tough to know them all.</p>
<p>To make up for this, you should try using tools like <a rel="nofollow" target="_blank" href="https://github.com/RetireJS/retire.js">Retire.js</a>, which scans your Node app for dependencies that contain vulnerabilities.</p>
<p>For example, Ember.js has a specific XSS vulnerability in a few different versions (<a rel="nofollow" target="_blank" href="https://groups.google.com/forum/#!topic/ember-security/1h6FRgr8lXQ">CVE-2014-0046</a>), all of which are checked by Retire.js. When you execute <code>retire</code> in your project&apos;s directory, it&apos;ll compare packages in <code>node_modules</code> to a public repository of vulnerabilities and report to you which of your dependencies are insecure.</p>
<p>There are just far too many vulnerabilities in a lot of these packages to check yourself, so you&apos;re better off letting a tool like this do it for you.</p>
<p>You can easily make this a part of your workflow by integrating it with Grunt or Gulp, thanks to the plugins provided. The details are in the README.</p>
<p>Another option is to just run it in a <code>prepublish</code> command, which would run before npm sends your package to the repository. Just add something like this to your <code>package.json</code>:</p>
<pre><code class="language-json">{
  &quot;name&quot;: &quot;myProject&quot;,
  &quot;version&quot;: &quot;0.0.1&quot;,
  &quot;scripts&quot;: {
    &quot;prepublish&quot;: &quot;retire&quot;,
  }
}
</code></pre>
<h3 id="becarefulwiththechild_processmodule">Be Careful with the <code>child_process</code> Module</h3>
<p>Like <code>eval</code>, using <code>spawn</code> and <code>exec</code> from the <code>child_process</code> module can be really useful, but also <em>really</em> dangerous. Any user input that sneaks in to these commands could mean your system gets compromised pretty quickly (especially if you&apos;re running your app with sudo!).</p>
<p>For example, <a rel="nofollow" target="_blank" href="https://en.wikipedia.orfg/wiki/ImageMagick">Image Magick</a> is a very popular command line tool for displaying, converting, and editing images. With so many web-apps using images these days, Image Magick is often being used in the background for things like cropping and resizing. To use this tool with Node, you might see code like this:</p>
<pre><code class="language-javascript">child = child_process.exec(&apos;convert &apos; + imageFilename + &apos; &apos; + imageFilename + &apos;.bmp&apos;, [&apos;-depth&apos;, &apos;24&apos;], function(err, stdout, stderr) {
    console.log(&apos;Done&apos;);
});
</code></pre>
<p>This may look harmless, but with a carefully crafted <code>imageFilename</code>, you can execute any code you want in the shell.</p>
<p><code>exec</code> should only be used if it doesn&apos;t depend on user input <em>or</em> if the arguments are heavily sanitized. Even better, check npm for a library that wraps the command line tool. These are usually built with this kind of security in mind, or at least have more eyes on the code to check for problems. For Image Magick, there are a few modules available, like <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/gm">gm</a>.</p>
<h3 id="understandthevulnerabilities">Understand the Vulnerabilities</h3>
<p>Many vulnerabilities in web applications apply to all services, regardless of programming language and framework used. Although, <em>how</em> you attack those services may differ based on the technology stack you&apos;re using. To better defend yourself, you really need to learn how these exploits work.</p>
<p>Lucky for you, <a rel="nofollow" target="_blank" href="https://www.owasp.org/index.php/Top_10_2013-Top_10">OWASP</a> puts out a list of the top 10 risks to web applications. Review these and then do a thorough analysis of your website to see if any of these apply to you.</p>
<p>Even better, check out <a rel="nofollow" target="_blank" href="https://github.com/OWASP/NodeGoat">NodeGoat</a>, which is a deployable website created by OWASP meant to teach you how to identify these risks in Node applications specifically. There is no better way to learn these concepts than actually doing it yourself.</p>
<p>The <a rel="nofollow" target="_blank" href="https://nodegoat.herokuapp.com/tutorial">tutorial</a> provided will walk you through all of the risks, showing specific examples of how to both exploit and defend against the vulnerabilities. For example, here is a video provided by OWASP showing how to inject JavaScript using a web form:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/krOx9QWwcYw" frameborder="0" allowfullscreen></iframe>
<h3 id="moreinfo">More Info</h3>
<p>Node security is a big topic, so it wouldn&apos;t be reasonable to try and cover it all here. If you&apos;re interested in getting more details, I&apos;d suggest reading some more resources, like these:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://nodesecurity.io/advisories">Node Security Advisories</a></li>
<li><a rel="nofollow" target="_blank" href="http://lab.cs.ttu.ee/dl93">Analysis of Node.js platform web application security</a> [PDF]</li>
<li><a rel="nofollow" target="_blank" href="https://medium.com/@fun_cuddles/opening-files-in-node-js-considered-harmful-d7de566d499f#.z9awc1ctt">Opening files in Node.js considered harmful</a></li>
<li><a rel="nofollow" target="_blank" href="https://groups.google.com/forum/#!forum/nodejs-sec">Node.js Security Google Group</a></li>
<li><a rel="nofollow" target="_blank" href="https://cdn.oreillystatic.com/en/assets/1/event/106/Top%20Overlooked%20Security%20Threats%20To%20Node_js%20Web%20Applications%20Presentation%201.pdf">Top Overlooked Security Threats To Node.js Web Applications</a> [PDF]</li>
</ul>
<h3 id="conclusion">Conclusion</h3>
<p>All too often the security of an application is an after-thought to development and design. It&apos;s difficult enough just to get your code to work correctly, let alone making it safe to use for your users.</p>
<p>Luckily you&apos;re not the only one going through these problems, so that means there are plenty of tools and resources out there created by others to help you secure your apps quickly and easily. Just take the time to search NPM, ask questions on forums, or even hire an expert. It&apos;s definitely worth the time and money!</p>
<p><em>What other ways do you secure your Node applications? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					