
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h2 id="introduction">Introduction</h2>
<p>I&apos;ll admit that I was one of those people that decided to learn Node.js simply because of the buzz around it and how much everyone was talking about it. I figured there must be something special about it if it has this much support so early on in its life. I mostly came from a C, Java, and Python background, so JavaScript&apos;s asynchronous style was much different than anything I had encountered before.</p>
<p>As many of you probably know, all JavaScript really is underneath is a single-threaded event loop that processes queued events. If you were to execute a long-running task within a single thread then the process would block, causing other events to have to wait to be processed (i.e. UI hangs, data doesn&apos;t get saved, etc). This is exactly what you want to avoid in an event-driven system. <a href="https://www.youtube.com/watch?v=QgwSUtYSUqA" rel="nofollow" target="_blank">Here</a> is a great video explaining much more about the JavaScript event loop.</p>
<p>To solve this blocking problem, JavaScript heavily relies on callbacks, which are functions that run after a long-running process (IO, timer, etc) has finished, thus allowing the code execution to proceed past the long-running task.</p>
<pre><code class="language-javascript">downloadFile(&apos;example.com/weather.json&apos;, function(err, data) {
	console.log(&apos;Got weather data:&apos;, data);
});
</code></pre>
<h2 id="theproblemcallbackhell">The problem: Callback hell</h2>
<p>While the concept of callbacks is great in theory, it can lead to some really confusing and difficult-to-read code. Just imagine if you need to make callback after callback:</p>
<pre><code class="language-javascript">getData(function(a){
    getMoreData(a, function(b){
        getMoreData(b, function(c){ 
            getMoreData(c, function(d){ 
	            getMoreData(d, function(e){ 
		            ...
		        });
	        });
        });
    });
});
</code></pre>
<p>As you can see, this can really get out of hand. Throw in some <code>if</code> statements, <code>for</code> loops, function calls, or comments and you&apos;ll have some very hard-to-read code. Beginners especially fall victim to this, not understanding how to avoid this &quot;pyramid of doom&quot;.</p>
<h2 id="alternatives">Alternatives</h2>
<h3 id="designaroundit">Design around it</h3>
<p>So many programmers get caught up in callback hell because of this (poor design) alone. They don&apos;t really think about their code structure ahead of time and don&apos;t realize how bad their code has gotten until after its too late. As with any code you&apos;re writing, you should stop and think about what can be done to make it simpler and more readable before, or while, writing it. Here are a few tips you can use to <strong>avoid callback hell</strong> (or at least manage it).</p>
<h4 id="usemodules">Use modules</h4>
<p>In just about every programming language, one of the best ways to reduce complexity is to modularize. JavaScript programming is no different. Whenever you&apos;re writing code, take some time to step back and figure out if there has been a common pattern you frequently encounter.</p>
<p>Are you writing the same code multiple times in different places? Do different parts of your code follow a common theme? If so, you have an opportunity to clean things up and abstract out and reuse code.</p>
<p>There are thousands of modules out there you can look at for reference, but here are a few to consider. They handle common, but very specific, tasks that would otherwise clutter your code and reduce readability: <a href="https://github.com/blakeembrey/pluralize" rel="nofollow" target="_blank">Pluralize</a>, <a href="https://www.npmjs.com/package/csv" rel="nofollow" target="_blank">csv</a>, <a href="https://www.npmjs.com/package/qs" rel="nofollow" target="_blank">qs</a>, <a href="https://www.npmjs.com/package/clone" rel="nofollow" target="_blank">clone</a>.</p>
<h4 id="giveyourfunctionsnames">Give your functions names</h4>
<p>When reading code (especially messy, unorganized code), its easy to lose track of the logic flow, or even syntax, when small spaces are congested with so many nested callbacks. One way to help combat this is to name your functions, so all you&apos;ll have to do is glance at the name and you&apos;ll a better idea as to what it does. It also gives your eyes a syntax reference point.</p>
<p>Consider the following code:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

var myFile = &apos;/tmp/test&apos;;
fs.readFile(myFile, &apos;utf8&apos;, function(err, txt) {
    if (err) return console.log(err);

    txt = txt + &apos;\nAppended something!&apos;;
    fs.writeFile(myFile, txt, function(err) {
        if(err) return console.log(err);
        console.log(&apos;Appended text!&apos;);
    });
});
</code></pre>
<p>Looking at this may take you a few seconds to realize what each callback does and where it starts. Adding a little extra information (names) to the functions can make a big difference for readability, especially when you&apos;re multiple levels deep in callbacks:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

var myFile = &apos;/tmp/test&apos;;
fs.readFile(myFile, &apos;utf8&apos;, function appendText(err, txt) {
    if (err) return console.log(err);

    txt = txt + &apos;\nAppended something!&apos;;
    fs.writeFile(myFile, txt, function notifyUser(err) {
        if(err) return console.log(err);
        console.log(&apos;Appended text!&apos;);
    });
});
</code></pre>
<p>Now just a quick glance will tell you the first function appends some text while the second function notifies the user of the change.</p>
<h4 id="declareyourfunctionsbeforehand">Declare your functions beforehand</h4>
<p>One of the best ways to reduce code clutter is by maintaining better separation of code. If you declare a callback function beforehand and call it later, you&apos;ll avoid the deeply nested structures that make callback hell so difficult to work with.</p>
<p>So you could go from this...</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

var myFile = &apos;/tmp/test&apos;;
fs.readFile(myFile, &apos;utf8&apos;, function(err, txt) {
    if (err) return console.log(err);

    txt = txt + &apos;\nAppended something!&apos;;
    fs.writeFile(myFile, txt, function(err) {
        if(err) return console.log(err);
        console.log(&apos;Appended text!&apos;);
    });
});
</code></pre>
<p>...to this:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

function notifyUser(err) {
    if(err) return console.log(err);
    console.log(&apos;Appended text!&apos;);
};

function appendText(err, txt) {
    if (err) return console.log(err);

    txt = txt + &apos;\nAppended something!&apos;;
    fs.writeFile(myFile, txt, notifyUser);
}

var myFile = &apos;/tmp/test&apos;;
fs.readFile(myFile, &apos;utf8&apos;, appendText);
</code></pre>
<p>While this can be a great way to help ease the problem, it doesn&apos;t completely solve the problem. When reading code written in this way, if you don&apos;t remember exactly what each function does then you&apos;ll have to go back and look at each one to retrace the logic flow, which can take time.</p>
<h3 id="asyncjs">Async.js</h3>
<p>Thankfully, libraries like <a href="https://github.com/caolan/async" rel="nofollow" target="_blank">Async.js</a> exist to try and curb the problem. Async adds a thin layer of functions on top of your code, but can greatly reduce the complexity by avoiding callback nesting.</p>
<p>Many helper methods exist in Async that can be used in different situations, like <a href="https://github.com/caolan/async#seriestasks-callback" rel="nofollow" target="_blank">series</a>, <a href="https://github.com/caolan/async#parallel" rel="nofollow" target="_blank">parallel</a>, <a href="https://github.com/caolan/async#waterfall" rel="nofollow" target="_blank">waterfall</a>, etc. Each function has a specific use-case, so take some time to learn which one will help in which situations.</p>
<p>As good as Async is, like anything, its not perfect. Its very easy to get carried away by combining series, parallel, forever, etc, at which point you&apos;re right back to where you started with messy code. Be careful not to prematurely optimize. Just because a few async tasks can be run in parallel doesn&apos;t always mean they should. In reality, since Node is only single-threaded, running tasks in parallel on using Async has little to no performance gain.</p>
<p>The code from above can be simplified using Async&apos;s waterfall:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);
var async = require(&apos;async&apos;);

var myFile = &apos;/tmp/test&apos;;

async.waterfall([
    function(callback) {
        fs.readFile(myFile, &apos;utf8&apos;, callback);
    },
    function(txt, callback) {
        txt = txt + &apos;\nAppended something!&apos;;
        fs.writeFile(myFile, txt, callback);
    }
], function (err, result) {
    if(err) return console.log(err);
    console.log(&apos;Appended text!&apos;);
});
</code></pre>
<h3 id="promises">Promises</h3>
<p>Although Promises can take a bit to grasp, in my opinion they are one of the more important concepts you can learn in JavaScript. During development of one of my <a href="https://polymetrics.io">SaaS apps</a>, I ended up rewriting the entire codebase using Promises. Not only did it reduce the number of lines of code drastically, but it made the logical flow of the code much easier to follow.</p>
<p>Here is an example using the very fast and very popular Promise library, <a href="https://github.com/petkaantonov/bluebird" rel="nofollow" target="_blank">Bluebird</a>:</p>
<pre><code class="language-javascript">var Promise = require(&apos;bluebird&apos;);
var fs = require(&apos;fs&apos;);
Promise.promisifyAll(fs);

var myFile = &apos;/tmp/test&apos;;
fs.readFileAsync(myFile, &apos;utf8&apos;).then(function(txt) {
	txt = txt + &apos;\nAppended something!&apos;;
	fs.writeFile(myFile, txt);
}).then(function() {
	console.log(&apos;Appended text!&apos;);
}).catch(function(err) {
	console.log(err);
});
</code></pre>
<p>Notice how this solution is not only shorter than the previous solutions, but it is easier to read as well (although, admittedly, Promise-style code can take some getting used to). Take the time to learn and understand Promises, it&apos;ll be worth your time. However, Promises are definitely not the solution to all our problems in asynchronous programming, so don&apos;t assume by using them you will have a fast, clean, bug-free app. The key is knowing when they&apos;ll be useful to you.</p>
<p>A few Promise libraries you should check out are <a href="https://github.com/kriskowal/q" rel="nofollow" target="_blank">Q</a>, <a href="https://github.com/petkaantonov/bluebird" rel="nofollow" target="_blank">Bluebird</a>, or the <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise" rel="nofollow" target="_blank">built-in Promises</a> if you&apos;re using ES6.</p>
<h3 id="asyncawait">Async/Await</h3>
<p><em>Note: This is an ES7 feature, which currently isn&apos;t supported in Node or io.js. However, you can use it right now with a transpiler like <a rel="nofollow" target="_blank" href="https://babeljs.io">Babel</a>.</em></p>
<p>Another option to clean up your code, and my soon-to-be favorite (when it has broader support), is using <code>async</code> functions. This will allow you to write code that looks much more like synchronous code, yet is still asynchronous.</p>
<p>An example:</p>
<pre><code class="language-javascript">async function getUser(id) {
    if (id) {
        return await db.user.byId(id);
    } else {
        throw &apos;Invalid ID!&apos;;
    }
}

try {
	let user = await getUser(123);
} catch(err) {
	console.error(err);
}
</code></pre>
<p>The <code>db.user.byId(id)</code> call returns a <code>Promise</code>, which we&apos;d normally have to use with <code>.then()</code>, but with <code>await</code> we can return the resolved value directly.</p>
<p>Notice that the function containing the <code>await</code> call is prefixed with <code>async</code>, which tells us that it contains asynchronous code and must also be called with <code>await</code>.</p>
<p>Another big advantage to this method is we can now use <code>try/catch</code>, <code>for</code>, and <code>while</code> with our asynchronous functions, which is much more intuitive than chaining promises together.</p>
<p>Aside from using transpilers like Babel and <a rel="nofollow" target="_blank" href="https://github.com/google/traceur-compiler">Traceur</a>, you can also get functionality like this in Node with the <a href="https://www.npmjs.com/package/asyncawait">asyncawait</a> package.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Avoid such common problems as callback hell isn&apos;t easy, so don&apos;t expect to end your frustrations right away. We all get caught in it. Just try to slow down and take some time to think about the structure of your code. Like anything, practice makes perfect.</p>
<p><em>Have you run in to callback hell? If so, how do you get around it? Tell us in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					