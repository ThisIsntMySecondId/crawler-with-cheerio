
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>Writing unit tests is one of those things a lot of people forget to do or just avoid altogether, but when you have them they&apos;re lifesavers.</p>
<p><a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Test-driven_development">Test-driven development</a>, which means you write your tests before your code, is a great goal to strive for, but takes discipline and planning when you&apos;re programming. To make this whole process a lot easier, you need easy-to-use and powerful testing and assertion frameworks, which is exactly what <a rel="nofollow" target="_blank" href="https://mochajs.org/">Mocha</a> and <a rel="nofollow" target="_blank" href="http://chaijs.com/">Chai</a> are.</p>
<p>In this article I&apos;ll introduce you to these two libraries and show you how to use them together to quickly create readable and functional unit tests.</p>
<h3 id="chai">Chai</h3>
<p>Chai is an assertion library that provides both the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Behavior-driven_development">BDD</a> and <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Test-driven_development">TDD</a> styles of programming for testing your code in any testing framework.</p>
<p>Throughout this article, we&apos;ll focus on the BDD style using Chai&apos;s <code>expect</code> interface.</p>
<p><code>expect</code> uses a more natural language API to write your assertions, which will make your tests easier to write and improve upon later on down the road. This is done by chaining together getters to create and execute the assertion, making it easier to translate requirements in to code:</p>
<pre><code class="language-javascript">var user = {name: &apos;Scott&apos;};

// Requirement: The object &apos;user&apos; should have the property &apos;name&apos;

expect(user).to.have.property(&apos;name&apos;);
</code></pre>
<p>A few more examples of these getters are:</p>
<ul>
<li><code>to</code></li>
<li><code>be</code></li>
<li><code>is</code></li>
<li><code>and</code></li>
<li><code>has</code></li>
<li><code>have</code></li>
</ul>
<p>Quite a few of these getters can be chained together and used with assertion methods like <code>true</code>, <code>ok</code>, <code>exist</code>, and <code>empty</code> to create some complex assertions in just one line. A few examples:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var expect = require(&apos;chai&apos;).expect;

// Simple assertions
expect({}).to.exist;
expect(26).to.equal(26);
expect(false).to.be.false;
expect(&apos;hello&apos;).to.be.string;

// Modifiers (&apos;not&apos;)
expect([1, 2, 3]).to.not.be.empty;

// Complex chains
expect([1, 2, 3]).to.have.length.of.at.least(3);
</code></pre>
<p>A full list of the available methods can be found <a rel="nofollow" target="_blank" href="http://chaijs.com/api/bdd/">here</a>.</p>
<p>You might also want to check out the list of available <a rel="nofollow" target="_blank" href="http://chaijs.com/plugins/">plugins</a> for Chai. These make it much easier to test more complex features.</p>
<p>Take <a href="http://chaijs.com/plugins/chai-http/">chai-http</a> for example, which is a plugin that helps you test server routes.</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var chai = require(&apos;chai&apos;);
var chaiHttp = require(&apos;chai-http&apos;);

chai.use(chaiHttp);

chai.request(app)
    .put(&apos;/api/auth&apos;)
    .send({username: &apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="0e7d6d617a7a4e7d7a6f6d656f6c7b7d6b206d6163">[email&#xA0;protected]</a>&apos;, passsword: &apos;abc123&apos;})
    .end(function(err, res) {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
    });
</code></pre>
<h3 id="mocha">Mocha</h3>
<p>Mocha is a testing framework for Node.js that gives you the flexibility to run asynchronous (or synchronous) code serially. Any uncaught exceptions are shown alongside the test case in which it was thrown, making it easy to identify exactly what failed and why.</p>
<p>To use Mocha, I&apos;d suggest you install it globally with npm:</p>
<pre><code class="language-bash">$ npm install mocha -g
</code></pre>
<p>You&apos;ll want it to be a global install since the <code>mocha</code> command is used to run the tests for the project in your local directory.</p>
<p>Creating test cases are easy using the <code>describe()</code> method. <code>describe()</code> is used to bring structure to your tests by grouping other <code>describe()</code> calls and <code>it()</code> methods together, which is where the actual tests are located. This is probably best described with an example:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var expect = require(&apos;chai&apos;).expect;

describe(&apos;Math&apos;, function() {
    describe(&apos;#abs()&apos;, function() {
        it(&apos;should return positive value of given negative number&apos;, function() {
            expect(Math.abs(-5)).to.be.equal(5);
        });
    });
});
</code></pre>
<p><em>Note that with Mocha tests you don&apos;t need to <code>require()</code> any of the Mocha methods. These methods are provided globally when run with the <code>mocha</code> command.</em></p>
<p>In order to run these tests, save your file and use the <code>mocha</code> command:</p>
<pre><code class="language-bash">$ mocha .


  Math
    #abs()
      &#x2713; should return positive value of given number 


  1 passing (9ms)
</code></pre>
<p>The output is a breakdown of the tests that ran and their results. Notice how the nested <code>describe()</code> calls carry over to the results output. It&apos;s useful to have all of the tests for a given method or feature nested together.</p>
<p>These methods are the basis for the Mocha testing framework. Use them to compose and organize your tests however you like. We&apos;ll see one example of this in the next section.</p>
<h3 id="writingtests">Writing Tests</h3>
<p>The recommended way to organize your tests within your project is to put all of them in their own <code>/test</code> directory. By default, Mocha checks for unit tests using the globs <code>./test/*.js</code> and <code>./test/*.coffee</code>. From there, it will load and execute any file that calls the <code>describe()</code> method.</p>
<p>Personally, I like to use the suffix <code>.test.js</code> for the source files that actually contain Mocha tests. So an example directory structure could look like this:</p>
<pre><code>&#x251C;&#x2500;&#x2500; package.json
&#x251C;&#x2500;&#x2500; lib
&#x2502;   &#x251C;&#x2500;&#x2500; db.js
&#x2502;   &#x251C;&#x2500;&#x2500; models.js
&#x2502;   &#x2514;&#x2500;&#x2500; util.js
&#x2514;&#x2500;&#x2500; test
    &#x251C;&#x2500;&#x2500; db.test.js
    &#x251C;&#x2500;&#x2500; models.test.js
    &#x251C;&#x2500;&#x2500; util.test.js
    &#x2514;&#x2500;&#x2500; util.js
</code></pre>
<p><code>util.js</code> wouldn&apos;t contain any actual unit tests, just utility functions to help with testing.</p>
<p>You can use whatever structure makes sense to you (which is the nice thing about Mocha), but this has worked well for me in the past.</p>
<p>When it comes to actually writing the tests, it helps to organize them using the <code>describe()</code> methods. You can organize them by feature, function, file, or something else.</p>
<p>Expanding on our example from the previous section, we&apos;ll choose to organize tests by function, which results in something that looks like this:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var expect = require(&apos;chai&apos;).expect;

describe(&apos;Math&apos;, function() {
    describe(&apos;#abs()&apos;, function() {
        it(&apos;should return positive value of given negative number&apos;, function() {
            expect(Math.abs(-5)).to.be.equal(5);
        });

        it(&apos;should return positive value of given positive number&apos;, function() {
            expect(Math.abs(3)).to.be.equal(3);
        });

        it(&apos;should return 0 given 0&apos;, function() {
            expect(Math.abs(0)).to.be.equal(0);
        });
    });
});
</code></pre>
<p>Running the tests would then give you the output:</p>
<pre><code class="language-bash">$ mocha .


  Math
    #abs()
      &#x2713; should return positive value of given negative number 
      &#x2713; should return positive value of given positive number 
      &#x2713; should return 0 given 0 


  3 passing (11ms)
</code></pre>
<p>Expanding even further (I promise, this is the last one I&apos;ll show), you might even have tests for multiple methods in a single file. In this case the methods are grouped by the <code>Math</code> object:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var expect = require(&apos;chai&apos;).expect;

describe(&apos;Math&apos;, function() {
    describe(&apos;#abs()&apos;, function() {
        it(&apos;should return positive value of given negative number&apos;, function() {
            expect(Math.abs(-5)).to.be.equal(5);
        });

        it(&apos;should return positive value of given positive number&apos;, function() {
            expect(Math.abs(3)).to.be.equal(3);
        });

        it(&apos;should return 0 given 0&apos;, function() {
            expect(Math.abs(0)).to.be.equal(0);
        });
    });

    describe(&apos;#sqrt()&apos;, function() {
        it(&apos;should return the square root of a given positive number&apos;, function() {
            expect(Math.sqrt(25)).to.be.equal(5);
        });

        it(&apos;should return NaN for a given negative number&apos;, function() {
            expect(Math.sqrt(-9)).to.be.NaN;
        });

        it(&apos;should return 0 given 0&apos;, function() {
            expect(Math.sqrt(0)).to.be.equal(0);
        });
    });
});
</code></pre>
<p>Output:</p>
<pre><code class="language-bash">$ mocha .


  Math
    #abs()
      &#x2713; should return positive value of given negative number 
      &#x2713; should return positive value of given positive number 
      &#x2713; should return 0 given 0 
    #sqrt()
      &#x2713; should return the square root of a given positive number 
      &#x2713; should return NaN for a given negative number 
      &#x2713; should return 0 given 0 


  6 passing (10ms)
</code></pre>
<p>Okay, you get the idea.</p>
<p>Admittedly, most unit tests aren&apos;t this simple. A lot of times you&apos;ll probably need other resources to perform your tests, like a database, or some other external resource. In order to set this up, we can use one or more of the following <a rel="nofollow" target="_blank" href="https://mochajs.org/#hooks">Mocha hook</a> methods:</p>
<ul>
<li><code>before()</code>: Runs before all tests in the given block</li>
<li><code>beforeEach()</code>: Runs before each test in the given block</li>
<li><code>after()</code>: Runs after all tests in the given block</li>
<li><code>afterEach()</code>: Runs after each test in the given block</li>
</ul>
<p>These hooks are the perfect place for performing setup and teardown work required for your tests. As I already mentioned, one of the common use-cases is to establish a connection to your database before running the tests, which is shown in the following example:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var expect = require(&apos;chai&apos;).expect;
var Camo = require(&apos;camo&apos;);
var User = require(&apos;../models&apos;).User;

describe(&apos;Users&apos;, function() {

    var database = null;

    before(function(done) {
        Camo.connect(&apos;mongodb://localhost/app_test&apos;).then(function(db) {
            database = db;
            return database.dropDatabase();
        }).then(function() {}).then(done, done);
    });

    afterEach(function(done) {
        database.dropDatabase().then(function() {}).then(done, done);
    });

    describe(&apos;#save()&apos;, function() {
        it(&apos;should save User data to database&apos;, function(done) {
            // Use your database here...
        });
    });

    describe(&apos;#load()&apos;, function() {
        it(&apos;should load User data from database&apos;, function(done) {
            // Use your database here...
        });
    });
});
</code></pre>
<p>Before <em>any</em> of the tests are run, the function sent to our <code>before()</code> method is run (and only run once throughout the tests), which establishes a connection to the database. Once this is done, our test suites are then run.</p>
<p>Since we wouldn&apos;t want the data from one test suite to affect our other tests, we need to clear the data from our database after each suite is run. This is what <code>afterEach()</code> is for. We use this hook to clear all of the database data after <em>each</em> test case is run, so we can start from a clean slate for the next tests.</p>
<h3 id="runningtests">Running Tests</h3>
<p>For the majority of cases, this part is pretty simple. Assuming you&apos;ve already installed Mocha and navigated to the project directory, most projects just need to use the <code>mocha</code> command with no arguments to run their tests.</p>
<pre><code class="language-bash">$ mocha


  Math
    #abs()
      &#x2713; should return positive value of given negative number 
      &#x2713; should return positive value of given positive number 
      &#x2713; should return 0 given 0 
    #sqrt()
      &#x2713; should return the square root of a given positive number 
      &#x2713; should return NaN for a given negative number 
      &#x2713; should return 0 given 0 


  6 passing (10ms)
</code></pre>
<p>This is slightly different than our previous examples since we didn&apos;t need to tell Mocha where our tests were located. In this example, the test code is in the expected location of <code>/test</code>.</p>
<p>There are, however, some helpful options you may want to use when running tests. If some of your tests are failing, for example, you probably don&apos;t want to run the entire suite every time you make a change. For some projects, the full test suite could take a few minutes to complete. That&apos;s a lot of wasted time if you really only need to run one test.</p>
<p>For cases like this, you should tell Mocha which tests to run. This can be done using the <code>-g &lt;pattern&gt;</code> or <code>-f &lt;sub-string&gt;</code> options.</p>
<p>Again, using the examples from above, we can use the <code>-g</code> option to only run our <code>#sqrt()</code> tests:</p>
<pre><code class="language-bash">$ mocha -g sqrt


  Math
    #sqrt()
      &#x2713; should return the square root of a given positive number 
      &#x2713; should return NaN for a given negative number 
      &#x2713; should return 0 given 0 


  3 passing (10ms)
</code></pre>
<p>Notice that the <code>#abs()</code> tests were not included in this run. If you plan accordingly with your test names, this option can be utilized to only run specific sections of your tests.</p>
<p>These aren&apos;t the only useful options, however. Here are a few more options for Mocha that you might want to check out:</p>
<ul>
<li><code>--invert</code>: Inverts <code>-g</code> and <code>-f</code> matches</li>
<li><code>--recursive</code>: Include sub-directories</li>
<li><code>--harmony</code>: Enable all harmony features in Node</li>
</ul>
<p>You can check out the full list of options by using the <code>mocha -h</code> command, or on <a rel="nofollow" target="_blank" href="https://mochajs.org/#usage">this page</a>.</p>
<h3 id="wheretolearnmore">Where to Learn More</h3>
<p>There is far more to this topic than we can cover in a short blog post, so if you want to learn more then I&apos;d recommend checking out the following resources:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://mochajs.org/">Mocha documentation</a></li>
<li><a rel="nofollow" target="_blank" href="http://chaijs.com/">Chai documentation</a></li>
</ul>
<p>Instead of just reading documentation, you could also try a course on this subject, which I linked to below. The instructor goes in to great detail on how to get set up using Mocha, Chai, and Sinon for testing Node.js code, as well as in-depth videos for each topic.</p>
<!--<a class="udemy-link" rel="nofollow" href="http://stackabu.se/learn-js-mochai-chai-sinon">![Learn Javascript Unit Testing With Mocha, Chai and Sinon](https://s3.amazonaws.com/stackabuse/media/udemy-learn-js-mocha-chai-sinon.jpg)</a>-->
<p><strong><a class="udemy-link" rel="nofollow" href="http://stackabu.se/learn-js-mochai-chai-sinon">Learn Javascript Unit Testing With Mocha, Chai and Sinon</a></strong></p>
<p>It&apos;s a great supplement to this article, and it&apos;s also easier to digest than the documentation websites thanks to its video format.</p>
<h3 id="conclusion">Conclusion</h3>
<p>Keep in mind that both Mocha and Chai can be used for testing just about any type of Node project, whether it&apos;s a library, command-line tool, or even a website. Utilizing the various options and plugins available to you, you should be able to satisfy your testing needs pretty easily. Each of these libraries are very useful for validating your code and should be used in just about all of your Node projects.</p>
<p>Hopefully this has served as a useful introduction to Mocha and Chai. There is a lot more to learn than what I&apos;ve presented here, so be sure to check out the docs for more info.</p>
<p><em>Have any helpful tips for writing Mocha/Chai tests? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					