
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p>Writing to files is a frequent need when programming in any language. Like other programming languages, JavaScript with Node.js makes dealing with the file system intuitive through the use of a module dealing with the operating system&apos;s file system.</p>
<p>The <a rel="nofollow" target="_blank" href="https://nodejs.org/api/fs.html">fs</a> module contains the functionality for manipulating files and dealing with the computing platform&apos;s file system. Along with reading from and writing to files, you can use the module to query for file statistics like file sizes and counts.</p>
<p>By default, the <code>fs</code> module will write files with an encoding of &apos;utf8&apos;. <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/UTF-8">UTF-8</a> is an encoding commonly used in web pages and other documents. File encoding refers to the character set that is used for the contents of the file. Commonly used encodings are &apos;utf8&apos;, &apos;ascii&apos;, &apos;binary&apos;, &apos;hex&apos;, &apos;base64&apos; and &apos;utf16le&apos;.</p>
<p>Each character encoding has specific advantages and instances where it makes the most sense. For instance, &apos;ascii&apos; is an easy-to-read and write encoding, but it will strip the high bit from the data stream. For reasons like this you will want to select your character encoding with care.</p>
<p>In the next few sections we&apos;ll present the different ways <code>fs</code> allows you to write to the file system, including their differences and advantages.</p>
<h3 id="method1usingfswritefile">Method 1: Using fs.writeFile</h3>
<p>The <code>fs</code> module includes a high-level <code>writeFile</code> method that can write data to files asynchronously. This means, like with a lot of operations in Node.js, the I/O is non-blocking, and emits an event when the operation finishes. We can write a callback function to run when the <code>writeFile</code> returns.</p>
<p>Here is a simple code example of writing some song lyrics to a file:</p>
<pre><code class="language-javascript">// writefile.js

const fs = require(&apos;fs&apos;);

let lyrics = &apos;But still I\&apos;m having memories of high speeds when the cops crashed\n&apos; + 
             &apos;As I laugh, pushin the gas while my Glocks blast\n&apos; + 
             &apos;We was young and we was dumb but we had heart&apos;;

// write to a new file named 2pac.txt
fs.writeFile(&apos;2pac.txt&apos;, lyrics, (err) =&gt; {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log(&apos;Lyric saved!&apos;);
});
</code></pre>
<p>We specified the file name, along with the characters to write, as well as a callback function to run when the method returns. We can also pass an options object or string that specifies the encoding to use, as well as mode and flag. If you need to specify the character encoding then here is the way you&apos;ll need to call the method:</p>
<pre><code class="language-javascript">fs.writeFile(&apos;2pac.txt&apos;, &apos;Some other lyric&apos;, &apos;ascii&apos;, callback);
</code></pre>
<p>Note that if the file doesn&apos;t yet exist, then calling this method will actually <em>create the file</em> as well, so you don&apos;t need to worry about that on your end.</p>
<p>There is a synchronous method <code>fs.writeFileSync</code> that you can use in place of <code>fs.writeFile</code>.</p>
<p>The difference is that the <code>fs.writeFileSync</code> method performs input/output operations synchronously, blocking the Node.js event loop while the file is written. This may interfere with the performance of your Node.js application if you overdo these synchronous writes. In most cases, you should prefer using the asynchronous writes.</p>
<p>Just know that you should be careful when using these two methods because they will create a new file every time <em>or</em> they&apos;ll replace the contents if it already exists. If you merely want to update an existing file, you&apos;ll want to use <code>appendFile</code>, which we go over later in this article.</p>
<h3 id="method2usingfswrite">Method 2: Using fs.write</h3>
<p>Unlike the high-level <code>fs.writeFile</code> and <code>fs.writeFileSync</code> methods, you can leverage more control when writing to files in Node.js using the low-level <code>fs.write</code> method. The <code>fs.write</code> method allows fine control over the position in the file to begin writing at, a buffer which you can specify to write, as well as which part of the buffer you want to write out to the file.</p>
<p>In addition, using low-level writing functionality allows you to know precisely when file descriptors are released and to respond accordingly, such as by sending push notifications in your application.</p>
<p>Here is an example where we write another few lines of lyrics to a different file using <code>fs.write</code>.</p>
<pre><code class="language-javascript">// fs_write.js

const fs = require(&apos;fs&apos;);

// specify the path to the file, and create a buffer with characters we want to write
let path = &apos;ghetto_gospel.txt&apos;;
let buffer = new Buffer(&apos;Those who wish to follow me\nI welcome with my hands\nAnd the red sun sinks at last&apos;);

// open the file in writing mode, adding a callback function where we do the actual writing
fs.open(path, &apos;w&apos;, function(err, fd) {
    if (err) {
        throw &apos;could not open file: &apos; + err;
    }

    // write the contents of the buffer, from position 0 to the end, to the file descriptor returned in opening our file
    fs.write(fd, buffer, 0, buffer.length, null, function(err) {
        if (err) throw &apos;error writing file: &apos; + err;
        fs.close(fd, function() {
            console.log(&apos;wrote the file successfully&apos;);
        });
    });
});
</code></pre>
<p>You will want to use <code>fs.write</code> when performing fine-grained updates to files, for instance, writing a sequence of bytes at a known position in the middle of a file.</p>
<p>These methods work with file descriptors, and you have to open the file for writing with <code>fs.open</code> and then, at the end, close it again with <code>fs.close</code> in order to avoid memory leakages.</p>
<h3 id="method3usingfscreatewritestream">Method 3: Using fs.createWriteStream</h3>
<p>When handling particularly large files, or files that come in chunks, say from a network connection, using <a rel="nofollow" target="_blank" href="https://nodejs.org/api/stream.html">streams</a> is preferable to writing files in one go via the above methods that write entire files.</p>
<p>Streams write small amounts of data at a time. While this has the disadvantage of being slower because data is transferred in chunks, it has advantages for RAM performance. Since the whole file is not loaded in memory all at once, RAM usage is lower.</p>
<p>To write to a file using streams, you need to create a new writable stream. You can then write data to the stream at intervals, all at once, or according to data availability from a server or other process, then close the stream for good once all the data packets have been written.</p>
<p>Here is a code example of how we do this:</p>
<pre><code class="language-javascript">// write_stream.js

const fs = require(&apos;fs&apos;);

let writeStream = fs.createWriteStream(&apos;secret.txt&apos;);

// write some data with a base64 encoding
writeStream.write(&apos;aef35ghhjdk74hja83ksnfjk888sfsf&apos;, &apos;base64&apos;);

// the finish event is emitted when all data has been flushed from the stream
writeStream.on(&apos;finish&apos;, () =&gt; {
    console.log(&apos;wrote all data to file&apos;);
});

// close the stream
writeStream.end();
</code></pre>
<p>We created a writable stream, then wrote some data to the stream. We have included a log statement when the &quot;finish&quot; event is emitted, letting us know that all data has been flushed to the underlying system. In this case, that means all data has been written to the file system.</p>
<p>Given the performance advantages, streams are a technique you will see used widely in Node.js territory, not just for file writing. Another example would be using streams to receive data from a network connection.</p>
<h3 id="errorhandlingwhenwritingtoafile">Error Handling when Writing to a File</h3>
<p>When writing to files, many different errors can occur during input/output. Your code should address these potential errors. One simple thing to do is to throw the errors as Node.js exceptions. This crashes the program, and is therefore not recommended except in cases where you have little other recourse.</p>
<p>For example, if the error can only occur as a programmer mistake, also known as a bug, then crashing the program may be the best response since it alerts the programmer of the error right away and doesn&apos;t hide the error.</p>
<p>When you are dealing with operational errors, such as specifying a path that is inaccessible, then there are two approaches to take. One is to call a callback function with the error. In the callback, you then include some logic of handling the error, such as logging it.</p>
<p>Alternatively, you can include try/catch blocks around the code calling the section of the program where errors can occur.</p>
<h3 id="appendingtoafilewithfsappendfile">Appending to a File with fs.appendFile</h3>
<p>In cases where we just want to add to a file&#x2019;s contents, we can use the <code>fs.appendFile</code> high level method and its synchronous counterpart, <code>fs.appendFileSync</code>. Using the <code>fs.appendFile</code> creates the file if it does not exist, and appends to the file otherwise.</p>
<p>Here is an example of using <code>fs.appendFile</code> to write to a file.</p>
<pre><code class="language-javascript">// append_file.js

const fs = require(&apos;fs&apos;);

// add a line to a lyric file, using appendFile
fs.appendFile(&apos;empirestate.txt&apos;, &apos;\nRight there up on Broadway&apos;, (err) =&gt; {
    if (err) throw err;
    console.log(&apos;The lyrics were updated!&apos;);
});
</code></pre>
<p>Here, we are using <code>appendFile</code> to add an extra line to a file holding the following lyrics:</p>
<pre><code class="language-text">// empirestate.txt

Empire State of Mind - JAY-Z

I used to cop in Harlem;
hola, my Dominicanos
</code></pre>
<p>Using <code>fs.appendFile</code> does not overwrite the file, but updates it from the end position, with the new data we append. This is a useful feature when you just want to update a file, such as continuously adding data to a single log file.</p>
<p>So after running our code the text file will look like this:</p>
<pre><code class="language-text">// empirestate.txt

Empire State of Mind - JAY-Z

I used to cop in Harlem;
hola, my Dominicanos
Right there up on Broadway
</code></pre>
<p>As you can see, the old text is still there and our new string has been added to the end.</p>
<h3 id="learnmore">Learn More</h3>
<p>Want to learn more about the fundamentals of Node.js? Personally, I&apos;d recommend taking an online course like <a class="bos-link" href="https://stackabu.se/wes-bos-learn-node">Learn Node.js by Wes Bos</a>. Not only will you learn the most up-to-date ES2017 syntax, but you&apos;ll get to build a full stack restaurant app. In my experience, building real-world apps like this is the fastest way to learn.</p>
<h3 id="conclusion">Conclusion</h3>
<p>As we saw, there are multiple approaches to consider when writing to a file in Node.js. The simplest way, and often the most appropriate, is to use the <code>writeFile</code> method in the <code>fs</code> module. This allows you to write to a specified file path, with asynchronous behavior, and creates a new file or replaces one if it exists at the path.</p>
<p>If you are writing larger files, you should consider the chunked writing capabilities of writable streams. These enable writing buffers of data in chunks, instead of loading the data all at once in memory. Using streams helps you achieve better performance in such cases.</p>
<p>For special situations, you can use the lower-level <code>fs</code> utilities like <code>fs.write</code>. The <code>fs</code> module supports fine-grained file-writing operations to match your specific needs.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					