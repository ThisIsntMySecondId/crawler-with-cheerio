
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>One of my absolute favorite things about Node is how easy it is to create simple command line interface (CLI) tools. Between argument parsing with <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/yargs">yargs</a> to managing tools with npm, Node just makes it easy.</p>
<p>Some examples of the kinds of tools I&apos;m referring to are:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/forever">forever</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/uglifyjs">uglifyjs</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/is-up-cli">is-up-cli</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/jshint">jshint</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/speed-test">speed-test</a></li>
</ul>
<p>When installed (with the <code>-g</code> option), these packages can be executed from anywhere on the command line and work much like the built-in Unix tools.</p>
<p>I&apos;ve been creating a few Node.js applications for the command line lately and thought it might be helpful to write up a post on it to help you get started. So throughout this article I&apos;ll be showing you how to create a command line tool to get location data for IP addresses and URLs.</p>
<p>If you&apos;ve seen the Stack Abuse article on <a href="https://stackabuse.com/learn-node-js-a-beginners-guide/">learning Node.js</a>, you may recall we created a package called <code>twenty</code> that had similar functionality. We&apos;ll be building off of that project and turning it in to a proper CLI tool with more functionality.</p>
<h3 id="settinguptheproject">Setting up the project</h3>
<p>Let&apos;s start by creating a new directory and setting up the project using npm:</p>
<pre><code class="language-bash">$ mkdir twenty
$ npm init
</code></pre>
<p>Press enter for all of the prompts in the last command, and you should have your <code>package.json</code> file.</p>
<p><em>Note that since I&apos;ve already taken the package name <code>twenty</code> on npm, you&apos;ll have to rename it to something else if you actually want to publish. Or you could also <a rel="nofollow" target="_blank" href="https://docs.npmjs.com/misc/scope">scope</a> your project.</em></p>
<p>Then, create the <code>index.js</code> file:</p>
<pre><code class="language-bash">$ touch index.js
</code></pre>
<p>This is all we really need to get started for now, and we&apos;ll be adding to the project as we move on.</p>
<h3 id="parsingarguments">Parsing arguments</h3>
<p>Most CLI apps take in arguments from the user, which is the most common way of getting input. For most cases, parsing the arguments isn&apos;t too difficult since there are usually only a handful of commands and flags. But as the tool becomes more complex, more flags and commands will be added, and argument parsing can become surprisingly difficult.</p>
<p>To help us with this, we&apos;ll be using a package called <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/yargs"><code>yargs</code></a>, which is the successor to the popular <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/optimist">optimist</a> package.</p>
<p><code>yargs</code> was created to help you parse commands from the user, like this:</p>
<pre><code class="language-javascript">var argv = require(&apos;yargs&apos;).argv;
</code></pre>
<p>Now complex optstrings like <code>node index.js install -v --a=22 -cde -x derp</code> can be easily accessed:</p>
<pre><code class="language-javascript">var argv = require(&apos;yargs&apos;).argv;

argv._[0]   // &apos;install&apos;
argv.v      // true
argv.a      // 22
argv.c      // true
argv.d      // true
argv.e      // true
argv.x      // &apos;derp&apos;
</code></pre>
<p><code>yargs</code> will even help you with specifying the command interface, so if the user&apos;s input doesn&apos;t meet certain requirements it&apos;ll show them an error message. So, for example, we can tell <code>yargs</code> we want at least 2 arguments:</p>
<pre><code class="language-javascript">var argv = require(&apos;yargs&apos;)
    .demand(2)
    .argv
</code></pre>
<p>And if the user doesn&apos;t provide at least two, they&apos;ll see this default error message:</p>
<pre><code>$ node index.js foo

Not enough non-option arguments: got 1, need at least 2
</code></pre>
<p>There is a lot more to <code>yargs</code> than just this, so check out the readme for more info.</p>
<p>For <code>twenty</code>, we&apos;ll be taking in a few optional arguments, like an IP address and some flags. For now, we&apos;ll be using <code>yargs</code> like this:</p>
<pre><code class="language-javascript">var argv = require(&apos;yargs&apos;)
    .alias(&apos;d&apos;, &apos;distance&apos;)
    .alias(&apos;j&apos;, &apos;json&apos;)
    .alias(&apos;i&apos;, &apos;info&apos;)
    .usage(&apos;Usage: $0 [options]&apos;)
    .example(&apos;$0 -d 8.8.8.8&apos;, &apos;find the distance (km) between you and Google DNS&apos;)
    .describe(&apos;d&apos;, &apos;Get distance between IP addresses&apos;)
    .describe(&apos;j&apos;, &apos;Print location data as JSON&apos;)
    .describe(&apos;i&apos;, &apos;Print location data in human readable form&apos;)
    .help(&apos;h&apos;)
    .alias(&apos;h&apos;, &apos;help&apos;)
    .argv;
</code></pre>
<p>Since none of our arguments are required, we wont&apos; be using <code>.demand()</code>, but we do use <code>.alias()</code>, which tells <code>yargs</code> that the user can use the short or long form of each flag. We&apos;ve also added some help documentation to show the user for when they need it.</p>
<h3 id="structuringtheapplication">Structuring the application</h3>
<p>Now that we can get input from the user, how do we take that input and translate it to a command with the optional arguments? There are a few modules out there designed to help you do this, including:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="http://flatironjs.org/">Flatiron</a> with the <a rel="nofollow" target="_blank" href="https://github.com/flatiron/flatiron#example-cli-application">CLI plugin</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/commander">Commander</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/vorpal">Vorpal</a></li>
</ul>
<p>With many of these frameworks the argument parsing is actually done for you, so you don&apos;t even need to use <code>yargs</code>. And in <code>commander</code>&apos;s case, most of its functionality is a lot like <code>yargs</code>, although it does provide ways to route commands to functions.</p>
<p>Since our application is fairly simple we&apos;ll just stick with using <code>yargs</code> for now.</p>
<h3 id="addingthecode">Adding the code</h3>
<p>We won&apos;t spend too much time here since it is specific to just our CLI app, but here is the code specific to our application:</p>
<pre><code class="language-javascript">var dns = require(&apos;dns&apos;);
var request = require(&apos;request&apos;);

var ipRegex = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/;

var toRad = function(num) {
    return num * (Math.PI / 180);
};

var getIpInfo = function(server, callback) {
    var ipinfo = function(p, cb) {
        request(&apos;http://ipinfo.io/&apos; + p, function(err, response, body) {
            var json = JSON.parse(body);
            cb(err, json);
        });
    };

    if (!server) {
        return ipinfo(&apos;json&apos;, callback);
    } else if (!server.match(ipRegex)) {
        return dns.lookup(server, function(err, data) {
            ipinfo(data, callback);
        });
    } else {
        return ipinfo(server, callback);
    }
};

var ipDistance = function(lat1, lon1, lat2, lon2) {
    // Earth radius in km
    var r = 6371;

    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lon2 - lon1);
    lat1 = toRad(lat1);
    lat2 = toRad(lat2);

    var a = Math.sin(dLat / 2.0) * Math.sin(dLat / 2.0) + 
        Math.sin(dLon / 2.0) * Math.sin(dLon / 2.0) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));
    return r * c;
};

var findLocation = function(server, callback) {
    getIpInfo(server, function(err, data) {
        callback(null, data.city + &apos;, &apos; + data.region);
    });
};

var findDistance = function(ip1, ip2, callback) {
    var lat1, lon1, lat2, lon2;

    getIpInfo(ip1, function(err, data1) {
        var coords1 = data1.loc.split(&apos;,&apos;);
        lat1 = Number(coords1[0]);
        lon1 =  Number(coords1[1]);
        getIpInfo(ip2, function(err, data2) {
            var coords2 = data2.loc.split(&apos;,&apos;);
            lat2 =  Number(coords2[0]);
            lon2 =  Number(coords2[1]);

            var dist = ipDistance(lat1, lon1, lat2, lon2);
            callback(null, dist);
        });
    });
};
</code></pre>
<p>For the full source code, you can find the repository <a rel="nofollow" target="_blank" href="https://github.com/scottwrobinson/twentyjs">here</a>.</p>
<p>The only thing left we have to do with the code is hook up the CLI arguments with the application code above. To make it easy, we&apos;ll put all of this in a function called <code>cli()</code>, which we&apos;ll use later.</p>
<p>Encapsulating the argument parsing and command mapping within <code>cli()</code> helps keep the application code separate, thus allowing this code to be imported as a library with <code>require()</code>.</p>
<pre><code class="language-javascript">var cli = function() {
    var argv = require(&apos;yargs&apos;)
        .alias(&apos;d&apos;, &apos;distance&apos;)
        .alias(&apos;j&apos;, &apos;json&apos;)
        .alias(&apos;i&apos;, &apos;info&apos;)
        .usage(&apos;Usage: $0 [IP | URL] [--d=IP | URL] [-ij]&apos;)
        .example(&apos;$0 -d 8.8.8.8&apos;, &apos;find the distance (km) between you and Google DNS&apos;)
        .describe(&apos;d&apos;, &apos;Get distance between IP addresses&apos;)
        .describe(&apos;j&apos;, &apos;Print location data as JSON&apos;)
        .describe(&apos;i&apos;, &apos;Print location data in human readable form&apos;)
        .help(&apos;h&apos;)
        .alias(&apos;h&apos;, &apos;help&apos;)
        .argv;

    var path = &apos;json&apos;;
    if (argv._[0]) {
        path = argv._[0];
    }

    if (argv.d) {
        findDistance(path, argv.d, function(err, distance) {
            console.log(distance);
        });
    } else if (argv.j) {
        getIpInfo(path, function(err, data) {
            console.log(JSON.stringify(data, null, 4));
        });
    } else if (argv.i) {
        getIpInfo(path, function(err, data) {
            console.log(&apos;IP:&apos;, data.ip);
            console.log(&apos;Hostname:&apos;, data.hostname);
            console.log(&apos;City:&apos;, data.city);
            console.log(&apos;Region:&apos;, data.region);
            console.log(&apos;Postal:&apos;, data.postal);
            console.log(&apos;Country:&apos;, data.country);
            console.log(&apos;Coordinates:&apos;, data.loc);
            console.log(&apos;ISP:&apos;, data.org);
        });
    } else {
        findLocation(path, function(err, location) {
            console.log(location);
        });
    }
};

exports.info = getIpInfo;
exports.location = findLocation;
exports.distance = findDistance;
exports.cli = cli;
</code></pre>
<p>Here you can see we basically just use <code>if...else</code> statements to determine which command to run. You could get a lot fancier and use Flatiron to map regex strings to commands, but that&apos;s a bit of overkill for what we&apos;re doing here.</p>
<h3 id="makingitexecutable">Making it executable</h3>
<p>In order for us to be able to execute the app, we need to specify a few things in our <code>package.json</code> file, like where the executable resides. But first, let&apos;s create the executable and its code. Create a file called <code>twenty</code> in the directory <code>twenty/bin/</code> and add this to it:</p>
<pre><code class="language-javascript">#!/usr/bin/env node
require(&apos;../index&apos;).cli();
</code></pre>
<p>The shebang (<code>#!/usr/bin/env node</code>) tells Unix how to execute the file, allowing us to leave out the <code>node</code> prefix. The second line just loads the code from above and calls the <code>cli()</code> function.</p>
<p>In <code>package.json</code>, add the following JSON:</p>
<pre><code class="language-json">&quot;bin&quot;: {
    &quot;twenty&quot;: &quot;./bin/twenty&quot;
}
</code></pre>
<p>This just tells npm where to find the executable when installing the package with the <code>-g</code> (global) flag.</p>
<p>So now, if you install <code>twenty</code> as a global...</p>
<pre><code class="language-bash">$ npm install -g twenty
</code></pre>
<p>...you can then get the locations of servers and IP addresses:</p>
<pre><code class="language-bash">$ twenty 198.41.209.141 #reddit
San Francisco, California

$ twenty rackspace.com
San Antonio, Texas

$ twenty usa.gov --j
{
    &quot;ip&quot;: &quot;216.128.241.47&quot;,
    &quot;hostname&quot;: &quot;No Hostname&quot;,
    &quot;city&quot;: &quot;Phoenix&quot;,
    &quot;region&quot;: &quot;Arizona&quot;,
    &quot;country&quot;: &quot;US&quot;,
    &quot;loc&quot;: &quot;33.3413,-112.0598&quot;,
    &quot;org&quot;: &quot;AS40289 CGI TECHNOLOGIES AND SOLUTIONS INC.&quot;,
    &quot;postal&quot;: &quot;85044&quot;
}

$ twenty stackabuse.com
Ashburn, Virginia
</code></pre>
<p>And there you have it, the Stack Abuse server is located in Asburn, Virginia. Interesting =)</p>
<p>For the full source code, check out the project on <a rel="nofollow" target="_blank" href="https://github.com/scottwrobinson/twentyjs">Github</a>.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					