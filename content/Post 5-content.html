
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p>Over the last few years especially, neural networks (NNs) have really taken off as a practical and efficient way of solving problems that can&apos;t be easily solved by an algorithm, like face detection, voice recognition, and medical diagnosis. This is largely thanks to recent discoveries on how to better train and tune a network, as well as the increasing speed of computers.</p>
<p>Just recently, a student at Imperial College London created a NNs called <a rel="nofollow" target="_blank" href="https://www.technologyreview.com/view/541276/deep-learning-machine-teaches-itself-chess-in-72-hours-plays-at-international-master/">Giraffe</a> that could be trained in only 72 hours to play chess at the same level as a FIDE International Master. Computers playing chess at this level isn&apos;t really new, but the way in which this program was created <em>is</em> new. These programs usually take years to build and are tuned with the help of a real grandmaster, while Giraffe, on the other hand, was built simply using a deep neural network, some creativity, and a huge dataset of chess games. This is yet another example of the promise neural networks have been showing recently, and they&apos;re only going to improve.</p>
<h3 id="brainjs">Brain.js</h3>
<p>The downside to NNs, and artificial intelligence in general, is that the field is very math-heavy, which tends to scare people away from the it before they even start. The highly technical nature of NNs and all of the jargon that goes along with it makes it hard for the uninitiated to take advantage. This is where <a rel="nofollow" target="_blank" href="https://github.com/harthur/brain">Brain.js</a> comes in to play. Brain.js does a great job simplifying the process of creating and training an NN by utilizing the ease-of-use of JavaScript and by limiting the API to just a few method calls and options.</p>
<p>Don&apos;t get me wrong, you still need to know some of the concepts behind NNs, but this simplification makes it much less daunting.</p>
<p>For example, to train a network to approximate the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Exclusive_or">XOR</a> function (which is one of the standard NN examples), all you&apos;d need is:</p>
<pre><code class="language-javascript">var brain = require(&apos;brain&apos;);

var net = new brain.NeuralNetwork();

net.train([{input: [0, 0], output: [0]},
           {input: [0, 1], output: [1]},
           {input: [1, 0], output: [1]},
           {input: [1, 1], output: [0]}]);

var output = net.run([1, 0]);  // [0.933]
</code></pre>
<p>This code just creates a new network (<code>net</code>), <code>train</code>s the network using an array of examples, and then <code>run</code>s the network with an input of <code>[1, 0]</code>, which correctly results in <code>[0.933]</code> (aka <code>1</code>).</p>
<p>While Brain.js doesn&apos;t have a ton of options to allow you to customize your networks, the API accepts enough parameters to make it useful for simple applications. You can set the number and size of your hidden layers, error threshold, learning rate, and more:</p>
<pre><code class="language-javascript">var net = new brain.NeuralNetwork({
	hiddenLayers: [128,64]
});

net.train({
	errorThresh: 0.005,  // error threshold to reach before completion
	iterations: 20000,   // maximum training iterations 
	log: true,           // console.log() progress periodically 
	logPeriod: 10,       // number of iterations between logging 
	learningRate: 0.3    // learning rate 
});
</code></pre>
<p>See the documentation for a full list of options.</p>
<p>While you are limited in the types of networks you can build, that doesn&apos;t mean you can&apos;t make anything meaningful. Take <a rel="nofollow" target="_blank" href="https://codepen.io/birjolaxew/blog/cracking-captchas-with-neural-networks">this project</a> for example. The author gathered a bunch of captcha images for his dataset, used some simple image processing to pre-process the images, and then used Brain.js to create a neural network that identifies each individual character.</p>
<h4 id="advantages">Advantages</h4>
<p>As I&apos;ve already mentioned, Brain.js is great for quickly creating a simple NN in a high-level language where you can take advantage of the huge number of open source libraries. With a good dataset and a few lines of code you can create some really interesting functionality.</p>
<p>Highly scientific/computational libraries written in JavaScript like this tend to get <a rel="nofollow" target="_blank" href="https://www.reddit.com/r/programming/comments/2afbxu/brain_neural_networks_in_javascript/cius1c3">criticized pretty heavily</a>, but personally I think Brain.js has its place in JS as long as you have the right expectations and application. For example, JS is the dominant (only?) language running client-side in the browser, so why not take advantage of this library for things like in-browser games, ad placement (boring, I know), or character recognition?</p>
<h4 id="disadvantages">Disadvantages</h4>
<p>While we can definitely get some value from a library like this, it isn&apos;t perfect. As I&apos;ve mentioned, the library limits your network architecture to a point where you can only do simple applications. There isn&apos;t much of a possibility for softmax layers or other structure. It would be nice to at least have the option for more customization of the architecture instead of hiding everything from you.</p>
<p>Probably my biggest complaint is that the library is written in <em>pure</em> JavaScript. Training a NN is a slow process that can take thousands of iterations (meaning millions or billions of operations) to train on millions of data points. JavaScript isn&apos;t a fast language by any means and should really have <a href="/how-to-create-c-cpp-addons-in-node/">addons</a> for things like this to speed up the computations while training. The captcha cracker from above took a surprisingly low training time of 20 minutes, but all it takes is a few more inputs and some more data to increase training time by a few hours or even days, as you&apos;ll see in my example below.</p>
<p>Unfortunately, this library has already been abandoned by its author (the Github description is prepended with &quot;[UNMAINTAINED]&quot;). While I get that it can be hard to keep up the demands of a popular open source library, it&apos;s still disappointing to see, and we can only hope that someone <em>qualified</em> can fill the void. I&apos;m sure there is a good fork already in the works, but it might take some searching to find it.</p>
<h3 id="example">Example</h3>
<p>Here I&apos;ll be showing you a slightly more involved example of how to use Brain. In this example, I created a NN that can recognize a single handwritten digit (0-9). The dataset I&apos;m using is the popular <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/MNIST_database">MNIST</a> dataset, which contains over 50,000 samples of handwritten digits. This kind of problem is known as Optical Character Recognition (<a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Optical_character_recognition">OCR</a>), which is a popular application of NNs.</p>
<p>The recognition works by taking in a 28x28 greyscale image of a handwritten digit, and outputting the digit that the network thinks it &quot;saw&quot;. This means we&apos;ll have 784 inputs (one for each pixel) with values between 0-255, and there will be 10 outputs (one for each digit). Each output will have a value of 0-1, essentially acting as the confidence level that that particular digit is the correct answer. The highest confidence value is then our answer.</p>
<p>On to the code:</p>
<pre><code class="language-javascript">var brain = require(&apos;brain&apos;);
var fs = require(&apos;fs&apos;);

var getMnistData = function(content) {
	var lines = content.toString().split(&apos;\n&apos;);

	var data = [];
	for (var i = 0; i &lt; lines.length; i++) {
		var input = lines[i].split(&apos;,&apos;).map(Number);

		var output = Array.apply(null, Array(10)).map(Number.prototype.valueOf, 0);
		output[input.shift()] = 1;

		data.push({
			input: input,
			output: output
		});
	}

	return data;
};

fs.readFile(__dirname + &apos;/train.csv&apos;, function (err, trainContent) {
	if (err) {
		console.log(&apos;Error:&apos;, err);
	}

	var trainData = getMnistData(trainContent);

	console.log(&apos;Got &apos; + trainData.length + &apos; samples&apos;);

	var net = new brain.NeuralNetwork({hiddenLayers: [784, 392, 196]});

	net.train(trainData, {
		errorThresh: 0.025,
		log: true,
		logPeriod: 1,
		learningRate: 0.1
	});
});
</code></pre>
<p>The <code>train.csv</code> file is just a CSV with one image per line. The first value is the digit shown in the image, and the next 784 values are the pixel data.</p>
<p>The number of layers and nodes I chose was a bit arbitrary, and probably unnecessarily high for this OCR application. However, when you can&apos;t take advantage of things like softmaxes or pooling, you might have better luck upping the number of layers and node count.</p>
<p>Training took easily over an hour to get some decent results. While this was expected, I was still a bit disappointed to have to wait so long to test out a new network structure or new learning parameters. A simple application like this shouldn&apos;t take so long, but that&apos;s the price you pay for an all-JavaScript implementation.</p>
<p>To test the network, I loaded another file, <code>test.csv</code>, and used that as a baseline to compare networks. That way we get a better idea of performance since we&apos;re testing out inputs that the network hasn&apos;t already been trained on.</p>
<p>Here is how I tested the network (I&apos;m only showing the relevant parts):</p>
<pre><code class="language-javascript">// Require packages...

fs.readFile(__dirname + &apos;/test.csv&apos;, function (err, testContent) {
	if (err) {
		console.log(&apos;Error:&apos;, err);
	}

	// Load training data...

	// Train network...

	// Test it out
	var testData = getMnistData(testContent);

	var numRight = 0;

	console.log(&apos;Neural Network tests:&apos;);
	for (i = 0; i &lt; testData.length; i++) {
		var resultArr = net.run(testData[i].input);
		var result = resultArr.indexOf(Math.max.apply(Math, resultArr));
		var actual = testData[i].output.indexOf(Math.max.apply(Math, testData[i].output));

		var str = &apos;(&apos; + i + &apos;) GOT: &apos; + result + &apos;, ACTUAL: &apos; + actual;
		str += result === actual ? &apos;&apos; : &apos; -- WRONG!&apos;;

		numRight += result === actual ? 1 : 0;

		console.log(str);
	}

	console.log(&apos;Got&apos;, numRight, &apos;out of 350, or &apos; + String(100*(numRight/350)) + &apos;%&apos;);
});
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>While there are some shortcomings, overall I think Brain.js can be very useful and add a lot of value to JavaScript/Node applications. Its ease-of-use should allow just about anyone to get started with neural networks.</p>
<p>In case you want something with more flexibility, or you&apos;re bothered by the lack of support for Brain.js, take a look at <a rel="nofollow" target="_blank" href="https://github.com/cazala/synaptic">Synaptic</a>, which allows for much more customization in your network architecture. It doesn&apos;t have quite the popularity/attention that Brain has, but it seems to be the next best choice for neural networks in JavaScript.</p>
<p><em>What&apos;s your experience with Brain.js? Are there any other AI packages you recommend? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					