
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="whatarecommandlinearguments">What are Command Line Arguments?</h3>
<p>Command line arguments are strings of text used to pass additional information to a program when an application is run through the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Command-line_interface">command line interface</a> (CLI) of an operating system. Command line arguments typically include information used to set configuration or property values for an application.</p>
<p>In most cases the arguments are passed after the program name in your prompt. An example of the syntax of command line arguments looks like this:</p>
<pre><code class="language-sh">$ [runtime] [script_name] [argument-1 argument-2 argument-3 ... argument-n]
</code></pre>
<p>Here runtime can be anything that executes a program/script e.g. <code>sh</code>, <code>java</code>, <code>node</code>, etc. The arguments are usually separated by a space - however there are some runtimes that use commas to distinguish between multiple command line arguments. Also, depending on the program, you can pass arguments in the form of key-value pairs, which we&apos;ll see later in this article.</p>
<h3 id="whyusecommandlinearguments">Why Use Command Line Arguments?</h3>
<p>The following are some of the major benefits of using command line arguments:</p>
<ul>
<li>You can pass information to an application before it starts. This is particularly useful if you want to perform large number configuration settings.</li>
<li>Command line arguments are passed as strings to your program. String data types can easily be converted to other data types within an application, making the arguments very flexible.</li>
<li>You can pass unlimited number of arguments via the command line.</li>
<li>Command line arguments are used in conjunction with scripts and batch files, which is particularly useful for automated testing.</li>
</ul>
<p>And here are some of the disadvantages of using them:</p>
<ul>
<li>The biggest disadvantage of passing information via the command line is that interface has steep learning curve, so it&apos;s difficult for most people to use unless they have a good deal of experience using CLI tools.</li>
<li>Command line applications can be difficult to use unless you&apos;re using a desktop or laptop computer, so they&apos;re not typically used on smaller devices like phones or tablets.</li>
</ul>
<h3 id="passingcommandlineargumentsinnodejs">Passing Command Line Arguments in Node.js</h3>
<p>Like many other languages, Node.js applications also accept command line arguments. By default Node will handle accepting the arguments for you, but there are also some great third party packages that add some very useful features.</p>
<p>In this section we&apos;ll show you how to use arguments via the built-in way (<code>process.argv</code>), as well as with the popular packages minimist and yargs.</p>
<h4 id="usingprocessargv">Using process.argv</h4>
<p>The simplest way of retrieving arguments in Node.js is via the <code>process.argv</code> array. This is a global object that you can use without importing any additional libraries to use it. You simply need to pass arguments to a Node.js application, just like we showed earlier, and these arguments can be accessed within the application via the <code>process.argv</code> array.</p>
<p>The first element of the <code>process.argv</code> array will always be a file system path pointing to the <code>node</code> executable. The second element is the name of the JavaScript file that is being executed. And the third element is the first argument that was actually passed by the user.</p>
<blockquote>
<p>To avoid confusion, I&apos;d like to remind you beginners out there that JavaScript uses zero-based indexes on arrays (like many other languages), which means that first element will be stored at &quot;0th&quot; index and last element will be stored at &quot;n-1&quot; index, where &quot;n&quot; is the total number of elements in the array.</p>
</blockquote>
<p>Now let&apos;s write a simple Node script that prints all of the command line arguments passed to the application, along with their index. Copy and paste the following code to a file named &quot;processargv.js&quot;.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

for (let j = 0; j &lt; process.argv.length; j++) {
    console.log(j + &apos; -&gt; &apos; + (process.argv[j]));
}
</code></pre>
<p>All this script does is loop through the <code>process.argv</code> array and prints the indexes, along with the elements stored in those indexes. It&apos;s very useful for debugging if you ever question what arguments you&apos;re receiving, and in what order.</p>
<p>Now, to run this type the following command. Just make sure you are in the directory where the &quot;processargv.js&quot; file is saved.</p>
<pre><code class="language-sh">$ node processargv.js tom jack 43
</code></pre>
<p>Here we are passing three arguments to the &quot;processargv.js&quot; program. You will see that &quot;tom&quot; will be stored at 2nd index while &quot;jack&quot; and &quot;43&quot; will be stored at 3rd and 4th indexes, respectively. The output should look something like this:</p>
<pre><code class="language-sh">$ node processargv.js tom jack 43
0 -&gt; /Users/scott/.nvm/versions/node/v4.8.0/bin/node
1 -&gt; /Users/scott/javascript/processargv.js
2 -&gt; tom
3 -&gt; jack
4 -&gt; 43
</code></pre>
<p>You can see that first index contains the path to our <code>node</code> executable (which will likely have a different path than mine), the second index contains the path to the script file, and the rest of the indexes contain the arguments that we passed in their respective sequence.</p>
<h4 id="usingtheminimistmodule">Using the minimist Module</h4>
<p>Another way to retrieve command line arguments in a Node.js application is using the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/minimist">minimist</a> module. The minimist module will parse arguments from the <code>process.argv</code> array and transform it in to an easier-to-use associative array. In the associative array you can access the elements via index names in addition to the index numbers. Take a look at the following example.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const args = require(&apos;minimist&apos;)(process.argv.slice(2));

console.log(args);
console.log(args.i);
</code></pre>
<p>Save the above code in &quot;minimist.js&quot;. In the above code we use the <code>slice</code> method of the <code>process.argv</code> global object. The <code>slice</code> method, in this case, removes all prior array elements starting from the index passed to it as the parameter. Here we know that the argument we manually pass are stored starting from the second index, we passed 2 to the slice function. We then printed the entire <code>args</code> object. We also printed single element of the array using the named index i.e. &quot;i&quot;, rather than the index number.</p>
<p>Now when we pass argument to this program we can also specify the character by which we want to access the element. Since in the script we use the &quot;i&quot; index name to access the element, we have to specify the element stored at this index. Take a look at the script to execute the above program.</p>
<pre><code class="language-sh">$ node minimist.js &#x2013;i jacob &#x2013;j 45
</code></pre>
<p>Notice here we specified &quot;i&quot; as the name for second index the value stored at that index is &quot;jacob&quot;. Similarly the third index is named as &quot;j&quot; and value at this index is 45. The output of above command will be as follows:</p>
<pre><code class="language-sh">$ node minimist.js -i jacob -j 45
{ _: [], i: &apos;jacob&apos;, j: 45 }
jacob
</code></pre>
<p>While not as feature-rich as some of the other arg-parsing modules (see <code>yargs</code> below), minimist has a few useful features you may want to look in to, like aliasing and defaults.</p>
<p>If you set an alias, then the caller of your program can use the alias in addition to the regular option name. This is useful if you want to have a shorthand notation for your options, as seen below:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const minimist = require(&apos;minimist&apos;);

let args = minimist(process.argv.slice(2), {
    alias: {
        h: &apos;help&apos;,
        v: &apos;version&apos;
    }
});

console.log(&apos;args:&apos;, args);
</code></pre>
<p>Calling this code with the <code>-h</code> option sets both <code>h: true</code> and <code>help: true</code> in the output args:</p>
<pre><code class="language-sh">$ node minimist-alias.js -h
args: { _: [], h: true, help: true }
</code></pre>
<p>As for minimist&apos;s defaults feature, if no value is passed for an option then the default you set in your program will be used automatically.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const minimist = require(&apos;minimist&apos;);

let args = minimist(process.argv.slice(2), {
    default: {
        port: 8080
    },
});

console.log(&apos;args:&apos;, args);
</code></pre>
<p>Calling this code without the <code>-port</code> option still yields you with a <code>port</code> value in the returned <code>args</code> object:</p>
<pre><code class="language-sh">$ node minimist-defaults.js
args: { _: [], port: 8080 }
</code></pre>
<p>For more options, check out the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/minimist">README</a>.</p>
<h4 id="usingtheyargsmodule">Using the yargs Module</h4>
<p>Another module to help you parse command line arguments passed to Node programs is the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/yargs">yargs</a> module. Using this module you can pass arguments in the form of key-value pairs and later access the argument values in your program using corresponding keys.</p>
<p>Note: You can install the <code>yargs</code> module with the following command:</p>
<pre><code class="language-sh">$ npm install yargs
</code></pre>
<p>Now, take a look at the following script:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const args = require(&apos;yargs&apos;).argv;

console.log(&apos;Name: &apos; + args.name);
console.log(&apos;Age: &apos; + args.age);
</code></pre>
<p>In the above script we display the values provided for the &apos;name&apos; and &apos;age&apos; arguments, which were passed via the command line. Now save the above script in a file named &quot;yargs.js&quot;.</p>
<p>Note: When you execute above program you have to pass values for &apos;name&apos; and &apos;age&apos; arguments, otherwise <code>undefined</code> will be given for the arguments by default and you will see <code>undefined</code> written to the console.</p>
<p>To execute above program, execute the following command in your prompt:</p>
<pre><code class="language-sh">$ node yargs.js --name=jacob --age=45
</code></pre>
<p>Here we are passing values for both name and age arguments. The output of above script will look like this:</p>
<pre><code class="language-sh">$ node yargs.js --name=jacob --age=45
Name: jacob
Age: 45
</code></pre>
<p>The <code>yargs</code> package is a very powerful one, not only providing the standard arg-parsing features but many advanced features as well. While we won&apos;t go through all of the features in this article, we&apos;ll at least touch on one of its most popular features.</p>
<p>One of the most powerful ways to use <code>yargs</code> is the <code>.command()</code> option, which helps you create, expose, and call Node functions via the command line. Here is a simple example of this feature in action:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const argv = require(&apos;yargs&apos;)
    .command(&apos;upload&apos;, &apos;upload a file&apos;, (yargs) =&gt; {}, (argv) =&gt; {
        console.log(&apos;Uploading your file now...&apos;);

        // Do stuff here
    }).argv;
</code></pre>
<p>Calling this program with the &quot;upload&quot; command will invoke the function you passed to it, which in this case just prints to the command line. However, I&apos;m sure you can imagine passing a much more capable function that uses the parsed <code>argv</code> object to determine what file to send where.</p>
<p>For example, this command could be called like this (assuming the code above is stored in a file called &quot;s3-tool.js&quot;:</p>
<pre><code class="language-sh">$ node s3-tool.js upload --file=my-file.txt --bucket=my-s3-bucket
Uploading your file now...
</code></pre>
<p>And you don&apos;t have to stop there, you can even create a default command:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const argv = require(&apos;yargs&apos;)
    .command(&apos;*&apos;, &apos;the default command handler&apos;, () =&gt; {}, (argv) =&gt; {
        console.log(&apos;This function is called by default&apos;);
    }).argv;
</code></pre>
<p>The <code>.command()</code> feature is even powerful enough to infer your required and optional parameters from a string:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const yargs = require(&apos;yargs&apos;);

yargs.command(&apos;login &lt;username&gt; [password]&apos;, &apos;authenticate with the server&apos;).argv
</code></pre>
<p>For more information on the advanced features, check out the <code>yargs</code> <a rel="nofollow" target="_blank" href="https://github.com/yargs/yargs/blob/HEAD/docs/advanced.md">advanced topics documentation</a>.</p>
<h3 id="conclusion">Conclusion</h3>
<p>It might be a bit surprising to you, but as you may have noticed, argument parsing can actually be a pretty complex topic. Depending on your needs you can make it as simple or complicated as you want. Regardless, there is a suitable solution for all needs, whether it&apos;s the basic <code>process.argv</code> array or the powerful <code>yargs</code> package.</p>
<p><em>How have you used <code>minimist</code> and <code>yargs</code> to create CLI programs? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					