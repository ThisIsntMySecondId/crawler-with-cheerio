
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p><strong>Edit</strong>: <em>Updated Camo code to v0.12.1</em></p>
<h2 id="whatiscamo">What is Camo?</h2>
<p><a href="https://github.com/scottwrobinson/camo">Camo</a> is an ES6 ODM with class-based models. A few of its main features are: dead-simple schema declaration, intuitive schema inheritance, and support for multiple database backends.</p>
<p>A simple Camo model might look like this:</p>
<pre><code class="language-javascript">var Document = require(&apos;camo&apos;).Document;

class Car extends Document {
	constructor() {
		super();

		this.make = String;
		this.miles = Number;
		this.numWheels = {
			type: Number;
			default: 4
		};
	}
}
</code></pre>
<p>To install, just use:</p>
<pre><code>npm install camo --save

AND

npm install nedb --save
OR
npm install mongodb --save
</code></pre>
<h2 id="whyanotherodm">Why another ODM?</h2>
<p>As much as I wanted to like Mongoose, like everyone else seemed to, I just couldn&apos;t get myself to embrace it. It may have been because I was still new to JavaScript, and I hadn&apos;t quite embraced the functional programming aspect as much as I should have, but I was left disappointed with how models were declared and the lack of schema inheritance (or at least it&apos;s <a href="http://www.laplacesdemon.com/2014/02/19/model-inheritance-node-js-mongoose/" rel="nofollow" target="_blank">clunkiness</a>).</p>
<p>Coming from a Java background, I&apos;m very fond of classes. So designing models without easily being able to use classes or inheritance was difficult for me. Seeing as ES6 has traditional class and inheritance support, I was surprised to see no Node.js ODMs were class-based.</p>
<p>Lastly, I liked how in SQL you could easily switch between small, portable databases like SQLite for development and larger, more scalable databases like PostgreSQL for production. Seeing as <a href="https://github.com/louischatriot/nedb" rel="nofollow" target="_blank">NeDB</a> nicely fills that gap for MongoDB, I wanted an ODM that allowed me to easily switch between these databases.</p>
<h2 id="features">Features</h2>
<h3 id="classes">Classes</h3>
<p>As mentioned above, Camo revolves around ES6 classes for creating models. Each model should either extend the <code>Document</code> class, the <code>EmbeddedDocument</code> class, or another model. Schemas are declared in the constructors, where you can specify the data type, defaults, choices, and other validators.</p>
<p>Virtuals, methods, and statics can be used to manipulate and retrieve the model&apos;s data, just like a normal, non-persisted class.</p>
<pre><code class="language-javascript">var Document = require(&apos;camo&apos;).Document;

class Manufacturer extends Document {
	constructor() {
		super();

		this.name = String;
	}
}

class Car extends Document {
	constructor() {
		super();

		this.make = Manufacturer;
		this.model = String;
		this.year = {
			type: Number,
			min: 1900,
			max: 2015
		};
		this.miles = {
			type: Number,
			min: 0
		};
		this.numWheels = {
			type: Number;
			default: 4
		};
	}

	get isUnderWarranty() {
		return this.miles &lt; 50000;
	}

	milesPerYear() {
		return this.miles / (new Date().getFullYear() - this.year)
	};
}
</code></pre>
<h3 id="embeddeddocuments">Embedded documents</h3>
<p>One of the main advantages to using MongoDB is its nested data structure, and you should be able to easily take advantage of that in your ODM. Camo has built-in embedded document support, so you can treat nested document data just like you would a normal document.</p>
<pre><code class="language-javascript">var Document = require(&apos;camo&apos;).Document;
var EmbeddedDocument = require(&apos;camo&apos;).EmbeddedDocument;

class Warranty extends EmbeddedDocument {
	constructor() {
		super();

		this.miles = Number;
		this.years = Number;
		this.isUnlimitedMiles = Boolean;
	}

	isCovered(car) {
		var thisYear = new Date().getFullYear();
		return ((car.miles &lt;= this.miles) || this.isUnlimitedMiles) &amp;&amp;
			((thisYear - car.year) &lt;= this.years)
	};
}

class Manufacturer extends Document {
	constructor() {
		super();

		this.name = String;

		this.basicWarranty = Warranty;
		this.powertrainWarranty = Warranty;
		this.corrosionWarranty = Warranty;
	}
}
</code></pre>
<h3 id="multidatabasesupport">Multi-database support</h3>
<p>Not every project requires a huge database with replication, load balancing, and support for millions of reads/writes per second, which is exactly why it was important for Camo to support multiple database backends, like NeDB.</p>
<p>Most projects start out small and eventually grow in to larger, more popular products that require faster and more robust databases. With <a href="https://github.com/louischatriot/nedb">NeDB</a>, you get a subset of MongoDB&apos;s most commonly used API commands without the need to set up the full database. Its like having the equivalent of SQLite, but for Mongo. To switch between the databases, just provide a different connection string.</p>
<pre><code class="language-javascript">var connect = require(&apos;camo&apos;).connect;

var uri;
var neUri = &apos;nedb://memory&apos;;
var mongoUri = &apos;mongodb://localhost/car-app&apos;;

uri = neUri;
if (process.env.NODE_ENV === &apos;production&apos;) {
	uri = mongoUri;
}

connect(uri).then(function(db) {
	// Ready to use Camo!
});
</code></pre>
<p>As of v0.5.5, Camo supports MongoDB and NeDB. We&apos;re planning to add support for more Mongo-like databases, including <a href="http://lokijs.org/#/" target="_blank" rel="nofollow">LokiJS</a>, and <a href="http://www.taffydb.com/" target="_blank" rel="nofollow">TaffyDB</a>. With NeDB, and the additions of LokiJS and TaffyDB, you can use Camo in the browser as well.</p>
<h3 id="inheritance">Inheritance</h3>
<p>Arguably one of the best features of Camo is the schema inheritance. Simply use the ES6 class inheritance to extend a schema.</p>
<pre><code class="language-javascript">var Document = require(&apos;camo&apos;).Document;

class Vehicle extends Document {
	constructor() {
		super();

		this.make = String;
		this.model = String;
		this.year = Number;
		this.miles = Number;
	}
}

class Car extends Vehicle {
	constructor() {
		super();

		this.numberOfDoors = String;
	}
}
</code></pre>
<p>The subclasses can override or extend any schemas, virtuals, methods, or statics, which results in more readable, and simpler, code.</p>
<h2 id="iscamoproductionready">Is Camo production-ready?</h2>
<p>While Camo already has quite a few features and only a few known bugs, it is still very much a work in progress. Here is a short list of the main features we still want to add to the project before declaring v1.0:</p>
<ul>
<li>Return a <code>Query</code> object from findOne/find/delete/etc</li>
<li><s>Add skip/limit support to queries</s></li>
<li><s>Add an option to only populate specified references</s></li>
<li>Add support for LokiJS and TaffyDB</li>
</ul>
<p>That being said, Camo <em>is</em> already being used in production code over at <a href="https://polymetrics.io">Polymetrics</a> (a SaaS that provides metrics for Stripe). The code-base for Polymetrics was originally built on Mongoose, but was then replaced with Camo without any problems. Testing and development is much easier now that we can easily switch between databases.</p>
<p>Camo made designing the models for Polymetrics much easier as well. Much of the data we need to download and save from Stripe has the same fields (like dates, meta-data, etc), so extending a common schema allowed us to write less code as well as enabling us to only need to change the base schema instead of having to make the same change to many files.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Head on over to the <a href="https://www.npmjs.com/package/camo">npm</a> or <a href="https://github.com/scottwrobinson/camo">Github</a> pages and check out the project. <strong>The README is currently the best source for documentation</strong>, so if something is missing please let me know.</p>
<p>As always, any suggestions, questions, feedback, or pull requests are welcome! Feel free to contact me with via <a href="https://twitter.com/ScottWRobinson">Twitter</a>, <a href="https://github.com/scottwrobinson">Github</a>, or <a href="/cdn-cgi/l/email-protection#3a4959554e4e7a494e5b59515b584f495f14595557">email</a>.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					