
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>One of the most fundamental uses of an HTTP server is to serve static files to a user&apos;s browser, like CSS, JavaScript, or image files. Beyond normal browser usage, there are thousands of other reasons you&apos;d need to serve a static files, like for downloading music or scientific data. Either way, you&apos;ll need to come up with a simple way to let the user download these files from your server.</p>
<p>One simple way to do this is to create a Node HTTP server. As you probably know, Node.js excels at handling I/O-intensive tasks, which makes it a natural choice here. You can choose to create your own simple HTTP server from the base <a rel="nofollow" target="_blank" href="https://nodejs.org/api/http.html">http</a> module that&apos;s shipped with Node, or you can use the popular <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/serve-static">serve-static</a> package, which provides many common features of a static file server.</p>
<p>The end goal of our static server is to let the user specify a file path in the URL and have that file returned as the contents of the page. However, the user shouldn&apos;t be able to specify just <em>any</em> path on our server, otherwise a malicious user could try to take advantage of a misconfigured system and steal sensitve information. A simple attack might look like this: <code>localhost:8080/etc/shadow</code>. Here the attacker would be requesting the <code>/etc/shadow</code> file. To prevent these kinds of attacks, we should be able to tell the server to only allow the user to download certain files, or only files from certain directories (like <code>/var/www/my-website/public</code>).</p>
<h3 id="creatingyourown">Creating your own</h3>
<p>This section is meant for those of you needing a more custom option, or for those wanting to learn how static servers (or just servers in general) work. If you have a fairly common use-case, then you&apos;d be better off moving on to the next section and start working directly with the <code>serve-static</code> module.</p>
<p>While creating your own server from the <code>http</code> module takes a bit of work, it can be very rewarding in showing you how servers work underneath, which includes trade-offs for performance and security concerns that need to be taken in to account. Although, it is fairly easily create your own custom Node static server using only the built-in <code>http</code> module, so we don&apos;t have to dive too deep in to the internals of an HTTP server.</p>
<p>Obviously the <code>http</code> module isn&apos;t going to be as easy to use as something like Express, but it&apos;s a great starting point as an HTTP server. Here I&apos;ll show you how to create a simple static HTTP server, which you can then add on to and customize to your liking.</p>
<p>Let&apos;s start by just initializing and running our HTTP server:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var http = require(&apos;http&apos;);

var staticServe = function(req, res) {
    res.statusCode = 200;
    res.write(&apos;ok&apos;);
    return res.end();
};

var httpServer = http.createServer(staticServe);

httpServer.listen(8080);
</code></pre>
<p>If you run this code and navigate to <code>localhost:8080</code> in your browser then all you&apos;ll see is &apos;ok&apos; on the screen. This code handles all requests to the address <code>localhost:8080</code>. Even for non-root paths like <code>localhost:8080/some/url/path</code> you&apos;ll still get the same response. So every request received by the server is handled by the <code>staticServe</code> function, which is where the bulk of our static server logic will be.</p>
<p>The next step is to get a file path from the user, which we acquire by using the URL path. It would probably be a bad idea to let the user specify an absolute path on our system for a few reasons:</p>
<ul>
<li>The server shouldn&apos;t reveal details of the underlying operating system</li>
<li>The user should be limited in the files they can download so they can&apos;t try and access sensitive files, like <code>/etc/shadow</code>.</li>
<li>The URL shouldn&apos;t require redundant parts of the file path (like the root of the directory: <code>/var/www/my-website/public/...</code>)</li>
</ul>
<p>Given these requirements, we need to specify a base path for the server and then use the given URL as a relative path off of the base. To achieve this, we can use the <code>.resolve()</code> and <code>.join()</code> funtions from Node&apos;s <a rel="nofollow" target="_blank" href="https://nodejs.org/api/path.html">path</a> module:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var path = require(&apos;path&apos;);
var http = require(&apos;http&apos;);

var staticBasePath = &apos;./static&apos;;

var staticServe = function(req, res) {
    var resolvedBase = path.resolve(staticBasePath);
    var safeSuffix = path.normalize(req.url).replace(/^(\.\.[\/\\])+/, &apos;&apos;);
    var fileLoc = path.join(resolvedBase, safeSuffix);
    
    res.statusCode = 200;

    res.write(fileLoc);
    return res.end();
};

var httpServer = http.createServer(staticServe);

httpServer.listen(8080);
</code></pre>
<p>Here we construct the full file path using a base path, <code>staticBasePath</code>, and the given URL, which we then print to the user.</p>
<p>Now if you navigate to the same <code>localhost:8080/some/url/path</code> URL, you should see the following text printed to the browser:</p>
<pre><code>/Users/scott/Projects/static-server/static/some/url/path
</code></pre>
<p>Keep in mind that your file path will likely be different than mine, depending on your OS, username, and project path. The most important take-away is the last few directories shown (<code>static/some/url/path</code>).</p>
<p>By removing &apos;.&apos; and &apos;..&apos; from <code>req.url</code>, and then using the <code>.resolve()</code>, <code>.normalize()</code>, and <code>.join()</code> methods, we&apos;re able to restrict the user to only accessing files within the <code>./static</code> directory. Even if you try to refer to a parent directory using <code>..</code> you won&apos;t be able to access any parent directories outside of &apos;static&apos;, so our other data is safe.</p>
<p><strong>Note</strong>: Our path-joining code has not been thoroughly tested and should be considered unsafe without proper testing on your own!</p>
<p>Now that we&apos;ve restricted the path to only return files in the given directory, we can start serving up the actual files. To do this, we&apos;ll simply use the <code>fs.readFile()</code> method to load the file contents.</p>
<p>In order to better serve up the contents, all we have to do is send the file to the user using the <code>res.write(content)</code> method, much like we did with the file path earlier. If we can&apos;t find the requested file, we&apos;ll instead return a 404 error.</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var fs = require(&apos;fs&apos;);
var path = require(&apos;path&apos;);
var http = require(&apos;http&apos;);

var staticBasePath = &apos;./static&apos;;

var staticServe = function(req, res) {
    var resolvedBase = path.resolve(staticBasePath);
    var safeSuffix = path.normalize(req.url).replace(/^(\.\.[\/\\])+/, &apos;&apos;);
    var fileLoc = path.join(resolvedBase, safeSuffix);
    
    fs.readFile(fileLoc, function(err, data) {
        if (err) {
            res.writeHead(404, &apos;Not Found&apos;);
            res.write(&apos;404: File Not Found!&apos;);
            return res.end();
        }
        
        res.statusCode = 200;

        res.write(data);
        return res.end();
    });
};

var httpServer = http.createServer(staticServe);

httpServer.listen(8080);
</code></pre>
<p>Great! Now we have a primitive static file server.</p>
<p>There are still quite a few improvements we can make on this code, like file caching, adding more HTTP headers, and security checks. We&apos;ll briefly take a look at some of these in the next few sub-sections.</p>
<h4 id="caching">Caching</h4>
<p>The simplest caching technique is to just use unbounded in-memory caching. This is a good starting point, but it shouldn&apos;t be used in production (you can&apos;t always just cache <em>everything</em> in memory). All we need to do here is create a plain JavaScript object to hold the contents from files we&apos;ve previously loaded. Then on subsequent file requests, we can check if the file has already been loaded using the file path as the lookup key. If data exists in the cache object for the given key then we return the saved content, otherwise we open the file as before:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var fs = require(&apos;fs&apos;);
var path = require(&apos;path&apos;);
var http = require(&apos;http&apos;);

var staticBasePath = &apos;./static&apos;;

var cache = {};

var staticServe = function(req, res) {
    var resolvedBase = path.resolve(staticBasePath);
    var safeSuffix = path.normalize(req.url).replace(/^(\.\.[\/\\])+/, &apos;&apos;);
    var fileLoc = path.join(resolvedBase, safeSuffix);

    // Check the cache first...
    if (cache[fileLoc] !== undefined) {
        res.statusCode = 200;

        res.write(cache[fileLoc]);
        return res.end();
    }
    
    // ...otherwise load the file
    fs.readFile(fileLoc, function(err, data) {
        if (err) {
            res.writeHead(404, &apos;Not Found&apos;);
            res.write(&apos;404: File Not Found!&apos;);
            return res.end();
        }

        // Save to the cache
        cache[fileLoc] = data;
        
        res.statusCode = 200;

        res.write(data);
        return res.end();
    });
};

var httpServer = http.createServer(staticServe);

httpServer.listen(8080);
</code></pre>
<p>As I mentioned, we probably shouldn&apos;t just let the cache be unbounded, otherwise we may take up all of the system memory. A better approach would be to use a smarter cache algorithm, like <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/lru-cache">lru-cache</a>, which implements the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Cache_algorithms#Examples">least recently used</a> cache concept. This way if a file isn&apos;t requested for a while, then it is removed from the cache and memory is conserved.</p>
<h4 id="streams">Streams</h4>
<p>Another big improvement we can make is to load the file contents using <a rel="nofollow" target="_blank" href="https://nodejs.org/api/stream.html">streams</a> instead of <code>fs.readFile()</code>. The problem with <code>fs.readFile()</code> is that it needs to load and buffer <em>all</em> of the file contents before it can be sent to the user.</p>
<p>Using a stream, on the other hand, we can send the file contents to the user <em>as it&apos;s being loaded from disk</em>, byte by byte. Since we don&apos;t have to wait for the entire file to load, this reduces both the time it takes to respond to the user&apos;s request <em>and</em> the memory it takes to handle the request since we don&apos;t need to load the entire file at once.</p>
<p>Using a non-stream approach like <code>fs.readFile()</code> can get especially costly for us if the user has a slow connection, which would mean we&apos;d have to keep the file contents in memory even longer. With streams, we don&apos;t have this problem since the data is only loaded and sent from the file system as fast as the user&apos;s connection can accept it. This concept is called <a href="https://en.wikipedia.org/wiki/Back_pressure#Back_pressure_in_information_technology">back pressure</a>.</p>
<p>A simple example that implements streaming is given here:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var fs = require(&apos;fs&apos;);
var path = require(&apos;path&apos;);
var http = require(&apos;http&apos;);

var staticBasePath = &apos;./static&apos;;

var cache = {};

var staticServe = function(req, res) {
    var resolvedBase = path.resolve(staticBasePath);
    var safeSuffix = path.normalize(req.url).replace(/^(\.\.[\/\\])+/, &apos;&apos;);
    var fileLoc = path.join(resolvedBase, safeSuffix);
    
        var stream = fs.createReadStream(fileLoc);

        // Handle non-existent file
        stream.on(&apos;error&apos;, function(error) {
            res.writeHead(404, &apos;Not Found&apos;);
            res.write(&apos;404: File Not Found!&apos;);
            res.end();
        });

        // File exists, stream it to user
        res.statusCode = 200;
        stream.pipe(res);
};

var httpServer = http.createServer(staticServe);

httpServer.listen(8080);
</code></pre>
<p>Note that this doesn&apos;t add any caching like we showed earlier. If you want to include it, all you need to do is add a listener to the stream&apos;s <code>data</code> event and incrementally save the chunks to the cache. I&apos;ll leave this up to you to implement on your own :)</p>
<h3 id="servestatic">serve-static</h3>
<p>If you need a static file server for production use, there are a few other options you may want to consider instead of writing your own from scratch. <a rel="nofollow" target="_blank" href="https://www.nginx.com/">Nginx</a> is one of the best options out there, but if your use-case requires you to use Node for whatever reason, or you have something against Nginx, then the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/serve-static">serve-static</a> module also works very well.</p>
<p>The great thing about this module, in my opinion, is that it can also be used as <a href="/how-to-write-express-js-middleware/">middleware</a> for the popular web framework, Express. This seems to be the use-case for most people since they usually need to serve dynamic content along with their static files anyway.</p>
<p>First, if you want to use it a stand-alone server, you can use the <code>http</code> module with <code>serve-static</code> and <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/finalhandler">finalhandler</a> in just a few lines like this:</p>
<pre><code class="language-javascript">var http = require(&apos;http&apos;);
var finalhandler = require(&apos;finalhandler&apos;);
var serveStatic = require(&apos;serve-static&apos;);

var staticBasePath = &apos;./static&apos;;

var serve = serveStatic(staticBasePath, {&apos;index&apos;: false});

var server = http.createServer(function(req, res){
    var done = finalhandler(req, res);
    serve(req, res, done);
})

server.listen(8080);
</code></pre>
<p>Otherwise if you&apos;re using Express, then all you need to do is add it as middleware:</p>
<pre><code class="language-javascript">var express = require(&apos;express&apos;)
var serveStatic = require(&apos;serve-static&apos;)

var staticBasePath = &apos;./static&apos;;
 
var app = express()
 
app.use(serveStatic(staticBasePath, {&apos;index&apos;: false}))
app.listen(8080)
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>In this article I&apos;ve presented a few options for running a static file server with Node.js. Keep in mind that there are still more options out there than what I&apos;ve mentioned here.</p>
<p>For example, there are a few other similar modules, like <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/node-static">node-static</a> and <a href="https://www.npmjs.com/package/http-server">http-server</a>. I just didn&apos;t use them here since <code>serve-static</code> is much more widely used, and therefore probably more stable. Just know that there are other options that are worth checking out.</p>
<p><em>If you have any other improvements to make the static file servers faster, feel free to post them in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					