
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>One of the most common things you&apos;ll want to do with just about any programming language is open and read a file. With most languages, this is pretty simple, but for JavaScript veterans it might seem a bit weird. For so many years JavaScript was only available in the browser, so front-end developers may only be familiar with the <a rel="nofollow" target="_blank" href="http://www.htmlgoodies.com/beyond/javascript/read-text-files-using-the-javascript-filereader.html#fbid=PfBQ7GOxcEB">FileReader API</a> or similar.</p>
<p>Node.js, as you probably know, is much different than your typical JavaScript in the browser. It has its own set of libraries meant for handling OS and filesystem tasks, like opening and reading files. In this article I&apos;ll show you how to use Node.js to read files. Specifically, we&apos;ll be using the <a rel="nofollow" target="_blank" href="https://nodejs.org/api/fs.html">fs</a> module to do just that.</p>
<p>There are two ways you can open and read a file using the <code>fs</code> module:</p>
<ul>
<li>Load all of the contents at once (buffering)</li>
<li>Incrementally load contents (streaming)</li>
</ul>
<p>Each of these methods will be explained in the next two sections.</p>
<h3 id="bufferingcontentswithfsreadfile">Buffering Contents with fs.readFile</h3>
<p>This is the most common way to read a file with Node.js, especially for beginners, due to its simplicity and convenience. Although, as you&apos;ll come to realize in the next section, it isn&apos;t necessarily the best or most efficient.</p>
<p>Here is a quick example using <a rel="nofollow" target="_blank" href="https://nodejs.org/api/fs.html#fs_fs_readfile_file_options_callback">fs.readFile</a>:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

fs.readFile(&apos;my-file.txt&apos;, &apos;utf8&apos;, function(err, data) {
    if (err) throw err;
    console.log(data);
});
</code></pre>
<p>The <code>data</code> argument to the callback contains the full contents of the file represented as a string in <code>utf8</code> format. If you omit the <code>utf8</code> argument completely, then the method will just return the raw contents in a <a rel="nofollow" target="_blank" href="https://nodejs.org/api/buffer.html">Buffer</a> object. Removing the <code>utf8</code> argument in the above code (and assuming <em>my-file.txt</em> contained the string &quot;Hey there!&quot;), we&apos;d get this output:</p>
<pre><code class="language-bash">$ node read-file.js
&lt;Buffer 48 65 79 20 74 68 65 72 65 21&gt;
</code></pre>
<p>You may have noticed that <code>fs.readFile</code> returns the contents in a callback, which means this method runs asynchronously. This should be used whenever possible to avoid blocking the main execution thread, but sometimes you have to do things synchronously, in which case Node provides you with a <a rel="nofollow" target="_blank" href="https://nodejs.org/api/fs.html#fs_fs_readfilesync_file_options">readFileSync</a> method.</p>
<p>This method works exactly the same way, except that the file contents are returned directly from the function call and the execution thread is blocked while it loads the file. I typically use this in start-up sections of my programs (like when we&apos;re loading config files) or in command-line apps where blocking the main thread isn&apos;t a big deal.</p>
<p>Here is how to load a file synchronously with Node:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

try {
    var data = fs.readFileSync(&apos;my-file.txt&apos;, &apos;utf8&apos;);
    console.log(data);    
} catch(e) {
    console.log(&apos;Error:&apos;, e.stack);
}
</code></pre>
<p>Notice that with the blocking (synchronous) call we have to use <code>try...catch</code> to handle any errors, unlike the non-blocking (asynchronous) version where errors were just passed to us as arguments.</p>
<p>Other than the way these methods return data and handle errors, they work very much the same.</p>
<h3 id="streamingcontentswithfscreatereadstream">Streaming Contents with fs.createReadStream</h3>
<p>The second way to open and read a file is to open it as a <a rel="nofollow" target="_blank" href="https://nodejs.org/api/stream.html">Stream</a> using the <a rel="nofollow" target="_blank" href="https://nodejs.org/api/fs.html#fs_fs_createreadstream_path_options">fs.createReadStream</a> method. All Node streams are instances of the <a rel="nofollow" target="_blank" href="https://nodejs.org/api/events.html#events_class_eventemitter">EventEmitter</a> object, allowing you to subscribe to important events.</p>
<p>A readable stream object can be useful for a lot of reasons, a few of which include:</p>
<ul>
<li><em>Smaller memory footprint</em>. Since the target file&apos;s data is loaded in chunks, not as much memory is required to store the data in a buffer.</li>
<li><em>Faster response time</em>. For time-sensitive applications, the time between the request and response is critical. Streams cut down the response time (especially for large files) since they don&apos;t need to wait to load the entire file before returning data.</li>
<li><em>Piping data</em>. The stream abstraction allows you to use a common interface between data producers and consumers to pass that data around via pipes. This is very similar to the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Pipeline_%28Unix%29">Unix pipe</a> concept.</li>
</ul>
<p>Although it really isn&apos;t very hard to use streams, they can be a bit intimidating and aren&apos;t quite as intuitive as the <code>fs.readFile</code> method. Here is the &apos;hello world&apos; of file streaming:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);

var data = &apos;&apos;;

var readStream = fs.createReadStream(&apos;my-file.txt&apos;, &apos;utf8&apos;);

readStream.on(&apos;data&apos;, function(chunk) {
    data += chunk;
}).on(&apos;end&apos;, function() {
    console.log(data);
});
</code></pre>
<p>This code does exactly what the code in the first section does, except that we have to &quot;collect&quot; chunks of data before printing it out to the console. If your file is fairly small then you&apos;ll probably only ever receive a single chunk, but for larger files, like audio and video, you&apos;ll have to collect multiple chunks. This is the case where you&apos;ll start to notice the real value of streaming files.</p>
<p>Note that the example I showed above mostly defeats the purpose of using a stream since we end up collecting the data in a buffer (variable) anyway, but at least it gives you an idea as to how they work. A better example showing the strengths of file streams can be seen here, in an Express route that handles a file request:</p>
<pre><code class="language-javascript">var fs = require(&apos;fs&apos;);
var path = require(&apos;path&apos;);
var http = require(&apos;http&apos;);

var staticBasePath = &apos;./static&apos;;

var staticServe = function(req, res) {
    var fileLoc = path.resolve(staticBasePath);
    fileLoc = path.join(fileLoc, req.url);
    
        var stream = fs.createReadStream(fileLoc);

        stream.on(&apos;error&apos;, function(error) {
            res.writeHead(404, &apos;Not Found&apos;);
            res.end();
        });

        stream.pipe(res);
};

var httpServer = http.createServer(staticServe);
httpServer.listen(8080);
</code></pre>
<p>All we do here is open the file with <code>fs.createReadStream</code> and pipe it to the response object, <code>res</code>. We can even subscribe to <code>error</code> events and handle those as they happen. It&apos;s a much better method to handling files once you learn how to properly use it. For a more complete example and explanation of the above code, check out this article on <a href="/node-http-servers-for-static-file-serving/">creating static file servers with Node</a>.</p>
<h3 id="conclusion">Conclusion</h3>
<p>From this article you should have learned the basics of reading files, as well as some advanced loading methods using Stream objects. Knowing when to use them is the key, and should be carefully considered for memory-constrained or time-constrained applications.</p>
<p><em>What&apos;s your preferred method of handling files? How have you used Streams in the past? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					