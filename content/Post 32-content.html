
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>One of the most exciting features coming to JavaScript (and therefore Node.js) is the <code>async</code>/<code>await</code> syntax being introduced in <a rel="nofollow" target="_blank" href="https://github.com/hemanth/es7-features">ES7</a>. Although it&apos;s basically just syntactic sugar on top of Promises, these two keywords alone should make writing asynchronous code in Node much more bearable. It all but eliminates the problem of <a href="/avoiding-callback-hell-in-node-js/">callback hell</a>, and even let&apos;s us use control-flow structures around our asynchronous code.</p>
<p>Throughout this article we&apos;ll take a look at what&apos;s wrong with Promises, how the new <code>await</code> feature can help, and how you can start using it <em>right now</em>.</p>
<h3 id="theproblemwithpromises">The Problem with Promises</h3>
<p>The concept of a &quot;promise&quot; in JavaScript has been around for a while, and it&apos;s been usable for years now thanks to 3rd party libraries like <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/bluebird">Bluebird</a> and <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/q">q</a>, not to mention the recently added native support in ES6.</p>
<p>They&apos;ve been a great solution to the problem of callback hell, but unfortunately they don&apos;t solve all of the asynchronous problems. While a great improvement, Promises leave us wanting even more simplification.</p>
<p>Let&apos;s say you want to use Github&apos;s REST API to find the number of stars a project has. In this case, you&apos;d likely use the great <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/request-promise">request-promise</a> library. Using the Promise-based approach, you have to make the request and get the result back within the callback you pass to <code>.then()</code>, like this:</p>
<pre><code class="language-javascript">var request = require(&apos;request-promise&apos;);

var options = {
    url: &apos;https://api.github.com/repos/scottwrobinson/camo&apos;,
    headers: {
        &apos;User-Agent&apos;: &apos;YOUR-GITHUB-USERNAME&apos;
    }
};

request.get(options).then(function(body) {
    var json = JSON.parse(body);
    console.log(&apos;Camo has&apos;, json.stargazers_count, &apos;stars!&apos;);
});
</code></pre>
<p>This will print out something like:</p>
<pre><code class="language-bash">$ node index.js
Camo has 1,000,000 stars!
</code></pre>
<p>Okay, maybe that number is a slight exaggeration, but you get the point ;)</p>
<p>Making just one request like this isn&apos;t too hard with Promises, but what if we want to make the same request for lots of different repositories on GitHub? And what happens if we need to add control flow (like conditionals or loops) around the requests? As your requirements become more complicated, Promises become harder to work with and still end up complicating your code. They&apos;re still better than the normal callbacks since you don&apos;t have unbounded nesting, but they don&apos;t solve all of your problems.</p>
<p>For more complicated scenarios like the one in the following code, you need to get good at chaining Promises together and understanding <em>when and where</em> your asynchronous code gets executed.</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var request = require(&apos;request-promise&apos;);

var headers = {
    &apos;User-Agent&apos;: &apos;YOUR-GITHUB-USERNAME&apos;
};

var repos = [
    &apos;scottwrobinson/camo&apos;,
    &apos;facebook/react&apos;,
    &apos;scottwrobinson/twentyjs&apos;,
    &apos;moment/moment&apos;,
    &apos;nodejs/node&apos;,
    &apos;lodash/lodash&apos;
];

var issueTitles = [];

var reqs = Promise.resolve();

repos.forEach(function(r) {
    var options = { url: &apos;https://api.github.com/repos/&apos; + r, headers: headers };

    reqs = reqs.then(function() {
        return request.get(options);
    }).then(function(body) {
        var json = JSON.parse(body);

        var p = Promise.resolve();

        // Only make request if it has open issues
        if (json.has_issues) {
            var issuesOptions = { url: &apos;https://api.github.com/repos/&apos; + r + &apos;/issues&apos;, headers: headers };
            p = request.get(issuesOptions).then(function(ibody) {
                var issuesJson = JSON.parse(ibody);

                if (issuesJson[0]) {
                    issueTitles.push(issuesJson[0].title);
                }
            });
        }

        return p;
    });
});

reqs.then(function() {
    console.log(&apos;Issue titles:&apos;);
    issueTitles.forEach(function(t) {
        console.log(t);
    });
});
</code></pre>
<p><strong>Note</strong>: Github aggressively rate-limits unauthenticated requests, so don&apos;t be surprised if you get cut off after running the above code only a few times. You can increase this limit by <a rel="nofollow" target="_blank" href="https://developer.github.com/v3/#increasing-the-unauthenticated-rate-limit-for-oauth-applications">passing a client ID/secret</a>.</p>
<p>At the time of this writing, executing this code would yield the following:</p>
<pre><code class="language-bash">$ node index.js
Issue titles:
feature request: bulk create/save support
Made renderIntoDocument tests asynchronous.
moment issue template
test: robust handling of env for npm-test-install
</code></pre>
<p>Just by adding a <code>for</code> loop and an <code>if</code> statement to our asynchronous code makes it much harder to read and understand. This kind of complexity can only be sustained for so long before it becomes too difficult to work with.</p>
<p>Looking at the code, can you immediately tell me where the requests are actually getting executed, or in what order each code block runs? Probably not without reading through it carefully.</p>
<h3 id="simplifyingwithasyncawait">Simplifying with Async/Await</h3>
<p>The new <code>async</code>/<code>await</code> syntax allows you to still use Promises, but it eliminates the need for providing a callback to the chained <code>then()</code> methods. The value that would have been sent to the <code>then()</code> callback is instead returned directly from the asynchronous function, just as if it were a synchronous blocking function.</p>
<pre><code class="language-javascript">let value = await myPromisifiedFunction();
</code></pre>
<p>While seemingly simple, this is a huge simplification to the design of asynchronous JavaScript code. The only extra syntax needed to achieve this is the <code>await</code> keyword. So if you understand how Promises work, then it won&apos;t be too hard to understand how to use these new keywords since they build on top of the concept of Promises. All you really have to know is that <em>any Promise can be <code>await</code>-ed</em>. Values can also be <code>await</code>-ed, just like a Promise can <code>.resolve()</code> on a integer or string.</p>
<p>Let&apos;s compare the Promise-based method with the <code>await</code> keyword:</p>
<p><strong>Promises</strong></p>
<pre><code class="language-javascript">var request = require(&apos;request-promise&apos;);

request.get(&apos;https://api.github.com/repos/scottwrobinson/camo&apos;).then(function(body) {
    console.log(&apos;Body:&apos;, body);
});
</code></pre>
<p><strong>await</strong></p>
<pre><code class="language-javascript">var request = require(&apos;request-promise&apos;);

async function main() {
    var body = await request.get(&apos;https://api.github.com/repos/scottwrobinson/camo&apos;);
    console.log(&apos;Body:&apos;, body);
}
main();
</code></pre>
<p>As you can see, <code>await</code> indicates that you want to resolve the Promise and <em>not return that actual Promise object</em> as it would normally. When this line is executed, the <code>request</code> call will get put on the event loop&apos;s stack and execution will yield to other asynchronous code that is ready to be processed.</p>
<p>The <code>async</code> keyword is used when you&apos;re defining a function that contains asynchronous code. This is an indicator that a Promise is returned from the function and should therefore be treated as asynchronous.</p>
<p>Here is a simple example of its usage (notice the change in the function definition):</p>
<pre><code class="language-javascript">async function getCamoJson() {
    var options = {
        url: &apos;https://api.github.com/repos/scottwrobinson/camo&apos;,
        headers: {
            &apos;User-Agent&apos;: &apos;YOUR-GITHUB-USERNAME&apos;
        }
    };
    return await request.get(options);
}

var body = await getCamoJson();
</code></pre>
<p>Now that we know how to use <code>async</code> and <code>await</code> together, let&apos;s see what the more complex Promise-based code from earlier looks like now:</p>
<pre><code class="language-javascript">&quot;use strict&quot;;

var request = require(&apos;request-promise&apos;);

var headers = {
    &apos;User-Agent&apos;: &apos;scottwrobinson&apos;
};

var repos = [
    &apos;scottwrobinson/camo&apos;,
    &apos;facebook/react&apos;,
    &apos;scottwrobinson/twentyjs&apos;,
    &apos;moment/moment&apos;,
    &apos;nodejs/node&apos;,
    &apos;lodash/lodash&apos;
];

var issueTitles = [];

async function main() {
    for (let i = 0; i &lt; repos.length; i++) {
        let options = { url: &apos;https://api.github.com/repos/&apos; + repos[i], headers: headers };
        let body = await request.get(options);
        let json = JSON.parse(body);

        if (json.has_issues) {
            let issuesOptions = { url: &apos;https://api.github.com/repos/&apos; + repos[i] + &apos;/issues&apos;, headers: headers };
            let ibody = await request.get(issuesOptions);
            let issuesJson = JSON.parse(ibody);

            if (issuesJson[0]) {
                issueTitles.push(issuesJson[0].title);
            }
        }
    }

    console.log(&apos;Issue titles:&apos;);
    issueTitles.forEach(function(t) {
        console.log(t);
    });
}

main();
</code></pre>
<p>It is certainly more readable now that it can be written like many other linearly-executed languages.</p>
<p>Now the only problem is that each <code>request.get()</code> call is executed in series (meaning each call has to wait until the previous call has finished before executing), so we have to wait longer for the code to complete execution before getting our results. The better option would be to run the HTTP GET requests in parallel. This can still be done by utilizing <code>Promise.all()</code> like we would have done before. Just replace the <code>for</code> loop with a <code>.map()</code> call and send the resulting array of Promises to <code>Promise.all()</code>, like this:</p>
<pre><code class="language-javascript">// Init code omitted...

async function main() {
    let reqs = repos.map(async function(r) {
        let options = { url: &apos;https://api.github.com/repos/&apos; + r, headers: headers };
        let body = await request.get(options);
        let json = JSON.parse(body);

        if (json.has_issues) {
            let issuesOptions = { url: &apos;https://api.github.com/repos/&apos; + r + &apos;/issues&apos;, headers: headers };
            let ibody = await request.get(issuesOptions);
            let issuesJson = JSON.parse(ibody);

            if (issuesJson[0]) {
                issueTitles.push(issuesJson[0].title);
            }
        }
    });

    await Promise.all(reqs);
}

main();
</code></pre>
<p>This way you can take advantage of the speed of parallel execution <em>and</em> the simplicity of <code>await</code>.</p>
<p>There are more benefits than just being able to use traditional control-flow like loops and conditionals. This linear approach lets us get back to using the <code>try...catch</code> statement for handling errors. With Promises you had to use the <code>.catch()</code> method, which worked, but could cause confusion determining which Promises it caught exceptions for.</p>
<p>So now this...</p>
<pre><code class="language-javascript">request.get(&apos;https://api.github.com/repos/scottwrobinson/camo&apos;).then(function(body) {
    console.log(body);
}).catch(function(err) {
    console.log(&apos;Got an error:&apos;, err.message);
});

// Got an error: 403 - &quot;Request forbidden by administrative rules. Please make sure your request has a User-Agent header...&quot;
</code></pre>
<p>...can be expressed like this:</p>
<pre><code class="language-javascript">try {
    var body = await request.get(&apos;https://api.github.com/repos/scottwrobinson/camo&apos;);
    console.log(body);
} catch(err) {
    console.log(&apos;Got an error:&apos;, err.message)
}

// Got an error: 403 - &quot;Request forbidden by administrative rules. Please make sure your request has a User-Agent header...&quot;
</code></pre>
<p>While it&apos;s about the same amount of code, it is much easier to read and understand for someone transitioning to JavaScript from another language.</p>
<h3 id="usingasyncrightnow">Using Async Right Now</h3>
<p>The async feature is still in the proposal stage, but don&apos;t worry, there are still a few ways you can use this in your code <em>right now</em>.</p>
<h4 id="v8">V8</h4>
<p>While it hasn&apos;t quite made its way in to Node yet, the V8 team has <a rel="nofollow" target="_blank" href="https://groups.google.com/forum/#!topic/v8-users/rGgUWxckYfM">stated publicly</a> their intention to implement the <code>async</code>/<code>await</code> feature. They&apos;ve even already committed the prototype runtime implementation, which means harmony support shouldn&apos;t be too far behind.</p>
<h4 id="babel">Babel</h4>
<p>Arguably the most popular option is to transpile your code using <a rel="nofollow" target="_blank" href="https://babeljs.io/">Babel</a> and its various plugins. Babel is extremely popular thanks to its ability to mix and match ES6 and ES7 features using their plugin system. While a bit more complicated to get set up, it also provides a lot more control to the developer.</p>
<h4 id="regenerator">Regenerator</h4>
<p>The <a rel="nofollow" target="_blank" href="https://github.com/facebook/regenerator">regenerator</a> project by Facebook doesn&apos;t have as many features as Babel, but it is a simpler way to get async transpiling working.</p>
<p>The biggest problem I&apos;ve had with it is that its errors aren&apos;t very descriptive. So if there is a syntax error in your code, you won&apos;t get much assistance from regenerator in finding it. Other than that, I&apos;ve been happy with it.</p>
<h4 id="traceur">Traceur</h4>
<p>I don&apos;t have any experience with this one personally, but <a rel="nofollow" target="_blank" href="https://github.com/google/traceur-compiler">Traceur</a> (by Google) seems to be another popular option with a lot of available features. You can find more info <a rel="nofollow" target="_blank" href="https://github.com/google/traceur-compiler/wiki/LanguageFeatures">here</a> for details on what ES6 and ES7 features can be transpiled.</p>
<h4 id="asyncawait">asyncawait</h4>
<p>Most of the options available to you involve either transpiling or using a nightly build of V8 to get <code>async</code> working. Another option is to use the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/asyncawait">asyncawait</a> package, which provides a function to resolve Promises similarly to the <code>await</code> feature. It is a nice vanilla ES5 way of getting similar-looking syntax.</p>
<h3 id="conclusion">Conclusion</h3>
<p>And that&apos;s it! Personally, I&apos;m most excited about this feature in ES7, but there are some other <a rel="nofollow" target="_blank" href="https://github.com/hemanth/es7-features">great features in ES7</a> that you should check out, like class decorators and properties.</p>
<p><em>Do you use transpiled ES7 code? If so, which feature has been the most beneficial to your work? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					