
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>Node.js is great for a lot of reasons, one of which is the speed in which you can build meaningful applications. However, as we all know, this comes at the price of performance (as compared to native code). To get around this, you can write your code to interface with faster code written in C or C++. All we need to do is let Node know where to find this code and how to interface with it.</p>
<p>There are a few ways to solve this problem depending on what level of abstraction you want. We&apos;ll start with the lowest abstraction, which is the Node <a href="https://nodejs.org/api/addons.html" rel="nofollow" target="_blank">Addon</a>.</p>
<h3 id="addons">Addons</h3>
<p>An addon works by providing the glue between Node and C/C++ libraries. For the typical Node developer this may be a bit complicated as you&apos;ll have to get in to actually writing C/C++ code to set up the interface. However, between this article and the Node documentation, you should be able to get some simple interfaces working.</p>
<p>There are a few things we need to go over before we can jump in to creating addons. First of all, we need to know how to compile (something Node developers happily forget about) the native code. This is done using <a href="https://github.com/nodejs/node-gyp" rel="nofollow" target="_blank">node-gyp</a>. Then, we&apos;ll briefly talk about <a href="https://www.npmjs.com/package/nan" rel="nofollow" target="_blank">nan</a>, which helps with handling different Node API versions.</p>
<h4 id="nodegyp">node-gyp</h4>
<p>There are a lot of different kinds of processors out there (x86, ARM, PowerPC, etc), and even more operating systems to deal with when compiling your code. Luckily, <code>node-gyp</code> handles all of this for you. As described by their Github page, <code>node-gyp</code> is a &quot;cross-platform command-line tool written in Node.js for compiling native addon modules for Node.js&quot;. Essentially, <code>node-gyp</code> is just a wrapper around <a href="https://code.google.com/p/gyp/" rel="nofollow" target="_blank">gyp</a>, which is made by the Chromium team.</p>
<p>The project&apos;s README has some great instructions on how to install and use the package, so you should read that for more detail. In short, to use <code>node-gyp</code> you need to do the following.</p>
<p>Go to your project&apos;s directory:</p>
<pre><code class="language-sh">$ cd my_node_addon
</code></pre>
<p>Generate the appropriate build files using the <code>configure</code> command, which will create either a <code>Makefile</code> (on Unix), or <code>vcxproj</code> (on Windows):</p>
<pre><code class="language-sh">$ node-gyp configure
</code></pre>
<p>And finally, build the project:</p>
<pre><code class="language-sh">$ node-gyp build
</code></pre>
<p>This will generate a <code>/build</code> directory containing, among other things, the compiled binary.</p>
<p>Even when using higher abstractions like the <code>ffi</code> package, it&apos;s still good to understand what is happening under the hood, so I&apos;d recommend you take the time to learn the ins and outs of <code>node-gyp</code>.</p>
<h4 id="nan">nan</h4>
<p><code>nan</code> (Native Abstractions for Node) is an easily overlooked module, but it&apos;ll save you hours of frustration. Between Node versions <code>v0.8</code>, <code>v0.10</code>, and <code>v0.12</code>, the V8 versions used went through some big changes (in addition to changes within Node itself), so <code>nan</code> helps hide these changes from you and provides a nice, consistent interface.</p>
<p>This native abstraction works by providing C/C++ objects/functions in the <code>#include &lt;nan.h&gt;</code> header file.</p>
<p>To use it, install the <code>nan</code> package:</p>
<pre><code class="language-sh">$ npm install --save nan
</code></pre>
<p>Add these lines to your binding.gyp file:</p>
<pre><code class="language-json">&quot;include_dirs&quot; : [ 
    &quot;&lt;!(node -e \&quot;require(&apos;nan&apos;)\&quot;)&quot;
]
</code></pre>
<p>And you&apos;re ready to use the <a href="https://www.npmjs.com/package/nan#javascript-accessible-methods" rel="nofollow" target="_blank">methods/functions</a> from <code>nan.h</code> within your hooks instead of the original <code>#include &lt;node.h&gt;</code> code. I&apos;d highly recommend you use <code>nan</code>. There isn&apos;t much point in re-inventing the wheel in this case.</p>
<h4 id="creatingtheaddon">Creating the Addon</h4>
<p>Before starting on your addon, make sure you take some time to familiarize yourself with the following libraries:</p>
<ul>
<li>The V8 JavaScript C++ library, which is used for actually interfacing with JavaScript (like creating functions, calling objects, etc).
<ul>
<li><strong>NOTE</strong>: <code>node.h</code> is the default file suggested, but really <code>nan.h</code> should be used instead</li>
</ul>
</li>
<li><a href="https://github.com/libuv/libuv" rel="nofollow" target="_blank">libuv</a>, a cross-platform asynchronous I/O library written in C. This library is useful for when performing any type of I/O (opening a file, writing to the network, setting a timer, etc) in your native libraries and you need to make it asynchronous.</li>
<li>Internal Node libraries. One of the more important objects to understand is <code>node::ObjectWrap</code>, which most objects derive from.</li>
</ul>
<p>Throughout the rest of this section, I&apos;ll walk you through an actual example. In this case, we&apos;ll be creating a hook to the C++ <code>&lt;cmath&gt;</code> library&apos;s <code>pow</code> function. Since you should almost always be using <code>nan</code>, that&apos;s what I&apos;ll be using throughout the examples.</p>
<p>For this example, in your addon project you should have at least these files present:</p>
<ul>
<li><code>pow.cpp</code></li>
<li><code>binding.gyp</code></li>
<li><code>package.json</code></li>
</ul>
<p>The C++ file doesn&apos;t need to be named <code>pow.cpp</code>, but the name typically reflects either that it is an addon, or its specific function.</p>
<pre><code class="language-c++">// pow.cpp
#include &lt;cmath&gt;
#include &lt;nan.h&gt;

void Pow(const Nan::FunctionCallbackInfo&lt;v8::Value&gt;&amp; info) {

	if (info.Length() &lt; 2) {
		Nan::ThrowTypeError(&quot;Wrong number of arguments&quot;);
		return;
	}

	if (!info[0]-&gt;IsNumber() || !info[1]-&gt;IsNumber()) {
		Nan::ThrowTypeError(&quot;Both arguments should be numbers&quot;);
		return;
	}

	double arg0 = info[0]-&gt;NumberValue();
	double arg1 = info[1]-&gt;NumberValue();
	v8::Local&lt;v8::Number&gt; num = Nan::New(pow(arg0, arg1));

	info.GetReturnValue().Set(num);
}

void Init(v8::Local&lt;v8::Object&gt; exports) {
	exports-&gt;Set(Nan::New(&quot;pow&quot;).ToLocalChecked(),
				 Nan::New&lt;v8::FunctionTemplate&gt;(Pow)-&gt;GetFunction());
}

NODE_MODULE(pow, Init)
</code></pre>
<p>Note there is not semicolon (<code>;</code>) at the end of <code>NODE_MODULE</code>. This is done intentionally since <code>NODE_MODULE</code> is not actually a function - it&apos;s a macro.</p>
<p>The above code may seem a bit daunting at first for those that haven&apos;t written any C++ in a while (or ever), but it really isn&apos;t too hard to understand. The <code>Pow</code> function is the meat of the code where we check the number of arguments passed, the types of the arguments, call the native <code>pow</code> function, and return the result to the Node application. The <code>info</code> object contains everything about the call that we need to know, including the arguments (and their types) and a place to return the result.</p>
<p>The <code>Init</code> function mostly just associates the <code>Pow</code> function with the<br>
&quot;pow&quot; name, and the <code>NODE_MODULE</code> macro actually handles registering the addon with Node.</p>
<p>The <code>package.json</code> file is not much different from a normal Node module. Although it doesn&apos;t seem to be required, most Addon modules have <code>&quot;gypfile&quot;: true</code> set within them, but the build process seems to still work fine without it. Here is what I used for this example:</p>
<pre><code class="language-json">{
  &quot;name&quot;: &quot;addon-hook&quot;,
  &quot;version&quot;: &quot;0.0.0&quot;,
  &quot;description&quot;: &quot;Node.js Addon Example&quot;,
  &quot;main&quot;: &quot;index.js&quot;,
  &quot;dependencies&quot;: {
    &quot;nan&quot;: &quot;^2.0.0&quot;
  },
  &quot;scripts&quot;: {
    &quot;test&quot;: &quot;node index.js&quot;
  }
}
</code></pre>
<p>Next, this code needs to be built into a &apos;pow.node&apos; file, which is the binary of the Addon. To do this, you&apos;ll need to tell <code>node-gyp</code> what files it needs to compile and the binary&apos;s resulting filename. While there are many other options/configurations you can use with <code>node-gyp</code>, for this example we don&apos;t need a whole lot. The <code>binding.gyp</code> file can be as simple as:</p>
<pre><code class="language-json">{
	&quot;targets&quot;: [
		{
			&quot;target_name&quot;: &quot;pow&quot;,
			&quot;sources&quot;: [ &quot;pow.cpp&quot; ],
			&quot;include_dirs&quot;: [
				&quot;&lt;!(node -e \&quot;require(&apos;nan&apos;)\&quot;)&quot;
			]
		}
	]
}
</code></pre>
<p>Now, using <code>node-gyp</code>, generate the appropriate project build files for the given platform:</p>
<pre><code class="language-sh">$ node-gyp configure
</code></pre>
<p>And finally, build the project:</p>
<pre><code class="language-sh">$ node-gyp build
</code></pre>
<p>This should result in a <code>pow.node</code> file being created, which will reside in the <code>build/Release/</code> directory. To use this hook in your application code, just <code>require</code> in the <code>pow.node</code> file (sans the &apos;.node&apos; extension):</p>
<pre><code class="language-javascript">var addon = require(&apos;./build/Release/pow&apos;);

console.log(addon.pow(4, 2));		// Prints &apos;16&apos;
</code></pre>
<h3 id="nodeforeignfunctioninterface">Node Foreign Function Interface</h3>
<p><strong>Note</strong>: The <code>ffi</code> package is formerly known as <code>node-ffi</code>. Be sure to add the newer <code>ffi</code> name to your dependencies to avoid a lot of confusion during <code>npm install</code> :)</p>
<p>While the Addon functionality provided by Node gives you all the flexibility you need, not all developers/projects will need it. In many cases an abstraction like <code>ffi</code> will do just fine, and typically require very little to no C/C++ programming.</p>
<p><code>ffi</code> only loads dynamic libraries, which can be limiting for some, but it also makes the hooks much easier to set up.</p>
<pre><code class="language-javascript">var ffi = require(&apos;ffi&apos;);

var libm = ffi.Library(&apos;libm&apos;, {
	&apos;pow&apos;: [ &apos;double&apos;, [ &apos;double&apos;, &apos;double&apos; ] ]
});

console.log(libm.pow(4, 2));	// 16
</code></pre>
<p>The above code works by specifying the library to load (libm), and specifically which methods to load from that library (pow). The <code>[ &apos;double&apos;, [ &apos;double&apos;, &apos;double&apos; ] ]</code> line tells <code>ffi</code> what the return type and parameters of the method is, which in this case is two <code>double</code> parameters and a <code>double</code> returned.</p>
<h3 id="conclusion">Conclusion</h3>
<p>While it may seem intimidating at first, creating an Addon really isn&apos;t too bad after you&apos;ve had the chance to work through a small example like this on your own. When possible, I&apos;d suggest hooking in to a dynamic library to make creating the interface and loading the code much easier, although for many projects this may not be possible or the best choice.</p>
<p><em>Are there any examples of libraries you&apos;d like to see bindings for? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					