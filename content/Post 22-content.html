
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>Using modules is an essential part of building complete applications and software systems using Node.js. In the absence of modules, your code would be fragmented and difficult to run, let alone maintain over time. But what is a module? And how exactly are you supposed to use <code>module.exports</code> to build your Node.js programs?</p>
<p>A module is a discrete program, contained in a single file in Node.js. Modules are therefore tied to files, with one module per file. Modules are available in other programming languages. Node.JS uses the CommonJS system of modules, but there are other module types used in the JavaScript ecosystem. The most prominent of these other module systems are the Asynchronous Module Definition (AMD) and the (ECMAScript 6) ES6 module systems.</p>
<p>As we will see, <code>module.exports</code> is an object that the current module returns when it is &quot;required&quot; in another program or module.</p>
<p>You can include functionality from other modules in any other module. To do so is referred to as &quot;requiring&quot; the module, which is simply calling for a certain special object representing the functionality of the module.</p>
<h3 id="sharingcodewithmoduleexports">Sharing Code with module.exports</h3>
<p>For everyday use, modules allow us to compose bigger programs out of smaller pieces. <a rel="nofollow" target="_blank" href="https://nodejs.org/api/modules.html">Modules</a> become the basic building blocks of the larger piece of software that collectively, they define.</p>
<p>Under the covers, the module keeps track of itself through an object named <code>module</code>. Inside each module, therefore, &apos;module&apos; refers to the object representing the current module. This object holds metadata about the module, such as the filename of the module, as well as the module&apos;s id.</p>
<p>Here is a little snippet of code you can run to see the values of these example properties on a module:</p>
<pre><code class="language-javascript">// module1.js

console.log(module.filename);
console.log(module.id);
console.log(module.exports);
</code></pre>
<p>You can run this using the command <code>node module1.js</code>. You will see, for example, that the <code>module.filename</code> property is set to a file path that ends with the correct name of the file in which this module exists, which is <code>module1.js</code>. Here is an example output for the code above:</p>
<pre><code class="language-sh">$ node module1.js
/Users/scott/projects/sandbox/javascript/module-test/module1.js
.
{}
</code></pre>
<p>In Node.js, the practice of making a module&apos;s code available for other modules to use is called &quot;exporting&quot; values.</p>
<p>But if a piece of complex software is to be built from individual modules, you may already be thinking about the next questions:</p>
<p><strong>Important Question #1</strong>: How does Node.js identify the &quot;main&quot; module to start running the program?</p>
<p>Node.js identifies the main module to run by the arguments that get passed to the <code>node</code> executable. For instance if we have a module contained in the file <strong>server.js</strong>, along with other parts of our program contained in files <strong>login.js</strong> and <strong>music_stream.js</strong>, invoking the command <code>node server.js</code> identifies the <strong>server</strong> module as the main one. That main module, in turn, will call for the functionality in the other modules by &quot;requiring&quot; them.</p>
<p><strong>Important Question #2</strong>: How does a module share its code with other modules?</p>
<p>The <code>module</code> object has a special property, called <code>exports</code>, which is responsible for defining what a module makes available for other modules to use. In Node.js terminology, <code>module.exports</code> defines the values that the module exports. Remember that &quot;exporting&quot; is simply making objects or values available for other modules to import and use.</p>
<p>Therefore, we can export any value or function or other object we would like to export by attaching it as a property of the <code>module.exports</code> object. For example, if we would like to export a variable named <code>temperature</code>, we could make it available for use outside the module by simply adding it as a new property of <code>module.exports</code> as follows:</p>
<pre><code class="language-javascript">module.exports.temperature = temperature; 
</code></pre>
<h3 id="exportingandrequiringfunctionsandvariableswithmoduleexports">Exporting and Requiring Functions and Variables with module.exports</h3>
<p>Now that we have seen the conceptual meaning of a module, as well as why we use modules, let&apos;s put into practice these ideas by actually creating a module, defining functions, and then exporting those functions so that they can be used by other modules.</p>
<p>As an example, here is a new module that gives book recommendations. In the code below, I have defined a variable as well as a few functions. Later, we will access the functionality of these book recommendations from another module.</p>
<pre><code class="language-javascript">// book_recommendations.js

// stores the favorite author in a constant variable
const favoriteAuthor = { name: &quot;Ken Bruen&quot;, genre: &quot;Noir&quot;, nationality: &quot;Irish&quot; };

// returns the favorite book
function favoriteBook() {
    return { title: &quot;The Guards&quot;, author: &quot;Ken Bruen&quot; };
}
 
// returns a list of good books
function getBookRecommendations() {
    return [
        {id: 1, title: &quot;The Guards&quot;, author: &quot;Ken Bruen&quot;},
        {id: 2, title: &quot;The Stand&quot;, author: &quot;Steven King&quot;},
        {id: 3, title: &quot;The Postman Always Rings Twice&quot;, author: &quot;James M. Cain&quot;}
    ];
}
 
// exports the variables and functions above so that other modules can use them
module.exports.favoriteAuthor = favoriteAuthor;
module.exports.favoriteBook = favoriteBook;
module.exports.getBookRecommendations = getBookRecommendations;
</code></pre>
<p>We&apos;ve added all the variables and functions we would like to export to <code>module.exports</code> as properties of the object. We have just accomplished our goal of exporting these functions and variables from the <strong>book_recommendations</strong> module.</p>
<p>Now let&apos;s see how we would be able to import this module and access its functionality from another module.</p>
<p>In order to import the module, we need to use a special keyword used to import things, and it is called <a rel="nofollow" target="_blank" href="https://medium.freecodecamp.org/requiring-modules-in-node-js-everything-you-need-to-know-e7fbd119be8">require</a>. Where <code>module.exports</code> lets us set things for export, <code>require</code> lets us specify modules to be imported into the current module.</p>
<p>The functionality for importing modules is provided in a module named <code>require</code>, available on the global scope. This module&apos;s main export is a function to which we pass the path of the module we would like to import. For instance, to import a module defined in <code>music.js</code>, we would <code>require(&apos;./music&apos;)</code>, where we have specified the relative path.</p>
<p>Now we can see how easy it is to import anything using <code>require</code>. Going back to our <code>book_recommendations</code> module, we can import it and access the functions it exports. This is shown in the next code listing. This module prints a message describing recommended birthday gifts. It gets recommended books from the imported book recommendations module, and combines them with music recommendations.</p>
<p>Create a new module as shown below, then run it as shown earlier to see it using the functions defined in the imported <strong>book_recommendations</strong> module.</p>
<pre><code class="language-javascript">// birthday_gifts.js

// import the book recommendations module
let books = require(&apos;./book_recommendations&apos;);

// gets some music recommendations as well
let musicAlbums = [
    { artist: &quot;The Killers&quot;, title: &quot;Live From The Royal Albert Hall&quot; },
    { artist: &quot;Eminem&quot;, title: &quot;The Marshall Mathers LP&quot; }
];

// the two best items from each category
let topIdeas = function() {
    return [musicAlbums[0], books.favoriteBook()];
}
 
// outputs a message specifying the customer&apos;s recommended gifting items
let gifts = function() {
    console.log(&quot;Your recommended gifts are:\n&quot;);
    console.log(&quot;######MUSIC######&quot;);

    for (let i = 0, len = musicAlbums.length; i &lt; len; i++) {
        console.log(musicAlbums[i].title + &quot; by &quot; + musicAlbums[i].artist);
    }

    console.log(&quot;######BOOKS######&quot;);

    let recommendedBooks = books.getBookRecommendations();

    for (let i = 0, len = recommendedBooks.length; i &lt; len; i++) {
        console.log(recommendedBooks[i].title + &quot; by &quot; + recommendedBooks[i].author);
    }

    console.log(&quot;\n\nYours&quot;);
    console.log(&quot;Shop Staff\n*************&quot;);
    console.log(&quot;P.S. If you have a limited budget, you should just get the music album &quot; + topIdeas()[0].title + &quot; and the book &quot; + topIdeas()[1].title + &quot;.&quot;);
}

console.log(&quot;Welcome to our gift shop.\n&quot;);

// Get the gifts
gifts();
</code></pre>
<p>As you can see, we used <code>require</code> to import the <strong>book_recommendations</strong> module. Inside the new module, we could access variables and functions that had been exported by adding them to <code>module.exports</code>.</p>
<p>With both modules complete, invoking <code>node birthday_gifts.js</code> prints out a neat message with the customer&apos;s complete gift recommendations. You can see the output in the following image.</p>
<pre><code class="language-text">Welcome to our gift shop.

Your recommended gifts are:

######MUSIC######
Live From The Royal Albert Hall by The Killers
The Marshall Mathers LP by Eminem
######BOOKS######
The Guards by Ken Bruen
The Stand by Steven King
The Postman Always Rings Twice by James M. Cain


Yours
Shop Staff
*************
P.S. If you have a limited budget, you should just get the music album Live From The Royal Albert Hall and the book The Guards.
</code></pre>
<p>This pattern of composing Node.js programs from smaller modules is something you will often see, like with <a href="/how-to-write-express-js-middleware/">Express middleware</a>, for example.</p>
<h3 id="exportingandrequiringclasseswithmoduleexports">Exporting And Requiring Classes with module.exports</h3>
<p>In addition to functions and variables, we can also use <code>module.exports</code> to export other complex objects, such as classes. If you are not familiar with using classes or other Node.js fundamentals, you can take a look at our <a href="/learn-node-js-a-beginners-guide/">Node.js for beginners guide</a>.</p>
<p>In the following example, we create a Cat class which contains a name and age for Cat objects. Then we export the Cat class by attaching it as a property of the <code>module.exports</code> object. As you can see, this is not that different from how we exported functions and variables before.</p>
<pre><code class="language-javascript">// cat.js

// constructor function for the Cat class
function Cat(name) {
    this.age = 0;
    this.name = name;
}
 
// now we export the class, so other modules can create Cat objects
module.exports = {
    Cat: Cat
}
</code></pre>
<p>Now we can access this Cat class by importing the <code>cat</code> module. Once that&apos;s done, we can create new Cat objects and use them in the importing module as shown in the following example. Again, you should try running this code with <code>node cat_school.js</code> to see the names and ages of the new cats at your command prompt.</p>
<pre><code class="language-javascript">// cat_school.js

// import the cat module
let cats = require(&apos;./cat&apos;);
let Cat = cats.Cat;

// creates some cats
let cat1 = new Cat(&quot;Manny&quot;);
let cat2 = new Cat(&quot;Lizzie&quot;);

// Let&apos;s find out the names and ages of cats in the class
console.log(&quot;There are two cats in the class, &quot; + cat1.name + &quot; and &quot; + cat2.name + &quot;.&quot;);
console.log(&quot;Manny is &quot; + cat1.age + &quot; years old &quot; +  &quot; and Lizzie is &quot; + cat2.age + &quot; years old.&quot;);
</code></pre>
<p>As we just saw, exporting a class can be accomplished by attaching the class as a property of the <code>module.exports</code> object. First, we created a class using a constructor function. Then we exported the class using <code>module.exports</code>. To use the class, we then required it in another module, and then created instances of the class.</p>
<p>For an example exporting a class that was created with ES6 syntax, see the <code>Book</code> class below.</p>
<h3 id="analternativeusingtheshorthandexportsvsmoduleexports">An Alternative: Using the Shorthand exports VS module.exports</h3>
<p>While we can continue assigning things for export as properties of <code>module.exports</code>, there exists a shorthand way of exporting things from the module. This shorthand way involves using just <code>exports</code> instead of <code>module.exports</code>. There are some <a rel="nofollow" target="_blank" href="https://medium.freecodecamp.org/node-js-module-exports-vs-exports-ec7e254d63ac">differences between the two</a>. The key thing to notice here, however, is that you <strong>must</strong> assign your new values as properties of the shortcut <code>export</code> object, and not assign objects directly to overwrite the value of <code>export</code> itself.</p>
<p>Here is an example in which I use this shorthand way to export a couple of objects from a module named <strong>film_school</strong>.</p>
<pre><code class="language-javascript">// film_school.js

// a beginner film course
let film101 = {
    professor: &apos;Mr Caruthers&apos;,
    numberOfStudents: 20,
    level: &apos;easy&apos;
}
 
// an expert film course
let film102 = {
    professor: &apos;Mrs Duguid&apos;,
    numberOfStudents: 8,
    level: &apos;challenging&apos; 
}
 
// export the courses so other modules can use them
exports.film101 = film101;
exports.film102 = film102;
</code></pre>
<p>Notice how we are assigning the objects as, for example, <code>exports.film101 = ...</code> instead of <code>exports = film101</code>. That later assignment would not export the variable, but mess up your shortcut exports entirely.</p>
<p>The exporting done in the shorthand way above, could have been achieved in the long-form way we have used with <code>module.exports</code> using the following lines for the exporting.</p>
<pre><code class="language-javascript">// export the courses so other modules can use them
module.exports.film101 = film101;
module.exports.film102 = film102;
</code></pre>
<p>We could also export the two objects by assigning an object directly to <code>module.exports</code> but this would not work with <code>exports</code>.</p>
<pre><code class="language-javascript">// export the courses so other modules can use them
module.exports = {
    film101: film101,
    film102: film102
}
</code></pre>
<p>The two are very similar, and rightly so. These are two ways of achieving the same thing, but <code>exports</code> can trip you up if you assign an object to exports the way you would assign to <code>module.exports</code>.</p>
<h3 id="differencesbetweennodejsmodulesandes6modules">Differences Between Node.js Modules and ES6 Modules</h3>
<p>The modules used in Node.js follow a module specification known as the <a rel="nofollow" target="_blank" href="http://requirejs.org/docs/commonjs.html">CommonJS</a> specification. The recent updates to the JavaScript programming language, in the form of ES6, specify changes to the language, adding things like new class syntax and a <a rel="nofollow" target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import">module system</a>. This module system is different from Node.js modules. A module in ES6 looks like the following:</p>
<pre><code class="language-javascript">// book.js
const favoriteBook = {
    title: &quot;The Guards&quot;,
    author: &quot;Ken Bruen&quot;
}

// a Book class using ES6 class syntax
class Book {
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }

    describeBook() {
        let description = this.title + &quot; by &quot; + this.author + &quot;.&quot;;
        return description;
    }
}

// exporting looks different from Node.js but is almost as simple
export {favoriteBook, Book};
</code></pre>
<p>To import this module, we&apos;d use the ES6 <code>import</code> functionality, as follows.</p>
<pre><code class="language-javascript">// library.js

// import the book module
import {favoriteBook, Book} from &apos;book&apos;;

// create some books and get their descriptions
let booksILike = [
    new Book(&quot;Under The Dome&quot;, &quot;Steven King&quot;),
    new Book(&quot;Julius Ceasar&quot;, &quot;William Shakespeare&quot;)
];

console.log(&quot;My favorite book is &quot; + favoriteBook + &quot;.&quot;);
console.log(&quot;I also like &quot; + booksILike[0].describeBook() + &quot; and &quot; + booksILike[1].describeBook());
</code></pre>
<p>ES6 modules look almost as simple as the modules we have used in Node.js, but they are incompatible with Node.js modules. This has to do with the way modules are loaded differently between the two formats. If you use a compiler like <a rel="nofollow" target="_blank" href="https://babeljs.io/">Babel</a>, you can mix and match module formats. If you intend to code on the server alone with Node.js, however, you can stick to the module format for Node.js which we covered earlier.</p>
<h3 id="learnmore">Learn More</h3>
<p>Want to learn more about the fundamentals of Node.js? Personally, I&apos;d recommend an online course, like <a class="bos-link" href="https://stackabu.se/wes-bos-learn-node">Wes Bos&apos; Learn Node.js</a> since the videos are much easier to follow and you&apos;ll actually get to build a real-world application.</p>
<h3 id="conclusion">Conclusion</h3>
<p>The use of <code>module.exports</code> allows us to export values, objects and styles from Node.js modules. Coupled with the use of <code>require</code> to import other modules, we have a complete ecosystem for composing large programs out of smaller parts. When we combine a number of modules that take care of unique parts of functionality, we can create larger, more useful, but easy to maintain applications and software systems.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					