
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>We all know Node.js is great at handling lots of events asynchronously, but what a lot of people <em>don&apos;t</em> know is that all of this is done on a single thread. Node.js actually is not multi-threaded, so all of these requests are just being handled in the event loop of a single thread.</p>
<p>So why not get the most out of your quad-core processor by using a Node.js cluster? This will start up multiple instances of your code to handle even more requests. This may sound a bit difficult, but it is actually pretty easy to do with the <a rel="nofollow" target="_blank" href="https://nodejs.org/api/cluster.html">cluster</a> module, which was introduced in Node.js v0.8.</p>
<p>Obviously this is helpful to any app that can divide work between different processes, but it&apos;s especially important to apps that handle many IO requests, like a website.</p>
<p>Unfortunately, due to the complexities of parallel processing, clustering an application on a server isn&apos;t always straight-forward. What do you do when you need multiple processes to listen on the same port? Recall that only one process can access a port at any given time. The naive solution here is to configure each process to listen on a different port and then set up Nginx to <a rel="nofollow" target="_blank" href="https://www.nginx.com/resources/glossary/load-balancing/">load balance</a> requests between the ports.</p>
<p>This is a viable solution, but it requires a lot more work setting up and configuring each process, and not to mention configuring Nginx. With this solution you&apos;re just adding more things for yourself to manage.</p>
<p>Instead, you can fork the master process in to multiple child processes (typically having one child per processor). In this case, the children <em>are</em> allowed to share a port with the parent (thanks to inter-process communication, or <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Inter-process_communication">IPC</a>), so there is no need to worry about managing multiple ports.</p>
<p>This is exactly what the <code>cluster</code> module does for you.</p>
<h3 id="workingwiththeclustermodule">Working with The Cluster Module</h3>
<p>Clustering an app is extremely simple, especially for web server code like <a rel="nofollow" target="_blank" href="https://expressjs.com/">Express</a> projects. All you really need to do is this:</p>
<pre><code class="language-javascript">var cluster = require(&apos;cluster&apos;);
var express = require(&apos;express&apos;);
var numCPUs = require(&apos;os&apos;).cpus().length;

if (cluster.isMaster) {
    for (var i = 0; i &lt; numCPUs; i++) {
        // Create a worker
        cluster.fork();
    }
} else {
    // Workers share the TCP connection in this server
    var app = express();

    app.get(&apos;/&apos;, function (req, res) {
        res.send(&apos;Hello World!&apos;);
    });

    // All workers use this port
    app.listen(8080);
}
</code></pre>
<p>The functionality of the code is split up in to two parts, the master code and the worker code. This is done in the if-statement (<code>if (cluster.isMaster) {...}</code>). The master&apos;s only purpose here is to create all of the workers (the number of workers created is based on the number of CPUs available), and the workers are responsible for running separate instances of the Express server.</p>
<p>When a worker is forked off of the main process, it re-runs the code from the beginning of the module. When the worker gets to the if-statement, it returns <code>false</code> for <code>cluster.isMaster</code>, so instead it&apos;ll create the Express app, a route, and then listens on port <code>8080</code>. In the case of a quad-core processor, we&apos;d have four workers spawned, all listening on the same port for requests to come in.</p>
<p>But how are requests divided up between the workers? Obviously they can&apos;t (and shouldn&apos;t) all be listening and responding to every single request that we get. To handle this, there is actually an embedded load-balancer within the <code>cluster</code> module that handles distributing requests between the different workers. On Linux and OSX (but not Windows) the round-robin (<code>cluster.SCHED_RR</code>) policy is in effect by default. The only other scheduling option available is to leave it up to the operating system (<code>cluster.SCHED_NONE</code>), which is default on Windows.</p>
<p>The scheduling policy can be set either in <code>cluster.schedulingPolicy</code> or by setting it on the environment variable <code>NODE_CLUSTER_SCHED_POLICY</code> (with values of either &apos;rr&apos; or &apos;none&apos;).</p>
<p>You might also be wondering how different processes can be sharing a single port. The difficult part about running so many processes that handle network requests is that traditionally only one can have a port open at once. The big benefit of <code>cluster</code> is that it handles the port-sharing for you, so any ports you have open, like for a web-server, will be accessible for all children. This is done via IPC, which means the master just sends the port handle to each worker.</p>
<p>Thanks to features like this, clustering is super easy.</p>
<h3 id="clusterforkvschild_processfork">cluster.fork() vs child_process.fork()</h3>
<p>If you have prior experience with <a rel="nofollow" target="_blank" href="https://nodejs.org/api/child_process.html"><code>child_process</code></a>&apos;s <code>fork()</code> method then you may be thinking that <code>cluster.fork()</code> is somewhat similar (and they are, in many ways), so we&apos;ll explain some key differences about these two forking methods in this section.</p>
<p>There are a few main differences between <code>cluster.fork()</code> and <code>child_process.fork()</code>. The <code>child_process.fork()</code> method is a bit lower-level and requires you to pass the location (file path) of the module as an argument, plus other optional arguments like the current working directory, the user that owns the process, environment variables, and more.</p>
<p>Another difference is that <code>cluster</code> starts the worker execution from the beginning of the same module from which it ran. So if your app&apos;s entry point is <code>index.js</code>, but the worker is spawned in <code>cluster-my-app.js</code>, then it&apos;ll still start its execution from the beginning at <code>index.js</code>. <code>child_process</code> is different in that it spawns execution in whatever file is passed to it, and not necessarily the entry point of the given app.</p>
<p>You might have already guessed that the <code>cluster</code> module actually uses the <code>child_process</code> module underneath for creating the children, which is done with <code>child_process</code>&apos;s own <code>fork()</code> method, allowing them to communicate via IPC, which is how port handles are shared among workers.</p>
<p>To be clear, forking in Node is very different than a <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Fork_%28system_call%29">POISIX fork</a> in that it doesn&apos;t actually clone the current process, but it does start up a new V8 instance.</p>
<p>Although this is one of the easiest ways to multi-thread, it should be used with caution. Just because you&apos;re able to spawn 1,000 workers doesn&apos;t mean you should. Each worker takes up system resources, so only spawn those that are really needed. The Node docs state that since each child process is a new V8 instance, you need to expect a 30ms startup time for each and at least 10mb of memory per instance.</p>
<h3 id="errorhandling">Error Handling</h3>
<p>So what do you do when one (or more!) of your workers die? The whole point of clustering is basically lost if you can&apos;t restart workers after they crash. Lucky for you the <code>cluster</code> module extends <code>EventEmitter</code> and provides an &apos;exit&apos; event, which tells you when one of your worker children dies.</p>
<p>You can use this to log the event and restart the process:</p>
<pre><code class="language-javascript">cluster.on(&apos;exit&apos;, function(worker, code, signal) {
    console.log(&apos;Worker %d died with code/signal %s. Restarting worker...&apos;, worker.process.pid, signal || code);
    cluster.fork();
});
</code></pre>
<p>Now, after just 4 lines of code, it&apos;s like you have your own internal process manager!</p>
<h3 id="performancecomparisons">Performance Comparisons</h3>
<p>Okay, now to the interesting part. Let&apos;s see how much clustering actually helps us.</p>
<p>For this experiment, I set up a web-app similar to the example code I showed above. But the biggest difference is that we&apos;re simulating work being done within the Express route by using the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/sleep">sleep</a> module and by returning a bunch of random data to the user.</p>
<p>Here is that same web-app, but with clustering:</p>
<pre><code class="language-javascript">var cluster = require(&apos;cluster&apos;);
var crypto = require(&apos;crypto&apos;);
var express = require(&apos;express&apos;);
var sleep = require(&apos;sleep&apos;);
var numCPUs = require(&apos;os&apos;).cpus().length;

if (cluster.isMaster) {
    for (var i = 0; i &lt; numCPUs; i++) {
        // Create a worker
        cluster.fork();
    }
} else {
    // Workers share the TCP connection in this server
    var app = express();

    app.get(&apos;/&apos;, function (req, res) {
        // Simulate route processing delay
        var randSleep = Math.round(10000 + (Math.random() * 10000));
        sleep.usleep(randSleep);

        var numChars = Math.round(5000 + (Math.random() * 5000));
        var randChars = crypto.randomBytes(numChars).toString(&apos;hex&apos;);
        res.send(randChars);
    });

    // All workers use this port
    app.listen(8080);
}
</code></pre>
<p>And here is the &apos;control&apos; code from which we&apos;ll make our comparisons. It is essentially the same exact thing, just without <code>cluster.fork()</code>:</p>
<pre><code class="language-javascript">var crypto = require(&apos;crypto&apos;);
var express = require(&apos;express&apos;);
var sleep = require(&apos;sleep&apos;);

var app = express();

app.get(&apos;/&apos;, function (req, res) {
    // Simulate route processing delay
    var randSleep = Math.round(10000 + (Math.random() * 10000));
    sleep.usleep(randSleep);

    var numChars = Math.round(5000 + (Math.random() * 5000));
    var randChars = crypto.randomBytes(numChars).toString(&apos;hex&apos;);
    res.send(randChars);
});

app.listen(8080);
</code></pre>
<p>To simulate a heavy user load, we&apos;ll be using a command line tool called <a rel="nofollow" target="_blank" href="https://github.com/JoeDog/siege">Siege</a>, which we can use to make a bunch of simultaneous requests to the URL of our choice.</p>
<p>Siege is also nice in that it tracks performance metrics, like availability, throughput, and the rate of requests handled.</p>
<p>Here is the Siege command we&apos;ll be using for the tests:</p>
<pre><code class="language-bash">$ siege -c100 -t60s http://localhost:8080/
</code></pre>
<p>After running this command for both versions of the app, here are some of the more interesting results:</p>
<table class="table">
  <tbody><tr>
    <th>Type</th>
    <th>Total Requests Handled</th>
    <th>Requests/second</th>
    <th>Average response time</th>
    <th>Throughput</th>
  </tr>
  <tr>
    <td>No clustering</td>
    <td>3467</td>
    <td>58.69</td>
    <td>1.18 secs</td>
    <td>0.84 MB/sec</td>
  </tr>
  <tr>
    <td>Clustering (4 processes)</td>
    <td>11146</td>
    <td>188.72</td>
    <td>0.03 secs</td>
    <td>2.70 MB/sec</td>
  </tr>
</tbody></table>
<p>As you can see, the clustered app has around a 3.2x improvement over the single-process app for just about all of the metrics listed, except for average response time, which has a much more significant improvement.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					