
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>These days our web applications tend to have a lot of integrations with other services, whether it be interacting with a REST service like Twitter, or downloading images from Flickr. Using Node/JavaScript is one of the most popular languages to handle applications like this. Either way, you&apos;ll be making a lot of HTTP requests, which means you&apos;ll need a solid module to make writing the code much more bearable.</p>
<p>The <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/request">request</a> module is by far the most popular (non-standard) Node package for making HTTP requests. Actually, it is really just a wrapper around Node&apos;s built in <a rel="nofollow" target="_blank" href="https://nodejs.org/api/http.html">http</a> module, so you can achieve all of the same functionality on your own with <code>http</code>, but <code>request</code> just makes it a whole lot easier.</p>
<h3 id="makinghttprequests">Making HTTP Requests</h3>
<p>While there are quite a few options available to you in <code>request</code> (many of which we&apos;ll cover throughout this article), it can be pretty simple to use as well. The &quot;hello world&quot; example for this library is as easy as passing a URL and a callback:</p>
<pre><code class="language-javascript">const request = require(&apos;request&apos;);

request(&apos;http://stackabuse.com&apos;, function(err, res, body) {
    console.log(body);
});
</code></pre>
<p>The code above submits an HTTP GET request to stackabuse.com and then prints the returned HTML to the screen. This type of request works for any HTTP endpoint, whether it returns HTML, JSON, an image, or just about anything else.</p>
<p>The first argument to <code>request</code> can either be a URL string, or an object of options. Here are some of the more common options you&apos;ll encounter in your applications:</p>
<ul>
<li><code>url</code>: The destination URL of the HTTP request</li>
<li><code>method</code>: The HTTP method to be used (GET, POST, DELETE, etc)</li>
<li><code>headers</code>: An object of HTTP headers (key-value) to be set in the request</li>
<li><code>form</code>: An object containing key-value form data</li>
</ul>
<pre><code class="language-javascript">const request = require(&apos;request&apos;);

const options = {
    url: &apos;https://www.reddit.com/r/funny.json&apos;,
    method: &apos;GET&apos;,
    headers: {
        &apos;Accept&apos;: &apos;application/json&apos;,
        &apos;Accept-Charset&apos;: &apos;utf-8&apos;,
        &apos;User-Agent&apos;: &apos;my-reddit-client&apos;
    }
};

request(options, function(err, res, body) {
    let json = JSON.parse(body);
    console.log(json);
});
</code></pre>
<p>Using the <code>options</code> object, this request uses the GET method to retrieve JSON data directly from Reddit, which is returned as a string in the <code>body</code> field. From here, you can use <code>JSON.parse</code> and use the data as a normal JavaScript object.</p>
<p>This same request format can be used for any <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods">type of HTTP method</a>, whether it&apos;s DELETE, PUT, POST, or OPTIONS. Although, not all methods are used exactly the same. Some, like the POST method, can include data within the request. There are a few ways this data can be sent, some of which are:</p>
<ul>
<li><code>body</code>: A <code>Buffer</code>, <code>String</code>, or <code>Stream</code> object (can be an object if <code>json</code> option is set to <code>true</code>)</li>
<li><code>form</code>: An object of key-value pair data (we&apos;ll go over this later)</li>
<li><code>multipart</code>: An array of objects that can contain their own headers and body attributes</li>
</ul>
<p>Each fulfills a different need (and there are even more ways to send data, which can be found in <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/request#requestoptions-callback">this section of request&apos;s README</a>). The <code>request</code> module does contain some convenience methods that make these a bit easier to work with, however, so be sure to read the full docs to avoid making your code more difficult than it has to be.</p>
<p>Speaking of helper methods, a much more succinct way of calling the different HTTP methods is to use the respective helper methods provided. Here are a few of the more commonly used ones:</p>
<ul>
<li><code>request.get(options, callback)</code></li>
<li><code>request.post(options, callback)</code></li>
<li><code>request.head(options, callback)</code></li>
<li><code>request.delete(options, callback)</code></li>
</ul>
<p>While this won&apos;t save you a ton of lines of code, it will at least make your code a bit easier to understand by allowing you to just look at the method being called and not having to parse through all of the various options to find it.</p>
<h3 id="forms">Forms</h3>
<p>Whether you&apos;re interfacing with a REST API or creating a bot to crawl and submit data on websites, at some point you&apos;ll need to submit data for a form. As always with <code>request</code>, this is can be done a few different ways, depending on your needs.</p>
<p>For regular forms (URL-encoded, with a <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Media_type">MIME</a> type of <code>application/x-www-form-urlencoded</code>), you&apos;re best off using the <code>.post()</code> convenience method with the form object:</p>
<pre><code class="language-javascript">let options = {
    url: &apos;http://http://mockbin.com/request&apos;,
    form: {
        email: &apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="c1aca481a4b9a0acb1ada4efa2aeac">[email&#xA0;protected]</a>&apos;,
        password: &apos;myPassword&apos;
    }
};

request.post(options, callback);
</code></pre>
<p>This will upload data just like an HTML form would, with the only limitation being that you can&apos;t upload files this way. In order to do that, you need to use the <code>formData</code> option instead, which uses the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/form-data">form-data</a> library underneath.</p>
<p>Using <code>formData</code> instead, we can now pass file data to the server via <code>Buffer</code>s, <code>Stream</code>s, or even non-file data (as before) with simple key-value pairs.</p>
<pre><code class="language-javascript">let formData = {
    // Pass single file with a key
    profile_pic: fs.createReadStream(__dirname + &apos;/me.jpg&apos;),

    // Pass multiple files in an array
    attachments: [
        fs.readFileSync(__dirname + &apos;/cover-letter.docx&apos;),  // Buffer
        fs.createReadStream(__dirname + &apos;/resume.docx&apos;),    // Stream
    ],

    // Pass extra meta-data with your files
    detailed_file: {
        value: fs.createReadStream(__dirname + &apos;/my-special-file.txt&apos;),
        options: {
            filename: &apos;data.json&apos;,
            contentType: &apos;application/json&apos;
        }
    },

    // Simple key-value pairs
    username: &apos;ScottWRobinson&apos;
};

request.post(&apos;http://http://mockbin.com/request&apos;, {formData: formData}, callback);
</code></pre>
<p>This will send your files with a MIME type of <code>multipart/form-data</code>, which is a multipart form upload.</p>
<p>While this will be more than sufficient for most users&apos; use-cases, there are times where you need even more fine-grained control, like pre/post CLRFs (new-lines), chunking, or specifying your own multiparts. For more info on these extra options, check out <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/request#multipartrelated">this section of the <code>request</code> README</a>.</p>
<h3 id="streams">Streams</h3>
<p>One of the most under-used features in many programming languages, in my opinion, are streams. Their usefulness extends beyond just network requests, but this serves as a perfect example as to why you should use them. For a short description on how and why you should use them, check out the &quot;Streams&quot; section of the <a href="https://stackabuse.com/node-http-servers-for-static-file-serving/#streams">Node HTTP Servers for Static File Serving</a> article.</p>
<p>In short, using streams for large amounts of data (like files) can help reduce your app&apos;s memory footprint and response time. To make this easier to use, each of the <code>request</code> methods can <code>pipe</code> their output to another stream.</p>
<p>In this example, we download the Node.js logo using a GET request and stream it to a local file:</p>
<pre><code class="language-javascript">let fileStream = fs.createWriteStream(&apos;node.png&apos;);
request(&apos;https://nodejs.org/static/images/logos/nodejs-new-white-pantone.png&apos;).pipe(fileStream);
</code></pre>
<p>As soon as the HTTP request starts returning parts of the downloaded image, it will &apos;pipe&apos; that data directly to the file &apos;node.png&apos;.</p>
<p>Downloading a file this way has some other benefits as well. Streams are great for applying transformations on data as it is downloaded. So, for example, let&apos;s say you&apos;re downloading a large amount of sensitive data with <code>request</code> that needs to be encrypted immediately. To do this, you can apply an encryption transform by piping the output of <code>request</code> to <a rel="nofollow" target="_blank" href="https://nodejs.org/api/crypto.html#crypto_crypto_createcipher_algorithm_password">crypto.createCipher</a>:</p>
<pre><code class="language-javascript">let url = &apos;http://example.com/super-sensitive-data.json&apos;;
let pwd = new Buffer(&apos;myPassword&apos;);

let aesTransform = crypto.createCipher(&apos;aes-256-cbc&apos;, pwd);
let fileStream = fs.createWriteStream(&apos;encrypted.json&apos;);

request(url)
    .pipe(aesTransform)     // Encrypts with aes256
    .pipe(fileStream)       // Write encrypted data to a file
    .on(&apos;finish&apos;, function() {
        console.log(&apos;Done downloading, encrypting, and saving!&apos;);
    });
</code></pre>
<p>It&apos;s easy to overlook streams, and many people do when they&apos;re writing code, but they can help out your performance quite a bit, especially with a library like <code>request</code>.</p>
<h3 id="miscconfigurations">Misc. Configurations</h3>
<p>There is a lot more to HTTP requests than just specifying a URL and downloading the data. For larger applications, and especially those that have to support a wider range of environments, your requests may need to handle quite a few configuration parameters, like proxies or special SSL trust certificates.</p>
<p>One important misc. feature to point out is the <code>request.defaults()</code> method, which lets you specify default parameters so you don&apos;t have to give them for every request you make.</p>
<pre><code class="language-javascript">let req = request.defaults({
    headers: {
        &apos;x-access-token&apos;: &apos;123abc&apos;,
        &apos;User-Agent&apos;: &apos;my-reddit-client&apos;
    }
});

req(&apos;http://your-api.com&apos;, function(err, res, body) {
    console.log(body);
});
</code></pre>
<p>Now, in the example above, all requests made with <code>req</code> will always have the headers <code>x-access-token</code> and <code>User-Agent</code> set. This is ideal for setting headers like these, proxy servers, or TLS/SSL configurations.</p>
<p>Throughout the rest of this section, we&apos;ll take a look at some more common features you&apos;ll come across:</p>
<h5 id="proxies">Proxies</h5>
<p>Whether your computer is behind a corporate proxy or you want to redirect your traffic to another country, at some point you may need to specify a proxy address. The simplest way to achieve this is to use the <code>proxy</code> option, which takes an address in which the traffic is proxied through:</p>
<pre><code class="language-javascript">let options = {
    url: &apos;https://www.google.com&apos;,
    proxy: &apos;http://myproxy.com&apos;
};

request(options, callback);
</code></pre>
<p>The <code>options</code> object is one way to specify a proxy, but <code>request</code> also uses the following environment variables to configure a proxy connection:</p>
<ul>
<li>HTTP_PROXY / http_proxy</li>
<li>HTTPS_PROXY / https_proxy</li>
<li>NO_PROXY / no_proxy</li>
</ul>
<p>This gives you quite a bit more control, like setting which sites <em>shouldn&apos;t</em> be proxied via the <code>NO_PROXY</code> variable.</p>
<h5 id="tlsssl">TLS/SSL</h5>
<p>Sometimes an API needs to have some extra security and therefore requires a client certificate. This is actually fairly common with private corporate APIs, so it&apos;s worth knowing how to do this.</p>
<p>Another possible scenario is that you want your HTTP requests to explicitly trust certain <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Certificate_authority">certificate authorities</a>, which could include certificates self-signed by you or your company.</p>
<p>As with all of the other configurations we&apos;ve seen so far, these are set in the <code>options</code> object:</p>
<pre><code class="language-javascript">const fs = require(&apos;fs&apos;);
const request = require(&apos;request&apos;);

let myCertFile = fs.readFileSync(__dirname + &apos;/ssl/client.crt&apos;)
let myKeyFile = fs.readFileSync(__dirname + &apos;/ssl/client.key&apos;)
let myCaFile = fs.readFileSync(__dirname + &apos;/ssl/ca.cert.pem&apos;)
 
var options = {
    url: &apos;https://mockbin.com/request&apos;,
    cert: myCertFile,
    key: myKeyFile,
    passphrase: &apos;myPassword&apos;,
    ca: myCaFile
};
 
request.get(options);
</code></pre>
<h5 id="basicauthentication">Basic Authentication</h5>
<p>Sites that use <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Basic_access_authentication">basic access authentication</a> can still be accessed using the <code>auth</code> option:</p>
<pre><code class="language-javascript">const request = require(&apos;request&apos;);
 
var options = {
    url: &apos;https://mockbin.com/request&apos;,
    auth: {
        username: &apos;ScottWRobinson&apos;,
        password: &apos;myPassword&apos;
    }
};
 
request.get(options);
</code></pre>
<p>This option sets one of the HTTP headers as <code>&quot;authorization&quot;: &quot;Basic c2NvdHQ6cGFzc3dvcmQh&quot;</code>. The &apos;Basic&apos; string in the &apos;authorization&apos; header declares this to be a Basic Auth request and the alphanumeric string that follows is a RFC2045-MIME encoding (a variant of Base64) of our username and password.</p>
<h5 id="redirects">Redirects</h5>
<p>I&apos;ve found that in some applications, like web scraping, there are quite a few cases where you need to follow redirects in order for your request to be successful. As you probably guessed, there is an option to specify whether to follow redirects by default, but <code>request</code> goes even one step further and will let you provide a function that can be used to conditionally determine if the redirect should be followed.</p>
<p>A few of the redirect options are:</p>
<ul>
<li><code>followRedirect</code>: If <code>true</code>, then follow all HTTP 3xx redirects. Or submit a <code>function(res) {}</code> that is used to determine whether or not to follow the redirect</li>
<li><code>followAllRedirects</code>: Follow all non-GET HTTP 3xx redirects</li>
<li><code>maxRedirects</code>: The maximum number of times to follow chained redirects (defaults to 10)</li>
</ul>
<h3 id="conclusion">Conclusion</h3>
<p>No doubt <code>request</code> is a powerful module, and likely one that you&apos;ll use often. Given all of the features it provides, it can act as a great starting point for anything from a web crawler to a client library for your API.</p>
<p>There are quite a few more options and configurations that can be used with <code>request</code> than what we&apos;ve shown here, so be sure to check out the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/request">documentation</a> for more details. Keep in mind that not everything in the module is documented, so you may need to do some more searching/experimenting to find your answer.</p>
<p><em>Have you used <code>request</code> in any of your projects? If so, how?</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					