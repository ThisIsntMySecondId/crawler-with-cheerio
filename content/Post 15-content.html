
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>Chances are you&apos;ve heard of Hapi by now. And you might be wondering how it compares to the Express web framework in Node.js development. In this article, we will compare the frameworks head-to-head and explore the differences in experience for the developer.</p>
<h3 id="similaritiesanddifferencesbetweenhapiandexpress">Similarities and Differences Between Hapi and Express</h3>
<p>Both <a rel="nofollow" target="_blank" href="https://expressjs.com/">Express</a> and <a rel="nofollow" target="_blank" href="https://hapijs.com/">Hapi</a> aim to be highly flexible, simple, and extensible. This similarity means that both have easy-to-use APIs, they&apos;re highly modular, and can support your application as it grows potentially very large.</p>
<p>The learning curve for these frameworks, since they are quite straightforward, is low, unlike a more opinionated framework like Meteor. If you are coming from using Express you should be able to quickly pick up Hapi, and vice versa.</p>
<p>There are, however, some philosophical differences between the two frameworks, which we&apos;ll describe throughout this article.</p>
<p>Hapi.js includes more &quot;batteries&quot; by default than Express does. For instance, when parsing payload from forms to via &quot;POST&quot; requests, with Express, you have to include the <code>body-parser</code> <a href="/how-to-write-express-js-middleware/">middleware</a>. You then use that to parse the POST payload and use the data from the user. On the other hand, with Hapi, you need no middleware. The payload is parsed for you by the framework itself, and you can access the payload directly on the request object. Little conveniences like that are replete in Hapi.</p>
<h3 id="basicserverandrouting">Basic Server and Routing</h3>
<p>To get started we will create <strong>hapiapp</strong>, an example Hapi app that shows basic Hapi functionality. We will also create <strong>expressapp</strong>, a mirror image of hapiapp that uses Express so we can compare the frameworks side by side.</p>
<p>Both of our apps will use ES6 JavaScript.</p>
<p>Create two directories, <em>hapiapp</em> and <em>expressapp</em>.</p>
<p>Inside each, run the command:</p>
<pre><code class="language-sh">$ npm init
</code></pre>
<p>Then accept all the defaults by pressing &quot;Enter&quot;. This creates a package.json file inside each directory, thus creating our two different apps, <em>hapiapp</em> and <em>expressapp</em>.</p>
<p>First, let&apos;s see basic routing in action in Hapi.js. Inside the <em>hapiapp</em> directory, install the <em>Hapi</em> module by running the following command:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="8ee6effee7cebfb8a0baa0bd">[email&#xA0;protected]</a> --save
</code></pre>
<p>Then create a file index.js with the following contents:</p>
<pre><code class="language-javascript">// hapiapp/index.js

const Hapi = require(&apos;hapi&apos;);

// create our server
const server = new Hapi.Server();

// configure the port
server.connection({
    port: 8000,
    host: &apos;localhost&apos;
});

// basic routes
server.route({
    method: &apos;GET&apos;,
    path: &apos;/&apos;,
    handler: (request, reply) =&gt; reply(&apos;Hello World&apos;)
});

server.route({
    method: &apos;GET&apos;,
    path: &apos;/hello/{name}&apos;,
    handler: (request, reply) =&gt; reply(`Hello ${request.params.name}`)
});

// start the server
server.start((err) =&gt; {
    if (err) {
        console.error(err);
    }

    console.log(&apos;App running on port 8000...&apos;);
});
</code></pre>
<p>This basic app creates two routes at <code>localhost:8000</code> and <code>localhost:8000/hello/&lt;name&gt;</code>. The first one will print out a simple &quot;Hello World&quot; to the user. The second, however, is a dynamic route that prints out &quot;Hello&quot; followed by the name we input in the browser after the second &quot;/&quot; slash.</p>
<p>From the <em>hapiapp</em> directory run the app using the following command and try out the URLs given above:</p>
<pre><code class="language-sh">$ node index.js
</code></pre>
<p>The equivalent Express app involves a very similar setup. Inside our <em>expressapp</em> directory, install Express as a dependency using the command:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="2c49545c5e495f5f6c18021d1a021e">[email&#xA0;protected]</a> --save
</code></pre>
<p>Then create a file index.js with the following contents:</p>
<pre><code class="language-javascript">// expressapp/index.js

const express = require(&apos;express&apos;);

// create our app
const app = express();

// basic routes
app.get(&apos;/&apos;, (req, res) =&gt; res.send(&apos;Hello World!&apos;));
app.get(&apos;/hello/:name&apos;, (req, res) =&gt; res.send(`Hello ${req.params.name}`));

// start the server
app.listen(3000, () =&gt; console.log(&apos;App running on port 3000...&apos;));
</code></pre>
<p>This accomplishes the same functionality as the Hapi app. To run it, from inside our <em>expressapp</em> directory, run the command:</p>
<pre><code class="language-sh">$ node index.js
</code></pre>
<p>Visit the routes given above, except now we are running at <code>localhost:3000</code> instead of <code>localhost:8000</code>.</p>
<p>Both code examples have a straightforward simplicity. The server setup is remarkably similar, though Express looks more compact here.</p>
<h3 id="workingwithmiddleware">Working with Middleware</h3>
<p>&quot;Middleware&quot; is the name given to software modules that work on HTTP requests in succession before the final output is returned to the user in a response.</p>
<p>Middleware can work as functions which we define inside the application, or functions defined in third-party middleware libraries.</p>
<p>Express lets you attach middleware to handle each request. Hapi, however, works with plugins that provide middleware functionality.</p>
<p>An example of middleware is CSRF tokens, which help prevent Cross-Site Request Forgery (CSRF) hacking attacks. Without CSRF tokens, attackers can impersonate legitimate requests and steal data or run malicious code on your applications.</p>
<p>The approaches taken by Hapi and Express to deal with CSRF attacks is similar. It involves generating a secret token on the server for each form that submits data back to the server. Then the server checks each POST request for the correct, difficult-to-forge token. If it is absent, the server rejects the request, thus avoiding malicious code execution attempts. In this case the middleware will generate a CSRF token for each request so you don&apos;t have to do it within your application code.</p>
<p>The difference between the two frameworks here is mainly cosmetic, with Hapi using a plugin, <a rel="nofollow" target="_blank" href="https://github.com/hapijs/crumb">Crumb</a>, for the token generation and processing. Express, on the other hand, uses a middleware known as <a rel="nofollow" target="_blank" href="https://github.com/expressjs/csurf">csurf</a> to generate and process CSRF tokens.</p>
<p>Here is a Hapi code example generating <em>Crumb</em> tokens:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const Hapi = require(&apos;hapi&apos;);
const Vision = require(&apos;vision&apos;);

const server = new Hapi.Server({
    host: &apos;127.0.0.1&apos;,
    port: 8000
});

const plugins = [
    Vision,
    {
        plugin: require(&apos;../&apos;),
        options: {
            restful: true
        }
    }
];

// Add Crumb plugin
(async () =&gt; {
    await server.register(plugins);

    server.route([
        // a &quot;crumb&quot; cookie should be set with any request
        // for cross-origin requests, set CORS &quot;credentials&quot; to true
        // a route returning the crumb can be created like this

        {
            method: &apos;GET&apos;,
            path: &apos;/generate&apos;,
            handler: function (request, h) {

                return {
                    crumb: server.plugins.crumb.generate(request, h)
                };
            }
        },

        // request header &quot;X-CSRF-Token&quot; with crumb value must be set in request for this route

        {
            method: &apos;PUT&apos;,
            path: &apos;/crumbed&apos;,
            handler: function (request, h) {

                return &apos;Crumb route&apos;;
            }
        }
    ]);

    await server.start();

    console.log(&apos;Example restful server running at:&apos;, server.info.uri);
})();
</code></pre>
<p>And the roughly equivalent Express code using <em>csurf</em>.</p>
<pre><code class="language-javascript">const cookieParser = require(&apos;cookie-parser&apos;)
const csrf = require(&apos;csurf&apos;)
const bodyParser = require(&apos;body-parser&apos;)
const express = require(&apos;express&apos;)

// setup route middlewares
let csrfProtection = csrf({ cookie: true })
let parseForm = bodyParser.urlencoded({ extended: false })

// create express app
let app = express()

// parse cookies
// we need this because &quot;cookie&quot; is true in csrfProtection
app.use(cookieParser())

app.get(&apos;/form&apos;, csrfProtection, function (req, res) {
    // pass the csrfToken to the view
    res.render(&apos;send&apos;, { csrfToken: req.csrfToken() })
});

app.post(&apos;/process&apos;, parseForm, csrfProtection, function (req, res) {
    res.send(&apos;data is being processed&apos;)
});
</code></pre>
<p>Middleware libraries can also help you with implementations of other common functionality such as authentication. You can find a list of some of the Express middleware at this <a rel="nofollow" target="_blank" href="https://expressjs.com/en/resources/middleware.html">middleware directory</a>. For Hapi, there is a comparable <a rel="nofollow" target="_blank" href="https://hapijs.com/plugins">library of Hapi plugins</a>.</p>
<h3 id="servingimagesandstaticassets">Serving Images and Static Assets</h3>
<p>Serving static files is remarkably similar between Hapi and Express. Hapi uses the <a rel="nofollow" target="_blank" href="https://github.com/hapijs/inert">Inert</a> plugin, whereas Express uses the <a rel="nofollow" target="_blank" href="https://expressjs.com/en/starter/static-files.html">express.static</a> built-in middleware.</p>
<p>In our <em>hapiapp</em> application, we can serve static files from a <em>public</em> directory by creating a directory at <code>hapiapp/public</code> and the placing in it a file <em>runsagainstbulls.jpg</em>.</p>
<p>Here is the image file. Right click and download it into your application.</p>
<img alt="Example image" src="https://s3.amazonaws.com/stackabuse/media/hapi-vs-express-comparing-node-js-frameworks-1.jpg" class="img-responsive">
<p>You&apos;ll also want to install <em><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e980878c9b9da9ddc7dbc7d8">[email&#xA0;protected]</a></em> using:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="88e1e6edfafcc8bca6baa6b9">[email&#xA0;protected]</a> --save
</code></pre>
<p>Now we can serve the static file using the following code. Add this before the lines that start the server in <code>hapiapp/index.js</code>.</p>
<pre><code class="language-javascript">// hapiapp/index.js

...

// serve static files using inert
// require inert for this route
server.register(require(&apos;inert&apos;), (err) =&gt; {
    if (err) {
        throw err;
    }

    // serve the runswithbulls image from the public folder
    server.route({
        method: &apos;GET&apos;,
        path: &apos;/image&apos;,
        handler: (request, reply) =&gt; {
            reply.file(&apos;./public/runsagainstbulls.jpg&apos;);
        }
    });
});
</code></pre>
<p>Run the server again and visit <code>localhost:8000/image</code> and you should see the image served back to you.</p>
<img alt="Serving static files" src="https://s3.amazonaws.com/stackabuse/media/hapi-vs-express-comparing-node-js-frameworks-2.png" class="img-responsive">
<p>In Express, there is a bit less setup. Just copy the same image to a newly-created <em>public</em> folder inside <em>expressapp</em>. Then add this line to your index.js file before the line that starts the server:</p>
<pre><code class="language-javascript">// expressapp/index.js

...

// serve static files from the public folder
app.use(express.static(&apos;public&apos;));
</code></pre>
<p>Start your server and visit <code>localhost:3000/runsagainstbulls.jpg</code> and you should see the image being served by Express.</p>
<img alt="Serving static in Express" src="https://s3.amazonaws.com/stackabuse/media/hapi-vs-express-comparing-node-js-frameworks-3.png" class="img-responsive">
<p>By default Express will serve all files in the configured folder, and Hapi can be configured to do the same with Inert, although it is not the default setting.</p>
<h3 id="usingtemplateengines">Using Template Engines</h3>
<p>Both Hapi and Express can render and serve templates like using engines like Handlebars, Twig, EJS and others.</p>
<p>In our <em>hapiapp</em> folder, install the <a rel="nofollow" target="_blank" href="https://github.com/hapijs/vision">vision plugin</a> for rendering support with:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="11677862787e7f51253f203f20">[email&#xA0;protected]</a> --save
</code></pre>
<p>Also install <a rel="nofollow" target="_blank" href="http://handlebarsjs.com/">Handlebars</a> template engine with:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="137b727d777f767172616053273d233d2223">[email&#xA0;protected]</a> --save
</code></pre>
<p>Then let&apos;s create a Handlebars template in a new directory <em>templates</em>, and a new file in that directory, named <em>cats.html</em>. We&apos;re going to make a listing of cats, and display them using this template.</p>
<p>Update <em>cats.html</em> as follows:</p>
<pre><code class="language-html">&lt;!-- hapiapp/templates/cats.html --&gt;
&lt;h2&gt;All My Cats&lt;/h2&gt;

&lt;ol&gt;
    {{#each cats}}
        &lt;li&gt;{{name}}&lt;/li&gt;
    {{/each}}
&lt;/ol&gt;
</code></pre>
<p>This Handlebars template loops through a collection of cat objects, and renders the name of each cat.</p>
<p>Inside index.js, before starting the server, add the following code, which configures vision and creates a route at <code>/cats</code>, and supplies a list of cats to be displayed there.</p>
<pre><code class="language-javascript">// hapiapp/index.js

...

// configure vision to render Handlebars templates
server.register(require(&apos;vision&apos;), (err) =&gt; {
    if (err) {
        throw err;
    }

    server.views({
        engines: {
            html: require(&apos;handlebars&apos;),
        },
        path: __dirname + &apos;/templates&apos;
    });
});

// display cats at /cats using the cats handlebars template
server.route({
    method: &apos;GET&apos;,
    path: &apos;/cats&apos;,
    handler: (request, reply) =&gt; {

        reply.view(&apos;cats&apos;, {
            cats: [
                {name: &quot;Blinky the Cat&quot;},
                {name: &quot;Sammy the Happy Cat&quot;},
                {name: &quot;Eto the Thug Cat&quot;},
                {name: &quot;Liz a quiet cat&quot;},
            ]
        });
    }
});
</code></pre>
<p>Start the server again and visit <code>localhost:8000/cats</code>, and you should see a list of cats displayed on the page.</p>
<img alt="List of Cats" src="https://s3.amazonaws.com/stackabuse/media/hapi-vs-express-comparing-node-js-frameworks-4.png" class="img-responsive">
<p>To replicate this functionality in Express, first create a directory named <em>views</em> inside the <em>expressapp</em> root directory. Then create a Handlebars file <em>cats.hbs</em> with contents similar to what we had for Hapi.</p>
<pre><code class="language-html">&lt;!-- expressapp/views/cats.hbs --&gt;
&lt;h2&gt;All My Cats&lt;/h2&gt;

&lt;ol&gt;
    {{#each cats}}
        &lt;li&gt;{{name}}&lt;/li&gt;
    {{/each}}
&lt;/ol&gt;
</code></pre>
<p>Now, back in <em>expressapp</em> install the Handlebars engine for Express with:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="4d252f3e0d79637d637c">[email&#xA0;protected]</a> --save
</code></pre>
<p>Then to render the template and display our cats, update index.js, adding the following code just before the part that starts the server:</p>
<pre><code class="language-javascript">// expressapp/index.js

...

// use handlebars as the view engine
app.set(&apos;view engine&apos;, &apos;hbs&apos;);

// display a list of cats at /cats
app.get(&apos;/cats&apos;, function (req, res) {

    res.render(&apos;cats&apos;, {
        cats: [
            {name: &quot;Blinky the Cat&quot;},
            {name: &quot;Sammy the Happy Cat&quot;},
            {name: &quot;Eto the Thug Cat&quot;},
            {name: &quot;Liz a quiet cat&quot;},
        ]
    });
});
</code></pre>
<p>Restart the Express server, and visit <code>localhost:3000/cats</code>, and we should see the list of cats displayed as it was for Hapi.</p>
<p>You will notice that the Hapi example needed a bit more configuration than the Express example. Both code examples are quite simple to follow, however. In a small app like this one, there is little to choose between the two frameworks. In a larger application, however, there will be more noticeable differences. For example, Hapi has a reputation for involving more boilerplate code than Express. On the other hand, it also does a bit more for you out of the box than Express, so there are definitely trade-offs.</p>
<h3 id="connectingtoadatabase">Connecting to a Database</h3>
<p>MongoDB is a battle-tested NoSQL database to use in your applications. With the <a rel="nofollow" target="_blank" href="http://mongoosejs.com/index.html">mongoose</a> object-modeling library, we can connect MongoDB to both Hapi and Express apps. For this part, you will want to have MongoDB <a rel="nofollow" target="_blank" href="https://docs.mongodb.com/manual/installation/">installed</a> and running on your system if you don&apos;t already have it.</p>
<p>To see how this works for both frameworks, let us now update our sample applications to store cat objects in a database. Likewise, our listings will fetch cat objects from the database.</p>
<p>The steps we have to follow are:</p>
<ul>
<li>Set up a MongoDB connection to localhost</li>
<li>Define a mongoose schema</li>
<li>Create objects</li>
<li>Store objects</li>
<li>Retrieve documents from the database</li>
</ul>
<p>Back in our <em>hapiapp</em> directory, install <em>mongoose</em> with:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="731e1c1d141c1c001633475d42425d42">[email&#xA0;protected]</a> --save
</code></pre>
<p>Since we want the ability to create new cats from our <code>/cats</code> view, update the cats template to include a form for new cats. Add the following lines above the list of cats in <code>hapiapp/templates/cats.html</code>.</p>
<pre><code class="language-html">&lt;!-- hapiapp/templates/cats.html --&gt;

...

&lt;form class=&quot;&quot; action=&quot;/cats&quot; method=&quot;post&quot;&gt;
    &lt;input type=&quot;text&quot; name=&quot;name&quot; placeholder=&quot;New cat&quot;&gt;
&lt;/form&gt;
</code></pre>
<p>We can now import <em>mongoose</em> at the top of our index.js file. Just below the line where we import <em>Hapi</em>, add the following lines:</p>
<pre><code class="language-javascript">// hapiapp/index.js

...

const Hapi = require(&apos;hapi&apos;);
const mongoose = require(&apos;mongoose&apos;);

// connect to MongoDB
mongoose.connect(&apos;mongodb://localhost/hapiappdb&apos;, { useMongoClient: true })
    .then(() =&gt; console.log(&apos;MongoDB connected&apos;))
    .catch(err =&gt; console.error(err));

// create a Cat model that can be persisted to the database
const Cat = mongoose.model(&apos;Cat&apos;, {name: String});
</code></pre>
<p>Now update our &quot;GET&quot; route for <code>/cats</code> to fetch cats dynamically from MongoDB. We will also add a new &quot;POST&quot; route where we can save cats to the database. Here is the code to make these changes, replacing the old GET <code>/cats</code> code:</p>
<pre><code class="language-javascript">// hapiapp/index.js

...

// display cats at /cats using the cats handlebars template
server.route({
    method: &apos;GET&apos;,
    path: &apos;/cats&apos;,
    handler: (request, reply) =&gt; {

        let cats = Cat.find((err, cats) =&gt; {
            console.log(cats);
            reply.view(&apos;cats&apos;, {cats: cats});
        });
    }
});

// post cats
server.route({
    method: &apos;POST&apos;,
    path: &apos;/cats&apos;,
    handler: (request, reply) =&gt; {
        let catname = request.payload.name;
        let newCat = new Cat({name: catname});

        newCat.save((err, cat) =&gt; {
            if (err) {
                console.error(err);
            }

            return reply.redirect().location(&apos;cats&apos;);
        });
    }
});
</code></pre>
<p>Now when you run the server again and visit <code>localhost:8000/cats</code>, you will see that the cat list is now empty. But there is a form where you can enter new cat names and press the <strong>Enter</strong> key, and your cats will be saved to MongoDB. Enter a few cats and you get a list of cats displayed, as shown below:</p>
<img alt="New cat listings" src="https://s3.amazonaws.com/stackabuse/media/hapi-vs-express-comparing-node-js-frameworks-5.png" class="img-responsive">
<p>For Express, installing and setting up Mongoose will look almost the same. Back in <em>expressapp</em>, install Mongoose with:</p>
<pre><code class="language-sh">$ npm install <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e4898b8a838b8b9781a4d0cad5d5cad5">[email&#xA0;protected]</a> --save
</code></pre>
<p>Then add the Mongoose connection code and <code>Cat</code> model in index.js after importing Express.</p>
<pre><code class="language-javascript">// expressapp/index.js

...

const express = require(&apos;express&apos;);
const mongoose = require(&apos;mongoose&apos;);

// connect to MongoDB
mongoose.connect(&apos;mongodb://localhost/expressappdb&apos;, { useMongoClient: true })
    .then(() =&gt; console.log(&apos;MongoDB connected&apos;))
    .catch(err =&gt; console.error(err));

// create a Cat model that can be persisted to the database
const Cat = mongoose.model(&apos;Cat&apos;, {name: String});
</code></pre>
<p>Notice the similarity to what we did for Hapi.</p>
<p>Now update our <em>expressapp/views/cats.hbs</em> template with the form code.</p>
<pre><code class="language-html">&lt;!-- expressapp/views/cats.hbs --&gt;

...

&lt;form class=&quot;&quot; action=&quot;/cats&quot; method=&quot;post&quot;&gt;
    &lt;input type=&quot;text&quot; name=&quot;name&quot; placeholder=&quot;New cat&quot;&gt;
&lt;/form&gt;
</code></pre>
<p>To enable Express to parse the form contents, we will need to import the <em>body-parser</em> middleware. After the line importing Express in index.js, import <em>body-parser</em> as follows:</p>
<pre><code class="language-javascript">// expressapp/index.js

...

const express = require(&apos;express&apos;);
const bodyParser = require(&apos;body-parser&apos;)
</code></pre>
<p>In addition, activate the middleware after the line creating our Express app, as follows:</p>
<pre><code class="language-javascript">// expressapp/index.js

...

// create our app
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
</code></pre>
<p>Now update our &quot;GET&quot; path for <code>/cats</code> and create a new &quot;POST&quot; route that create cats in the database:</p>
<pre><code class="language-javascript">// expressapp/index.js

...

// display a list of cats at /cats
app.get(&apos;/cats&apos;, function (req, res) {

    let cats = Cat.find((err, cats) =&gt; {
        console.log(cats);
        res.render(&apos;cats&apos;, {cats: cats});
    });
});

// post new cats and save them in the database
app.post(&apos;/cats&apos;, function (req, res) {
    console.log(req.body.name);

    let catname = req.body.name;
    let newCat = new Cat({name: catname});

    newCat.save((err, cat) =&gt; {
        if (err) {
            console.error(err);
        }
        console.log(`Cat ${req.body.name} saved to database.`)
        res.redirect(&apos;/cats&apos;);
    });
});
</code></pre>
<p>Now, restart the server and visit <code>localhost:3000/cats</code>. Just like for the Hapi app, we will now see an empty space with a form on top. We can enter cat names. The view will refresh, showing us a list of cats now saved to our MongoDB database.</p>
<p>This is one instance where the Express set up was a bit more complex than the equivalent for Hapi. However, notice that we didn&apos;t actually need to do too much with Hapi or Express to get the database connection set up - most of the work here was for the GET and POST routes.</p>
<h3 id="choosingbetweenhapiandexpress">Choosing Between Hapi and Express</h3>
<p>As we saw, Hapi and Express have a lot in common. Both have an active user base, with Hapi being widely adopted by big enterprise teams. However, Express continues to outpace most other frameworks in adoption, especially for smaller teams.</p>
<p>Hapi makes sense if you have a well-defined app idea that fits within sensible defaults for the programming approach. Hapi takes a particular approach to things like app security, and its plugins system is not as extensive as the Express middleware ecosystem. If you are a large team, the conventions of Hapi can come in useful to keep your code maintainable.</p>
<p>Express, on the other hand, works best if you are after flexibility and making most development decisions on your own. For example, there might be ten different middleware that do the same thing in the Express ecosystem. It&apos;s up to you to consider from many options and make your choice. Express is best if you have an app you are not sure about the exact direction it will take. Whatever contingencies arise, the large Express ecosystem will provide the tools to make it happen.</p>
<h3 id="learnmore">Learn More</h3>
<p>Want to learn more about web development in Node.js and actually create some practical Express.js apps? Here are a few courses that will not only give you some great experience with this web framework, but they will better you as a full stack engineer:</p>
<ul>
<li><a class="udemy-link" rel="nofollow" href="http://stackabu.se/web-developer-bootcamp">The Web Developer Bootcamp</a></li>
<li><a class="udemy-link" rel="nofollow" href="http://stackabu.se/node-react-fullstack-web-development">Node with React: Fullstack Web Development</a></li>
</ul>
<p>Otherwise you can also get a great start by visiting each framework&apos;s respective websites:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://expressjs.com/">Express Website</a></li>
<li><a rel="nofollow" target="_blank" href="https://hapijs.com/">Hapi Website</a></li>
</ul>
<p>Either way, the most important thing is you actually try them out, work through some examples, and decide on your own which is best for you.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					