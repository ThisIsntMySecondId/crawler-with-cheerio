
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p>For many people, actually running your code in a production environment is an afterthought, and just writing the code is where the real challenge is at. While this is mostly true in my experiences, finding a reliable and easy way to run your app can be pretty difficult itself.</p>
<p>There are a few things you need to consider when running your app(s).</p>
<ul>
<li>Where will the output/logs go?</li>
<li>What happens if the app crashes?</li>
<li>How do I keep track of all my daemon processes?</li>
<li>How do I provide configurations for each process?</li>
</ul>
<p>This is where the Node <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/forever"><code>forever</code></a> package is really useful. It helps you easily manage all of the above problems and more, which I&apos;ll describe in the next few sections.</p>
<h3 id="whatisforever">What is Forever?</h3>
<p><code>forever</code> is a command-line utility for Node applications written entirely in JavaScript. It is meant to simplify your life in a production environment by managing (starting, stopping, restarting, etc) Node processes and their configurations. You can use it on the command line or programmatically (via <code>forever-monitor</code>) within your Node code.</p>
<p>You can specify these configurations via the command line or a JSON configuration file. I personally prefer the JSON file since you can configure multiple Node processes in a single file, making it easy to launch all of your processes at once. This way I don&apos;t hesitate to break up a big monolith in to multiple independent services since managing them will be much easier thanks to utilities like <code>forever</code>.</p>
<p>It works underneath by running <a rel="nofollow" target="_blank" href="https://github.com/flatiron/flatiron">Flatiron server</a> as a daemon process to manage your other daemon processes, so if/when your code fails and crashes, <code>forever</code> will be there to automatically restart it and hopefully avoid downtime for your users. Running <code>forever</code> with a Flatiron server is an easy way to keep the utility up and running while monitoring for user input, and thanks to the <a rel="nofollow" target="_blank" href="https://github.com/flatiron/flatiron#create-a-cli-application-with-flatironpluginsclioptions">CLI plugin</a> this was easy for the <code>forever</code> developers to do. If you&apos;re going to create a program like <code>forever</code> (i.e. a long-running daemon process that receives user commands) then I&apos;d highly recommend you do it this way. It will make things a whole lot easier.</p>
<p>The interprocess communication happens via sockets, which is handled by the high-level <a rel="nofollow" target="_blank" href="https://github.com/foreverjs/nssocket"><code>nssocket</code></a> library. The socket files that allow communication to be re-opened between processes reside in <code>[forever.root]/socks</code>. Naturally, the messages between the processes are serialized as JSON. It&apos;s another helpful library to check out if you ever want event-based socket communication.</p>
<p>I should note, one important feature about <code>forever</code> (and other process management tools like it) is that you can use it for non-Node applications as well, like Python, Ruby, etc. The <code>-c</code>, or command flag (which you&apos;ll see later), let&apos;s you tell <code>forever</code> how to run your app. Using this, you can tell it to use any other interpreter for execution, like Python:</p>
<pre><code class="language-sh">$ forever start -c python py_script.py
</code></pre>
<p>So even if you don&apos;t like Node, or just need to use a different language for a project, keep in mind that this is still useful to you.</p>
<h3 id="howtouseforever">How to use Forever</h3>
<p>Ok, on to the important part, how to actually use the damn thing. Let&apos;s start out with a simple example, like just starting and stopping a web-app. For our example, we&apos;ll be starting and stopping a &quot;hello world&quot; Express server:</p>
<pre><code class="language-javascript">// index.js

var express = require(&apos;express&apos;);
var app = express();

app.get(&apos;/&apos;, function (req, res) {
  res.send(&apos;Hello World!&apos;);
});

var server = app.listen(8080);
</code></pre>
<p>Install <code>forever</code> with <code>npm install forever -g</code>, and start the server with:</p>
<pre><code class="language-sh">$ forever start -a index.js
</code></pre>
<p>This will launch the Express app as a background process. The <code>-a</code> flag appends logs to the default log file located in <code>forever</code>&apos;s root directory (<code>~/.forever</code> by default on Unix systems). You won&apos;t be able to <code>start</code> the app without this flag if the log file already exists.</p>
<p>Inspect the running process with <code>forever list</code>:</p>
<pre><code>info:    Forever processes running
data:        uid  command                                         script   forever pid   id logfile                        uptime      
data:    [0] moMB /Users/scott/.nvm/versions/node/v4.1.2/bin/node index.js 21389   21390    /Users/scott/.forever/moMB.log 0:0:0:3.345
</code></pre>
<p>To stop the process, just refer to it by id (<code>21390</code>), uid (<code>moMB</code>), pid (<code>21389</code>), index (<code>0</code>), or script name (<code>index.js</code>):</p>
<pre><code class="language-sh">$ forever stop index.js
</code></pre>
<p>While this is nice, it could be better. <code>index.js</code> isn&apos;t very unique to our app, and the PID is hard to remember, so why give it a better name? That way you don&apos;t have to keep running <code>forever list</code> to get info on your running process.</p>
<pre><code class="language-sh">$ forever start -a --uid myapp index.js
</code></pre>
<p>Now you can just stop it with:</p>
<pre><code class="language-sh">$ forever stop myapp
</code></pre>
<p>Two arguments on the command line isn&apos;t so bad, but it can become a bit much when you start adding arguments for log files, different Node executables, working directories, and more. So instead of specifying everything on the command line, you can use a JSON configuration file like this:</p>
<pre><code class="language-json">{
    // Comments are allowed!
    &quot;uid&quot;: &quot;myapp&quot;,
    &quot;append&quot;: true,
    &quot;watch&quot;: true,
    &quot;script&quot;: &quot;index.js&quot;,
    &quot;sourceDir&quot;: &quot;/home/scott/myapp&quot;,
    &quot;command&quot;: /Users/scott/.nvm/versions/node/v4.1.2/bin/node
}
</code></pre>
<p>Assuming this file is in your current working directory and named <code>forever.json</code>, use it like this:</p>
<pre><code class="language-sh">$ forever start ./forever.json
</code></pre>
<p>As you can see, you can even tell <code>forever</code> which version of Node to use, which is really convenient when you have multiple apps on a single server that require different versions (like the io.js fork).</p>
<pre><code class="language-json">[
  {
    // App using Node v0.11.8
    &quot;uid&quot;: &quot;myapp1&quot;,
    &quot;append&quot;: true,
    &quot;watch&quot;: true,
    &quot;script&quot;: &quot;index.js&quot;,
    &quot;sourceDir&quot;: &quot;/Users/scott/myapp1&quot;,
    &quot;command&quot;: &quot;/Users/scott/.nvm/versions/node/v0.11.8/bin/node&quot;
  },
  {
    // App using io.js
    &quot;uid&quot;: &quot;myapp2&quot;,
    &quot;append&quot;: true,
    &quot;watch&quot;: true,
    &quot;script&quot;: &quot;index.js&quot;,
    &quot;sourceDir&quot;: &quot;/Users/scott/myapp2&quot;,
    &quot;command&quot;: &quot;/Users/scott/.nvm/versions/io.js/v2.2.1/bin/node&quot;,
    &quot;args&quot;: [&quot;--port&quot;, &quot;8080&quot;]
  }
]
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>There are a few tools to help you run Node apps, most notably <code>forever</code> and <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/pm2"><code>pm2</code></a>. I&apos;ve tried both and thought <code>forever</code> was a bit easier to use (mostly just because it&apos;s simpler and has less features/options to worry about), while <code>pm2</code> is more powerful. If you&apos;re really feeling ambitious, you can try out <code>pm2</code>&apos;s cluster mode or API to help monitor metrics about your running app.</p>
<p><em>What other interesting configurations and uses do you have for process management tools like <code>forever</code>? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					