
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p>User authentication is one of those things that you probably don&apos;t think too much about, but just about every website or app out there requires it. If you had to implement authentication yourself, could you? Well don&apos;t worry, you probably won&apos;t have to. Since this functionality is so common, just about every language/web framework I&apos;ve come across already has a solution ready to go, and for Node, you can use the Express Passport plugin.</p>
<p>As always, I&apos;d recommend you trust the popular packages for things like this as they&apos;ve probably already gone through the headaches of finding bugs and vulnerabilities within the code.</p>
<h3 id="whatispassport">What is Passport</h3>
<p>The <a rel="nofollow" target="_blank" href="http://passportjs.org/">Passport</a> package is an expandable and modular authentication middleware for Node.js that adds authentication functionality to your Express app. When most people think of authentication, they think of the traditional username and password combination. While this still is very popular, using other services to authenticate a user through <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/OAuth">OAuth</a> has become another popular method. With Passport being a very extensible middleware, it allows you to plug in over 300 different authentication providers like Facebook, Twitter, Google, and more, most of which use the OAuth standard.</p>
<p>Not all of the strategies use OAuth, howerver. You can also choose from hash-based (passing a hash via query string, POST data, or in the HTTP headers), <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/JSON_Web_Token">JSON web tokens</a>, or even just <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Basic_access_authentication">HTTP Basic authentication</a> (like this: <code>http://username:<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="5323322020243c213713362b323e233f367d303c3e">[email&#xA0;protected]</a>/</code>). Although some of these strategies may take different parameters, they will largely be the same so you should be able to easily swap strategies if needed.</p>
<p>To track sessions for a logged-in user between requests after the initial authentication, Passport saves session data in a cookie, which it creates and sends to the user. This is all handled behind the scenes for you for all strategies.</p>
<h3 id="howtousepassport">How to use Passport</h3>
<p>To use Passport, you&apos;ll need to hook it up to your Express <code>app</code> just like you would any other <a href="https://stackabuse.com/how-to-write-express-js-middleware">middleware</a>:</p>
<pre><code class="language-javascript">var express = require(&apos;express&apos;);
var passport = require(&apos;passport&apos;);

var app = express();

app.configure(function() {
	app.use(express.cookieParser());
	app.use(express.bodyParser());
	app.use(express.session({ secret: &apos;derpy&apos; }));

	app.use(passport.initialize());
	app.use(passport.session());	// Required for persistent login sessions (optional, but recommended)
});
</code></pre>
<p>Take note that if you decide to enable sessions, then you need to use <code>express.session()</code> before <code>passport.session()</code> to ensure that the user&apos;s login session is restored in the right order.</p>
<p>In most cases, you&apos;ll want to enable sessions, otherwise the user won&apos;t be remembered between requests.</p>
<p>Once you have Passport initialized and the sessions setup, it&apos;s time to set up your strategy. Like I said earlier, most are pretty similar setup-wise, so let&apos;s just take a look at the most commonly used strategy, <code>LocalStrategy</code>:</p>
<pre><code class="language-javascript">var LocalStrategy = require(&apos;passport-local&apos;).Strategy;

// Express/Passport setup here...

passport.use(new LocalStrategy(
	{
		usernameField: &apos;email&apos;,
        passwordField: &apos;password&apos;
	},
	function(email, password, done) {
        User.loadOne({ email: email }).then(function(user) {
            if (!user || !user.authenticate(password)) {
                return done(null, false, { message: &apos;Incorrect email or password.&apos; });
            }

            done(null, user);
        });
    })
);
</code></pre>
<p>The <code>User.loadOne...</code> part will be specific to your application (I&apos;m using the <a href="https://github.com/scottwrobinson/camo">Camo</a> ODM here). The callback sends you the user&apos;s username/email and password, which in the case of &quot;Local Strategy&quot; was retrieved from the submitted form data with input fields named <code>email</code> and <code>password</code>.</p>
<p>The HTML form that this data comes from might look something like this:</p>
<pre><code class="language-html">&lt;form method=&quot;post&quot; role=&quot;form&quot;&gt;
    &lt;input type=&quot;text&quot; name=&quot;email&quot;/&gt;
    &lt;input type=&quot;password&quot; name=&quot;password&quot;/&gt;
    &lt;button type=&quot;submit&quot;&gt;Login&lt;/button&gt;
&lt;/form&gt;
</code></pre>
<p>Notice the fields are named <code>email</code> and <code>password</code> like we specified in the <code>LocalStrategy</code> above.</p>
<p>With this email and password being extracted from <code>req</code> by Passport, we can look up the user in our database and then check the given password against the one we got from the database (don&apos;t worry, it&apos;s a <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Salt_%28cryptography%29">salted hash</a> of the password). Returning a valid user object to the <code>done</code> callback tells Passport that the credentials were valid. Otherwise we return <code>false</code> and an error message.</p>
<p>The last thing you need to do is tell Passport how to serialize and deserialize the user object. What this means is that Passport needs you to tell it how to uniquely identify a user with just a string, like with an ID or an email address.</p>
<pre><code class="language-javascript">// Express/Passport setup here...

// Passport strategy setup here...

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.loadOne({ _id: id }).then(function(user) {
        done(null, user);
    }).catch(function(err) {
        done(err, null);
    });
});
</code></pre>
<p>In the <code>serializeUser</code> function above we have already retrieved the user data from the database and need to tell Passport what information to use in the cookie to uniquely identify the user, which in this case is <code>user.id</code>.</p>
<p>For <code>deserializeUser</code>, Passport got the cookie string (the user ID) and needs to turn it in to a user object. So we use the <code>id</code> to look up the user in the database. What you return here is passed in each request as <code>req.user</code> to the route.</p>
<h3 id="conclusion">Conclusion</h3>
<p>Most all web-apps and many native apps use some form of authentication, so you&apos;d be pretty well off letting Passport handle all of the hard work for you. All you need to do is decide on a strategy.</p>
<p>Not that you need any more motivation to use Passport, but keep in mind that getting authentication right is hard, and with it being the gatekeeper to your users&apos; sensitive data, you&apos;d be better off trusting it to a package that&apos;s been thoroughly tested and proven.</p>
<p><em>What do you think of Passport, and how have you used it in the past? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					