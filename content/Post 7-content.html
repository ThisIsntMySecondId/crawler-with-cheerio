
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p><a href="https://expressjs.com/" rel="nofollow" target="_blank">Express</a> is by far the most popular web framework for Node.js thanks to its simple API, available plugins, and huge community. Thanks to the community, there is no shortage of documentation and examples on how to use the core Express API, which is great, but it&apos;s not always immediately clear how to improve the performance of your web application as it grows. Here I&apos;ll show you some of the easiest and most effective ways to improve the performance of your Express apps.</p>
<h4 id="gzipcompression">gzip Compression</h4>
<p>gzip compression isn&apos;t anything new to web servers, but it&apos;s an easy thing to forget about, especially when you&apos;re used to using frameworks that enable it by default. This is one of those improvements that is extremely easy to add, and it provides a great performance boost. Compressing your page content can <strong>reduce page size by up to 70%</strong>.</p>
<pre><code class="language-javascript">var compression = require(&apos;compression&apos;);
var express = require(&apos;express&apos;);

var app = express();

app.use(compression());
</code></pre>
<h4 id="runexpressinproductionmode">Run Express in Production Mode</h4>
<p>By default, Express will run in development mode, which is easy to overlook, especially for those just starting out with Node.js/Express.</p>
<p>So, whats the difference between production and development mode anyway? Turns out, in development mode the view templates are read from a file for each request, whereas in production mode the templates are loaded once and cached. This is done so you can easily make changes on the fly without having to restart the app every time during development. In a production environment, however, this can greatly reduce your performance since you have to deal with slow file IO as compared to much faster RAM.</p>
<p>Lucky for you, getting Express in to production mode is easy. It&apos;s just a matter of setting an environment variable.</p>
<pre><code class="language-bash">$ export NODE_ENV=production
</code></pre>
<p>Be careful with this method though. If the server restarts, you&apos;ll lose this environment variable and go back to using development mode. A more permanent solution is to set the variable within your <code>.bash_profile</code>:</p>
<pre><code class="language-bash">$ echo export NODE_ENV=production &gt;&gt; ~/.bash_profile
$ source ~/.bash_profile
</code></pre>
<h4 id="minifywithuglify">Minify with Uglify</h4>
<p>For just about every website, especially those with lots of styling and client-side functionality, static assets can be a huge drag on performance. Having to send over multiple JavaScript and CSS files for each request eventually takes its toll on your server, and that&apos;s not even considering the time the user has to wait for all the separate HTTP requests to finish on the client side.</p>
<p>To help mitigate this, you can use a utility package like <a href="https://github.com/mishoo/UglifyJS/" rel="nofollow" target="_blank">Uglify</a> to minify and concatenate your JavaScript and CSS files. Combine this with a task runner like <a href="https://gruntjs.com/" rel="nofollow" target="_blank">Grunt</a> and you&apos;ll easily be able to automate the process and not have to worry about it. A fairly capable Grunt file (using the <a href="https://github.com/gruntjs/grunt-contrib-uglify" rel="nofollow" target="_blank">grunt-contrib-uglify</a> plugin) might look something like this:</p>
<pre><code class="language-javascript">module.exports = function(grunt) {

	grunt.initConfig({
		uglify: {
			options: {
				banner: &apos;/*! &lt;%= pkg.name %&gt; &lt;%= grunt.template.today(&quot;dd-mm-yyyy&quot;) %&gt; */\n&apos;
			},
			dist: {
				files: {
					&apos;dist/&lt;%= pkg.name %&gt;.min.js&apos;: [&apos;&lt;%= concat.dist.dest %&gt;&apos;]
				}
			}
    	}
	});

	grunt.loadNpmTasks(&apos;grunt-contrib-uglify&apos;);
	grunt.registerTask(&apos;default&apos;, [&apos;uglify&apos;]);
};
</code></pre>
<h4 id="reduceyourmiddleware">Reduce Your Middleware</h4>
<p>I&apos;m not saying you should never use middleware, obviously it&apos;s necessary and there are many reasons to use middleware in your Express apps, but it&apos;s easy to go overboard and copy all the middleware you see other apps using. Look through your list of dependencies and decide if you really need everything you have installed (cookies, sessions, i18n, request loggers, etc).</p>
<p>In some cases you only need middleware packages for development, so you can easily disable these in production:</p>
<pre><code class="language-javascript">var express = require(&apos;express&apos;);
var debugMiddleware = require(&apos;my-debug-middleware&apos;);

var app = express();

if (process.env.NODE_ENV !== &apos;production&apos;) {
	app.use(debugMiddleware());
}
</code></pre>
<h4 id="increasemaxsockets">Increase Max Sockets</h4>
<p>By default the Node.js HTTP server has a socket pool limit of only 5. This is a very conservative number and most servers can handle a much higher number of sockets than this.</p>
<p>Alternatively, you can allow as many sockets as possible:</p>
<pre><code class="language-javascript">var http = require(&apos;http&apos;);
var https = require(&apos;https&apos;);

http.globalAgent.maxSockets = Infinity;
https.globalAgent.maxSockets = Infinity;
</code></pre>
<p><strong>Edit</strong>: This only applies for Node v0.10 and older. As of v0.12, <code>maxSockets</code> (for both <code>http</code> and <code>https</code>) is set to <code>Infinity</code>.</p>
<h4 id="usecachecontrol">Use Cache-Control</h4>
<p>You can set an HTTP header that will tell the user&apos;s browser to cache the content it just received. This is usally a good idea for static assets (JS, CSS, etc) that change infrequently. For assigning app-wide cache settings, use:</p>
<pre><code class="language-javascript">var express = require(&apos;express&apos;);
var app = express();

app.use(express.static(__dirname + &apos;/public&apos;, { maxAge: 31557600 }));
</code></pre>
<p>This will assign the cache settings for everything in the <code>public</code> directory. For more fine-grained control, you can set caching based on individual requests/routes:</p>
<pre><code class="language-javascript">var express = require(&apos;express&apos;);
var app = express();

app.get(&apos;/index.html&apos;, function (req, res) {
	res.setHeader(&apos;Cache-Control&apos;, &apos;public, max-age=86400&apos;);
	res.render(&apos;index.html&apos;);
});
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>Keep in mind that you can find some great tips by browsing open source websites, like the <a href="https://github.com/tryghost/Ghost" rel="nofollow" target="_blank">Ghost</a> blogging platform. These applications are developed and used by thousands of people, so you can usually find some great useful snippets that you wouldn&apos;t hear about otherwise.</p>
<p>There are quite a few things you can do outside of Express to speed things up, so if you haven&apos;t already, check out Redis, Nginx, and other caching mechanisms to give your app a boost. Believe me, it&apos;s worth your time.</p>
<p><em>Have any more Express performance tips? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					