
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>JavaScript is undoubtedly one of the most popular programming languages out there today, and for good reason. It can easily be run in your browser, on a server, on your desktop, or even on your phone as an app. One of the most popular and easiest ways to write JavaScript is using <a rel="nofollow" target="_blank" href="https://nodejs.org/en/">Node.js</a>.</p>
<p>There are quite a few resources out there to learn Node.js, but not many of them really give you the background, tools, and resources you need to actually succeed at writing Node code.</p>
<p>So what I&apos;m aiming to do here is provide you with a guide that I had wish I had when just starting out. I&apos;ll start with a short description of what Node actually is and what it&apos;s doing behind the curtain, then I&apos;ll give you some concrete examples that you can try yourself right in the browser, and finally I&apos;ll give you a bunch of resources to guide you through some more useful and practical examples/concepts.</p>
<p>Note that this guide will not teach you how to code, but instead it will guide you through the basics of the Node run-time and npm.</p>
<h3 id="whatisnode">What is Node</h3>
<p>Node is a server-side cross-platform runtime environment that runs on the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/V8_%28JavaScript_engine%29">V8 JavaScript engine</a>, which powers Google&apos;s Chrome browser. This is really the heart of Node and is the component that actually parses and executes the code.</p>
<p>The V8 engine does this by compiling the JavaScript to native machine code, which makes it much faster than an interpreter. To speed things up even more, the compiled code is optimized (and re-optimized) dynamically at runtime based on heuristics of the code&apos;s execution profile. This means that as the program runs, the engine actually tracks its performance and makes the code faster based on certain factors that are tracked.</p>
<p>As a runtime, Node&apos;s big focus is to use an event-driven, non-blocking IO model to make it lightweight and fast. For some, this programming model can be a bit confusing at first, but it actually does a great job of simplifying development for heavy-IO applications, like websites.</p>
<p>This design is ideal for optimizing your code&apos;s throughput and scalability, which is a big reason for why it has become so popular. For example, someone got it to handle <a rel="nofollow" target="_blank" href="http://www.jayway.com/2015/04/13/600k-concurrent-websocket-connections-on-aws-using-node-js/">600,000 concurrent websocket connections</a>, which is insane. Now, he had to do a bit of custom configuration, but that doesn&apos;t make it any less impressive. This is exactly why companies like IBM, Microsoft, and PayPal are using Node for their web services.</p>
<p>Now, Node doesn&apos;t even need to be fast to make it attractive. One of my favorite features is actually the package manager, <a rel="nofollow" target="_blank" href="https://npmjs.com/">npm</a>. A lot of languages lack a good package manager like this. npm is a command line tool you can use to initialize modules, manage dependencies, or run tests, among other things.</p>
<p><img src="https://s3.amazonaws.com/stackabuse/media/npm-logo-small.png" alt="npm logo" data-pagespeed-url-hash="2104375504" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></p>
<p>The public repository is open for anyone to download and publish code to. As of the time of this writing, npm is hosting over 210,000 modules, ranging from websites to command line tools to API wrappers.</p>
<p><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/camo">Here is an example</a> of a package I created. You can see that the main page is the README, which describes what the package does and how to use it. You also get a quick rundown of other info, like number of downloads, the repository location, and the software license used.</p>
<h4 id="whatnodeisgoodfor">What Node is good for</h4>
<p>Among other things, Node is probably best-suited for building websites and tools that require real-time, synchronous interaction. Chat sites/apps are a good example of this since they&apos;re usually very IO-heavy. The non-blocking event-driven model allows it to handle lots of requests simultaneously.</p>
<p>It is also very good for creating the front-end for web APIs (via REST). This is because it is optimized for event-driven IO (which I already touched on) <em>and</em> it handles JSON natively, so there is very little to no parsing needed.</p>
<h4 id="whatnodeisnotgoodfor">What Node is <strong>not</strong> good for</h4>
<p>On the other end, let&apos;s see what Node is <em>not</em> good at. Most notably, it is very ill-suited to performing heavy computational tasks. So if you wanted to do something like <a href="https://stackabuse.com/neural-networks-in-javascript-with-brain-js/">machine learning with Node</a>, you probably won&apos;t have the best experience.</p>
<p>Node is also still fairly young, so it&apos;s still under rapid development. In the past few months we&apos;ve gone from <code>v0.12.x</code> to <code>v5.1.x</code>. So if you need something more stable then this probably isn&apos;t for you.</p>
<p>And as for the asynchronous programming &quot;problem&quot;, I think the first part of <a rel="nofollow" target="_blank" href="http://qr.ae/RbHnFE">this Quora answer</a> does a good job explaining it:</p>
<blockquote>
<p>[Its] lack of inherent code organization is a huge disadvantage. It gets especially exacerbated when the development team as a whole isn&apos;t familiar with asynchronous programming or standard design patterns. There are just too many ways for code to get unruly and unmaintainable.</p>
</blockquote>
<p>Although asynchronous programming is a good thing overall, it does add complexity to your programs.</p>
<h3 id="thenoderepl">The Node REPL</h3>
<p>Ok, on to some code. We&apos;re going to start out pretty simple and just run a few commands in the REPL (read-eval-print loop), which is just an application that lets you interactively run Node code in a shell. A program written here is executed piece-wise instead of all at once.</p>
<p>I&apos;ll assume you&apos;re already familiar with JavaScript, so we&apos;ll just go through some Node-specific things throughout this article.</p>
<p>Let&apos;s try out one of the built-in modules that come with Node, like the <code>crypto</code> module.</p>
<p>Assuming you have Node installed already, run the <code>node</code> command in your shell, and type in the following code to the prompt line by line:</p>
<pre><code class="language-javascript">var crypto = require(&apos;crypto&apos;);

crypto.createHash(&apos;md5&apos;).update(&apos;hello world&apos;).digest(&apos;hex&apos;);
</code></pre>
<p>After entering the last line in the REPL, (or by clicking the &apos;run&apos; button above) you should see <em>5eb63bbbe01eeed093cb22bb8f5acdc3</em> printed out to the console.</p>
<p>The <code>crypto</code> module is loaded using the <code>require()</code> function, which handles the resolution and loading of code for you. More info on how that works <a rel="nofollow" target="_blank" href="https://nodejs.org/api/modules.html#modules_modules">here</a>.</p>
<p>Once the module has been loaded, you can use its functions, which in this case we use <code>createHash()</code>. Since REPLs execute code piece-wise they typically print out the returned value for each line, like you saw here.</p>
<p>You can use REPLs like this to quickly test out code without having to write it to a new file and execute it. It&apos;s almost like a general purpose sandbox environment.</p>
<h3 id="yourfirstprogram">Your first program</h3>
<p>REPLs are fun and all, but they will only get us so far. So let&apos;s move on and write our first real Node program. We won&apos;t worry about using third-party modules quite yet (but don&apos;t worry, we will later), so let&apos;s see what built-in code is available to us first. There is a good amount of code provided to you already, including (but not limited to):</p>
<ul>
<li><code>fs</code>: Simple wrappers provided over standard POSIX functions</li>
<li><code>http</code>: A lower-level HTTP server and client</li>
<li><code>os</code>: Provides some basic methods to tell you about the underlying operating system</li>
<li><code>path</code>: Utilities for handling and transforming file paths</li>
<li><code>url</code>: Utilities for URL resolution and parsing</li>
<li><code>util</code>: Standard utility functions like debugging, formatting, and inspection</li>
</ul>
<p>The amount of code built-in isn&apos;t on the level of Python, but it&apos;ll do the job. The real benefits come when you start getting in to the third-party modules.</p>
<p>For our first program, we&apos;ll create a simple utility that determines your location using your IP address (kinda creepy, I know):</p>
<pre><code class="language-javascript">var http = require(&apos;http&apos;);

var options = {
    hostname: &apos;ipinfo.io&apos;,
    port: 80,
    path: &apos;/json&apos;,
    method: &apos;GET&apos;
};

var req = http.request(options, function(res) {
    var body = &apos;&apos;;
    
    res.setEncoding(&apos;utf8&apos;);
    res.on(&apos;data&apos;, function(chunk) {
        body += chunk;
    });
    
    res.on(&apos;end&apos;, function() {
        var json = JSON.parse(body);
        console.log(&apos;Your location: &apos; + json.city + &apos;, &apos; + json.region);
    });
});

req.end();
</code></pre>
<p>Copy the code above and paste it in a file named &apos;index.js&apos;. Then, on the command line, navigate to the directory with the file you just created and run it with:</p>
<pre><code class="language-bash">$ node index.js
</code></pre>
<p>You should see &apos;Your location: [CITY], [REGION]&apos; printed to the command line. The city/region printed out will probably be pretty close to you, but not exact. Also, if no city/region is printed then that just means your IP info wasn&apos;t in the database.</p>
<p>Since this code doesn&apos;t use any 3rd-party dependencies, it doesn&apos;t need to have a <code>package.json</code> file or <code>node_modules</code> folder, which we&apos;ll explain more about in the next section.</p>
<h3 id="yourfirstpackage">Your first package</h3>
<p><em>Note that throughout this article I use &apos;package&apos; and &apos;module&apos; interchangeably.</em></p>
<p>For just about every website/tool/project you create with Node.js you&apos;ll also want to create a module around it. This is so you can specify dependencies, tests, scripts, repositories, etc.</p>
<p>A typical module consists of a few important things:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://docs.npmjs.com/files/package.json">package.json</a>: A JSON file containing all of the module information</li>
<li>node_modules/: A directory containing all the dependencies</li>
<li>index.js: The main code file</li>
<li>README.md: Documentation about the module</li>
<li>test/: A directory of tests for the module</li>
</ul>
<p>There are a ton of other things you can add to a module (like a .npmignore file, a&#xA0;docs directory, or editor configuration files), but the things listed above are some of the most common you&apos;ll see.</p>
<p>To show how all of this works, throughout the rest of this section we&apos;ll create our own package that builds on the previous example.</p>
<p>Instead of just telling you your location based on your IP address, we&apos;ll use some popular Node packages to create a tool that lets you find the location of any website&apos;s server. We&apos;ll call it <code>twenty</code> (<a rel="nofollow" target="_blank" href="https://www.urbandictionary.com/define.php?term=what%27s+your+20%3F">see why</a>).</p>
<h4 id="initializingthepackage">Initializing the package</h4>
<p>First, create and navigate to a new directory for your project:</p>
<pre><code class="language-bash">$ mkdir twenty
$ cd twenty
</code></pre>
<p>Then, use npm to initialize the project:</p>
<pre><code class="language-bash">$ npm init

This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sane defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install &lt;pkg&gt; --save` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
name: (twenty) 
version: (0.0.1) 
description: Locates the city/region of a given URL/IP address
entry point: (index.js) 
test command: 
git repository: 
keywords: 
license: (MIT) 
About to write to /Users/scott/projects/twenty/package.json:

{
  &quot;name&quot;: &quot;twenty&quot;,
  &quot;version&quot;: &quot;0.0.1&quot;,
  &quot;description&quot;: &quot;Locates the city/region of a given URL/IP address&quot;,
  &quot;main&quot;: &quot;index.js&quot;,
  &quot;scripts&quot;: {
    &quot;test&quot;: &quot;echo \&quot;Error: no test specified\&quot; &amp;&amp; exit 1&quot;
  },
  &quot;author&quot;: &quot;Scott Robinson &lt;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="80f3e3eff4f4c0f3f4e1e3ebe1e2f5f3e5aee3efed">[email&#xA0;protected]</a>&gt; (http://stackabuse.com)&quot;,
  &quot;license&quot;: &quot;MIT&quot;
}


Is this ok? (yes) yes
</code></pre>
<p>Fill out each prompt (starting at <code>name: (twenty)</code>), or don&apos;t enter anything and just press return to use the default settings. This will create a properly configured <code>package.json</code> file containing the following JSON:</p>
<pre><code class="language-json">{
  &quot;name&quot;: &quot;twenty&quot;,
  &quot;version&quot;: &quot;0.0.1&quot;,
  &quot;description&quot;: &quot;Locates the city/region of a given URL/IP address&quot;,
  &quot;main&quot;: &quot;index.js&quot;,
  &quot;scripts&quot;: {
    &quot;test&quot;: &quot;echo \&quot;Error: no test specified\&quot; &amp;&amp; exit 1&quot;
  },
  &quot;author&quot;: &quot;Scott Robinson &lt;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="bfccdcd0cbcbffcccbdedcd4deddcaccda91dcd0d2">[email&#xA0;protected]</a>&gt; (http://stackabuse.com)&quot;,
  &quot;license&quot;: &quot;MIT&quot;
}
</code></pre>
<p>This file is your starting point where all the project-specific info is saved.</p>
<h4 id="installdependencies">Install dependencies</h4>
<p>To add dependencies to your project, you can use the <code>npm install</code> command from the command line. For example, to add our first dependency, <code>request</code>, try running this:</p>
<pre><code class="language-bash">$ npm install --save request
</code></pre>
<p>The <code>install</code> command will download the latest <code>request</code> package from npm and save it in the <code>node_modules</code> directory. Adding the <code>--save</code> flag tells npm to save the package details in package.json under the &apos;dependencies&apos; section:</p>
<pre><code class="language-json">&quot;dependencies&quot;: {
    &quot;request&quot;: &quot;2.67.0&quot;
}
</code></pre>
<p>Now you&apos;ll be able to use the <code>request</code> module anywhere in your project code.</p>
<h4 id="improvingthecode">Improving the code</h4>
<p>The <code>request</code> module gives you functions to easily make all kinds of HTTP requests. The HTTP example we showed above wasn&apos;t too bad, but <code>request</code> makes the code even more compact and easier to read. The equivalent code using <code>request</code> would look like this:</p>
<pre><code class="language-javascript">var request = require(&apos;request&apos;);

request(&apos;http://ipinfo.io/json&apos;, function(error, response, body) {
    var json = JSON.parse(body);
    console.log(&apos;Your location: &apos; + json.city + &apos;, &apos; + json.region);
});
</code></pre>
<p>It would be more interesting if we could find the location of <em>any</em> IP address, and not just our own, so let&apos;s allow the user to enter an IP address as a command line argument. Like this:</p>
<pre><code class="language-bash">$ node index.js 8.8.8.8
</code></pre>
<p>To access this argument within our program, Node makes it available in the global <code>process</code> object as <code>process.argv</code>, which is an array. For the command we just executed above, <code>process.argv</code> would be <code>[&apos;node&apos;, &apos;index.js&apos;, &apos;8.8.8.8&apos;]</code>.</p>
<p>To make things even easier, we&apos;ll use the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/yargs">yargs</a> package to help us parse out command line arguments. With a simple program like this, <code>yargs</code> isn&apos;t really necessary, but I&apos;ll be improving on <code>twenty</code> in a later article, so we might as well add it now.</p>
<p>Just like <code>request</code>, we&apos;ll install it with:</p>
<pre><code class="language-bash">$ npm install --save yargs
</code></pre>
<p>Modifying the code to use <code>yargs</code> to grab the argument (or default to our own IP if no argument was give), we end up with this:</p>
<pre><code class="language-javascript">var request = require(&apos;request&apos;);
var argv = require(&apos;yargs&apos;).argv;

var path = &apos;json&apos;;

path = argv._[0] || path;

request(&apos;http://ipinfo.io/&apos; + path, function(error, response, body) {
    var json = JSON.parse(body);
    console.log(&apos;Server location: &apos; + json.city + &apos;, &apos; + json.region);
});
</code></pre>
<p>So far this tool is great for command line usage, but what if someone wants to use it as a dependency in their own code? As of now the code hasn&apos;t been exported, so you wouldn&apos;t be able to use it anywhere else but the command line. To tell Node which functions/variables to make available, we can use <code>module.exports</code>.</p>
<pre><code class="language-javascript">var request = require(&apos;request&apos;);
var argv = require(&apos;yargs&apos;).argv;

var findLocation = function(ip, callback) {
    var path;
    if (typeof(ip) === &apos;function&apos; || !ip) path = &apos;json&apos;;
    else path = ip;
    
    request(&apos;http://ipinfo.io/&apos; + path, function(error, response, body) {
        var json = JSON.parse(body);
        callback(null, json.city + &apos;, &apos; + json.region);
    });
};

module.exports = findLocation;
</code></pre>
<p>Great! Now anyone that downloads this package can require it anywhere in their code and use the <code>findLocation()</code> function.</p>
<p>But, you may have noticed that now we can&apos;t use it as a command line tool anymore. We <strong>don&apos;t</strong> want to put the rest of the old code in there like this, though:</p>
<pre><code class="language-javascript">var request = require(&apos;request&apos;);
var argv = require(&apos;yargs&apos;).argv;

var findLocation = function(ip, callback) {
    var path;
    if (typeof(ip) === &apos;function&apos; || !ip) path = &apos;json&apos;;
    else path = ip;
    
    request(&apos;http://ipinfo.io/&apos; + path, function(error, response, body) {
        var json = JSON.parse(body);
        callback(null, json.city + &apos;, &apos; + json.region);
    });
};

var arg = argv._[0] || path;

// This runs every time the file is loaded
findLocation(arg, function(err, location) {
    console.log(&apos;Server location: &apos; + location);
});

module.exports = findLocation;
</code></pre>
<p>This would be bad because then any time someone <code>require()</code>s this file to use the <code>findLocation()</code> function it&apos;ll print their own location to the command line. We need a way to determine if this file was called <em>directly</em> with <code>node index.js</code> and not by <code>require()</code>, so if it was called directly then we&apos;ll check the command line for arguments. This can be done by checking <code>require.main</code> against <code>module</code>, like this: <code>if (require.main === module) {...}</code>, which leaves us with:</p>
<pre><code class="language-javascript">var request = require(&apos;request&apos;);
var argv = require(&apos;yargs&apos;).argv;

var findLocation = function(ip, callback) {
    var path;
    if (typeof(ip) === &apos;function&apos; || !ip) path = &apos;json&apos;;
    else path = ip;
    
    request(&apos;http://ipinfo.io/&apos; + path, function(error, response, body) {
        var json = JSON.parse(body);
        callback(null, json.city + &apos;, &apos; + json.region);
    });
};

if (require.main === module) {
    findLocation(argv._[0], function(err, location) {
        console.log(&apos;Server location: &apos; + location);
    });
}

module.exports = findLocation;
</code></pre>
<p>Now we can use this code both on the command line <em>and</em> as a dependency.</p>
<p><em>Note: There is a better way to do the CLI/library hybrid, but we&apos;ll keep it simple and stick with this method for now. See <a rel="nofollow" target="_blank" href="https://github.com/scottwrobinson/twentyjs">twenty</a> on Github for more info, specifically the <code>bin</code> directory and <code>package.json</code> settings.</em></p>
<h4 id="publishingyourpackage">Publishing your package</h4>
<p>Finally, we&apos;ll want to make it available to others on npm. All you need to do to make the package available is run this in the package directory:</p>
<pre><code class="language-bash">$ npm publish
</code></pre>
<p>You&apos;ll be prompted for your username and password, and then the code is pushed to the registry.</p>
<p>Keep in mind that you&apos;ll either need to <a rel="nofollow" target="_blank" href="https://docs.npmjs.com/misc/scope">scope</a> your package or change its name since the name &apos;twenty&apos; is already taken by me.</p>
<h3 id="wheretogofromhere">Where to go from here</h3>
<p>With Node&apos;s popularity booming, there are <em>tons</em> of resources all over the internet. Here are a few of the most popular books and courses I&apos;ve come across, which will teach you a whole lot more than what I was able to show here:</p>
<ul>
<li><strong><a class="bos-link" rel="nofollow" target="_blank" href="http://stackabu.se/wes-bos-learn-node">Learn Node.js</a></strong> by Wes Bos</li>
<li><strong><a class="amazon-link" rel="nofollow" target="_blank" href="http://stackabu.se/node-js-web-dev-server-side-dev-node-10">Node.js Web Development: Server-side development with Node 10</a></strong></li>
<li><strong><a class="amazon-link" rel="nofollow" target="_blank" href="http://stackabu.se/node-js-design-patterns-master-best-practices">Node.js Design Patterns: Master best practices to build modular and scalable server-side web apps</a></strong></li>
<li><strong><a class="amazon-link" rel="nofollow" target="_blank" href="http://stackabu.se/beginning-node-js-book">Beginning Node.js</a></strong></li>
</ul>
<p>Or if you want to stick with some shorter tutorials, here are a few from Stack Abuse that might be helpful:</p>
<ul>
<li><a href="https://stackabuse.com/avoiding-callback-hell-in-node-js/">Avoiding Callback Hell in Node.js</a></li>
<li><a href="https://stackabuse.com/useful-node-packages-you-might-not-know-about/">Useful Node Packages You Might Not Know About</a></li>
<li><a href="https://stackabuse.com/es6-classes/">ES6 Classes</a></li>
<li><a href="https://stackabuse.com/run-periodic-tasks-in-node-with-node-cron/">Run Periodic Tasks in Node with node-cron</a></li>
</ul>
<p>Just keep in mind that the vast majority of what you learn will be from your own exploring of the language, tools, and packages. So while articles like this are nice for getting started, make sure you focus more on writing code than <em>reading about someone else writing code</em>. Experience trumps everything else.</p>
<h3 id="conclusion">Conclusion</h3>
<p>We only covered a small fraction of what Node and npm have to offer, so check out some of the resources I linked to above to find out more.</p>
<p>And I can&apos;t stress enough how important it is for you to get experience actually writing code. npm makes it really easy to browse packages and find their repositories. So find a package that&apos;s useful or interesting to you and see how it works underneath.</p>
<p><em>Are you a Node novice? What other Node topics do you want to learn about? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					