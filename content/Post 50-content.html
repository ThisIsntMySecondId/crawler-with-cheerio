
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p>Some of you Node veterans have probably heard of a few of these packages before, but I&apos;m hoping from this article you&apos;ll find some really useful ones that you&apos;d never heard of, like I did. I tend to forget there are so many packages out there, so I did some exploring and played around with a few. These are a few of my favorites.</p>
<h4 id="yargs">yargs</h4>
<p>The <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/yargs"><code>yargs</code></a> package is simple and straight-forward, and it&apos;ll save you from having to write boiler-plate code in all of your projects. It handles command line arguments for you, so the user can set flags and input any kind of data, including booleans, floating point numbers, and strings.</p>
<p><code>yargs</code> will even handle your &apos;usage&apos; output, so you can easily tell the user which options your program takes, including which ones are required.</p>
<pre><code class="language-javascript">var argv = require(&apos;yargs&apos;)
    .usage(&apos;Usage: $0 -x [num] -y [num]&apos;)
    .demand([&apos;x&apos;,&apos;y&apos;])
    .argv;
 
console.log(&apos;Pow(x, y):&apos;, Math.pow(argv.x, argv.y));
</code></pre>
<p>So using the code above, if we tried to run the script with just <code>node index.js -x 3</code>, then we&apos;d get this message:</p>
<pre><code class="language-text">Usage: index.js -x [num] -y [num]

Options:
  -x                                                                  [required]
  -y                                                                  [required]

Missing required argument: y
</code></pre>
<p><code>yargs</code> is nice enough to tell us what exactly we&apos;re missing in a nicely formatted message, and all we had to do was use the simple <code>.usage()</code> and <code>.demand()</code> methods. Just about every package could use this.</p>
<h4 id="toobusy">toobusy</h4>
<p>This was one of those packages that, admittedly, I wasn&apos;t too impressed with at first, but quickly realized how useful it could be.</p>
<p>It works by polling the Node event loop and tracks the &apos;lag&apos;, which is the time it takes for requests to be fulfilled. If the lag becomes too long, then <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/toobusy"><code>toobusy</code></a> will let you know, enabling you to return an HTTP 503 &quot;Service Unavailable&quot; code to the client.</p>
<p>This is important because the busier your server gets, the longer wait times become. This quickly becomes a compounding problem that will only get worse as time goes on. If you do nothing, the service will shut down (aka crash) for everyone. But if you stop the processing early to return HTTP 503 then at least some requests will get serviced. Better some than none, right?</p>
<p>You can use <code>toobusy</code> by installing it:</p>
<pre><code class="language-bash">npm install toobusy
</code></pre>
<p>And then integrating it with something like Express like this:</p>
<pre><code class="language-javascript">var toobusy = require(&apos;toobusy&apos;),
    express = require(&apos;express&apos;);
    
var app = express();
    
// Block requests if we get too busy 
app.use(function(req, res, next) {
	if (toobusy()) {
		res.send(503, &quot;Too many users!&quot;);
	} else {
		next();
	} 
});
  
var server = app.listen(3000);
  
process.on(&apos;SIGINT&apos;, function() {
	server.close();

	toobusy.shutdown();	// Allow us to exit normally

	process.exit();
});
</code></pre>
<p>It really doesn&apos;t take that much code, and even less configuration, so you could easily package this up in to a nice middleware and include it in all of your Express projects. Just make sure you&apos;re not cutting off any critical requests that have high priority or requests that contain critical data.</p>
<h4 id="chalk">chalk</h4>
<p>Like <code>toobusy</code>, I didn&apos;t really realize the usefulness of <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/chalk"><code>chalk</code></a> until I really took some time to think about it and where I&apos;ve seen it used. User interfaces on the command line are very hard to create since all you have to interact with the user is a window for displaying text and a single text input. So how do you get the important information to stand out? One of the best ways is to add formatting to your text. Express is a good example of this. From their output you can easily find the most important information right away, so you&apos;ll never miss something critical.</p>
<p>Here is the full list of all the different types of styling that <code>chalk</code> supports:</p>
<h5 id="modifiers">Modifiers</h5>
<ul>
<li><code>bold</code></li>
<li><code>underline</code></li>
<li><code>dim</code></li>
<li><code>reset</code></li>
<li><code>hidden</code></li>
<li><code>inverse</code></li>
<li><code>italic</code> <em>(not supported everywhere)</em></li>
<li><code>strikethrough</code> <em>(not supported everywhere)</em></li>
</ul>
<h5 id="colors">Colors</h5>
<ul>
<li><code>red</code></li>
<li><code>black</code></li>
<li><code>green</code></li>
<li><code>white</code></li>
<li><code>yellow</code></li>
<li><code>blue</code> <em>(on Windows a brighter version is used since the normal blue is illegible)</em></li>
<li><code>cyan</code></li>
<li><code>gray</code></li>
<li><code>magenta</code></li>
</ul>
<h5 id="backgroundcolors">Background colors</h5>
<ul>
<li><code>bgBlue</code></li>
<li><code>bgBlack</code></li>
<li><code>bgRed</code></li>
<li><code>bgGreen</code></li>
<li><code>bgCyan</code></li>
<li><code>bgYellow</code></li>
<li><code>bgWhite</code></li>
<li><code>bgMagenta</code></li>
</ul>
<p>While these are the only colors officially supported, any xterm-compliant terminal can use the full 8-bit color codes.</p>
<p>To format some text, you just have to pass the string through a function for coloring or formatting. So, if you wanted the user to see a critical error, you might want to add some formatting like this:</p>
<pre><code class="language-javascript">var chalk = require(&apos;chalk&apos;);

var str = chalk.red.bold(&apos;ERROR: &apos;) + chalk.bold(&apos;Everything just blew up...&apos;);
console.log(str);
</code></pre>
<h4 id="nodeinspector">node-inspector</h4>
<p>Good debuggers can be hard to find, especially ones with easy-to-use GUIs, which is why I&apos;m a big fan of <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/node-inspector">node-inspector</a>. This debugger works by showing you a web page GUI to debug your code. It has all the features of a standard debugger, like breakpoints, stepping in and out of code, and variable inspection. It also has some not-so-common features that are really useful, like CPU and heap profiling, network client request inspection, and the ability to live edit running code. This last feature is one of my favorites as it&apos;ll save you a lot of time.</p>
<p><img src="https://s3.amazonaws.com/stackabuse/media/inspector-screenshot.png" alt="node-inspector"></p>
<p>Note that Node Inspector is only compatible with Chrome and Opera since it uses the Blink Developer Tools, which is the JavaScript debugger interface you see and made compatible with Node.</p>
<p>For a long time I relied pretty heavily on using the console to output my debug information, which ended up taking a lot of time to add, edit, and remove my debug statements. Using the GUI has literally saved me hours of debugging time. Now, debuggers are nothing new, but some are much harder to use than others, and this is a good one to go with.</p>
<h4 id="terminalkit">terminal-kit</h4>
<p>If your Node app uses the command line for anything more than some simple text input/output then you should probably be using <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/terminal-kit">terminal-kit</a>. terminal-kit simplifies many of the aspects of interacting with the users so you can focus on building out the important stuff within your app. A few things terminal-kit does are:</p>
<ul>
<li>text styling (much like <code>chalk</code>)</li>
<li>editing the screen</li>
<li>progress bars</li>
<li>user input</li>
</ul>
<p>There are quite a few use cases that apply to terminal-kit. Like, for example, if you download something from the internet, then it would be helpful to show a progress bar to the user so they know the app hasn&apos;t just stalled. To show a dummy progress bar, you just have to do something like this:</p>
<pre><code class="language-javascript">var terminal = require( &apos;terminal-kit&apos; ).terminal;

var progressBar;
var progress = 0;

function updateProgress() {
    // Add random progress
    progress += Math.random() / 10;
    progressBar.update(progress);
    
    // Check if we&apos;re done
    if (progress &gt;= 1) {
        setTimeout(function() {
        	terminal(&apos;\n&apos;);
        	process.exit();
        }, 250);
    }
    else {
        setTimeout(updateProgress, 100 + Math.random() * 500);
    }
}

progressBar = terminal.progressBar({
    width: 80,
    title: &apos;Downloading file:&apos;,
    eta: true,
    percent: true
});

updateProgress();
</code></pre>
<p>The code above will produce something like this, which was taken from the terminal-kit README:</p>
<p><img src="https://raw.githubusercontent.com/cronvel/terminal-kit/master/sample/progress-bar-doc1.gif" alt="terminal-kit progress bar"></p>
<h4 id="validator">validator</h4>
<p>The <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/validator"><code>validator</code></a> package helps out with a bunch of common string validations (like email addresses, credit cards, IP addresses, etc). A package like this is essential for whenever you get input from a user. You can almost think of your user as the biggest threat to your product. They&apos;ll make mistakes and input some really weird things in to text boxes, so you need a proven package to validate the input for you and avoid data corruption or server crashes.</p>
<p>A few of the most useful validators are:</p>
<ul>
<li><code>isEmail(str [, options])</code></li>
<li><code>isIP(str [, version])</code></li>
<li><code>isMobilePhone(str, locale)</code></li>
<li><code>isURL(str [, options])</code></li>
</ul>
<p><code>validator</code> also has sanitizers, which can normalize, remove, or escape your input strings. For example, you might want to sanitize a user&apos;s comment to avoid them inputting malicious HTML/JavaScript. This is one of the most common use cases since it is so easy for an attacker to create a bot to test out this attack for them on thousands of sites.</p>
<p>Some useful sanitizers provided by <code>validator</code> are:</p>
<ul>
<li><code>blacklist(input, chars)</code></li>
<li><code>escape(input)</code></li>
<li><code>normalizeEmail(email [, options])</code></li>
<li><code>whitelist(input, chars)</code></li>
</ul>
<p>The <code>normalizeEmail()</code> method is an interesting one. It will ensure an email address is lowercase, and it even removes ignored characters from the username of GMail addresses. So, if you had the email <code><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="bdceded2c9c993cfd2dfd4d3ced2d396d9d8cfcdc4fddad2d2dad1d8d0dcd4d193ded2d0">[email&#xA0;protected]</a></code>, <code>normalizeEmail()</code> will normalize it to <code><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="493a2a263d3d3b262b20273a2627092e24282025672a2624">[email&#xA0;protected]</a></code> since GMail ignores dots (<code>.</code>) and tags.</p>
<h4 id="formidable">formidable</h4>
<p>One of the more difficult tasks I&apos;ve tackled in the past was handling file uploads, which is why <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/formidable"><code>formidable</code></a> made the list. <code>formidable</code> handles every part of the upload, including the multi-part parser, writing files to disk, and error handling. Even though many webapps don&apos;t allow the user to upload huge images and videos, many do allow profile pictures, which means you have to handle receiving the image, validate it, and write it to disk, which may not be an easy task depending on your constraints.</p>
<p>This is one of those packages that I&apos;m a big fan of because I really don&apos;t want to re-invent the wheel. It does one job, and it does it really well.</p>
<p>Here is an example using <code>formidable</code> with just a plain HTTP server, modified from an example given in the package itself:</p>
<pre><code class="language-javascript">var http = require(&apos;http&apos;);
var util = require(&apos;util&apos;);
var formidable = require(&apos;formidable&apos;);
var path = require(&apos;path&apos;);

var PORT = 8080;

var root = path.join(__dirname, &apos;../&apos;);
exports.dir = {
	root    : root,
	lib     : root + &apos;/lib&apos;,
	fixture : root + &apos;/test/fixture&apos;,
	tmp     : root + &apos;/test/tmp&apos;,
};

var server = http.createServer(function(req, res) {
  if (req.url == &apos;/&apos;) {
    res.writeHead(200, {&apos;content-type&apos;: &apos;text/html&apos;});
    res.end(
      &apos;&lt;form action=&quot;/post&quot; method=&quot;post&quot;&gt;&apos; +
      &apos;&lt;input type=&quot;text&quot; name=&quot;title&quot;&gt;&lt;br&gt;&apos; +
      &apos;&lt;input type=&quot;text&quot; name=&quot;data[foo][]&quot;&gt;&lt;br&gt;&apos; +
      &apos;&lt;input type=&quot;submit&quot; value=&quot;Submit&quot;&gt;&apos; +
      &apos;&lt;/form&gt;&apos;
    );
  } else if (req.url == &apos;/post&apos;) {
    var form = new formidable.IncomingForm(),
        fields = [];

    form
      .on(&apos;error&apos;, function(err) {
        res.writeHead(200, {&apos;content-type&apos;: &apos;text/plain&apos;});
        res.end(&apos;error:\n\n&apos; + util.inspect(err));
      })
      .on(&apos;field&apos;, function(field, value) {
        console.log(field, value);
        fields.push([field, value]);
      })
      .on(&apos;end&apos;, function() {
        console.log(&apos;-&gt; post done&apos;);
        res.writeHead(200, {&apos;content-type&apos;: &apos;text/plain&apos;});
        res.end(&apos;received fields:\n\n &apos; + util.inspect(fields));
      });
    form.parse(req);
  } else {
    res.writeHead(404, {&apos;content-type&apos;: &apos;text/plain&apos;});
    res.end(&apos;404&apos;);
  }
});

server.listen(PORT);

console.log(&apos;listening on http://localhost:&apos; + PORT + &apos;/&apos;);
</code></pre>
<h4 id="shelljs">shelljs</h4>
<p><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/shelljs"><code>shelljs</code></a> is a package that lets you use common Unix commands on any system, whether it&apos;s Windows, Linux, or Mac. This way you don&apos;t need to write both bash <em>and</em> batch scripts for your projects. <code>shelljs</code> gives you the Unix-like environment to work with, so if you write scripts to run tests, commit changes, or launch on a server, you only have to write it once.</p>
<p>You can do things like act on command output:</p>
<pre><code class="language-javascript">require(&apos;shelljs/global&apos;);

ls(&apos;*.js&apos;).forEach(function(file) {
	sed(&apos;-i&apos;, &apos;BUILD_VERSION&apos;, &apos;v2.0.3&apos;, file);
	sed(&apos;-i&apos;, /.*REMOVE_THIS_LINE.*\n/, &apos;&apos;, file);
	sed(&apos;-i&apos;, /.*REPLACE_THIS_LINE.*\n/, cat(&apos;macro.js&apos;), file);
});
</code></pre>
<p>Execute common commands:</p>
<pre><code class="language-javascript">require(&apos;shelljs/global&apos;);

mkdir(&apos;-p&apos;, &apos;release/data&apos;);
cp(&apos;-R&apos;, &apos;data/*&apos;, &apos;release/data&apos;);
</code></pre>
<p>Check for available binaries:</p>
<pre><code class="language-javascript">require(&apos;shelljs/global&apos;);

if (!which(&apos;git&apos;)) {
	echo(&apos;This script requires git!&apos;);
	exit(1);
}
</code></pre>
<p>And even run native commands like you would in an actual bash/batch script:</p>
<pre><code class="language-javascript">if (exec(&apos;git commit -am &quot;Release commit&quot;&apos;).code !== 0) {
  echo(&apos;Error: Git commit failed!&apos;);
  exit(1);
}
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>Hopefully from this article you found some useful tools that you&apos;ve never heard of, or maybe realized how useful some of these packages can be. A quick search or just browsing some open source projects can result in some good findings, so keep your eyes open. There are 190,000+ (as of 10/1/15) packages on npmjs.com, so whatever you&apos;re looking for is probably there.</p>
<p><em>What is your favorite &apos;unknown&apos; package? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					