
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>The package.json file is the heart of all npm packages and no matter what you might have in your project, one thing is for sure: there will always be a package.json file. Out of the many things contained within the <a rel="nofollow" target="_blank" href="https://docs.npmjs.com/files/package.json">package.json file</a>, dependency management is what we are going to talk about today.</p>
<p>I think that most developers would agree that managing dependencies was never an easy task in the early days of high-level languages like JavaScript. Just ask a Java developer what life was like before Maven came along; or maybe talk to a Ruby developer about how much of an impact RubyGems has on their development. The same goes for Node.js developers. Without the dependency management of npm and package.json, life wouldn&apos;t be nearly as easy.</p>
<h3 id="caretsandtildesinversionnumbers">Carets and Tildes in Version Numbers</h3>
<p>Within every npm-backed project, the dependencies are tracked and managed inside a package.json file via the &quot;dependencies&quot; JSON property. Normally these dependencies are pretty straight-forward and easy to understand. Although occasionally you may notice changes to the version numbers after using the <code>--save</code> option in package.json. Looking in to it further, you might see that the version numbers now have an unfamiliar character before them.</p>
<p>Specifically, you could end up with something like:</p>
<pre><code class="language-json">&quot;dependencies&quot;: {
  &quot;express&quot;: &quot;^3.9.2&quot;
}
</code></pre>
<p>Or another possibility is:</p>
<pre><code class="language-json">&quot;dependencies&quot;: {
  &quot;express&quot;: &quot;~3.9.2&quot;
}
</code></pre>
<p>So what is this &quot;^&quot; (or the &quot;~&quot;) character actually doing up there? Don&apos;t worry, it&apos;s pretty simple, and I&apos;ll attempt to explain.</p>
<p>The characters that come before the version numbers are prefixes that we refer to as the caret (^) and tilde (~) signs. They are added by developers (or npm) to <strong>indicate restrictions on which software version numbers can be used as a dependency in your project</strong>. These number schemes follow a convention known as <a rel="nofollow" target="_blank" href="http://semver.org/">semantic versioning</a>, or semver. The actual numbers themselves are used in the versioning system to indicate the type of changes made that led to the version number getting incremented. The numbers (3, 9, and 2) in the above example are referred to as the major, minor and patch numbers from left to right.</p>
<p>Before we get in to the details of what the caret and tilde actually mean, let&apos;s see a quick overview of when each of the digits in the version numbers should be incremented as changes are made to a codebase:</p>
<ol>
<li>Backward incompatible changes increment the <strong>major</strong> digit.</li>
<li>If a new (backward compatible) change is made, then the <strong>minor</strong> digit gets incremented.</li>
<li>Simple bug fixes (to existing functionality) increments the <strong>patch</strong> digit.</li>
</ol>
<p>When you execute <code>npm install</code> in a clean project directory, the <em>highest</em> available software version for a dependency that is able to satisfy the version number given in package.json is installed for each dependency. So if no ^ or ~ is given, then the exact version number given is used.</p>
<p>However, by not specifying the precise dependency version in the package.json file and using the caret (^) or the tilde (~) signs, npm allows you to widen the accepted version range. When the <code>--save</code> flag is used, the default functionality is to prefix the version with the caret sign. And of course, this can be configured with the <a href="/the-ultimate-guide-to-configuring-npm/#saveprefix">save-prefix parameter</a>.</p>
<p>With respect to semantic versioning, here is a breakdown of what codebase changes the caret sign will allow:</p>
<p><strong>Caret</strong> allows...</p>
<ul>
<li>Backward compatible new functionalities</li>
<li>Large internal refactor</li>
<li>Bug fixes</li>
<li>Deprecation of old functionality (which is still operational)</li>
</ul>
<p>With the caret you can get releases like 3.<em>.</em>, where the * characters will match the <em>highest</em> version number available. So changes in the major digit, like 4.0.0, will not be used in this case.</p>
<p><strong>Tilde</strong> allows...</p>
<ul>
<li>Bug fixes</li>
</ul>
<p>With tilde, you can get releases like 3.9.*. Only the latest bug fixes are allowed with the tilde.</p>
<p>So we can conclude by saying that the ~ character should be used if you want to <em>lock in</em> the patch number. You should use this when you are ready to accept only bug-fixes and don&apos;t want to face any possibly incompatible alterations. On the other hand, the ^ character is responsible for locking in the patch <em>and</em> the minor version numbers. It&apos;s to be used when you want to have backwards compatible new functionality as well as bug fixes.</p>
<h3 id="conclusion">Conclusion</h3>
<p>Keep in mind that very old versions of npm do not support ^ so use it with caution. Although, any of the npm versions released in the last year or so should be fine. In most cases this shouldn&apos;t affect you.</p>
<p>There really isn&apos;t a &quot;best choice&quot; here since both have their uses in different scenarios. It all depends on your project requirements and personal preference. Just being aware of this feature in semver and npm can save you a lot of headaches, as well as some time by keeping you from having to constantly manually update your dependency versions.</p>
<p><em>Do you have a preferred choice? Have you had a bad experience with using either the caret or the tilde? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					