
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>One of the best ways to exchange information between applications written in different languages is to use the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/JSON">JSON</a> (JavaScript Object Notation) format. Thanks to its uniformity and simplicity, JSON has almost completely replaced XML as the standard data exchange format in software, particularly in web services.</p>
<p>Given the extensive use of JSON in software applications, and especially JavaScript-based applications, it is important to know how to read and write JSON data to a file in Node.js. In this article we&apos;ll explain how to perform these functions.</p>
<h3 id="readingajsonfile">Reading a JSON File</h3>
<p>Let&apos;s first see how we can <a href="/read-files-with-node-js/">read a file</a> that has already been created. But before we do that we need to actually create the file. Open a new window in your favorite text editor and add the following text to it:</p>
<pre><code class="language-javascript">{ 
    &quot;name&quot;: &quot;Sara&quot;,
    &quot;age&quot;: 23,
    &quot;gender&quot;: &quot;Female&quot;,
    &quot;department&quot;: &quot;History&quot;,
    &quot;car&quot;: &quot;Honda&quot;
}
</code></pre>
<p>Now save this file as &quot;student.json&quot; to your project directory.</p>
<p>To read the JSON data from the file we can use the Node.js <a rel="nofollow" target="_blank" href="https://nodejs.org/api/fs.html">fs</a> module. There are two functions available in this module that we can use to read files from the file system: <code>readFile</code> and <code>readFileSync</code>.</p>
<p>Although both of these functions perform similar tasks i.e. reading files from disk, the difference lies in the way these functions are actually executed, which we&apos;ll explain in more detail in the sections below.</p>
<h5 id="usingfsreadfilesync">Using fs.readFileSync</h5>
<p>The <code>readFileSync</code> function reads data from a file in a synchronous manner. This function blocks the rest of the code from executing until all the data is read from a file. The function is particularly useful when your application has to load configuration settings before it can perform any other tasks.</p>
<p>To continue with our example, let&apos;s use this function to read the &quot;student.json&quot; file that we created earlier, using the <code>readFileSync</code> function. Add the following code to a &apos;.js&apos; file:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const fs = require(&apos;fs&apos;);

let rawdata = fs.readFileSync(&apos;student.json&apos;);
let student = JSON.parse(rawdata);
console.log(student);
</code></pre>
<p>In the above Node.js code we first load the <code>fs</code> module to our application. Next we use the <code>readFileSync</code> function and pass it the relative file path to the file that we want to read. If you print the object <code>rawdata</code> to the console, you will see raw data (in a <a rel="nofollow" target="_blank" href="https://nodejs.org/api/buffer.html">Buffer</a>) on the console screen:</p>
<pre><code class="language-javascript">&lt;Buffer 7b 20 0a 20 20 20 20 22 6e 61 6d 65 22 3a 20 22 53 61 72 61 22 2c 0a 20 20 20 20 22 61 67 65 22 3a 20 32 33 2c 0a 20 20 20 20 22 67 65 6e 64 65 72 22 ... &gt;
</code></pre>
<p>However, we want to read the file in its JSON format, not the raw hex data. This is where the <code>JSON.parse</code> function comes into play. This function handles parsing the raw data, converts it to ASCII text, and parses the actual JSON data in to a JavaScript object. Now, if you print the <code>student</code> object on the console, you will get the following output:</p>
<pre><code class="language-javascript">{ name: &apos;Sara&apos;,
  age: 23,
  gender: &apos;Female&apos;,
  department: &apos;History&apos;,
  car: &apos;Honda&apos; }
</code></pre>
<p>As you can see, the JSON from our file was successfully loaded in to the <code>student</code> object.</p>
<h5 id="usingfsreadfile">Using fs.readFile</h5>
<p>Another way you can read a JSON file in Node.js is using the <code>readFile</code> function. Unlike <code>readFileSync</code> function, the <code>readFile</code> function reads file data in an asynchronous manner. When a <code>readFile</code> function is called, the file reading process starts and immediately the control shifts to next line executing the remaining lines of code. Once the file data has been loaded, this function will call the callback function provided to it. This way you aren&apos;t blocking code execution while waiting for the operating system to get back to you with data.</p>
<p>In our example, the <code>readFile</code> function takes two parameters: The path to the file that is to be read and the callback function that is to be called when the file is read completely. You can optionally also include a parameter with options, but we won&apos;t be covering those in this article.</p>
<p>Take a look at the following example to understand how to use the <code>readFile</code> function.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const fs = require(&apos;fs&apos;);

fs.readFile(&apos;student.json&apos;, (err, data) =&gt; {
    if (err) throw err;
    let student = JSON.parse(data);
    console.log(student);
});

console.log(&apos;This is after the read call&apos;);
</code></pre>
<p>The code above does exactly what our previous code snippet did (with an extra <code>console.log</code> call), but it does so asynchronously. Here are a few of the differences, which you may have noticed:</p>
<ul>
<li><code>(err, data) =&gt; {}</code>: This is our callback function that is executed once the file is completely read</li>
<li><code>err</code>: Since we can&apos;t easily use try/catch with asynchronous code, the function instead gives us an <code>err</code> object if something goes wrong. It is <code>null</code> if there were no errors</li>
</ul>
<p>You may have also noticed that we print a string to the console immediately after calling <code>readFile</code>. This is to show you the behavior of asynchronous code. When the above script is executed, you will see that this <code>console.log</code> executes <em>before</em> the <code>readFile</code> callback function executes. This is because <code>readFile</code> does not block code from executing while it reads data from the file system.</p>
<p>The output of the code will look like this:</p>
<pre><code class="language-text">This is after the read call
{ name: &apos;Sara&apos;,
  age: 23,
  gender: &apos;Female&apos;,
  department: &apos;History&apos;,
  car: &apos;Honda&apos; }
</code></pre>
<p>As you can see, the last line of code in our file is actually the one that shows up first in the output.</p>
<h5 id="usingrequire">Using <code>require</code></h5>
<p>Another approach is to use the global <code>require</code> method to read and parse JSON files. This is the same method you use to load Node modules, but it can also be used to load JSON.</p>
<p>Take a look at the following example.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

let jsonData = require(&apos;./student.json&apos;);

console.log(jsonData);
</code></pre>
<p>It works exactly like the <code>readFileSync</code> code we showed above, but it is a globally available method that you can use anywhere, which has its advantages.</p>
<p>However there are a few drawbacks of <code>require</code> function:</p>
<ul>
<li>Require is synchronous function and is called only once, which means the calls receive a cached result. If the file is updated you can&apos;t re-read it using this method</li>
<li>Your file must have &apos;.json&apos; extension, so it can&apos;t be as flexible. Without the proper extension <code>require</code> doesn&apos;t treat the file as JSON file.</li>
</ul>
<h3 id="writingjsontoafile">Writing JSON to a File</h3>
<p>Similar to the <code>readFile</code> and <code>readFileSync</code> functions, there are two functions for <a href="/writing-to-files-in-node-js/">writing data to files</a>: <code>writeFile</code> and <code>writeFileSync</code>. As the names suggest, the <code>writeFile</code> method writes data to a file in an asynchronous way while <code>writeFileSync</code> function writes data to a file in a synchronous manner.</p>
<p>We&apos;ll take a closer look in the following sections.</p>
<h5 id="usingfswritefilesync">Using fs.writeFileSync</h5>
<p>The <code>writeFileSync</code> function accepts 2-3 parameters: The path of the file to write data to, the data to write, and an optional parameter.</p>
<p>Note that if the file doesn&apos;t already exist, then a new file is created for you. Take a look at the following example:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const fs = require(&apos;fs&apos;);

let student = { 
    name: &apos;Mike&apos;,
    age: 23, 
    gender: &apos;Male&apos;,
    department: &apos;English&apos;,
    car: &apos;Honda&apos; 
};
 
let data = JSON.stringify(student);
fs.writeFileSync(&apos;student-2.json&apos;, data);
</code></pre>
<p>In the above example we are storing our JSON object <code>student</code> to a file named &quot;student-2.json&quot;. Notice that here we have to use the <code>JSON.stringify</code> function before saving the data. Just like we needed to parse the data into JSON format when we read the JSON file, we need to &quot;stringify&quot; the data before we can store it in a string form in the file.</p>
<p>Execute the above code and open the &quot;student-2.json&quot; file. You should see following content in the file:</p>
<pre><code class="language-javascript">{&quot;name&quot;:&quot;Mike&quot;,&quot;age&quot;:23,&quot;gender&quot;:&quot;Male&quot;,&quot;department&quot;:&quot;English&quot;,&quot;car&quot;:&quot;Honda&quot;}
</code></pre>
<p>Although this is the data that we wanted to write, the data is in the form of one line of string, which is difficult for us to read. If you&apos;d like the serialized JSON to be human readable, then change the <code>JSON.Stringify</code> function as follows:</p>
<pre><code class="language-javascript">let data = JSON.stringify(student, null, 2);
</code></pre>
<p>Here we are telling the method to add newlines and a couple of indentations to the serialized JSON. Now if you open the &quot;student-2.json&quot; file, you should see the text in following format.</p>
<pre><code class="language-javascript">{
  &quot;name&quot;: &quot;Mike&quot;,
  &quot;age&quot;: 23,
  &quot;gender&quot;: &quot;Male&quot;,
  &quot;department&quot;: &quot;English&quot;,
  &quot;car&quot;: &quot;Honda&quot;
}
</code></pre>
<h5 id="usingfswritefile">Using fs.writeFile</h5>
<p>As I mentioned earlier, the <code>writeFile</code> function executes in asynchronous manner, which means our code is not blocked while data is written <em>to</em> the file. And just like the asynchronous methods from before, we need to pass a callback to this function.</p>
<p>Let&apos;s write another file, &quot;student-3.json&quot;, using the <code>writeFile</code> function.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const fs = require(&apos;fs&apos;);

let student = { 
    name: &apos;Mike&apos;,
    age: 23, 
    gender: &apos;Male&apos;,
    department: &apos;English&apos;,
    car: &apos;Honda&apos; 
};
 
let data = JSON.stringify(student, null, 2);

fs.writeFile(&apos;student-3.json&apos;, data, (err) =&gt; {
    if (err) throw err;
    console.log(&apos;Data written to file&apos;);
});

console.log(&apos;This is after the write call&apos;);
</code></pre>
<p>The output of the above script will be:</p>
<pre><code class="language-text">This is after the write call
Data written to file
</code></pre>
<p>And again, you can see that the last line of our code actually shows up first in the console since our callback hasn&apos;t been called yet. This ends up saving quite a bit of execution time if you have large amounts of data to write to your file, or if you have quite a few files to write to.</p>
<h3 id="learnmore">Learn More</h3>
<p>Want to learn more about the fundamentals of Node.js? Personally, I&apos;d recommend taking an online course like <a class="bos-link" href="https://stackabu.se/wes-bos-learn-node">Learn Node.js by Wes Bos</a>. Not only will you learn the most up-to-date ES2017 syntax, but you&apos;ll get to build a full stack restaurant app. In my experience, building real-world apps like this is the fastest way to learn.</p>
<h3 id="conclusion">Conclusion</h3>
<p>Throughout this article we showed how you can read and write JSON data from and to files, which is a very common and important task to know how to do as a web programmer.</p>
<p>There are a couple of methods in the <code>fs</code> module both for reading from and writing to JSON files. The <code>readFile</code> and <code>readFileSync</code> functions will read JSON data from the file in an asynchronous and synchronous manner, respectively. You can also use the global <code>require</code> method to handle reading/parsing JSON data from a file in a single line of code. However, <code>require</code> is synchronous and can only read JSON data from files with &apos;.json&apos; extension.</p>
<p>Similarly, the <code>writeFile</code> and <code>writeFileSync</code> functions from the <code>fs</code> module write JSON data to the file in an asynchronous and synchronous manner respectively.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					