
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="introduction">Introduction</h3>
<p>Do you ever wonder what&apos;s going on within all of the Express.js middleware that you&apos;re adding to your webapp? It&apos;s actually pretty impressive what kind of functionality you can add to your apps with just one line of code, or a few:</p>
<pre><code class="language-javascript">// requires...

var app = express();

app.use(&quot;/static&quot;, express.static(__dirname + &quot;/public&quot;));
app.use(cookieParser(&apos;sekret&apos;));
app.use(compress());
</code></pre>
<p>The last three lines above handle quite a bit of the webapp functionality for us. The first <code>app.use()</code> call tells Express where our static files are located and how to expose them, the <code>cookieParser(&apos;sekret&apos;)</code> middleware handles all cookie parsing (with encryption), and the last one automatically gzip compresses all of our HTTP body data. Not bad for just three lines of code.</p>
<p>These middleware are pretty typical in your average webapp, but you can find some that do more than just the standard data compression or cookie parsing. Take these for example:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://github.com/helmetjs/helmet">helmet</a>: Helps secure your app by setting various HTTP headers</li>
<li><a rel="nofollow" target="_blank" href="https://github.com/jamiesteven/express-simple-cdn">express-simple-cdn</a>: Easily use a CDN for your static assets</li>
<li><a rel="nofollow" target="_blank" href="https://github.com/coderaiser/join-io">join-io</a>: Join files on the fly to reduce HTTP requests count</li>
<li><a href="/adding-authentication-to-express-with-passport">passport</a>: Adds user authentication to selected routes</li>
</ul>
<p>And <a rel="nofollow" target="_blank" href="https://expressjs.com/resources/middleware.html">here</a> is a much larger list of middleware that you might want to use.</p>
<p>Now that you&apos;ve seen some examples, here is just about everything you can do with it:</p>
<ul>
<li>Execute any code, including asynchronous code</li>
<li>Make changes or additions to the request and response objects</li>
<li>End the request-response cycle</li>
<li>Call the next middleware in the stack</li>
</ul>
<p>With the endless possibilities, I&apos;m sure you have some ideas of your own that you&apos;d like to create, so throughout the rest of this article I&apos;ll be showing you how to write your own middleware. There are a few different types of middleware you can write (application, router, error-handling, etc), but in this article we&apos;ll focus only on the application-level.</p>
<h3 id="thebasics">The Basics</h3>
<p>Middleware can be thought of almost as if it&apos;s an Express route. They take the same parameters and everything, but unlike the normal routes you aren&apos;t required to provide a URL path for the middleware. The two biggest differences are how the path is treated and when it is called.</p>
<p>The provided path is treated as a prefix, so if you had something like <code>app.use(&apos;/api&apos;, ...)</code>, then your middleware will run if <code>/api</code> is called <em>and</em> if <code>/api/users</code> is called. This is different from routes where the path must be an exact match.</p>
<p>The URL path can be omitted from the <code>app.use()</code> call if you want your code to run for all requests, otherwise you can specify a path and have your code only run when that route (and all of its sub-routes) is requested. For example, this might be useful for adding authentication to only a few given routes.</p>
<p>A simple middleware might look like this:</p>
<pre><code class="language-javascript">var app = express();

app.use(function(req, res, next) {
  console.log(&apos;Called URL:&apos;, req.url);
  next();
});
</code></pre>
<p>Whereas a route handler looks like this:</p>
<pre><code class="language-javascript">var app = express();
 
app.get(&apos;/&apos;, function(req, res, next) {
  res.send(&apos;Hey there...&apos;);
});
</code></pre>
<p>See? They&apos;re basically the same thing, so writing these functions should feel pretty familiar to you.</p>
<p>The parameters used are:</p>
<ul>
<li><code>req</code>: An object containing all of the relevant request information. This could be anything from the URL requested to the body of a POST request to the IP address of the user.</li>
<li><code>res</code>: This is the response object, which is used to send back data to the user for the given request. You might use this to send back an HTTP 404 response code, or to send back rendered HTML via <code>res.render()</code>.</li>
<li><code>next</code>: And finally, the <code>next</code> parameter is a callback to tell Express when our middleware has finished. If you do any IO (like database calls) or heavy computation then you&apos;ll likely need to make the function asynchronous to prevent blocking the main execution thread, in which case you&apos;ll have to use <code>next</code>.</li>
</ul>
<p>It&apos;s worth noting that if the your middleware does not end the request-response cycle with <code>res.end(...)</code> then you <strong>must</strong> call <code>next()</code> to pass control to the next middleware. If you don&apos;t then the request will be left hanging and will timeout.</p>
<h3 id="anexample">An Example</h3>
<p>In this example we&apos;ll be creating middleware that helps you automatically translate text between languages. This isn&apos;t a typical i18n module though, we&apos;ll be using Google Translate instead.</p>
<p>Let&apos;s say you&apos;ve made a chat app that lets you talk to people all over the world, and to make it seamless you need the text to be automatically translated. In this use-case most i18n modules wouldn&apos;t work since you need to pre-translate all of the strings, which we can&apos;t do since we&apos;re dealing with user input.</p>
<p>Sure, you could handle the translation in each of your Express routes, <em>or</em> you could have it handled for you in middleware, which keeps your route code cleaner and prevents you from forgetting to add translation to every route that needs it.</p>
<p>The strings (user messages) come in through a REST API, so we&apos;ll need to check all of the API routes&apos; bodies for text to translate. All strings being saved to the database in the POST calls will be kept in their native languages, but all strings being retrieved from the database with GET calls will be translated to the language specified in the HTTP Accept-Language header that accompanies the user request.</p>
<p>I figured we wouldn&apos;t make all the messages in the database the same language since we would then need to translate some of them twice, which <a rel="nofollow" target="_blank" href="https://www.quora.com/What-happens-when-you-use-Google-Translate-multiple-times-in-a-row-how-is-the-language-altered-or-affected">degrades the translation quality</a>.</p>
<p>First off, let&apos;s write a simple function to call the Google Translate API:</p>
<pre><code class="language-javascript">var googleTranslate = require(&apos;google-translate&apos;)(&apos;YOUR-API-KEY&apos;);

var translate = function(text, lang, cb) {
	googleTranslate.translate(text, lang, function(err, translation) {
		if (!translation) cb(err, null);
		cb(err, translation.translatedText);
	});
}
</code></pre>
<p>Then we&apos;ll use that function in our middleware code, which is exported in <code>modules.export</code> for use by the application.</p>
<pre><code class="language-javascript">module.exports = function(req, res, next) {
	if (req.method === &apos;GET&apos;) {
		var lang = &apos;en&apos;;
		var langs = req.acceptsLanguages();
		if (langs[0] !== &apos;*&apos;) {
			if (langs[0].length &gt; 2) {
				// ex: en-US
				lang = langs[0].substring(0, 2);
			} else {
				// ex: en
				lang = langs[0];
			}
		}

		if (lang !== res.body.lang) {
			return translate(res.body.message, lang, function(err, translation) {
				res.body.message = translation;
				res.body.lang = lang;
				next();
			});
		}
	}

	next();
};
</code></pre>
<p><em><strong>NOTE</strong>: This isn&apos;t really how you modify a <code>Response</code> body. I&apos;m just simplifying it for the sake of brevity. If you want to see how to actually modify the body, then check out the <a rel="nofollow" target="_blank" href="https://github.com/expressjs/compression/blob/master/index.js">compression</a> middleware, which does it properly. You have to proxy the <code>res.write</code> and <code>res.end</code> functions, which I didn&apos;t do because it would just be a distraction from the concepts I&apos;m trying to show here.</em></p>
<p>And finally, we apply the middleware to our app. Just make sure that you call the <code>app.use</code> function <em>after</em> you&apos;ve already declared your routes. The order in which it is called is the order in which each function runs.</p>
<p>Also, make sure you call <code>next()</code> in each of your <code>/api</code> routes, otherwise the middleware won&apos;t run.</p>
<pre><code class="language-javascript">var expressGoogleTranslate = require(&apos;my-translation-middleware&apos;);

var app = express();

app.get(&apos;/api/message&apos;, function(req, res, next) {...});
app.get(&apos;/api/message/all&apos;, function(req, res, next) {...});
app.post(&apos;/api/message&apos;, function(req, res, next) {...});
app.delete(&apos;/api/message/id&apos;, function(req, res, next) {...});

app.use(&apos;/api&apos;, expressGoogleTranslate);
</code></pre>
<p>And that&apos;s all there is to it. Any string returned in the <code>Response</code> body that is a different language than what the user accepts will be translated by Google Translate, which detects which language the source text is in.</p>
<p>So if our response started off looking like this...</p>
<pre><code class="language-json">{
	&quot;message&quot;: &quot;The quick brown fox jumps over the lazy dog&quot;
	&quot;lang&quot;: &quot;en&quot;
}
</code></pre>
<p>...and the user only accepts Swahili, then after the middleware runs we&apos;ll get a final translation that looks like this:</p>
<pre><code class="language-json">
{
	&quot;message&quot;: &quot;Haraka kahawia mbweha anaruka juu ya mbwa wavivu&quot;
	&quot;lang&quot;: &quot;sw&quot;
}
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>Although it may sounds intimidating, middleware is <em>really</em> easy to create in Express. You can use it for just about anything, no matter how simple or complex it is.</p>
<p>Just be sure to do a quick search on npm for whatever you&apos;re trying to make since tons of code is already out there. I&apos;m sure there is a package already out there that does what my translation code does (and probably much better too).</p>
<p><em>Do you have any ideas for middleware to create, or how to improve my example above? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					