
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>In this tutorial we will show you the various ways of how to exit Node.js programs. You need to understand first that Node.js works on a single thread or main process. You can spawn additional child processes to handle extra work. Exiting the main process lets us exit from Node.</p>
<p>While there are many ways to exit from Node, some ways are better than others for certain situations, like if you&apos;re running a REPL or not. We&apos;ll explain this in more detail throughout the article.</p>
<h3 id="lettingascriptexitimplicitly">Letting a Script Exit Implicitly</h3>
<p>Exiting from a Node.js program started at the command line can be as simple as waiting for the script to finish executing. Implicitly, the Node.js process will exit when it reaches the end of the script.</p>
<p>You can see this by running the following scipt:</p>
<pre><code class="language-javascript">// batman.js
console.log(&apos;Batman begins&apos;);

process.on(&apos;exit&apos;, function(code) {
    return console.log(`About to exit with code ${code}`);
});
</code></pre>
<p>Run the program with the command <code>node batman.js</code>, and you will see it output the first statement about Batman. In addition, the &quot;exit&quot; callback fires, resulting in a print out of the message about exiting, and an exit code. You should see something similar to the following:</p>
<pre><code class="language-sh">$ node batman.js 
Batman begins
About to exit with code 0
</code></pre>
<p>Note that pending events and loops will block program exit. Add this repeating function at the end of the above script.</p>
<pre><code class="language-javascript">// batman.js

// ...

setInterval((function() {
    return console.log(&apos;I\&apos;m Batman!&apos;);
}), 1000);
</code></pre>
<p>Run it again. This time, the program does not exit, because the repeating <code>setInterval</code> function blocks Node.js from exiting. Your output will look similar to this:</p>
<pre><code class="language-sh">$ node batman.js 
Batman begins
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
...
</code></pre>
<p>Where the &quot;I&apos;m Batman!&quot; phrase is continuously printed until you forcefully exit, like with <code>Ctrl + C</code>, or close your terminal.</p>
<h3 id="usingprocessexit">Using process.exit()</h3>
<p>We can exit Node.js programs using the explicit <code>process.exit</code> function call. The <code>process.exit</code> function exits from the current Node.js process. It takes an exit code, which is an integer.</p>
<p>The <code>process</code> object is a <a href="/using-global-variables-in-node-js/">global variable</a> that lets us manage the current Node.js process. Since it is a global, we can access it from anywhere inside a Node.js program without using <code>require</code> to import it.</p>
<p>Let us update the last program which does not exit because of the <code>setInterval</code> function. This time, we will force it to exit using a <code>timeout</code> after 5 seconds have elapsed. Add the following function to the same &apos;batman.js&apos; script from before.</p>
<pre><code class="language-javascript">// batman.js

// ...

setTimeout((function() {
    return process.exit(22);
}), 5000);
</code></pre>
<p>When you run <code>node batman.js</code>, you will notice that this time, the program runs for a limited time and then exits with an exit code of 22.</p>
<pre><code class="language-sh">node batman.js 
Batman begins
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
About to exit with code 22
</code></pre>
<p>Using <code>process.exit</code> works for exiting from the REPL (which we&apos;ll see later) as well as for terminating running Node.js programs or scripts.</p>
<p>Node.js interprets non-zero codes as failure, and an exit code of 0 as success.</p>
<h3 id="exitingnodejsusingprocesskill">Exiting Node.js using process.kill()</h3>
<p>We can also exit Node.js using <code>process.kill</code> to kill the running Node.js process. The difference between this and <code>process.exit</code> is that <code>process.kill</code> takes the <em>pid</em>, or process id, of the process we want to kill, as well as an optional <em>signal</em> that we want to send to the process. This means we can send signals to kill processes other than the main Node.js process. This is useful in highly concurrent applications with many running processes.</p>
<p>To kill the main Node process, we just pass the <em>pid</em> of the main process.</p>
<p>To see this in operation, replace the <code>setTimeout</code> function in our previous code example with this version that uses <code>process.kill</code>.</p>
<pre><code class="language-javascript">// batman.js

// ...

setTimeout((function() {
    return process.kill(process.pid);
}), 5000);
</code></pre>
<p>The program exits on schedule as before, after some of the same statements are printed. This time the <em>pid</em> is also printed to the console (yours may be different than 15):</p>
<pre><code class="language-sh">$ node batman.js 
Batman begins
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
Terminated: 15
</code></pre>
<p>This method also works in the REPL as well as in Node.js programs.</p>
<h3 id="howtoexitnodejsusingprocessabort">How to Exit Node.js using process.abort</h3>
<p>Likewise, we can use <code>process.abort</code> to exit Node.js. This method works in the REPL as well as scripts and applications.</p>
<p>The difference between <code>process.abort</code>, <code>process.kill</code>, and <code>process.exit</code> is that <code>process.abort</code> always exits Node.js immediately and generates a core file. In addition, no event callbacks will run.</p>
<p><strong>Note</strong>: The core file just mentioned is not created if <code>ulimit -c</code> is set to <code>0</code>. See <a rel="nofollow" target="_blank" href="https://www.cyberciti.biz/tips/linux-core-dumps.html">this tutorial</a> for more details.</p>
<p>To see it in action, replace the <code>process.kill</code> call in our <code>batman.js</code> file with a call to <code>process.abort</code> and run <code>node batman.js</code>.</p>
<pre><code class="language-javascript">// batman.js

// ...

setTimeout((function() {
    return process.abort();
}), 5000);
</code></pre>
<p>When you run the program with <code>node batman.js</code>, the program prints out the lines as before, but exits after the timeout has run, printing out some information about the state of the program when it aborted.</p>
<pre><code class="language-sh">$ node batman.js 
Batman begins
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
I&apos;m Batman!
Abort trap: 6
</code></pre>
<h3 id="therepl">The REPL</h3>
<p>Since the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop">REPL</a> (Read&#x2013;eval&#x2013;print loop) is a different type of execution environment from your typical Node.js script, these exit strategies deserve their own special sections. This is because the REPL is an interactive environment and does not just implicitly exit like a script would.</p>
<p>As we&apos;ve alredy mentioned in some of the previous sections, some of the above strategies will work in the REPL as well. However, it&apos;s not typical to use them in this case, and you should instead use the following methods.</p>
<h4 id="exitingusingakeycombination">Exiting using a Key Combination</h4>
<p>Unlike vim, exiting from the REPL is really simple. In a running REPL, you can exit it by using the key combination <code>Ctrl + C</code>, and keying it in twice. This sends the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/SIGINT_(POSIX)">SIGINT</a>, or interruption signal, to the REPL. This is commonly used for quitting programs in POSIX systems.</p>
<h4 id="usingtheexitcommand">Using the .exit Command</h4>
<p>We can also exit a Node.js REPL by using the command &quot;.exit&quot;. When you enter this in a running Node REPL, the current REPL will exit. This invocation works similar to the <code>Ctrl + C</code> method discussed above.</p>
<h3 id="conclusion">Conclusion</h3>
<p>In many cases, using <code>process.exit</code> will suffice for exiting Node.js. However, as we saw, there are numerous alternatives. The various methods give you flexibility to exit from any particular place in your code. You need to be careful with this extra power, however, not to insert dangerous code that can result in unplanned exits, crashing your programs.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					