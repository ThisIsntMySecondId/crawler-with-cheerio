
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><h3 id="whatarewebsockets">What are Websockets?</h3>
<p>Over the past few years, a new type of communication started to emerge on the web and in mobile apps, called <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/WebSocket">websockets</a>. This protocol has been long-awaited and was finally standardized by the IETF in 2011, paving the way for widespread use.</p>
<p>This new protocol opens up a much faster and more efficient line of communication to the client. Like HTTP, websockets run on top of a TCP connection, but they&apos;re much faster because we don&apos;t have to open a new connection for each time we want to send a message since the connection is kept alive for as long as the server or client wants.</p>
<p>Even better, since the connection never dies we finally have full-duplex communication available to us, meaning we can <em>push data to the client instead of having to wait for them to ask for data from the server</em>. This allows for data to be communicated back and forth, which is ideal for things like real-time chat applications, or even games.</p>
<h3 id="howdowebsocketswork">How do Websockets Work?</h3>
<p>At its core, a websocket is just a TCP connection that allows for full-duplex communication, meaning either side of the connection can send data to the other, even at the same time.</p>
<p>To establish this connection, the protocol actually initiates the handshake as a normal HTTP request, but then gets &apos;upgraded&apos; using the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/HTTP/1.1_Upgrade_header">upgrade request</a> HTTP header, like this:</p>
<pre><code class="language-http">GET /ws/chat HTTP/1.1
Host: chat.example.com
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: q1PZLMeDL4EwLkw4GGhADm==
Sec-WebSocket-Protocol: chat, superchat
Sec-WebSocket-Version: 15
Origin: http://example.com
</code></pre>
<p>The server then sends back an HTTP 101 &quot;Switching Protocols&quot; response, acknowledging that the connection is going to be upgraded. Once the this connection has been made, it switches to a bidirectional binary protocol, at which point the application data can be sent.</p>
<p>All the protocol has to do to keep the connection open is send some ping/pong packets, which tells the other side that they&apos;re still there. To close the connection, a simple &quot;close connection&quot; packet is sent.</p>
<h3 id="somewebsocketexamples">Some Websocket Examples</h3>
<p>Of the many different websocket libraries for Node.js available to us, I chose to use <a rel="nofollow" target="_blank" href="https://github.com/socketio/socket.io">socket.io</a> throughout this article because it seems to be the most popular and is, in my opinion, the easiest to use. While each library has its own unique API, they also have many similarities since they&apos;re all built on top of the same protocol, so hopefully you&apos;ll be able to translate the code below to any library you want to use.</p>
<p>For the HTTP server, I&apos;ll be using <a rel="nofollow" target="_blank" href="https://expressjs.com/">Express</a>, which is the most popular Node server out there. Keep in mind that you can also just use the plain <a rel="nofollow" target="_blank" href="https://nodejs.org/api/http.html">http</a> module if you don&apos;t need all of the features of Express. Although, since most applications will use Express, that&apos;s what we&apos;ll be using as well.</p>
<p><strong>Note</strong>: Throughout these examples I have removed much of the boilerplate code, so some of this code won&apos;t work out of the box. In most cases you can refer to the first example to get the boilerplate code.</p>
<h4 id="establishingtheconnection">Establishing the Connection</h4>
<p>In order for a connection to be established between the client and server, the server must do two things:</p>
<ol>
<li>Hook in to the HTTP server to handle websocket connections</li>
<li>Serve up the <code>socket.io.js</code> client library as a static resource</li>
</ol>
<p>In the code below, you can see item (1) being done on the 3rd line. Item (2) is done for you (by default) by the <code>socket.io</code> library and is served on the path <code>/socket.io/socket.io.js</code>. By default, all websocket connections and resources are served within the <code>/socket.io</code> path.</p>
<p><strong>Server</strong></p>
<pre><code class="language-javascript">var app = require(&apos;express&apos;)();
var server = require(&apos;http&apos;).Server(app);
var io = require(&apos;socket.io&apos;)(server);

app.get(&apos;/&apos;, function(req, res) {
    res.sendFile(__dirname + &apos;/index.html&apos;);
});

server.listen(8080);
</code></pre>
<p>The client needs to do two things as well:</p>
<ol>
<li>Load the library from the server</li>
<li>Call <code>.connect()</code> to the server address and websocket path</li>
</ol>
<p><strong>Client</strong></p>
<pre><code class="language-html">&lt;script src=&quot;/socket.io/socket.io.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
    var socket = io.connect(&apos;/&apos;);
&lt;/script&gt;
</code></pre>
<p>If you navigate your browser to <code>http://localhost:8080</code> and inspect the HTTP requests behind the scenes using your browser&apos;s developer tools, you should be able to see the handshake being executed, including the GET requests and resulting HTTP 101 Switching Protocols response.</p>
<h4 id="sendingdatafromservertoclient">Sending Data from Server to Client</h4>
<p>Okay, now on to some of the more interesting parts. In this example we&apos;ll be showing you the most common way to send data from the server to the client. In this case, we&apos;ll be sending a message to a channel, which can be subscribed to and received by the client. So, for example, a client application might be listening on the &apos;announcements&apos; channel, which would contain notifications about system-wide events, like when a user joins a chat room.</p>
<p>On the server this is done by waiting for the new connection to be established, then by calling the <code>socket.emit()</code> method to send a message to all connected clients.</p>
<p><strong>Server</strong></p>
<pre><code class="language-javascript">io.on(&apos;connection&apos;, function(socket) {
    socket.emit(&apos;announcements&apos;, { message: &apos;A new user has joined!&apos; });
});
</code></pre>
<p><strong>Client</strong></p>
<pre><code class="language-html">&lt;script src=&quot;/socket.io/socket.io.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
    var socket = io.connect(&apos;/&apos;);
    socket.on(&apos;announcements&apos;, function(data) {
        console.log(&apos;Got announcement:&apos;, data.message);
    });
&lt;/script&gt;
</code></pre>
<h4 id="sendingdatafromclienttoserver">Sending Data from Client to Server</h4>
<p>But what would we do when we want to send data the other way, from client to server? It is very similar to the last example, using both the <code>socket.emit()</code> and <code>socket.on()</code> methods.</p>
<p><strong>Server</strong></p>
<pre><code class="language-javascript">io.on(&apos;connection&apos;, function(socket) {
    socket.on(&apos;event&apos;, function(data) {
        console.log(&apos;A client sent us this dumb message:&apos;, data.message);
    });
});
</code></pre>
<p><strong>Client</strong></p>
<pre><code class="language-html">&lt;script src=&quot;/socket.io/socket.io.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
    var socket = io.connect(&apos;/&apos;);
    socket.emit(&apos;event&apos;, { message: &apos;Hey, I have an important message!&apos; });
&lt;/script&gt;
</code></pre>
<h4 id="countingconnectedusers">Counting Connected Users</h4>
<p>This is a nice example to learn since it shows a few more features of <code>socket.io</code> (like the <code>disconnect</code> event), it&apos;s easy to implement, and it is applicable to many webapps. We&apos;ll be using the <code>connection</code> and <code>disconnect</code> events to count the number of active users on our site, and we&apos;ll update all users with the current count.</p>
<p><strong>Server</strong></p>
<pre><code class="language-javascript">var numClients = 0;

io.on(&apos;connection&apos;, function(socket) {
    numClients++;
    io.emit(&apos;stats&apos;, { numClients: numClients });

    console.log(&apos;Connected clients:&apos;, numClients);

    socket.on(&apos;disconnect&apos;, function() {
        numClients--;
        io.emit(&apos;stats&apos;, { numClients: numClients });

        console.log(&apos;Connected clients:&apos;, numClients);
    });
});
</code></pre>
<p><strong>Client</strong></p>
<pre><code class="language-html">&lt;script src=&quot;/socket.io/socket.io.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
    var socket = io.connect(&apos;/&apos;);
    socket.on(&apos;stats&apos;, function(data) {
        console.log(&apos;Connected clients:&apos;, data.numClients);
    });
&lt;/script&gt;
</code></pre>
<p>A much simpler way to track the user count on the server would be to just use this:</p>
<pre><code class="language-javascript">var numClients = io.sockets.clients().length;
</code></pre>
<p>But apparently there are <a rel="nofollow" target="_blank" href="https://github.com/socketio/socket.io/issues/463">some issues</a> surrounding this, so you might have to keep track of the client count yourself.</p>
<h4 id="roomsandnamespaces">Rooms and Namespaces</h4>
<p>Chances are as your application grows in complexity, you&apos;ll need more customization with your websockets, like sending messages to a specific user or set of users. Or maybe you want need strict separation of logic between different parts of your app. This is where rooms and namespaces come in to play.</p>
<p><strong>Note</strong>: These features are not part of the websocket protocol, but added on top by <code>socket.io</code>.</p>
<p>By default, <code>socket.io</code> uses the root namespace (<code>/</code>) to send and receive data. Programmatically, you can access this namespace via <code>io.sockets</code>, although many of its methods have shortcuts on <code>io</code>. So these two calls are equivalent:</p>
<pre><code class="language-javascript">io.sockets.emit(&apos;stats&apos;, { data: &apos;some data&apos; });
io.emit(&apos;stats&apos;, { data: &apos;some data&apos; });
</code></pre>
<p>To create your own namespace, all you have to do is the following:</p>
<pre><code class="language-javascript">var iosa = io.of(&apos;/stackabuse&apos;);
iosa.on(&apos;connection&apos;, function(socket){
    console.log(&apos;Connected to Stack Abuse namespace&apos;):
});
iosa.emit(&apos;stats&apos;, { data: &apos;some data&apos; });
</code></pre>
<p>Also, the client must connect to your namespace explicitly:</p>
<pre><code class="language-html">&lt;script src=&quot;/socket.io/socket.io.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
    var socket = io(&apos;/stackabuse&apos;);
&lt;/script&gt;
</code></pre>
<p>Now any data sent within this namespace will be separate from the default <code>/</code> namespace, regardless of which channel is used.</p>
<p>Going even further, within each namespace you can join and leave &apos;rooms&apos;. These rooms provide another layer of separation on top of namespaces, and since a client can <em>only be added to a room on the server side</em>, they also provide some extra security. So if you want to make sure users aren&apos;t snooping on certain data, you can use a room to hide it.</p>
<p>To be added to a room, you must <code>.join()</code> it:</p>
<pre><code class="language-javascript">io.on(&apos;connection&apos;, function(socket){
    socket.join(&apos;private-message-room&apos;);
});
</code></pre>
<p>Then from there you can send messages to everyone belonging to the given room:</p>
<pre><code class="language-javascript">io.to(&apos;private-message-room&apos;).emit(&apos;some event&apos;);
</code></pre>
<p>And finally, call <code>.leave()</code> to stop getting event messages from a room:</p>
<pre><code class="language-javascript">socket.leave(&apos;private-message-room&apos;);
</code></pre>
<h3 id="conclusion">Conclusion</h3>
<p>This is just one library that implements the websockets protocol, and there are many more out there, all with their own unique features and strengths. I&apos;d advise trying out some of the others (like <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/websocket">node-websockets</a>) so you get a feel for what&apos;s out there.</p>
<p>Within just a few lines, you can create some pretty powerful applications, so I&apos;m curious to see what you can come up with!</p>
<p><em>Have some cool ideas, or already created some apps using websockets? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					