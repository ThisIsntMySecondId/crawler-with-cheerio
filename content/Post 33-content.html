
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>When you think of a database the first things that might come in to your head might be MySQL, MongoDB, or PostgreSQL. While these are all great choices for storing data, they&apos;re all over-powered for the majority of applications.</p>
<p>Consider a desktop chat application written with the <a rel="nofollow" target="_blank" href="https://electron.atom.io/">Electron</a> framework in JavaScript. While the chat data (messages, contacts, history, etc) would likely originate from an API server, it would need to be stored locally within the app as well. You could potentially have thousands of messages, all of which would need to be stored for easy access and searching.</p>
<p>So what do you do? One option is to store all of this data in a file somewhere and just search it every time you need to retrieve it, but this can be inefficient. Another option is to just not cache the data locally and make a call to the API sever each time you need more data, but then your app will be less responsive and will use up much more network data.</p>
<p>A better idea is to use an embedded/lightweight database, like <a rel="nofollow" target="_blank" href="https://github.com/louischatriot/nedb">NeDB</a>. This makes more sense because your app won&apos;t be serving thousands of users or handling gigabytes of data.</p>
<p>NeDB is much like <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/SQLite">SQLite</a> in that it is a smaller, embeddable version of a much larger database system. Instead of being a smaller SQL datastore, NeDB is a smaller NoSQL datastore that mimics <a rel="nofollow" target="_blank" href="https://www.mongodb.org/">MongoDB</a>.</p>
<p>A lightweight database usually stores its data either in memory or in a plain text file (with indexes for fast look-ups). This helps reduce the overall footprint of the database on the system, which is perfect for smaller applications. For comparison, the MySQL tar file (for Mac OSX) is 337MB, while NeDB (uncompressed, not minified) is only about 1.5MB.</p>
<p>One of the greatest things about NeDB specifically is that its API is a subset of the MongoDB API, so if you&apos;re familiar with MongoDB then you should have no problem working with NeDB after the initial setup.</p>
<p><strong>Note</strong>: As of v1.8.0, NeDB hasn&apos;t yet updated to some of Mongo&apos;s new method names, like <code>insertOne</code>, <code>insertMany</code> and the removal of <code>findOne</code>.</p>
<h3 id="gettingstartedwithnedb">Getting Started with NeDB</h3>
<p>First, install the module with NPM:</p>
<pre><code class="language-bash">$ npm install nedb --save
</code></pre>
<p>The module is written in pure JavaScript, so there shouldn&apos;t be any issues compiling native add-ons like there sometimes are with the MongoDB drivers.</p>
<p>If you&apos;re planning on using it in the browser instead, use Bower to install:</p>
<pre><code class="language-bash">$ bower install nedb
</code></pre>
<p>Like all database clients, the first step is to connect to the backend database. However, in this case there is no external application to connect to, so instead we just need to tell it the location of your data. With NeDB, you have a few options for saving your data. The first option is to save the data in memory:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var db = new Datastore();

// Start issuing commands right away...
</code></pre>
<p>This will start you out with no data, and when you quit the application all of the saved data will be lost. Although it&apos;s great for use during testing or shorter sessions (like in the browser).</p>
<p>Or the other option is to save the data to a file. The difference here is that you need to specify the file location and load the data.</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var db = new Datastore({ filename: &apos;path/to/your/file&apos; });

db.loadDatabase(function(err) {
    // Start issuing commands after callback...
});
</code></pre>
<p>If you don&apos;t want to call <code>db.loadDatabase</code> for each database you load, then you can always use the <code>autoload: true</code> option as well.</p>
<p>One important thing to note is that each file is the equivalent of one <a rel="nofollow" target="_blank" href="https://docs.mongodb.org/manual/reference/glossary/#term-collection">collection</a> in MongoDB. So if you have multiple collections, you&apos;ll need to load multiple files on startup. So your code might look like this:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore({ filename: &apos;users.db&apos;, autoload: true });
var tweets = new Datastore({ filename: &apos;tweets.db&apos;, autoload: true });
var messages = new Datastore({ filename: &apos;messages.db&apos;, autoload: true });
</code></pre>
<h3 id="savingdata">Saving Data</h3>
<p>After loading your data from files (or creating in-memory storage), you&apos;ll want to start saving data.</p>
<p>Much like the Mongo drivers, you&apos;ll use <code>insert</code> to create a new document:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

var scott = {
    name: &apos;Scott&apos;,
    twitter: &apos;@ScottWRobinson&apos;
};

users.insert(scott, function(err, doc) {
    console.log(&apos;Inserted&apos;, doc.name, &apos;with ID&apos;, doc._id);
});

// Prints to console...
// (Note that ID will likely be different each time)
//
// &quot;Inserted Scott with ID wt3Nb47axiOpme9u&quot;
</code></pre>
<p>This insertion can easily be extended to save multiple documents at once. Using the same method, just pass an array of objects and each one will be saved and returned to you in the callback:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

var people = [];

var scott = {
    name: &apos;Scott Robinson&apos;,
    age: 28,
    twitter: &apos;@ScottWRobinson&apos;
};

var elon = {
    name: &apos;Elon Musk&apos;,
    age: 44,
    twitter: &apos;@elonmusk&apos;
};

var jack = {
    name: &apos;Jack Dorsey&apos;,
    age: 39,
    twitter: &apos;@jack&apos;
};

people.push(scott, elon, jack);

users.insert(people, function(err, docs) {
    docs.forEach(function(d) {
        console.log(&apos;Saved user:&apos;, d.name);
    });
});

// Prints to console...
//
// Saved user: Scott Robinson
// Saved user: Elon Musk
// Saved user: Jack Dorsey
</code></pre>
<p>Updating existing documents works much the same, except that you&apos;ll need to provide a query to tell the system which document(s) needs to be updated.</p>
<h3 id="loadingdata">Loading Data</h3>
<p>Now that we have a bunch of data saved, it&apos;s time to retrieve it back from the database. Again, we&apos;ll follow the same convention as Mongo with the <code>find</code> method:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

// Save a bunch of user data here...

users.findOne({ twitter: &apos;@ScottWRobinson&apos; }, function(err, doc) {
    console.log(&apos;Found user:&apos;, doc.name);
});

// Prints to console...
//
// Found user: Scott Robinson
</code></pre>
<p>And again, we can use a similar operation to retrieve multiple documents. The returned data is just an array of matching documents:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

// Save a bunch of user data here...

users.find({ age: { $lt: 40 }}, function(err, docs) {
    docs.forEach(function(d) {
        console.log(&apos;Found user:&apos;, d.name);
    });
});

// Prints to console...
//
// Found user: Jack Dorsey
// Found user: Scott Robinson
</code></pre>
<p>You might have noticed from this last code example that NeDB, as you&apos;d expect, is capable of more complex queries, like number comparisons. The following operators are all available for finding/matching documents:</p>
<ul>
<li><code>$lt</code>, <code>$lte</code>: less than, less than or equal</li>
<li><code>$gt</code>, <code>$gte</code>: greater than, greater than or equal</li>
<li><code>$in</code>: value contained in array</li>
<li><code>$nin</code>: value not contained in array</li>
<li><code>$ne</code>: not equal</li>
<li><code>$exists</code>: checks for the existence (or non-existence) of a given property</li>
<li><code>$regex</code>: match a property&apos;s string with regex</li>
</ul>
<p>You can also use the standard sorting, limiting, and skipping operations. If a callback isn&apos;t given to the <code>find</code> method, then a <code>Cursor</code> object will be returned to you instead, which you can then use for sorting, limiting, and skipping. Here is an example of sorting alphabetically by name:</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

// Save a bunch of user data here...

users.find({}).sort({name: 1}).exec(function(err, docs) {
    docs.forEach(function(d) {
        console.log(&apos;Found user:&apos;, d.name);
    });
});

// Prints to console...
//
// Found user: Elon Musk
// Found user: Jack Dorsey
// Found user: Scott Robinson
</code></pre>
<p>The other two operations, skip and limit, work very similarly to this.</p>
<p>There are quite a few more operators supported by the <code>find</code> and <code>findOne</code> methods, but we won&apos;t go in to all of them here. You can read in detail about the rest of these operations in the <a rel="nofollow" target="_blank" href="https://github.com/louischatriot/nedb#finding-documents">finding documents</a> section of the README.</p>
<h3 id="deletingdata">Deleting Data</h3>
<p>There isn&apos;t much to say about deleting data other than that it works similar to the <code>find</code> methods. You&apos;ll be using the same types of queries to find the relevant document(s) in the database. Those that are found are then removed.</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

// Save a bunch of user data here...

users.remove({ name: { $regex: /^Scott/ } }, function(err, numDeleted) {
     console.log(&apos;Deleted&apos;, numDeleted, &apos;user(s)&apos;);
});

// Prints to console...
//
// Deleted 1 user(s)
</code></pre>
<p>By default, the <code>remove</code> method only removes a single document. In order to remove multiple documents with a single call, you must set the <code>multi</code> option to <code>true</code>.</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

// Save a bunch of user data here...

users.remove({}, { multi: true }, function(err, numDeleted) {
     console.log(&apos;Deleted&apos;, numDeleted, &apos;user(s)&apos;);
});

// Prints to console...
//
// Deleted 3 user(s)
</code></pre>
<h3 id="indexingdata">Indexing Data</h3>
<p>Just like any other database, you can set indexes on your data for faster retrieval or to enforce certain constraints, like unique values. To create the index, use the <code>ensureIndex</code> method.</p>
<p>The three types of indexes currently supported are:</p>
<ul>
<li><code>unique</code>: ensure that the given field is unique throughout the collection</li>
<li><code>sparse</code>: don&apos;t index documents in which the given field is not defined</li>
<li><code>expireAfterSeconds</code>: delete the document after the given number of seconds (<a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Time_to_live">time to live</a>, or TTL)</li>
</ul>
<p>The TTL index is especially useful, in my opinion, since it saves you from having to write code to frequently scan for and delete data that has expired.</p>
<p>This can be useful, for example, with password reset requests. If you have a <code>PasswordReset</code> object stored in your database, you wouldn&apos;t want it to be valid forever. To help protect the user, it should probably expire and be removed after a few days. This TTL index can handle deleting it for you.</p>
<p>In the following example, we&apos;ve placed the <code>unique</code> constraint on the documents&apos; Twitter handles. This means that if a user is saved with the same Twitter handle as another user, an error will be thrown.</p>
<pre><code class="language-javascript">var Datastore = require(&apos;nedb&apos;);
var users = new Datastore();

users.ensureIndex({ fieldName: &apos;twitter&apos;, unique: true });

var people = [];

var jack = {
    name: &apos;Jack Dorsey&apos;,
    age: 39,
    twitter: &apos;@jack&apos;
};

var jackSmith = {
    name: &apos;Jack Smith&apos;,
    age: 68,
    twitter: &apos;@jack&apos;
};

people.push(jack, jackSmith);

users.insert(people, function(err, docs) {
    console.log(&apos;Uh oh...&apos;, err);
});

// Prints to console...
//
// Uh oh... Can&apos;t insert key @jack, it violates the unique constraint
</code></pre>
<h3 id="takingitfurther">Taking it Further</h3>
<p>While the NeDB API is easy to use and everything, your code can become pretty difficult to work with if it&apos;s not well thought out and organized. This is where object document mappers (<a rel="nofollow" target="_blank" href="https://stackoverflow.com/questions/12261866/what-is-the-difference-between-an-orm-and-an-odm">which is like an ORM</a>) come in to play.</p>
<p>Using the <a href="https://www.npmjs.com/package/camo">Camo ODM</a> (which I created), you can simply treat NeDB datastores as JavaScript classes. This allows you to specify a schema, validate data, extend schemas, and more. Camo even works with MongoDB as well, so you can use NeDB in testing/development environments and then use Mongo for your production system without having to change any of your code.</p>
<p>Here is a quick example of connecting to the database, declaring a class object, and saving some data:</p>
<pre><code class="language-javascript">var connect = require(&apos;camo&apos;).connect;
var Document = require(&apos;camo&apos;).Document;

class User extends Document {
    constructor() {
        super();

        this.name = String;
        this.age = Number;
        this.twitter = Sring;
    }

    get firstName() {
        return this.name.split(&apos; &apos;)[0];
    }
}

var scott = User.create({
    name: &apos;Scott Robinson&apos;,
    age: 28,
    twitter: &apos;@ScottWRobinson&apos;
});

var elon = User.create({
    name: &apos;Elon Musk&apos;,
    age: 44,
    twitter: &apos;@elonmusk&apos;
});

connect(&apos;nedb://memory&apos;).then(function(db) {
    return Promise.all([scott.save(), elon.save()]);
}).then(function(users) {
    users.forEach(function(u) {
        console.log(&apos;Saved user:&apos;, u.firstName);
    });

    return elon.delete();
}).then(function() {
    console.log(&apos;Deleted Elon!&apos;)
});

// Prints to console...
//
// Saved user: Scott
// Saved user: Elon
// Deleted Elon!
</code></pre>
<p>There is a lot more to this ODM than what I&apos;ve shown here. For more info, check out <a href="https://stackabuse.com/getting-started-with-camo/">this article</a> or the <a rel="nofollow" target="_blank" href="https://github.com/scottwrobinson/camo">project&apos;s README</a> for the documentation.</p>
<h3 id="conclusion">Conclusion</h3>
<p>With NeDB being quite small (and quite fast!), it&apos;s very easy to add it to just about any project. And with Camo in the mix, you only need a few lines of code to declare class-based objects that are much easier to create, delete, and manipulate.</p>
<p><em>If you&apos;ve ever used NeDB in one of your projects, we&apos;d love to hear about it. Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					