
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#settingparameters">Setting Parameters</a></li>
<li><a href="#listofpossibleparameters">List of Possible Parameters</a>
<ul>
<li><a href="#accesscontrolauthorization">Access Control/Authorization</a></li>
<li><a href="#caching">Caching</a></li>
<li><a href="#general">General</a></li>
<li><a href="#development">Development</a></li>
<li><a href="#networking">Networking</a></li>
<li><a href="#registry">Registry</a></li>
</ul>
</li>
<li><a href="#conclusion">Conclusion</a></li>
</ul>
<h3 id="introduction">Introduction</h3>
<p>The Node Package Manager, or <a rel="nofollow" target="_blank" href="https://npmjs.com/">npm</a>, is one of the best parts about Node, in my opinion. Package management can really make or break a language, so ensuring that it is easy to use and flexible is extremely important.</p>
<p>Throughout my use of Node, I only ever knew the basic npm commands like <code>save</code>, <code>install</code>, and <code>publish</code>, and even then I didn&apos;t really know the optional parameters that went along with them. After reading some of the help documentation recently, I thought it would be helpful to write up details on as many of the npm configurations as possible. Not only do I think this could be helpful to the readers, but it was extremely helpful to me to look through all the different flags/parameters and to actually test them out. I ended up learning a lot about npm that will help me out a bunch in the future.</p>
<p>For the most part, I tried to write up a unique description of each parameter (different from the help docs). Hopefully that way if the help docs confuse you (or don&apos;t have enough information), my description will give some more insight in to whatever you&apos;re looking for. I&apos;ll also be adding examples of some of the more confusing parameters, so if you know how to use some of the more undocumented options, like <code>searchopts</code>, I&apos;d love to see an example!</p>
<h3 id="settingparameters">Setting Parameters</h3>
<p>Unless otherwise noted, all of the parameters below can be set through a few different methods, each of which I&apos;ll describe briefly here. Depending on your use-case, utilize the different purposed for things like testing, project-specific configuration, global configuration, etc.</p>
<h4 id="npmrcfiles">npmrc Files</h4>
<p>npm allows you to use a few different <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Configuration_file">rc files</a>, much like <code>~/.bashrc</code>, to set your configurations. The four locations where the files may reside are:</p>
<ul>
<li>Per-project config file: <code>/path/to/my/project/.npmrc</code></li>
<li>Per-user config file: <code>~/.npmrc</code></li>
<li>Global config file: <code>$PREFIX/npmrc</code></li>
<li>Built-in npm config file: <code>/path/to/npm/npmrc</code></li>
</ul>
<pre><code class="language-bash">https-proxy=proxy.example.com
init-license=MIT
init-author-url=http://stackabuse.com
color=true
</code></pre>
<p>The file you use should depends on the parameter and scope you&apos;re wanting to set. So, for example, you&apos;d probably want to set <code>https-proxy</code> in the global npmrc file as opposed to the project-level npmrc file since all projects on the system will need the proxy settings.</p>
<h4 id="environmentvariable">Environment Variable</h4>
<p>There are a few enironment variables that npm will use over parameters set locally (or in an npmrc file). Some examples are <code>NODE_ENV</code> and <code>HTTPS_PROXY</code>. You can also set <em>any</em> npm parameter by prefixing an environment variable with <code>npm_config_</code>. So that way you can do things like <code>export npm_config_registry=localhost:1234</code>.</p>
<p>A lot of people are use to using environment variables for configuration, so this should be familiar to them. For example, a great way to configure a <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Docker_%28software%29">Docker</a> instance is to set environment variables from the dockerfile.</p>
<h4 id="flagsoncommandline">Flags on Command Line</h4>
<p>Not all parameters need to be permanently set in a file or environment variable. Many of them can be used within an npm command as a flag, prefixed with <code>--</code>.</p>
<p>For example, if you&apos;re installing a new package from the registry and want to save it to your <code>package.json</code> file, you&apos;ll want to use the <code>--save</code> flag, but that might not always be the case. In some cases you might want to use <code>--save-dev</code> or even <code>--save-optional</code>, so it wouldn&apos;t make sense to use npmrc.</p>
<h4 id="packagejsonfile">package.json File</h4>
<p>Within your <code>package.json</code> project file you can set parameters as well. In this case, the <code>config</code> map should be used, like this:</p>
<pre><code class="language-json">{
  &quot;name&quot;: &quot;module-name&quot;,
  &quot;version&quot;: &quot;10.3.1&quot;,
  &quot;config&quot;: {
    &quot;foo&quot;: &quot;bar&quot;,
    &quot;test-param&quot;: 12
  },
  &quot;dependencies&quot;: {
    &quot;express&quot;: &quot;4.2.x&quot;,
  }
}
</code></pre>
<p>Then from within your code you can access these parameters using the <code>process</code> global variable, like this: <code>process.env.npm_package_config_foo</code>. Notice the prefix <code>npm_package_config_</code>, which tells Node where to get the variable from.</p>
<p><strong>Note</strong>: This will only work when you run your project through an npm script (i.e. <em>not</em> just using <code>node index.js</code>).</p>
<h4 id="npmconfigset">npm config set</h4>
<p>And lastly, there is always the ability to set parameters via <code>npm config set</code>. This will take precedence over the <code>package.json</code> configurations. So, for example, if you ran <code>npm config set module-name:foo baz</code> from the command line (and had the <code>package.json</code> file from above), then your <code>foo</code> parameter would be <code>baz</code> instead of <code>bar</code>. The <code>module-name</code> scoping will ensure that this variable is not set for any other projects.</p>
<p>Like the method above, for this to work you <em>must</em> run the program via an npm script, like <code>npm run</code>.</p>
<h3 id="listofpossibleparameters">List of Possible Parameters</h3>
<p>I tried to categorize each parameter as best as possible, but many of them would work well in other categories too. So, after some contemplating, I just put each param in the category that made the most sense for the context.</p>
<p>Hopefully I did well enough organizing this so that you can use it as a go-to reference. Feel free to let me know if there are any mistakes or omissions!</p>
<h4 id="accesscontrolauthorization">Access Control/Authorization</h4>
<h5 id="access">access</h5>
<p>This sets the scope access level of a package, which defaults to <code>restricted</code>. Setting this parameter to <code>public</code> makes it publically viewable and installable. If your project is unscoped, then it is public.</p>
<ul>
<li>Default: restricted</li>
<li>Type: Access (string)</li>
</ul>
<h5 id="alwaysauth">always-auth</h5>
<p>Set to true if you want to require authentication for <em>every</em> time you access the registry, even for GET requests.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="ca">ca</h5>
<p>This is the Certificate Authority signing certificate that is used for trusting an SSL connection with the package registry. To specify the certificate, use the PEM format and replace all newlines with the <code>\n</code> character. So, for example, setting the CA might look like:</p>
<p><code>ca=&quot;-----BEGIN CERTIFICATE-----\nXXXX\nXXXX\n-----END CERTIFICATE-----&quot;</code></p>
<p>You can also trust multiple CAs by specifying an array of certificates, one for each line:</p>
<pre><code>ca[]=&quot;...&quot;
ca[]=&quot;...&quot;
</code></pre>
<p>Or, setting <code>ca</code> to null will specify the default known registrars.</p>
<ul>
<li>Default: The npm CA certificate</li>
<li>Type: String, Array or null</li>
</ul>
<h5 id="cafile">cafile</h5>
<p>Similar to the <code>ca</code> parameter, <code>cafile</code> allows you to set the trusted certificate for connecting to the registry. The difference here is that you can specify a file path to the certificate, which can contain one or multiple certificates.</p>
<ul>
<li>Default: null</li>
<li>Type: path</li>
</ul>
<h5 id="cert">cert</h5>
<p>The <code>cert</code> parameter specifies the client certificate for authenticating with a registry. This is opposed to the previous <code>ca</code> and <code>cafile</code> certificates in that it is for client authentication instead of registry authentication. If you host your own registry, this could be a good way to make it private without having to authenticate with a username and password.</p>
<ul>
<li>Default: null</li>
<li>Type: String</li>
</ul>
<h4 id="caching">Caching</h4>
<h5 id="cache">cache</h5>
<p>This is the location of npm&apos;s cache directory.</p>
<ul>
<li>Default: Windows: <code>%AppData%\npm-cache</code>, Posix: <code>~/.npm</code></li>
<li>Type: path</li>
</ul>
<h5 id="cachelockstale">cache-lock-stale</h5>
<p>The number of milliseconds before the cache folder lockfiles are considered stale.</p>
<ul>
<li>Default: 60000 (1 minute)</li>
<li>Type: Number</li>
</ul>
<h5 id="cachelockretries">cache-lock-retries</h5>
<p>Number of times to retry to acquire a lock on cache folder lockfiles.</p>
<ul>
<li>Default: 10</li>
<li>Type: Number</li>
</ul>
<h5 id="cachelockwait">cache-lock-wait</h5>
<p>Number of milliseconds to wait for cache lock files to expire.</p>
<ul>
<li>Default: 10000 (10 seconds)</li>
<li>Type: Number</li>
</ul>
<h5 id="cachemax">cache-max</h5>
<p>This is the maximum time (in seconds) in which an item is cached before updating with the registry. So if you anticipate a package to change fairly often, then you&apos;ll want to set this to a lower number.</p>
<p>The only time cached packages are purged is when the <code>npm cache clean</code> command is used (or, alternatively, you can manually clean out packages to pick and choose which are purged).</p>
<ul>
<li>Default: Infinity</li>
<li>Type: Number</li>
</ul>
<h5 id="cachemin">cache-min</h5>
<p>Opposite of the <code>cache-max</code> parameter, the <code>cache-min</code> parameter sets the minimum time (in seconds) to keep items in the cache before checking against the registry again.</p>
<ul>
<li>Default: 10</li>
<li>Type: Number</li>
</ul>
<h4 id="general">General</h4>
<h5 id="color">color</h5>
<p>The <code>color</code> param determines if coloring is used in the npm output. If set to true, then npm only prints colors for tty file descriptors. Or you can set it to <code>always</code> to always use colors.</p>
<ul>
<li>Default: true on Posix, false on Windows</li>
<li>Type: Boolean or &quot;always&quot;</li>
</ul>
<h5 id="description">description</h5>
<p>Determines if the package description is shown when using <code>npm search</code>.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="force">force</h5>
<p>Using <code>force</code> will make the various commands more forceful. You can almost think of it as using <code>sudo</code>, where you&apos;ll be able to bypass certain restrictions. So, to name a few examples, using this would mean a lifecycle script failure does not block progress, publishing overwrites previously published versions, npm skips the cache when requesting from the registry, or it would prevent checks against overwriting non-npm files.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="global">global</h5>
<p><code>global</code> causes a given command to operate in the &apos;global&apos; mode. Packages installed in this folder can be accessed by all users and projects on the system. This means that packages are installed in to the &apos;prefix&apos; folder, which is typically where node is installed. Specifically, the global packages will be located at <code>{prefix}/lib/node_modules</code>, bin files will be linked to <code>{prefix}/bin</code>, and man pages are would be linked to <code>{prefix}/share/man</code>.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="globalconfig">globalconfig</h5>
<p>The location of the config file to read for global configuration options.</p>
<ul>
<li>Default: {prefix}/etc/npmrc</li>
<li>Type: path</li>
</ul>
<h5 id="group">group</h5>
<p>This parameter tells npm which system group to use when running package scripts in global mode as the root user.</p>
<ul>
<li>Default: the group ID of the current process</li>
<li>Type: String or Number</li>
</ul>
<h5 id="long">long</h5>
<p>Whether or not to show detailed information when running <code>npm ls</code> and <code>npm search</code>.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="prefix">prefix</h5>
<p>This is the location where global items are installed, which by default is the install location of npm itself. If <code>prefix</code> is set on the command line, then non-global commands are forced to run in the given folder.</p>
<ul>
<li>Default: see <code>npm help 5 folders</code></li>
<li>Type: path</li>
</ul>
<h5 id="spin">spin</h5>
<p>The <code>spin</code> parameter determines whether or not an ASCII spinner is displayed while npm is waiting or processing something (assumging process.stderr is a TTY). This can be set to false to suppress the spinner completely, or set to &apos;always&apos; to output the spinner even for non-TTY outputs.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean or &quot;always&quot;</li>
</ul>
<h5 id="tmp">tmp</h5>
<p>The directory where temporary files and directories are stored. Once the npm process has completed successfully, all of the files and directories are deleted. If the process fails, however, the files and directories are <em>not</em> deleted so you can inspect them and debug the problem.</p>
<ul>
<li>Default: TMPDIR environment variable, or &quot;/tmp&quot;</li>
<li>Type: path</li>
</ul>
<h5 id="unicode">unicode</h5>
<p>The <code>unicode</code> parameter tells npm whether or not to use unicdoe characters in the tree output. If <code>false</code>, only ASCII characters are used to the draw the trees.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="unsafeperm">unsafe-perm</h5>
<p>When <code>unsafe-perm</code> is set to <code>true</code>, the user/group ID switching is suppressed when a package script is run. If <code>false</code>, non-root users will not be able to install packages.</p>
<ul>
<li>Default: false if running as root, true otherwise</li>
<li>Type: Boolean</li>
</ul>
<h5 id="usage">usage</h5>
<p>Using the <code>usage</code> flag reduces the amount of output when getting help for a command. Instead of showing you every possible flag/input to a command, like the <code>-H</code> flag would, it just gives you the gist of the help documentation. So, for example, executing <code>npm --usage search</code> would output <code>npm search [some search terms ...]</code>.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="user">user</h5>
<p>This is the UID to use when a package script is run as root. So if you don&apos;t want the script to have root permissions, set this to the UID of the user that has the correct permission level and access for the application. Running a package script as root can be dangerous!</p>
<ul>
<li>Default: &quot;nobody&quot;</li>
<li>Type: String or Number</li>
</ul>
<h5 id="userconfig">userconfig</h5>
<p>This is the location of a user-level configuration file. Each user on a system can have different settings for the npm install, and the file should be located at the path given in <code>userconfig</code>.</p>
<ul>
<li>Default: ~/.npmrc</li>
<li>Type: path</li>
</ul>
<h5 id="umask">umask</h5>
<p>This is the mask value to use when setting the file creation mode for both files and directories. The type of file/directory being created depends on the mask value used. If it is a directory or an executable, then the <code>umask</code> value is masked against <code>0777</code>. For all other files, the <code>umask</code> value is masked against <code>0666</code>. The defaults are <code>0755</code> and <code>0644</code> respectively, which is a fairly conservative mask for each file type.</p>
<ul>
<li>Default: 022</li>
<li>Type: Octal numeric string in range 0000..0777 (0..511)</li>
</ul>
<h5 id="version">version</h5>
<p>Using this flag outputs the version of npm installed. <strong>This only works when used on the command line as a flag</strong> like <code>npm --version</code>.</p>
<ul>
<li>Default: false</li>
<li>Type: boolean</li>
</ul>
<h5 id="versions">versions</h5>
<p>Using this flag is similar to <code>version</code>, but it outputs version detail (as JSON) on a few different packages, including the project in the current directory (if present), V8, npm, and details from <code>process.versions</code>. <strong>This only works when used on the command line as a flag</strong> like <code>npm --versions</code>.</p>
<p>An example output might look like:</p>
<pre><code class="language-json">{ &apos;my-project&apos;: &apos;0.0.1&apos;,
  npm: &apos;2.14.2&apos;,
  http_parser: &apos;2.3&apos;,
  modules: &apos;14&apos;,
  node: &apos;0.12.2&apos;,
  openssl: &apos;1.0.1m&apos;,
  uv: &apos;1.4.2-node1&apos;,
  v8: &apos;3.28.73&apos;,
  zlib: &apos;1.2.8&apos; }
</code></pre>
<ul>
<li>Default: false</li>
<li>Type: boolean</li>
</ul>
<h5 id="viewer">viewer</h5>
<p>This is the program to be used when viewing help content. If set to &apos;browser&apos;, the default web browser will open and show the help content in HTML.</p>
<ul>
<li>Default: &quot;man&quot; on Posix, &quot;browser&quot; on Windows</li>
<li>Type: path, &apos;man&apos;, or &apos;browser&apos;</li>
</ul>
<h4 id="development">Development</h4>
<h5 id="dev">dev</h5>
<p>Using this flag when installing packages will also install the dev-dependencies packages as well. This should almost always be used when not running a project in production.</p>
<p>This is similar to the <code>npat</code> flag.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="editor">editor</h5>
<p>This is the command (or path to an executable) to be run when opening an editor.</p>
<ul>
<li>Default: EDITOR environment variable if set, or &quot;vi&quot; on Posix, or &quot;notepad&quot; on Windows.</li>
<li>Type: path</li>
</ul>
<h5 id="enginestrict">engine-strict</h5>
<p>This parameter tells npm if it should follow the engine specification in a <code>package.json</code> file strictly. If set to <code>true</code>, then a package installation will fail if the current Node.js version does not match the one specified.</p>
<p>This is useful for when a package requires a certain Node.js version, or even io.js (possibly because the package uses ES6 features).</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="git">git</h5>
<p>This should be the command to use for running git commands. This could be useful for when git is installed, but it isn&apos;t on the PATH, in which case you&apos;d specify the path of the git install.</p>
<ul>
<li>Default: &quot;git&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="gittagversion">git-tag-version</h5>
<p>This tells npm if it should tag the commit when running the <code>npm version</code> command (which bumps the package version and saves it to <code>package.json</code>). This may help reduce mistakes (forgetting to tag the git commit, tagging it as the wrong version, etc), but it also gives you less control, so you&apos;ll have to weight the trade-offs.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="heading">heading</h5>
<p>The string to be printed when outputting debug information.</p>
<ul>
<li>Default: &quot;npm&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="ifpresent">if-present</h5>
<p>When using the <code>npm run-script</code> command, if the script is not defined in the <code>package.json</code> file, then npm exits with an error code. If <code>if-present</code> is set to <code>true</code>, then the error code is not returned. This is useful for when you optionally want to run a script, but don&apos;t care if it is not present. So, for example, maybe you have a script (script <code>A</code>) that is present in some of your projects, but not all, and you use another generic script (script <code>B</code>) to run it. This way if script <code>A</code> isn&apos;t present, then script <code>B</code> won&apos;t get an error and can safely keep executing.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="ignorescripts">ignore-scripts</h5>
<p>Set this flag to not run any scripts defined in the <code>package.json</code> file of a project.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="initmodule">init-module</h5>
<p>This is the path to a JavaScript file that helps with initializing a project. So if you have a custom configuration that you want all of your new projects to have (like maybe a dependency on <a rel="nofollow" target="_blank" href="https://github.com/petkaantonov/bluebird">Bluebird</a> or a default engine), then you can create a file in the location specified to handle the initialization for you.</p>
<ul>
<li>Default: ~/.npm-init.js</li>
<li>Type: path</li>
</ul>
<h5 id="initauthorname">init-author-name</h5>
<p>The default name used by <code>npm init</code> when creating a new project.</p>
<ul>
<li>Default: &quot;&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="initauthoremail">init-author-email</h5>
<p>The default author email used by <code>npm init</code> when creating a new project.</p>
<ul>
<li>Default: &quot;&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="initauthorurl">init-author-url</h5>
<p>The default author url used by <code>npm init</code> when creating a new project.</p>
<ul>
<li>Default: &quot;&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="initlicense">init-license</h5>
<p>The default license used by <code>npm init</code> when creating a new project.</p>
<ul>
<li>Default: &quot;ISC&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="initversion">init-version</h5>
<p>The default version used by <code>npm init</code> when creating a new project.</p>
<ul>
<li>Default: &quot;1.0.0&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="json">json</h5>
<p>This parameter determines whether or not npm writes its output as json or regular text.</p>
<p><strong>NOTE</strong>: npm claims that this feature is experimental and the structure of hte JSON objects is subject to change.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="link">link</h5>
<p>If <code>link</code> is set to true, then the local installs will be linked to the global package installs (if a matching package is present). One important by-product of this features is that by linking to global packages, local installs can then cause other things to be installed in the global space.</p>
<p>Links are created if at least one of the two conditions are met:</p>
<ul>
<li>
<p>The package is not already installed globally</p>
</li>
<li>
<p>the globally installed version is identical to the version that is being installed locally</p>
</li>
<li>
<p>Default: false</p>
</li>
<li>
<p>Type: Boolean</p>
</li>
</ul>
<h5 id="localaddress">local-address</h5>
<p>This is the IP address of the system&apos;s local networking interface to be used when connecting to the npm registry.</p>
<p><strong>NOTE</strong>: This must be an IPv4 address in Node v0.12 and earlier.</p>
<ul>
<li>Default: undefined</li>
<li>Type: IP Address</li>
</ul>
<h5 id="loglevel">loglevel</h5>
<p>This is the default log level for when running your application. If there is a log event higher (or equal to) than the one given here, then it is output to the user. When/if the application fails, <em>all</em> logs are written to <code>npm-debug.log</code> in the current working directory.</p>
<ul>
<li>Default: &quot;warn&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="logstream">logstream</h5>
<p>The stream used by the <code>npmlog</code> package at runtime.</p>
<p><strong>NOTE</strong>: This cannot be set on the command line. You must use another method, like a file or environment variable to configure it.</p>
<ul>
<li>Default: process.stderr</li>
<li>Type: Stream</li>
</ul>
<h5 id="message">message</h5>
<p>This is the commit message to be used by the <code>npm version</code> command. The &apos;%s&apos; formatting character will be replaced by the version number.</p>
<ul>
<li>Default: &quot;%s&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="nodeversion">node-version</h5>
<p>The Node version used when checking a package&apos;s <code>engines</code> declaration in the <code>package.json</code> file.</p>
<ul>
<li>Default: process.version</li>
<li>Type: semver or false</li>
</ul>
<h5 id="npat">npat</h5>
<p>Whether or not to run a package&apos;s tests on installation.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="onloadscript">onload-script</h5>
<p>This is the location of a package to <code>requre()</code> once npm loads. This is recommended for programmatic usage of npm.</p>
<ul>
<li>Default: false</li>
<li>Type: path, or &apos;false&apos;</li>
</ul>
<h5 id="optional">optional</h5>
<p>This tells npm to install the packages from the <code>optionalDependencies</code> map in the <code>package.json</code> file. Since these are <em>optional</em> dependencies, if one fails to install then npm will <em>not</em> abort the process.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="parseable">parseable</h5>
<p>The <code>parseable</code> parameter tells npm to format its output in to a parseable format when writing to standard output.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="production">production</h5>
<p>When set to <code>true</code>, npm runs in production mode, which mostly just means <code>devDependencies</code> are not installed. Note that you should use <code>NODE_ENV=&quot;production&quot;</code> environment variable instead when using lifecycle scripts.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="rollback">rollback</h5>
<p>Using this flag with npm will remove any packages that failed to install (maybe due to compilation/dependency error, for example).</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="save">save</h5>
<p>Using this flag with npm saves the given package to the local <code>package.json</code> file under <code>dependencies</code>. Alternatively, using this flag with the <code>npm rm</code> command will remove a dependency from the <code>dependencies</code> section of the <code>package.json</code> file.</p>
<p>Note that this only works when a <code>package.json</code> file is present in the current directory.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="savebundle">save-bundle</h5>
<p>If a package is saved at install time by using the <code>--save</code>, <code>--save-dev</code>, or <code>--save-optional</code> flags, then also put it in the <code>bundleDependencies</code> list. When used with the <code>npm rm</code> command, it removes it from the <code>bundledDependencies</code> list.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="savedev">save-dev</h5>
<p>Using this flag saves packages to the <code>devDependencies</code> list in the <code>package.json</code> file. The opposite is true when used with <code>npm rm</code>, meaning the package will be removed from <code>devDependencies</code>. Like the <code>save</code> flag, this only works if there is a <code>package.json</code> file present.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="saveexact">save-exact</h5>
<p>When a dependency is saved to the <code>package.json</code> file using one of the <code>--save</code>, <code>--save-dev</code> or <code>--save-optional</code> flags, then it will be configured using the exact version number instead of npm&apos;s default semver range operator.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="saveoptional">save-optional</h5>
<p>Using this flag saves packages to the <code>optionalDependencies</code> list in the <code>package.json</code> file. The opposite is true when used with <code>npm rm</code>, meaning the package will be removed from <code>optionalDependencies</code>. Like the <code>save</code> flag, this only works if there is a <code>package.json</code> file present.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="saveprefix">save-prefix</h5>
<p>This parameter determines how packages are saved to <code>package.json</code> if used with the <code>--save</code> or <code>--save-dev</code> flags. Using the default value as an example, if we save a package with the version <code>1.2.3</code>, then it will actually be saved in <code>package.json</code> as <code>^1.2.3</code>.</p>
<ul>
<li>Default: &apos;^&apos;</li>
<li>Type: String</li>
</ul>
<h5 id="scope">scope</h5>
<p>Using <code>scope</code> tells npm what scope to use for a scoped registry. This could be useful when using a private registry for the first time. Example:</p>
<p><code>npm login <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="517c7c22323e21346c113e2336303f382b3025383e3f">[email&#xA0;protected]</a> --registry=registry.example.com</code></p>
<p>This causes <code>@organization</code> to be mapped to this registry for future installations of packages specified according to the pattern <code>@organization/package</code>.</p>
<ul>
<li>Default: &quot;&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="shrinkwrap">shrinkwrap</h5>
<p>When <code>false</code>, the <code>npm-shrinkwrap.json</code> file is ignored during installation.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="signgittag">sign-git-tag</h5>
<p>When executing the <code>npm version</code> command and using this flag, the <code>-s</code> flag will be used during tagging to add a signature. In order for this to work, you must have already set up GPG keys in your git configs.</p>
<ul>
<li>Default: false</li>
<li>Type: Boolean</li>
</ul>
<h5 id="tag">tag</h5>
<p>When installing a package from npm and not specifying the version, this tag will be used instead.</p>
<ul>
<li>Default: latest</li>
<li>Type: String</li>
</ul>
<h5 id="tagversionprefix">tag-version-prefix</h5>
<p>The character prepended to the package version when using <code>npmversion</code>. This is useful for when other programs have a styling convention for versions.</p>
<ul>
<li>Default: &quot;v&quot;</li>
<li>Type: String</li>
</ul>
<h4 id="networking">Networking</h4>
<h5 id="httpsproxy">https-proxy</h5>
<p>The proxy used for outgoing HTTPS connections. If any of the following environment variables are set, then they are used instead: <code>HTTPS_PROXY</code>, <code>https_proxy</code>, <code>HTTP_PROXY</code>, <code>http_proxy</code>.</p>
<ul>
<li>Default: null</li>
<li>Type: url</li>
</ul>
<h5 id="proxy">proxy</h5>
<p>The proxy used for outgoing HTTP connections. If any of the following environment variables are set, then they are used instead: <code>HTTP_PROXY</code>, <code>http_proxy</code>.</p>
<ul>
<li>Default: null</li>
<li>Type: url</li>
</ul>
<h5 id="strictssl">strict-ssl</h5>
<p>This tells npm whether or not to use SSL for connecting with the registry via HTTPS.</p>
<ul>
<li>Default: true</li>
<li>Type: Boolean</li>
</ul>
<h5 id="useragent">user-agent</h5>
<p>Sets the User-Agent request header for HTTP(S) requests.</p>
<ul>
<li>Default: node/{process.version} {process.platform} {process.arch}</li>
<li>Type: String</li>
</ul>
<h4 id="registry">Registry</h4>
<h5 id="fetchretries">fetch-retries</h5>
<p>The number of times npm tries to contact the registry to fetch a package.</p>
<ul>
<li>Default: 2</li>
<li>Type: Number</li>
</ul>
<h5 id="fetchretryfactor">fetch-retry-factor</h5>
<p>The &quot;factor&quot; config for the retry module to use when fetching packages.</p>
<ul>
<li>Default: 10</li>
<li>Type: Number</li>
</ul>
<h5 id="fetchretrymintimeout">fetch-retry-mintimeout</h5>
<p>The minimum time to wait before timing out when fetching packages from the registry.</p>
<ul>
<li>Default: 10000 (10 seconds)</li>
<li>Type: Number (milliseconds)</li>
</ul>
<h5 id="fetchretrymaxtimeout">fetch-retry-maxtimeout</h5>
<p>The maximum time to wait before timing out when fetching packages from the registry.</p>
<ul>
<li>Default: 10000 (10 seconds)</li>
<li>Type: Number (milliseconds)</li>
</ul>
<h5 id="key">key</h5>
<p>This is the client key to use when authenticating with the registry.</p>
<ul>
<li>Default: null</li>
<li>Type: String</li>
</ul>
<h5 id="registry">registry</h5>
<p>The URL of the registry to use for fetching and publishing packages.</p>
<ul>
<li>Default: <a href="https://registry.npmjs.org/">https://registry.npmjs.org/</a></li>
<li>Type: url</li>
</ul>
<h5 id="searchopts">searchopts</h5>
<p>A space-separated list of options that are always used for searching the registry.</p>
<ul>
<li>Default: &quot;&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="searchexclude">searchexclude</h5>
<p>A space-separated list of limits that are always used for searching the registry.</p>
<ul>
<li>Default: &quot;&quot;</li>
<li>Type: String</li>
</ul>
<h5 id="searchsort">searchsort</h5>
<p>This indicates which field in the results should be sorted on. To reverse the sorting order, just prefix it with a <code>-</code>.</p>
<ul>
<li>Default: &quot;name&quot;</li>
<li>Type: String</li>
<li>Values: &quot;name&quot;, &quot;-name&quot;, &quot;date&quot;, &quot;-date&quot;, &quot;description&quot;, &quot;-description&quot;, &quot;keywords&quot;, &quot;-keywords&quot;</li>
</ul>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					