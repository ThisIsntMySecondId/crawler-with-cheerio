
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#extractingqueryparameters">Extracting Query Parameters</a>
<ul>
<li><a href="#extractqueryparameterswithoutexpress">Extract Query Parameters Without Express</a></li>
</ul>
</li>
<li><a href="#extractingrouteparameters">Extracting Route Parameters</a></li>
<li><a href="#conclusion">Conclusion</a></li>
</ul>
<h3 id="introduction">Introduction</h3>
<p>We&apos;ll be going over how to extract information from a URL in Express.js. Specifically, how do we extract information from a query string and how do we extract information from the URL path parameters?</p>
<p>In this article, I assume you have some experience with Node.js and creating <a rel="nofollow" target="_blank" href="https://expressjs.com/">Express.js</a> servers (or at least simple ones). The rest we&apos;ll explain throughout the article.</p>
<h3 id="extractingqueryparameters">Extracting Query Parameters</h3>
<p>Before we start, it&apos;d be helpful to first understand what exactly a query string/parameter is, and then we&apos;ll talk about how we can work with them.</p>
<p>So, <strong>what is a query parameter</strong>?</p>
<p>In simple terms, a query <em>string</em> is the part of a <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/URL">URL</a> (Uniform Resource Locater) <em>after</em> the question mark (?). It is meant to send small amounts of information to the server via the url. This information is usually used as parameters to query a database, or maybe to filter results. It&apos;s really up to you what they&apos;re used for.</p>
<p>Here is an example of a URL with query strings attached:</p>
<pre><code>http://stackabuse.com/?page=2&amp;limit=3
</code></pre>
<p>The query <em>parameters</em> are the actual key-value pairs like <code>page</code> and <code>limit</code> with values of <code>2</code> and <code>3</code>, respectively.</p>
<p>Now, let&apos;s move on to the first main purpose of this article - <strong>how to extract these from our Express request object</strong>.</p>
<p>This is a pretty common use-case in Express, and any HTTP server, so hopefully the examples and explanation I show here are clear.</p>
<p>Now, taking the same example from above:</p>
<pre><code>http://stackabuse.com/?page=2&amp;limit=3
</code></pre>
<p>We&apos;d like to extract both the <code>page</code> and <code>limit</code> parameters so we know which articles to return to the page that the user requested. While query parameters are typically used in <code>GET</code> requests, it&apos;s still possible to see them in <code>POST</code> and <code>DELETE</code> requests, among others.</p>
<p>Your query parameters can be retrieved from the <code>query</code> object on the <a rel="nofollow" target="_blank" href="https://expressjs.com/en/4x/api.html#req">request</a> object sent to your route. It is in the form of an object in which you can directly access the query parameters you care about. In this case Express handles all of the URL parsing for you and exposes the retrieved parameters as this object.</p>
<p>Let&apos;s take a look at an example of us getting query parameters in a route:</p>
<pre><code class="language-javascript">const express = require(&apos;express&apos;);
const bodyParser = require(&apos;body-parser&apos;);
const url = require(&apos;url&apos;);
const querystring = require(&apos;querystring&apos;);
const Article = require(&apos;./models&apos;).Article;

let app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Function to handle the root path
app.get(&apos;/&apos;, async function(req, res) {

    // Access the provided &apos;page&apos; and &apos;limt&apos; query parameters
    let page = req.query.page;
    let limit = req.query.limit;

    let articles = await Article.findAll().paginate({page: page, limit: limit}).exec();

    // Return the articles to the rendering engine
    res.render(&apos;index&apos;, {
        articles: articles
    });
});

let server = app.listen(8080, function() {
    console.log(&apos;Server is listening on port 8080&apos;)
});
</code></pre>
<p>In the example above, we assume the <code>page</code> and <code>limit</code> parameters always exist. If neither of these parameters are given in the URL, we&apos;d receive <code>undefined</code> for both <code>page</code> and <code>limit</code> instead.</p>
<h5 id="extractqueryparameterswithoutexpress">Extract Query Parameters Without Express</h5>
<p>As a quick bonus, I wanted to show you how to do the actual parsing on your own in case you need to extract information from a URL that isn&apos;t using Express, or any other web framework. It&apos;s fairly common to create a dead-simple server using the <code>http</code> module, so this is good to know.</p>
<p>Lucky for you, Node.js already provides some great core libraries that has this functionality built in, so it is just a matter of <code>require</code>-ing the module and calling a few lines of code.</p>
<p>Here is an example using the <a rel="nofollow" target="_blank" href="https://nodejs.org/api/querystring.html">querystring</a> and <a rel="nofollow" target="_blank" href="https://nodejs.org/api/url.html">url</a> packages.</p>
<pre><code class="language-javascript">const url = require(&apos;url&apos;);
const querystring = require(&apos;querystring&apos;);

let rawUrl = &apos;http://stackabuse.com/?page=2&amp;limit=3&apos;;

let parsedUrl = url.parse(rawUrl);
let parsedQs = querystring.parse(parsedUrl.query);

// parsedQs = { page: &apos;2&apos;, limit: &apos;3&apos; }
</code></pre>
<p>You can see in this code that we require two parsing steps to get the results we want.</p>
<p>Let&apos;s break this down a bit further and show what exactly is going on at each step. After calling <code>url.parse(rawUrl)</code> on our URL, this is what is returned to us:</p>
<pre><code class="language-javascript">{
    protocol: &apos;http:&apos;,
    slashes: true,
    auth: null,
    host: &apos;stackabuse.com&apos;,
    port: null,
    hostname: &apos;stackabuse.com&apos;,
    hash: null,
    search: &apos;?page=2&amp;limit=3&apos;,
    query: &apos;page=2&amp;limit=3&apos;,
    pathname: &apos;/&apos;,
    path: &apos;/?page=2&amp;limit=3&apos;,
    href: &apos;http://stackabuse.com/?page=2&amp;limit=3&apos;
}
</code></pre>
<p>Okay, we&apos;re a bit closer getting the data we need. But it needs to be broken down one more time. We can do this using the <code>querystring</code> package to parse the actual query string. For example:</p>
<pre><code class="language-javascript">let parsedQs = querystring.parse(parsedUrl.query);
</code></pre>
<p>And finally, our <code>parsedQs</code> object contains the following:</p>
<pre><code class="language-javascript">{
    page: &apos;2&apos;,
    limit: &apos;3&apos;
}
</code></pre>
<h3 id="extractingrouteparameters">Extracting Route Parameters</h3>
<p>In any web application another common way to structure your URLs is to place information within the actual URL path, which are simply called route parameters in Express. We can use these to structure web pages by information/data, which are especially useful in REST APIs.</p>
<p>Extracting these route parameters is similar to the query parameters. All we do is take the <code>req</code> object and retrieve our params from the <code>params</code> object. Pretty simple, right?</p>
<p>Let&apos;s take a look at an example of doing this in our Express route:</p>
<pre><code class="language-javascript">// Route to return all articles with a given tag
app.get(&apos;/tag/:id&apos;, async function (req, res) {
    
    // Retrieve the tag from our URL path
    var id = req.params.id;

    let articles = await Article.findAll({tag: id}).exec();

    res.render(&apos;tag&apos;, {
        articles: articles
    });
});
</code></pre>
<p>First thing to notice is that we tell Express that our route is <code>/tag/:id</code>, where <code>:id</code> is a placeholder for anything. It could be a string or a number. So whatever is passed in that part of the path is set as the <code>id</code> parameter.</p>
<p>If we were to navigate to the URL <code>http://stackabuse.com/tag/node</code> then <code>id</code> would be <code>node</code>, and we&apos;d get a bunch of articles that have the <code>node</code> tag on them. And <code>req.params</code> in this case would be <code>{id: &apos;node&apos;}</code>, just like the <code>query</code> object.</p>
<p>As you can see, we again just take our parameter directly from an object contained within the request object.</p>
<h3 id="conclusion">Conclusion</h3>
<p>In this article I presented ways to extract both the query string parameters and route path parameters a URL in the Express web framework. Here is a quick recap of how to extract the parameters we talked about:</p>
<ul>
<li><code>req.query</code>: directly access the parsed query string parameters</li>
<li><code>req.params</code>: directly access the parsed route parameters from the path</li>
</ul>
<p>While the actual act of retrieving this data is very simple, understanding where this info comes from and what it is can be confusing for some beginners. Hopefully this article cleared some things up for you. Feel free to let us know in the comments if there is anything that is unclear.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					