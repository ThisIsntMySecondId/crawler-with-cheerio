
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>The outgrowth of the world wide web over the last couple of decades has led to an enormous amount of data being collected and plastered onto webpages throughout the internet. A corollary to this hyperbolic production and distribution of content on the web is the curation of a vast amount of information that can be used in enumerable ways if one can effectively extract and aggregate it.</p>
<p>The most common ways to collect and aggregate data available on the web are (1) to request it from an API with technologies such as <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Representational_state_transfer">REST</a> or <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/SOAP">SOAP</a> and (2) to write a program to parse or scrape it from loosely-structured data, like HTML. The former is by far the preferable method for the programmer consuming the information but, this is often not a possibility due to development time and resources needed on the side of the producer. Therefore, more often than not the only available means to get at the prized data is to scrape it.</p>
<p>This article is going to present a technique for building a standalone Node.js application that collects (scrapes) tax data for the state of Nebraska and either presents that info to the user or calculates taxes based on a city and amount supplied.</p>
<p>The technologies to be utilized are:</p>
<ul>
<li>Node.js: A JavaScript runtime built on Chrome&apos;s V8 engine</li>
<li>Express: A Node.js web framework</li>
<li>Cheerio: An HTML parsing library that mirrors the familiar jQuery library API</li>
</ul>
<p>The source code can be found on GitHub <a rel="nofollow" target="_blank" href="https://github.com/amcquistan/taxman">here</a>.</p>
<h3 id="baseprojectsetup">Base Project Setup</h3>
<p>This tutorial will utilize the Node Package Manager (npm) to initialize the project, install libraries, and manage dependencies. Before we get started, make sure you&apos;ve <a href="/the-ultimate-guide-to-configuring-npm/">configured npm</a> for your environment.</p>
<p>Initialize the project accepting the basic default options:</p>
<img src="https://s3.amazonaws.com/stackabuse/media/build-web-scraped-api-express-cheerio-1.png" class="img-responsive">
<p>Install dependencies:</p>
<img src="https://s3.amazonaws.com/stackabuse/media/build-web-scraped-api-express-cheerio-2.png" class="img-responsive">
<p>Basic project structure:</p>
<img src="https://s3.amazonaws.com/stackabuse/media/build-web-scraped-api-express-cheerio-3.png" class="img-responsive">
<h3 id="express">Express</h3>
<p>We will be using <a rel="nofollow" target="_blank" href="https://expressjs.com/">Express</a> to build out our RESTful API for the tax calculation application. Express is a web application framework for Node applications that is both flexible in that it imposes few restrictions in how you develop your applications but very powerful because it provides several useful features that are used in a multitude of web applications.</p>
<h4 id="settingupexpress">Setting Up Express</h4>
<p>In server.js we will include some boilerplate Express setup code that will create the Express application, then register the routes module that we will make in the next subsection. At the end of the file we will instruct the Express app to listen to the port provided or 3500 which is a hardcoded port.</p>
<p>In server.js copy and paste in the following code:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const express = require(&apos;express&apos;);
const app = express();
const port = process.env.PORT || 3500;

const routes = require(&apos;./api/routes&apos;);
routes(app);

app.listen(port);

console.log(&quot;Node application running on port &quot; + port);
</code></pre>
<h4 id="routes">Routes</h4>
<p>We will set up routing in our application to respond to requests made to specific and meaningful URI paths. What do I mean by meaningful you may be asking? Well in the REST paradigm route paths are designed to expose resources within the application in self describing ways.</p>
<p>In the routes/index.js file copy and paste in the following code:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const taxCtrl = require(&apos;../controllers&apos;);

module.exports = (app) =&gt; {
    app.use([&apos;/calculate/:stateName/:cityName/:amount&apos;, &apos;/taxrate/:stateName/:cityName&apos;], (req, res, next) =&gt; {
        const state = req.params.stateName;
        if (!taxCtrl.stateUrls.hasOwnProperty(state.toLowerCase())) {
            res.status(404)
                    .send({message: `No state info found for ${state}`});
        } else {
            next();
        }
    });

    app.route(&apos;/taxrate/:stateName/:cityName&apos;)
        .get(taxCtrl.getTaxRate);

    app.route(&apos;/calculate/:stateName/:cityName/:amount&apos;)
        .get(taxCtrl.calculateTaxes);
  
    app.use((req, res) =&gt; {
        res.status(404)
            .send({url: `sorry friend, but url ${req.originalUrl} is not found`});
    });
}
</code></pre>
<p>The two routes being defined in this module are <em>/taxrate/:stateName/:cityName</em> and <em>/calculate/:stateName/:cityName/:amount</em>. They are registered with the <code>app</code> object that was passed into the module from the server.js script described above by calling the route method on the <code>app</code>. Within the route method the route is specified and then the <code>get</code> method is called, or chained, on the result of calling route. Inside the chained get method is a callback function that we will further discuss in the section on controllers. This method of defining routes is known as &quot;route chaining&quot;.</p>
<p>The first route describes an endpoint that will display the state and city tax rates in response to a GET request corresponding to <code>:stateName</code> and <code>:cityName</code>, respectively. In Express you specify what are known as &quot;route parameters&quot; by preceding a section of a route delimited between forward slashes with a colon to indicate a placeholder for a meaningful route parameter. The second route <em>/calculate/:stateName/:cityName/:amount</em> describes an endpoint that will calculate the city and state tax amounts as well as total amount based off the amount parameter of the route.</p>
<p>The two other invocations of the <code>app</code> object are specifying middleware. <a href="/how-to-write-express-js-middleware/">Express.js middleware</a> is an incredibly useful feature that has many applications which could easily warrant their own series of articles so, I won&apos;t be going into great depth here. Just know that middleware are functions that can hook into, access and modify the request, response, error, and next objects of an Express request-response cycle.</p>
<p>You register a middleware function by calling the <code>use</code> method on the <code>app</code> object and passing in unique combinations of routes and callback functions. The first middleware declared on the app object specifies our two URLs within an array and a callback function that checks to see if the state being passed to request tax information is available.</p>
<p>This demo application will only be developed to respond to request for cities in Nebraska but someone could quite easily extend it with other states given they have a publicly available static webpage of similar information. The second middleware serves as a catch all for any URL paths requested that are not specified.</p>
<h4 id="controllers">Controllers</h4>
<p>Controllers are the part of an Express application that handles the actual requests made to the defined routes and returns a response. Strictly speaking, controllers are not a requirement for developing Express applications. A callback function, anonymous or otherwise, can be used but using controllers lead to better organized code and separation of concerns. That being said, we will be using controllers as it is always a good idea to follow best practices.</p>
<p>In your controllers/index.js file copy and paste the following code.</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const svc = require(&apos;../services&apos;);

const getTaxRate = (req, res) =&gt; {
    const state = req.params.stateName;
    svc.scrapeTaxRates(state, stateUrls[state.toLowerCase()], (rates) =&gt; {
        const rate = rates.find(rate =&gt; {
            return rate.city.toLowerCase() === req.params.cityName.toLowerCase();
        });
        res.send(rate);
    });
}

const calculateTaxes = (req, res) =&gt; {
    const state = req.params.stateName;
    svc.scrapeTaxRates(state, stateUrls[state.toLowerCase()], (rates) =&gt; {
        const rate = rates.find(rate =&gt; {
            return rate.city.toLowerCase() === req.params.cityName.toLowerCase();
        });
        res.send(rate.calculateTax(parseFloat(req.params.amount)));
    });
}


const stateUrls = {
    nebraska: &apos;http://www.revenue.nebraska.gov/question/sales.html&apos;;
};

module.exports = {
    getTaxRate,
    calculateTaxes,
    stateUrls
};
</code></pre>
<p>The first thing you see being imported and declared in the controllers module is a constant called <code>svc</code> which is short for &quot;service&quot;. This service object serves as a reusable piece of functionality to request a webpage and parse the resultant HTML. I will go more into depth in the section on Cheerio and services on what is going on behind the scenes with this service object, but for now just know that it parses the HTML for the meaningful bits we are interested (i.e., tax rates).</p>
<p>The two functions that we are most interested in are <code>getTaxRate</code> and <code>calculateTaxes</code>. Both functions are passed in request and response (<code>req</code> and <code>res</code>) objects via the <code>route.get(...)</code> methods in the routes module. The <code>getTaxRate</code> function accesses the <code>stateName</code> route parameter from the params object of the request object.</p>
<p>The state name and its corresponding target URL (in this case only Nebraska and its government webpage displaying taxable information) are passed to the service object&apos;s method <code>scrapeTaxRates</code>. A callback function is passed as a third parameter to filter out and respond with the city information corresponding to the <code>cityName</code> parameter found in the route path.</p>
<p>The second controller function, <code>calculateTaxes</code>, again uses the service method <code>scrapeTaxRates</code> to request and parse the HTML, but this time it calculates the taxes via a method within the <code>TaxRate</code> class, which we&apos;ll discuss next in the section on models.</p>
<h4 id="models">Models</h4>
<p>Similar to controllers, models are not something that are strictly required for an Express application. However, models come in quite handy when we want to encapsulate data (state) and behavior (actions) within our applications in an organized manner.</p>
<p>In your models/index.js file, copy and paste the following code:</p>
<pre><code class="language-javascript">&apos;use strict&apos;

class TaxRate {
    constructor(state, city, localRate, stateRate) {
        this.state = state;
        this.city = city;
        this.localRate = localRate;
        this.stateRate = stateRate;
    }

    calculateTax (subTotal) {
        const localTax = this.localRate * subTotal;
        const stateTax = this.stateRate * subTotal;
        const total = subTotal + localTax + stateTax;
        return {
            localTax,
            stateTax,
            total
        };
    }
}

module.exports = TaxRate;
</code></pre>
<p>The only model (or more correctly stated: class) that we will define in our application is <code>TaxRate</code>. <code>TaxRate</code> contains member fields for holding data on state, city, local tax rate, and state tax rate. These are the class fields that make up the state of the object. There is only one class method, <code>calculateTax(...)</code>, which takes in the parameter representing a subtotal amount passed into the route <em>/calculate/:stateName/:cityName/:amount</em> path and will return an object representing the calculated tax quantities and final total amount.</p>
<h3 id="cheerio">Cheerio</h3>
<p><a rel="nofollow" target="_blank" href="https://github.com/cheeriojs/cheerio">Cheerio</a> is a lightweight JavaScript library which implements the jQuery core to access, select, and query HTML in server-side apps. In our case we will be using Cheerio to parse the HTML in the static webpage we request from the Nebraska government&apos;s website displaying tax information.</p>
<h4 id="services">Services</h4>
<p>In our little application we will use a custom services module to implement the requesting of the HTML page from the Nebraska government&apos;s web site as well as the parsing of the resultant HTML to extract the data we desire.</p>
<p>In your services/index.js file copy and paste the following code:</p>
<pre><code class="language-javascript">&apos;use strict&apos;;

const http = require(&apos;http&apos;);
const cheerio = require(&apos;cheerio&apos;);
const TaxRate = require(&apos;../models&apos;);

const scrapeTaxRates = (state, url, cb) =&gt; {
    http.get(url, (res) =&gt; {
        let html = &apos;&apos;;
  
        res.on(&apos;data&apos;, chunk =&gt; {
            html += chunk;
        });
  
        res.on(&apos;end&apos;, () =&gt; {
            const parser = new Parser(state);
            const rates = parser.parse(html);
            cb(rates);
        });
    });
};

class Parser {
    constructor(state) {
        this.state = state;
    }

    parse(html) {
        switch(this.state.toLowerCase()) {
            case &apos;nebraska&apos;:
                return this.parseNebraska(html);
            default:
                return null;
        }
    }

    parseNebraska(html) {
        const $ = cheerio.load(html);
        let rates = [];
        $(&apos;tr&apos;).each((idx, el) =&gt; {
            const cells = $(el).children(&apos;td&apos;);
            if (cells.length === 5 &amp;&amp; !$(el).attr(&apos;bgcolor&apos;)) {
                const rawData = {
                    city: $(cells[0]).first().text(),
                    cityRate: $(cells[1]).first().text(),
                    totalRate: $(cells[2]).first().text()
                };
                rawData.cityRate = parseFloat(rawData.cityRate.replace(&apos;%&apos;, &apos;&apos;))/100;
                rawData.totalRate = parseFloat(rawData.totalRate.substr(0, rawData.totalRate.indexOf(&apos;%&apos;)))/100;
                rawData.stateRate = rawData.totalRate - rawData.cityRate;
                rates.push(new TaxRate(&apos;Nebraska&apos;, rawData.city, rawData.cityRate, rawData.stateRate));
            }
        });
        return rates;
    }
}

module.exports = {
    scrapeTaxRates;
};
</code></pre>
<p>The first three lines are importing (via <code>require()</code>) some module level objects <code>http</code>, <code>cheerio</code>, and <code>TaxRate</code>. <code>TaxRate</code> was described in the previous section on modules so we won&apos;t beat the proverbial dead horse and go over its use in too much detail, so suffice it to say it is used to store tax rate data and calculate taxes.</p>
<p>The <code>http</code> object is a Node module that is used to make requests from the server to another networked resource, which in our case is the tax rate webpage from the Nebraska government. The remaining one is Cheerio, which is used to parse the HTML using the familiar jQuery API.</p>
<p>The services module only exposes one publicly available function called <code>scrapeTaxRates</code>, which takes a state name string, URL string (for the state&apos;s page displaying tax rates), and a callback function to process the tax rates in unique ways specified by the calling client code.</p>
<p>Within the body of the <code>scrapeTaxRates</code> function the <code>get</code> method to the <code>http</code> object is called to request the webpage at the specified URL. The callback function passed to the <code>http.get(...)</code> method handles the processing of the response. In the processing of the response, a string of HTML is built and stored in a variable called <code>html</code>. This is done in an incremental fashion as the <code>data</code> event is fired and a buffered chunk of data is returned from the response.</p>
<p>Upon the firing of the <code>end</code> event a final callback function is invoked. Inside this callback the <code>Parser</code> class is instantiated and the parse method is called to parse the HTML and extract the information specific to the structure and layout of the Nebraska webpage. The parsed data is loaded into a series of <code>TaxRate</code> objects stored in an array and passed to the callback function to execute the logic specified in the calling client code (in our case, in the controller functions described earlier). It&apos;s in this last step that data is serialized and sent as a response to the caller of the REST API.</p>
<h3 id="conclusion">Conclusion</h3>
<p>In this short article we investigated how to design a simple lightweight Node.js application that scrapes useful tax rate information from a government website, which could be useful for e-commerce applications. The two primary purposes of the application are to collect tax rates, and either display that information for a given city, or to calculate taxes based on a state, city, and subtotal.</p>
<p>For example, below you will find screenshots of the application displaying taxes for the city of Omaha, and calculating taxes for a subtotal of $1000. In order to test this application <code>cd</code> into the root directory and type <code>$ node server.js</code> into the console. You will see a message that says, &quot;Node application running on port 3500&quot;.</p>
<img src="https://s3.amazonaws.com/stackabuse/media/build-web-scraped-api-express-cheerio-4.png" class="img-responsive">
<img src="https://s3.amazonaws.com/stackabuse/media/build-web-scraped-api-express-cheerio-5.png" class="img-responsive">
<p>I hope this article inspires you to further investigate the world of data scraping so you can create useful applications and meaningful data products. As always, I welcome any and all comments below.</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					