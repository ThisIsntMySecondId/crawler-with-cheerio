
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>As any experienced programmer knows, dates and times are incredibly common in most application-level code. You might use dates for tracking the creation of an object, to track the time since an event occurred, or to save the date of an upcoming event. However, dates aren&apos;t easy to work with, so it&apos;s important to have a library that is both accurate and has a simple interface. The standard JavaScript <code>Date</code> object isn&apos;t too bad, but it lacks some important features and isn&apos;t always simple to work with.</p>
<p>In this article you&apos;ll see how Moment makes dates and times easy to parse, format, and manipulate.</p>
<h2 id="parsingdates">Parsing Dates</h2>
<h3 id="strings">Strings</h3>
<p>By default, Moment attempts to parse date strings using the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/ISO_8601">ISO 8601</a> format, which has a wide range of valid dates. You can specify as little, or as much, time precision as you want in your date-time using this format. This is great for us since dates can take on many different forms, depending on the level of detail you want to specify.</p>
<p>Considering trying to parse all of these different formats on your own:</p>
<ul>
<li>20160628</li>
<li>2016-06-28T09</li>
<li>20160628T080910,123</li>
<li>2016-06-28 09:30:26.123</li>
<li>2016-06-28 09:30:26.123+07:00</li>
</ul>
<p>As you can see, not only does the time precision change, but the format in which it is specified can widely vary, which is why it&apos;s so important to have a capable time parser.</p>
<p>First off, the simplest way to create a <code>moment</code> object is to call the constructor with no arguments:</p>
<pre><code class="language-javascript">&gt; const moment = require(&apos;moment&apos;);
&gt; let m = moment();
</code></pre>
<p>This will instantiate a date object with the current time.</p>
<p>To parse a date-time string with Moment, just pass it to the constructor:</p>
<pre><code class="language-javascript">&gt; let date = moment(&apos;2016-06-28 09:30:26.123&apos;);
</code></pre>
<p>If for some reason Moment isn&apos;t able to parse the string you gave it, then it will fall back to using the built-in <code>new Date()</code> object for parsing.</p>
<p>To check if your date was parsed and valid, use the <code>.isValid()</code> method:</p>
<pre><code class="language-javascript">&gt; moment(&apos;2016-06-28 09:30:26.123&apos;).isValid();
true
&gt; moment(&apos;derp&apos;).isValid();
false
</code></pre>
<p>For all date objects created with Moment, no matter how you parse or create them, the time zone in the object will default to the current time zone, unless specified directly. To get UTC times back, use <code>moment.utc()</code> instead. For more information on time zones, check out the section <a href="#momenttimezones">Moment Time Zones</a>.</p>
<h3 id="specifyingformats">Specifying Formats</h3>
<p>One of my favorite parsing features in Moment is the string/format parser. It is basically like a reverse string formatter. You provide the date-time string to be parsed <em>and</em> another string that specifies the format that it&apos;s in. This way you can use strings of any format you want and still comfortably use them with Moment.</p>
<p>For example, in the US (for some reason) we like to format our dates as &quot;Month/Day/Year&quot;, whereas much of the rest of the world formats theirs as &quot;Day/Month/Year&quot;. This leaves a lot of room for confusion. For example, is the date &quot;11/06/2016&quot; supposed to be November 6th, or June 11th?</p>
<p><img src="https://s3.amazonaws.com/stackabuse/media/date-formats-world.png" alt="world time formats" title="World Time Formats"><br>
<small>Image: John Harding/Mona Chalabi via <a rel="nofollow" target="_blank" href="https://www.theguardian.com/news/datablog/2013/dec/16/why-do-americans-write-the-month-before-the-day">The Guardian</a></small></p>
<p>So how will we know if your dates are being parsed correctly? Using format specifiers like this ensures there are no ambiguities in your dates, assuming you know beforehand which format they&apos;re in. In the following example we&apos;re still able to parse the correct dates despite the different, and potentially confusing, formats.</p>
<pre><code class="language-javascript">&gt; let d1 = moment(&apos;11.06.2016&apos;, &apos;DD-MM-YYYY&apos;);
&gt; let d2 = moment(&apos;06/11/2016&apos;, &apos;MM-DD-YYYY&apos;);

&gt; d1.format();    // &apos;2016-06-11T00:00:00-05:00&apos;
&gt; d2.format();    // &apos;2016-06-11T00:00:00-05:00&apos;
</code></pre>
<p>Notice that we also use different delimiters in our date strings, &quot;.&quot; and &quot;/&quot;. Moment actually ignores all non-alphanumeric characters when using these formats, so you don&apos;t always need to worry about matching the formats perfectly.</p>
<p>For a complete set of available formatting tokens, check out <a rel="nofollow" target="_blank" href="http://momentjs.com/docs/#year-month-and-day-tokens">this section of the Moment.js docs</a>.</p>
<h3 id="unixtimestamps">Unix Timestamps</h3>
<p>As you&apos;d expect, Moment is also capable of parsing integer dates (<a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Unix_time">Unix time</a>) as well, either in the seconds or milliseconds format:</p>
<pre><code class="language-javascript">&gt; moment.unix(1467128085);      // Date in seconds from 1970
&gt; moment(1467128085747);        // Date in milliseconds from 1970
</code></pre>
<p>The only difference in the resulting times is the precision. <code>millisecondDate</code> will have a non-zero value for the millisecond field.</p>
<h2 id="printingdates">Printing Dates</h2>
<p>In my opinion, this is one of the more useful parts of Moment, mostly because JavaScript&apos;s built-in <code>Date</code> object doesn&apos;t have very good support for it. Surprisingly, the only built-in way to do formatting with <code>Date</code> is to use the <code>Date.toLocaleDateString()</code> method, which feels pretty clunky and not as flexible:</p>
<pre><code class="language-javascript">&gt; let d = new Date(1467128085747);
&gt; let options = {
...    weekday: &apos;long&apos;, year: &apos;numeric&apos;, month: &apos;short&apos;,
...    day: &apos;numeric&apos;, hour: &apos;2-digit&apos;, minute: &apos;2-digit&apos;
... };

&gt; date.toLocaleTimeString(&apos;en-us&apos;, options);
&apos;Tuesday, Jun 28, 2016, 10:34 AM&apos;
</code></pre>
<p>With Moment, we can easily achieve this same formatting with only one line of code (which I&apos;ll show in the next section).</p>
<p>We&apos;ll break this down in to a few subsections. First, we&apos;ll go over the traditional formatting with tokens, then we&apos;ll show the relative date formats available (like &quot;18 minutes ago&quot;), and finally we&apos;ll show how to format the dates as different kinds of structured data, like arrays, JSON, or a plain JavaScript <code>Object</code>.</p>
<h3 id="formatting">Formatting</h3>
<p>Use the <code>.format()</code> method to display the date as a string. Without any arguments, it prints the string in the ISO 8601 representation:</p>
<pre><code class="language-javascript">&gt; let date = moment.unix(1467128085);
&gt; date.format();
&apos;2016-06-28T10:34:45-05:00&apos;
</code></pre>
<p>Otherwise, you can provide your own format and customize it to your liking using tokens.</p>
<pre><code class="language-javascript">&gt; date.format(&apos;dddd, MMMM Do YYYY, h:mm a&apos;);
&apos;Tuesday, June 28th 2016, 10:34 am&apos;
</code></pre>
<p>You might notice that this is the same representation as the <code>Date.toLocaleTimeString()</code> example from above, but in one line. And all it took was the string &apos;dddd, MMMM Do YYYY, h:mm a&apos;.</p>
<p>Again, the <a rel="nofollow" target="_blank" href="http://momentjs.com/docs/#/displaying/format/">full list of format tokens</a> can be found at Moment&apos;s very thorough documentation website.</p>
<h3 id="relativeformats">Relative Formats</h3>
<p>Often times in web apps, for example, it&apos;s helpful to show the user how much time has elapsed since an event occurred. Instead of calculating this yourself, Moment offers some utility functions to handle this formatting for you.</p>
<p>In all cases, you can use any date from the past or future and the returned string will reflect the correct tense.</p>
<p>Out of the box, you get a few different options:</p>
<p><strong>Time from now</strong></p>
<p>Assuming today&apos;s date is July 1st, 2016, you&apos;d get the following relative formatting:</p>
<pre><code class="language-javascript">&gt; moment({year: 2016, month: 3, day: 13, hour: 10}).fromNow();
&apos;3 months ago&apos;
&gt; moment({year: 2016, month: 9, day: 23, hour: 10}).fromNow();
&apos;in 4 months&apos;
</code></pre>
<p>You can optionally pass a <code>Boolean</code> to <code>.fromNow()</code> telling it whether or not to include the &quot;ago&quot; (or &quot;in&quot;) string in the formatting. This way you can still easily customize the relative string if needed.</p>
<pre><code class="language-javascript">&gt; moment({year: 2016, month: 3, day: 13, hour: 10}).fromNow(true);
&apos;3 months&apos;
</code></pre>
<p><strong>Time from Date</strong></p>
<pre><code class="language-javascript">&gt; let may = moment({year: 2016, month: 5, day: 3});
&gt; let october = moment({year: 2016, month: 10, day: 9});
&gt;
&gt; may.from(october);
&apos;5 months ago&apos;
&gt; october.from(may);
&apos;in 5 months&apos;
</code></pre>
<p><strong>Time to now</strong></p>
<pre><code class="language-javascript">&gt; moment({year: 2016, month: 3, day: 13, hour: 10}).toNow();
&apos;in 3 months&apos;
&gt; moment({year: 2016, month: 9, day: 23, hour: 10}).toNow();
&apos;4 months ago&apos;
</code></pre>
<p><strong>Time to Date</strong></p>
<pre><code class="language-javascript">&gt; let may = moment({year: 2016, month: 5, day: 3});
&gt; let october = moment({year: 2016, month: 10, day: 9});
&gt; may.to(october)
&apos;in 5 months&apos;
&gt; 
&gt; october.to(may)
&apos;5 months ago&apos;
</code></pre>
<p>You might have noticed that both the &quot;from&quot; and &quot;to&quot; methods can be interchanged, depending on which dates are passed in the argument. It is all relative.</p>
<h3 id="structureddatetimes">Structured Date-times</h3>
<p>In some cases, it may be more convenient to have your date data in a structured format, possibly for use in an algorithm or serialization. Moment offers a few different ways to format the data in to data structures:</p>
<ul>
<li><strong>toDate()</strong>: Returns the Moment date as a JavaScript <code>Date</code></li>
<li><strong>toArray()</strong>: Returns date data as an array - <code>[ 2016, 5, 28, 10, 34, 45, 747 ]</code></li>
<li><strong>toJSON()</strong>: Returns ISO date string adjusted to UTC - &quot;2016-06-28T15:34:45.747Z&quot;</li>
<li><strong>toISOString()</strong>: Returns ISO date string adjusted to UTC - &quot;2016-06-28T15:34:45.747Z&quot;</li>
<li><strong>toObject()</strong>: Returns a plain JavaScript <code>Object</code> with date data - <code>{years: 2016, months: 5, date: 28, hours: 10, minutes: 34, seconds: 45, milliseconds: 747}</code></li>
<li><strong>toString()</strong>: Returns a formatted string similar to <code>Date.toString()</code> - &quot;Tue Jun 28 2016 10:34:45 GMT-0500&quot;</li>
</ul>
<h2 id="manipulatingdates">Manipulating Dates</h2>
<p>The ability to manipulate dates is also pretty important for many applications. And this isn&apos;t as simple as your normal arithmetic either - manipulating dates is hard. Can you easily figure out these date/time additions/subtractions? It isn&apos;t an easy task to program.</p>
<ul>
<li>February 21st + 13 weeks</li>
<li>3:14am + 424 minutes</li>
<li>July 1st - 1899400140 milliseconds</li>
</ul>
<p>Now, what if it&apos;s a leap year? Or a year with a <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Leap_second">leap second</a>? Lucky for you, you don&apos;t need to figure this out yourself. Moment already has for you.</p>
<p>There are quite a few time manipulation methods, so we&apos;ll only go over the more commonly used ones:</p>
<p><strong>Adding/Subtracting</strong></p>
<p>Use a number/string or an object to manipulate the date:</p>
<pre><code class="language-javascript">&gt; moment().add(7, &apos;days&apos;);
&gt; moment().subtract({days:13, months:3});
</code></pre>
<p>Chaining also works well:</p>
<pre><code class="language-javascript">&gt; moment().add({hours: 7}).subtract(13, &apos;minutes&apos;);
</code></pre>
<p><strong>Start/End of Time</strong></p>
<p>These convenience methods set the date/time to the end of the given unit of time. For example, if you have a date with a time of 2:15, but you need it to be the start of the day, you&apos;d use:</p>
<pre><code class="language-javascript">&gt; moment().startOf(&apos;day&apos;);
</code></pre>
<p>This will set the time to 12:00am of the same day. The same works for year, month, hour, and many more.</p>
<pre><code class="language-javascript">&gt; moment().endOf(&apos;year&apos;);   // sets date to 12-31-2016 23:59:59.999
</code></pre>
<p>I&apos;ve found this to be very useful in reporting applications where users can select time frames for reports, like Google Analytics. In order to retrieve the correct data, you need to have the correct range.</p>
<h2 id="momenttimezones">Moment Time Zones</h2>
<p>Moment supports setting time zone offsets out of the box, but if you need better time zone support, then you should consider using <code>moment-timezone</code>.</p>
<p>This library lets you specify time zones by city, region, or other identifiers, which can make things much simpler for user-facing applications.</p>
<p>To use it, install with npm and <code>require()</code> this in place of <code>moment</code>:</p>
<pre><code class="language-javascript">&gt; const moment = require(&apos;moment-timezone&apos;);
</code></pre>
<p>With over 550 time zone identifiers, you can split up your time zone specifiers by various regional categories and names:</p>
<ul>
<li>Time zone name: US/Central, US/Eastern, US/Mountain, etc</li>
<li>City: America/Chicago, America/Los_Angeles, Asia/Dubai, Australia/Sydney, etc</li>
<li>GMT Offset: Etc/GMT+6, Etc/GMT-2, Etc/GMT0, etc</li>
</ul>
<p>For a full list of time zone identifiers, you can see a full list of names by executing:</p>
<pre><code class="language-javascript">&gt; const moment = require(&apos;moment-timezone&apos;);
&gt; moment.tz.names()
</code></pre>
<p>To use these identifiers to set the time and time zone with the <code>.tz()</code> method:</p>
<pre><code class="language-javascript">&gt; moment.tz({year: 2016, month: 6, day: 30, hour: 11}, &apos;America/Los_Angeles&apos;).format();
&apos;2016-07-30T11:00:00-07:00&apos;
&gt; moment.tz({year: 2016, month: 6, day: 30, hour: 11}, &apos;America/Chicago&apos;).format();
&apos;2016-07-30T11:00:00-05:00&apos;
</code></pre>
<h2 id="conclusion">Conclusion</h2>
<p>Programmatically working with dates and times is hard, but it doesn&apos;t have to be the hardest thing you do. Moment is a great example of a library making a difficult topic much simpler with a clean and easy-to-use API.</p>
<p>In addition to the parsing, formatting, and manipulation that Moment provides, there is also add-on support for time zones via the <code>moment-timezone</code> package. Make life easier for yourself and your users by adding better support for time zones.</p>
<p><em>What other features of Moment do you use often? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					