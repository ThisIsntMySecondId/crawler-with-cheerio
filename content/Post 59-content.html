
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>Iterators and generators are usually a secondary thought when writing code, but if you can take a few minutes to think about how to use them to simplify your code, they&apos;ll save you a lot of debugging and complexity. With the new ES6 iterators and generators, JavaScript gets similar functionality to Java&apos;s Iterable, allowing us to customize our iteration on objects.</p>
<p>For example, if you had a Graph object, you can easily use a generator to walk through the nodes or edges. This makes for much cleaner code by putting the traversal logic within the Graph object where it belongs. This separation of logic is good practice, and iterators/generators make it easier to follow these best practices.</p>
<h4 id="es6iteratorsandgenerators">ES6 Iterators and Generators</h4>
<h5 id="iterators">Iterators</h5>
<p>Using iterators, you can create a way to iterate using the <code>for...of</code> construct for your custom object. Instead of using <code>for...in</code>, which just iterates through all of the object&apos;s properties, using <code>for...of</code> lets us make a much more custom and structured iterator in which we choose which values to return for each iteration.</p>
<p>Under the hood, <code>for...of</code> is actually using <code>Symbol.iterator</code>. Recall <a href="/es6-symbols">Symbols</a> are a new feature in JavaScript ES6. The <code>Symbol.iterator</code> is special-purpose symbol made especially for accessing an object&apos;s internal iterator. So, you could use it to retrieve a function that iterates over an array object, like so:</p>
<pre><code>var nums = [6, 7, 8];
var iterator = nums[Symbol.iterator]();
iterator.next();				// Returns { value: 6, done: false }
iterator.next();				// Returns { value: 7, done: false }
iterator.next();				// Returns { value: 8, done: false }
iterator.next();				// Returns { value: undefined, done: true }
</code></pre>
<p>When you use the <code>for...of</code> construct, this is actually what is being used underneath. Notice how each subsequent value is returned, along with an indicator telling you if you&apos;re at the The vast majority of the time you won&apos;t need to use <code>next()</code> manually like this, but the option is there in case you have a use case requiring more complex looping.</p>
<p>You can use <code>Symbol.iterator</code> to define specialized iteration for an object. So lets say you have your own object that&apos;s a wrapper for sentences, <code>Sentence</code>.</p>
<pre><code>function Sentence(str) {
    this._str = str;
}
</code></pre>
<p>To define how we iterate over the internals of the Sentence object, we supply a prototype iterator function:</p>
<pre><code>Sentence.prototype[Symbol.iterator] = function() {
    var re = /\S+/g;
    var str = this._str;

    return {
        next: function() {
            var match = re.exec(str);
            if (match) {
                return {value: match[0], done: false};
            }
            return {value: undefined, done: true};
        }
    }
};
</code></pre>
<p>Now, using the iterator we just created above (containing a regex string that matches only words), we can easily iterate over the words of any sentence we supply:</p>
<pre><code>var s = new Sentence(&apos;Good day, kind sir.&apos;);

for (var w of s) {
    console.log(w);
}

// Prints:
// Good
// day,
// kind
// sir.
</code></pre>
<h5 id="generators">Generators</h5>
<p>ES6 generators build on top of the what iterators supply by using special syntax for more easily creating the iteration function. Generators are defined using the <code>function*</code> keyword. Within a <code>function*</code>, you can repeatedly return values using <code>yield</code>. The <code>yield</code> keyword is used in generator functions to pause execution and return a value. It can be thought of as a generator-based version of the <code>return</code> keyword. In the next iteration, execution will resume at the last point that <code>yield</code> was used.</p>
<pre><code>function* myGenerator() {
    yield &apos;foo&apos;;
    yield &apos;bar&apos;;
    yield &apos;baz&apos;;
}

myGenerator.next();		// Returns {value: &apos;foo&apos;, done: false}
myGenerator.next();		// Returns {value: &apos;bar&apos;, done: false}
myGenerator.next();		// Returns {value: &apos;baz&apos;, done: false}
myGenerator.next();		// Returns {value: undefined, done: true}
</code></pre>
<p>Or, you could use the <code>for...of</code> construct:</p>
<pre><code>for (var n of myGenerator()) {
    console.log(n);
}

// Prints
// foo
// bar
// baz
</code></pre>
<p>In this case, we can see that the Generator then takes care of returning <code>{value: val, done: bool}</code> object for you, which is all handled under the hood in <code>for...of</code>.</p>
<p>So how can we use generators to our advantage? Going back to our previous example, we can simplify the <code>Sentence</code> iterator to the following code:</p>
<pre><code>Sentence.prototype[Symbol.iterator] = function*() {
    var re = /\S+/g;
    var str = this._str;
    var match;
    while (match = re.exec(str)) {
        yield match[0];
    }
};
</code></pre>
<p>Notice how the iterator (now a Generator) function is much smaller than the previous version. We no longer need to return an object with the <code>next</code> function, and we no longer need to deal with returning the <code>{value: val, done: bool}</code> object. While these savings may seem minimal in this example, its usefulness will easily be realized as your Generators grow in complexity.</p>
<h4 id="advantages">Advantages</h4>
<p>As <a href="https://jakearchibald.com/2014/iterators-gonna-iterate/">Jake Archibald</a> points out, some advantages of these generators are:</p>
<ul>
<li>
<p><strong>Laziness</strong>: The values aren&apos;t calculated ahead of time, so if you don&apos;t iterate until the end you won&apos;t have wasted the time calculating the unused values.</p>
</li>
<li>
<p><strong>Infinite</strong>: Since values aren&apos;t calculated ahead of time, you can have an infinite set of values be returned. Just make sure you break out of the loop at some point.</p>
</li>
<li>
<p><strong>String iteration</strong>: Thanks to <code>Symbol.iterator</code>, String now has its own iterator to make looping over characters much easier. Iterating over the character symbols of a string can be a real pain. This is especially useful now that JavaScript ES5 supports unicode.</p>
<p>for (var symbol of string) {<br>
console.log(symbol);<br>
}</p>
</li>
</ul>
<h4 id="conclusion">Conclusion</h4>
<p>While iterators and generators aren&apos;t huge additional features, they do help quite a bit to clean up code and to keep it organized. Keeping iteration logic with the object it belongs to is good practice, which seems to be much of the focus of ES6 features. The standard seems to be moving towards the structure-ability and design-friendliness of Java, while still maintaining the speed of development of dynamic languages.</p>
<p><em>What do you think of the new ES6 features? What kind of interesting use cases do you have for iterators and generators? Let us know in the comments!</em></p>
<p><em>Thanks to <a href="https://jakearchibald.com/2014/iterators-gonna-iterate/">Jake Archibald</a> for the great article detailing much of how iterators and generators work in ES6.</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					