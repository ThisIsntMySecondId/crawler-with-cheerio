
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p>One of the most common resources you&apos;ll interact with in a language like Node.js (primarily a web-focused language) are databases. And with SQL being the most common of all the different types, you&apos;ll need a good library to help you interact with it and its many features.</p>
<p>Bookshelf.js is among the most popular of the Node.js ORM packages. It stems from the <a rel="nofollow" target="_blank" href="https://github.com/tgriesser/knex">Knex.js</a>, which is a flexible query builder that works with PostgreSQL, MySQL and SQLite3. <a rel="nofollow" target="_blank" href="https://github.com/tgriesser/bookshelf/">Bookshelf.js</a> builds on top of this by providing functionality for creating data models, forming relations between these models, and other common tasks needed when querying a database.</p>
<p>Bookshelf also supports multiple database back-ends, like <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/MySQL">MySQL</a>, <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/PostgreSQL">PostgreSQL</a>, and <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/SQLite">SQLite</a>. This way you can easily switch databases when needed, or use a smaller DB like SQLite during development and Postgre in production.</p>
<p>Throughout this article I&apos;ll show you how to get the most out of this Node ORM, including connecting to a database, creating models, and saving/loading objects.</p>
<h3 id="installbookshelf">Install Bookshelf</h3>
<p>Bookshelf is a bit different than most Node packages in that it doesn&apos;t install all of its dependencies for you automatically. In this case, you must manually install Knex along with Bookshelf:</p>
<pre><code class="language-sh">$ npm install knex --save
$ npm install bookshelf --save
</code></pre>
<p>In addition to that, you need to choose which database you&apos;ll want to use Bookshelf with. Your choices are:</p>
<ul>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/pg">pg</a> (PostgreSQL)</li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/mysql">mysql</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/mariasql">mariasql</a></li>
<li><a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/sqlite3">sqlite3</a></li>
</ul>
<p>These can be installed with:</p>
<pre><code class="language-sh">$ npm install pg --save
$ npm install mysql --save
$ npm install mariasql --save
$ npm install sqlite3 --save
</code></pre>
<p>One thing that I tend to do with my projects is install a production-grade DB (like Postgre) using <code>--save</code>, while using <code>--save-dev</code> for a smaller DB like SQLite for use during development.</p>
<pre><code class="language-sh">$ npm install pg --save
$ npm install sqlite3 --save-dev
</code></pre>
<p>This way we can easily switch between the databases in production and development without having to worry about flooding my production environment with unnecessary dependencies.</p>
<h3 id="connectingtoadatabase">Connecting to a Database</h3>
<p>All of the lower-level functions, like connecting to the database, are handled by the underlying Knex library. So, naturally, in order to initialize your <code>bookshelf</code> instance you&apos;ll need to create a <code>knex</code> instance first, like this:</p>
<pre><code class="language-javascript">var knex = require(&apos;knex&apos;)({
    client: &apos;sqlite3&apos;,
    connection: {
        filename: &apos;./db.sqlite&apos;
    }
});

var bookshelf = require(&apos;bookshelf&apos;)(knex);
</code></pre>
<p>And now you&apos;re able to use the <code>bookshelf</code> instance to create your models.</p>
<h3 id="settingupthetables">Setting up the Tables</h3>
<p>Knex, as its own website states, is a &quot;batteries included&quot; SQL query builder, so you can do just about anything through Knex that you&apos;d want to do with raw SQL statements. One of these important features is table creation and manipulation. Knex can be used directly to set up your schema within the database (think database initialization, schema migration, etc).</p>
<p>So first of all, you&apos;ll want to create your table using <code>knex.schema.createTable()</code>, which will create and return a table object that contains a bunch of <a rel="nofollow" target="_blank" href="http://knexjs.org/#Schema-Building">schema building</a> functions, like <code>table.increments()</code>, <code>table.string()</code>, and <code>table.date()</code>. For each model you create, you&apos;ll need to do something like this for each one:</p>
<pre><code class="language-javascript">knex.schema.createTable(&apos;users&apos;, function(table) {
    table.increments();
    table.string(&apos;name&apos;);
    table.string(&apos;email&apos;, 128);
    table.string(&apos;role&apos;).defaultTo(&apos;admin&apos;);
    table.string(&apos;password&apos;);
    table.timestamps();
});
</code></pre>
<p>Here you can see that we create a table called &apos;users&apos;, which we then initialize with columns &apos;name&apos;, &apos;email&apos;, &apos;role&apos;, and &apos;password&apos;. We can even take it a step further and specify the maximum length of a string column (128 for column &apos;email&apos;) or a default value (&apos;admin&apos; for column &apos;role&apos;).</p>
<p>Some convenience functions are also provided, like <code>timestamps()</code>. This function will add two timestamp columns to the table, <code>created_at</code> and <code>updated_at</code>. If you use this, consider also setting the <code>hasTimestamps</code> property to <code>true</code> in your model (see &apos;Creating a Model&apos; below).</p>
<p>There are quite a few more options you can specify for each table/column, so I&apos;d definitely recommend checking out the full <a rel="nofollow" target="_blank" href="http://knexjs.org">Knex documentation</a> for more details.</p>
<h3 id="creatingamodel">Creating a Model</h3>
<p>One of my gripes about Bookshelf is that you always need an initialized <code>bookshelf</code> instance in order to create a model, so structuring some applications can be a bit messy if you keep all your models in different files. Personally, I prefer to just make <code>bookshelf</code> a global using <code>global.bookshelf = bookshelf</code>, but that isn&apos;t necessarily the best way to do it.</p>
<p>Anyway, let&apos;s see what it takes to create a simple model:</p>
<pre><code class="language-javascript">var User = bookshelf.Model.extend({
    tableName: &apos;users&apos;,
    hasTimestamps: true,

    verifyPassword: function(password) {
        return this.get(&apos;password&apos;) === password;
    }
}, {
    byEmail: function(email) {
        return this.forge().query({where:{ email: email }}).fetch();
    }
});
</code></pre>
<p>Here we have a pretty simple model to demonstrate some of the available features. First of all, the <em>only</em> required property is <code>tableName</code>, which tells the model where to save and load data from in the DB. Obviously it&apos;s pretty minimal to get a model set up since all of the schema declaration is already done elsewhere.</p>
<p>As for the rest of the properties/functions, here is a quick rundown of what <code>User</code> includes:</p>
<ul>
<li><code>tableName</code>: A string that tells the model where to save and load data from in the DB (required)</li>
<li><code>hasTimestamps</code>: A boolean value telling the model whether we need <code>created_at</code> and <code>updated_at</code> timestamps</li>
<li><code>verifyPassword</code>: An instance function</li>
<li><code>byEmail</code>: A class (static) function</li>
</ul>
<p>So, for example, we&apos;ll use <code>byEmail</code> as a shorter way to query a user by their email address:</p>
<pre><code class="language-javascript">User.byEmail(&apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="d7a4b4b8a3a397b2afb6baa7bbb2f9b4b8ba">[email&#xA0;protected]</a>&apos;).then(function(u) {
    console.log(&apos;Got user:&apos;, u.get(&apos;name&apos;));
});
</code></pre>
<p>Notice how you access model data in Bookshelf. Instead of using a direct property (like <code>u.name</code>), we have to use the <code>.get()</code> method.</p>
<h4 id="es6support">ES6 Support</h4>
<p>As of the time of this writing, Bookshelf doesn&apos;t seem to have full ES6 support (see <a rel="nofollow" target="_blank" href="https://github.com/tgriesser/bookshelf/issues/756">this issue</a>). However, you can still write much of your model code using the new ES6 classes. Using the model from above, we can re-create it using the new <code>class</code> syntax like this:</p>
<pre><code class="language-javascript">class User extends bookshelf.Model {
    get tableName() {
        return &apos;users&apos;;
    }

    get hasTimestamps() {
        return true;
    }

    verifyPassword(password) {
        return this.get(&apos;password&apos;) === password;
    }

    static byEmail(email) {
        return this.forge().query({where:{ email: email }}).fetch();
    }
}
</code></pre>
<p>And now this model can be used exactly as the one before. This method won&apos;t give you any functional advantages, but it&apos;s more familiar for some people, so take advantage of it if you want to.</p>
<h4 id="collections">Collections</h4>
<p>In Bookshelf you also need to create a separate object for collections of a given model. So if you want to perform an operation on multiple <code>User</code>s at the same time, for example, you need to create a <code>Collection</code>.</p>
<p>Continuing with our example from above, here is how we&apos;d create the <code>Users</code> <code>Collection</code> object:</p>
<pre><code class="language-javascript">var Users = bookshelf.Collection.extend({
    model: User
});
</code></pre>
<p>Pretty simple, right? Now we can easily query for all users with (although this was already possible with a model using <code>.fetchAll()</code>):</p>
<pre><code class="language-javascript">Users.forge().fetch().then(function(users) {
    console.log(&apos;Got a bunch of users!&apos;);
});
</code></pre>
<p>Even better, we can now use some nice model methods on the collection as a whole, instead of having to iterate over each model individually. One of these methods that seems to get a lot of use, in web-apps especially, is <code>.toJSON()</code>:</p>
<pre><code class="language-javascript">exports.get = function(req, res) {
    Users.forge().fetch().then(function(users) {
        res.json(users.toJSON());
    });
};
</code></pre>
<p>This returns a plain JavaScript object of the entire collection.</p>
<h3 id="extendingyourmodels">Extending your Models</h3>
<p>As a developer, one of the most important principles I&apos;ve followed is the <a rel="nofollow" target="_blank" href="https://en.wikipedia.org/wiki/Don&apos;t_repeat_yourself">DRY</a> (Don&apos;t Repeat Yourself) principle. This is just one of the many reasons why model/schema extension is so important to your software design.</p>
<p>Using Bookshelf&apos;s <code>.extend()</code> method, you can inherit all of the properties, instance methods, and class methods of a base model. This way  you can create and take advantage of base methods that aren&apos;t already provided, like <code>.find()</code>, <code>.findOne()</code>, etc.</p>
<p>One great example of model extension is in the <a rel="nofollow" target="_blank" href="https://www.npmjs.com/package/bookshelf-modelbase">bookshelf-modelbase</a> project, which provides many of the missing methods that you&apos;d expect to come standard in most ORMs.</p>
<p>If you were to create your own simple base model, it might look like this:</p>
<pre><code class="language-javascript">var model = bookshelf.Model.extend({
    hasTimestamps: [&apos;created_at&apos;, &apos;updated_at&apos;],
}, {
    findAll: function(filter, options) {
        return this.forge().where(filter).fetchAll(options);
    },

    findOne: function(query, options) {
        return this.forge(query).fetch(options);
    },

    create: function(data, options) {
        return this.forge(data).save(null, options);
    },
});
</code></pre>
<p>Now all of your models can take advantage of these useful methods.</p>
<h3 id="savingandupdatingmodels">Saving and Updating Models</h3>
<p>There are a couple different ways to save models in Bookshelf, depending on your preferences and format of your data.</p>
<p>The first, and most obvious way, is to just call <code>.save()</code> on a model instance.</p>
<pre><code class="language-javascript">var user = new User();
user.set(&apos;name&apos;, &apos;Joe&apos;);
user.set(&apos;email&apos;, &apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="b8d2d7ddf8ddc0d9d5c8d4dd96dbd7d5">[email&#xA0;protected]</a>&apos;);
user.set(&apos;age&apos;, 28);

user.save().then(function(u) {
    console.log(&apos;User saved:&apos;, u.get(&apos;name&apos;));
});
</code></pre>
<p>This works for a model you create yourself (like the one above), or with model instances that are returned to you from a query call.</p>
<p>The other option is to use the <code>.forge()</code> method and initialize it with data. &apos;Forge&apos; is really just a shorthand way for creating a new model (like <code>new User()</code>). But this way you don&apos;t need an extra line to create the model before starting the query/save string.</p>
<p>Using <code>.forge()</code>, the above code would look like this:</p>
<pre><code class="language-javascript">var data = {
    name: &apos;Joe&apos;,
    email: &apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="3f55505a7f5a475e524f535a115c5052">[email&#xA0;protected]</a>&apos;,
    age: 28
}

User.forge(data).save().then(function(u) {
    console.log(&apos;User saved:&apos;, u.get(&apos;name&apos;));
});
</code></pre>
<p>This won&apos;t really save you any lines of code, but it can be convenient if <code>data</code> is actually incoming JSON or something like that.</p>
<h3 id="loadingmodels">Loading Models</h3>
<p>Here I&apos;ll talk about how to load models from the database with Bookshelf.</p>
<p>While <code>.forge()</code> didn&apos;t really help us out that much in saving documents, it certainly helps in loading them. It would be a bit awkward to create an empty model instance just to load data from the database, so we use <code>.forge()</code> instead.</p>
<p>The simplest example of loading is to just fetch a single model using <code>.fetch()</code>:</p>
<pre><code class="language-javascript">User.forge({email: &apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="83e9ece6c3e6fbe2eef3efe6ade0ecee">[email&#xA0;protected]</a>&apos;}).fetch().then(function(user) {
    console.log(&apos;Got user:&apos;, user.get(&apos;name&apos;));
});
</code></pre>
<p>All we do here is grab a single model that matches the given query. As you can imagine, the query can be as complex as you&apos;d like (like constraining on <code>name</code> and <code>age</code> columns as well).</p>
<p>Just like in plain old SQL, you can greatly customize the query and the data that is returned. For example, this query will only give us the data we need to authenticate a user:</p>
<pre><code class="language-javascript">var email = &apos;...&apos;;
var plainTextPassword = &apos;...&apos;;

User.forge({email: email}).fetch({columns: [&apos;email&apos;, &apos;password_hash&apos;, &apos;salt&apos;]})
.then(function(user) {
    if (user.verifyPassword(plainTextPassword)) {
        console.log(&apos;User logged in!&apos;);
    } else {
        console.log(&apos;Authentication failed...&apos;);
    }
});
</code></pre>
<p>Taking this even further, we can use the <code>withRelations</code> option to automatically load related models, which we&apos;ll see in the next section.</p>
<h3 id="modelrelations">Model Relations</h3>
<p>In many applications, your models will need to refer to other models, which is achieved in SQL using foreign keys. A simple version of this is supported in Bookshelf via relations.</p>
<p>Within your model, you can tell Bookshelf exactly how other models are related to one another. This is achieved using the <code>belongsTo()</code>, <code>hasMany()</code>, and <code>hasOne()</code> (among others) methods.</p>
<p>So let&apos;s say you have two models, User and Address. The User can have multiple Addresses (one for shipping, one for billing, etc), but an Address can belong to only one User. Given this, we might set our models up like this:</p>
<pre><code class="language-javascript">var User = bookshelf.Model.extend({
    tableName: &apos;users&apos;,
    
    addresses: function() {
        return this.hasMany(&apos;Address&apos;, &apos;user_id&apos;);
    },
});

var Address = bookshelf.Model.extend({
    tableName: &apos;addresses&apos;,
    
    user: function() {
        return this.belongsTo(&apos;User&apos;, &apos;user_id&apos;);
    },
});
</code></pre>
<p><em>Note that I&apos;m using the <a rel="nofollow" href="https://github.com/tgriesser/bookshelf/wiki/Plugin:-Model-Registry">registry plugin</a> here, which allows me to refer to the Address model with a string.</em></p>
<p>The <code>hasMany()</code> and <code>belongsTo()</code> methods tells Bookshelf how each model is related to one another. The User &quot;has many&quot; Addresses, while the Address &quot;belongs to&quot; a single user. The second argument is the column name that indicates the location of the key of the model. In this case, <em>both</em> models reference the <code>user_id</code> column in the Address table.</p>
<p>Now we can take advantage of this relation by using the <code>withRelated</code> option on <code>.fetch()</code> methods. So if I wanted to load a user <em>and</em> all of their addresses with one call, I could just do:</p>
<pre><code class="language-javascript">User.forge({email: &apos;<a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="fa90959fba9f829b978a969fd4999597">[email&#xA0;protected]</a>&apos;}).fetch({withRelated: [&apos;addresses&apos;]})
.then(function(user) {
    console.log(&apos;Got user:&apos;, user.get(&apos;name&apos;));
    console.log(&apos;Got addresses:&apos;, user.related(&apos;addresses&apos;));
});
</code></pre>
<p>If we were to fetch the User model <em>without</em> the <code>withRelated</code> option then <code>user.related(&apos;addresses&apos;)</code> would just return an empty Collection object.</p>
<p>Be sure to take advantage of these relation methods, they&apos;re far easier to use than creating your own SQL JOINs :)</p>
<h3 id="thegood">The Good</h3>
<p>Bookshelf is one of those libraries that seems to try and not become overly bloated and just sticks to the core features. This is great because the features that <em>are</em> there work very well.</p>
<p>Bookshelf also has a nice, powerful API that lets you easily build your application on top of it. So you don&apos;t have to wrestle around with high-level methods that made poor assumptions on how they&apos;d be used.</p>
<h3 id="thebad">The Bad</h3>
<p>While I do think it&apos;s nice that Bookshelf/Knex provides you with some lower-level functions, I still think there is room for improvement. For example, all of the table/schema setup is left up to you, and there isn&apos;t an easy way to specify your schema (like in a plain JS object) within the model. The table/schema setup has to be specified in API calls, which isn&apos;t all that easy to read and debug.</p>
<p>Another gripe of mine is how they left out many of the helper methods should come standard with the base model, like <code>.create()</code>, <code>.findOne()</code>, <code>.upsert()</code>, and data validation. This is exactly why I mentioned the <code>bookshelf-modelbase</code> project earlier as it fills in many of these gaps.</p>
<h3 id="conclusion">Conclusion</h3>
<p>Overall I&apos;ve become quite a fan of using Bookshelf/Knex for SQL work, although I do think that some of the problems I just mentioned might be a turn-off for many developers that are used to using ORMs that do just about everything for them out of the box. On the other hand, for other developers that like to have a lot of control, this is the perfect library to use.</p>
<p>While I tried to cover as much of the core API as possible in this article, there are still quite a few features I didn&apos;t get to touch on, so be sure to check out the <a rel="nofollow" target="_blank" href="http://bookshelfjs.org/">project documentation</a> for more info.</p>
<p><em>Have you used Bookshelf.js or Knex.js? What do you think? Let us know in the comments!</em></p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					