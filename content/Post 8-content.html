
						<!--kg-card-begin: markdown--><!--kg-card-begin: markdown--><p><strong>Edit</strong>: <em>Updated code to Camo v0.12.1</em></p>
<h3 id="introduction">Introduction</h3>
<p>First of all, <a href="https://github.com/scottwrobinson/camo">Camo</a> is a new class-based ES6 ODM for MongoDB and Node. With mainstream ES6 quickly approaching us, I thought we were long overdue for an ODM that took advantage of the new features, so I created Camo. What bothered me the most when transitioning from Java to JavaScript programming was the lack of traditional-style classes. I think for beginners especially, this is an important feature for any ODM to have.</p>
<p>In this tutorial I&apos;ll show you how to use the basic features of Camo (schema declaration, saving, loading, etc). To make comparisons with Mongoose easier, I&apos;ll be showing similar examples to the article <a href="http://blog.modulus.io/getting-started-with-mongoose" rel="nofollow">Getting Started with Mongoose</a>. Hopefully from here you can decide which style/functionality you like the best for use in your projects.</p>
<h3 id="connectingcamotomongodb">Connecting Camo to MongoDB</h3>
<p><strong>Note</strong>: The code in this article uses <em>v0.12.1</em> of Camo.</p>
<p>To connect Camo to your database, just pass the connection string (typically of the form <code>mongodb://[ip-address]/[db-name]</code>) to the <code>connect()</code> method, which will return an instance of the Camo client. The client instance can be used for configuring the DB, but isn&apos;t needed for declaring, saving, or deleting your documents.</p>
<pre><code class="language-javascript">var connect = require(&apos;camo&apos;).connect;
 
var database;
var uri = &apos;mongodb://localhost/test&apos;;
connect(uri).then(function(db) {
    database = db;
});
</code></pre>
<h3 id="schemasandmodels">Schemas and Models</h3>
<p>Models are <a href="https://www.npmjs.com/package/camo#declaring-your-document">declared</a> using ES6 classes and must extend the <code>Document</code> object. The model&apos;s schema is declared within the constructor, and any member variable that does not start with an underscore (<code>_</code>) is included in the schema and saved to the DB. A member variable can be declared by either directly assigning it a type, or by assigning it an object with options.</p>
<pre><code class="language-javascript">var Document = require(&apos;camo&apos;).Document;
 
class Movie extends Document {
    constructor() {
        super();

        this.title = String;
        this.rating = {
            type: String,
            choices: [&apos;G&apos;, &apos;PG&apos;, &apos;PG-13&apos;, &apos;R&apos;]
        };
        this.releaseDate = Date;
        this.hasCreditCookie = Boolean;
    }

    static collectionName() {
        return &apos;movies&apos;;
    }
}
</code></pre>
<p>The collection name is declared by passing overriding the static <code>collectionName()</code> method. Although this name is rarely needed in the code, it will become useful when inspecting the MongoDB database manually, so it&apos;s best to keep it relevant to the model name.</p>
<p>If <code>collectionName()</code> is not provided, then Camo will automatically assign a collection name based on the name of the class.</p>
<h3 id="createretrieveupdateanddeletecrud">Create, Retrieve, Update, and Delete (CRUD)</h3>
<p>To create an instance of the model, just use the <code>create()</code> method, which handles much of the instantiation work for you. Optionally, data can be passed to <code>create()</code> that will be assigned to the member variables. If data is not passed for a variable, it is then either assigned a default value (if specified in the schema), or assigned <code>null</code>.</p>
<pre><code class="language-javascript">var thor = Movie.create({
    title: &apos;Thor&apos;,
    rating: &apos;PG-13&apos;,
    releaseDate: new Date(2011, 4, 2),
    hasCreditCookie: true
});

thor.save().then(function(t) {
    console.log(thor);
});
</code></pre>
<p>When <a href="https://www.npmjs.com/package/camo#creating-and-saving">saving</a> the instance, the data will be inserted in to the database if it has not previously been saved. If the instance has already been saved to the database, then the existing document is updated with the changes.</p>
<p>To <a href="https://www.npmjs.com/package/camo#deleting">delete</a> a document, just simply call the <code>delete</code> method on it. You can also use the static <code>deleteOne()</code>, <code>deleteMany()</code>, or <code>findOneAndDelete()</code> on the model class. The number of documents deleted from the database will be returned within the <code>Promise</code>.</p>
<pre><code class="language-javascript">thor.delete().then(function(numDeleted) {
    console.log(numDeleted);
});
</code></pre>
<p>To <a href="https://www.npmjs.com/package/camo#loading">load</a> a document from the database, you have a few choices depending on what you want to do. The options are:</p>
<ul>
<li><code>findOne()</code> for loading a single document (or null if one does not exist)</li>
</ul>
<pre><code class="language-javascript">// Load only the &apos;Thor&apos; movie
Movie.findOne({ title: &apos;Thor&apos; }).then(function(movie) {
	console.log(thor);
});
</code></pre>
<ul>
<li><code>find()</code> for loading multiple documents (or an empty array if none exist)</li>
</ul>
<pre><code class="language-javascript">// Load all movies that have a credit cookie
Movie.find({ hasCreditCookie: true }).then(function(movies) {
    console.log(thor);
});
</code></pre>
<ul>
<li><code>findOneAndUpdate()</code> for retrieving a document and updating it in a single atomic operation</li>
</ul>
<pre><code class="language-javascript">// Update &apos;Thor&apos; to have a rating of &apos;R&apos;
Movie.findOneAndUpdate({ title: &apos;Thor&apos; }, { rating: &apos;R&apos; }).then(function(movies) {
    console.log(thor);
});
</code></pre>
<h3 id="extras">Extras</h3>
<p>Thanks to Camo&apos;s support of ES6 classes, we can easily define statics, virtuals, and methods for the models, which makes the model code much more organized and readable.</p>
<pre><code class="language-javascript">var Document = require(&apos;camo&apos;).Document;
 
class Movie extends Document {
    constructor() {
        super();

        // Schema declaration here...
    }

    set releaseYear(year) {
        this.releaseDate.setYear(year);
    }

    get releaseYear() {
        return this.releaseDate.getFullYear();
    }

    yearsOld() {
        return new Date().getFullYear() - this.releaseDate.getFullYear();
    }

    static findAllRMovies() {
        return this.find({ rating: &apos;R&apos; });
    }
}

var uri = &apos;nedb://memory&apos;;

connect(uri).then(function(db) {
    var thor = Movie.create({
        title: &apos;The Matrix&apos;,
        rating: &apos;R&apos;,
        releaseDate: new Date(1999, 2, 31),
        hasCreditCookie: true
    });
    
    return thor.save();
}).then(function(t) {
    return Movie.findAllRMovies();
}).then(function(movies) {
    movies.forEach(function(m) {
        console.log(m.title + &apos;: &apos; + m.releaseDate.getFullYear());
    });
});
</code></pre>
<p>In addition to interfacing with MongoDB, Camo also supports <a href="https://github.com/louischatriot/nedb">NeDB</a>, which is like the SQLite-equivalent to Mongo. An example of using NeDB is shown above. It&apos;s very helpful to use during development and testing since data can be stored in either a file or just memory. This also means you can use Camo in front-end browser code!</p>
<p>Head on over to the <a href="https://github.com/scottwrobinson/camo">project page</a> for more info. Like what you see? Star the project and spread the word!</p>
<!--kg-card-end: markdown--><!--kg-card-end: markdown-->
					