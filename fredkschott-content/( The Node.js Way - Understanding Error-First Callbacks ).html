
      <header>
        <h2>The Node.js Way - Understanding Error-First Callbacks</h2>
        
      </header>
        <p class="date">
          March 12, 2014
          <span style="color: #777;">(Updated December 18, 2014)</span>
        </p>
      

<p>If Google&#x2019;s V8 Engine is the heart of your Node.js application, then callbacks are its veins. They enable a balanced, non-blocking flow of asynchronous control across modules and applications. But for callbacks to work at scale you&#x2019;ll needed a common, reliable protocol. The &#x201C;error-first&#x201D; callback (also known as an &#x201C;errorback&#x201D;, &#x201C;errback&#x201D;, or &#x201C;node-style callback&#x201D;) was introduced to solve this problem, and has since become the standard for Node.js callbacks. This post will define this pattern, its best practices, and exactly what makes it so powerful.</p>

<h3 id="why-standardize">Why Standardize?</h3>

<p>Node&#x2019;s heavy use of callbacks dates back to a style of programming older than JavaScript itself. <a href="http://en.wikipedia.org/wiki/Continuation-passing_style">Continuation-Passing Style (CPS)</a> is the old-school name for how Node.js uses callbacks today. In CPS, a &#x201C;continuation function&#x201D; (read: &#x201C;callback&#x201D;) is passed as an argument to be called once the rest of that code has been run. This allows different functions to asynchronously hand control back and forth across an application.</p>

<p>Node.js relies on asynchronous code to stay fast, so having a dependable callback pattern is crucial. Without one, developers would be stuck maintaining different signatures and styles between each and every module. The error-first pattern was introduced into Node core to solve this very problem, and has since spread to become today&#x2019;s standard. While every use-case has different requirements and responses, the error-first pattern can accommodate them all.</p>

<h3 id="defining-an-error-first-callback">Defining an Error-First Callback</h3>

<p>There&#x2019;s really only two rules for defining an error-first callback:</p>

<ol>
<li><strong>The first argument of the callback is reserved for an error object.</strong> If an error occurred, it will be returned by the first <code>err</code> argument.</li>
<li><strong>The second argument of the callback is reserved for any successful response data.</strong> If no error occurred, <code>err</code> will be set to null and any successful data will be returned in the second argument.</li>
</ol>

<p>Really&#x2026; that&#x2019;s it. Easy, right? Obviously there are some important best practices as well, but before we dig into those lets put together a real-life example with the basic method <code>fs.readFile()</code>:</p>

<pre class="code-block"><code class="language-javascript">fs.readFile(&apos;/foo.txt&apos;, function(err, data) {
  // TODO: Error Handling Still Needed!
  console.log(data);
});
</code></pre>

<p><code>fs.readFile()</code> takes in a file path to read from, and calls your callback once it has finished. If all goes well, the file contents are returned in the <code>data</code> argument. But if somethings goes wrong (the file doesn&#x2019;t exist, permission is denied, etc) the first <code>err</code> argument will be populated with an error object containing information about the problem.</p>

<p>Its up to you, the callback creator, to properly handle this error. You can throw if you want your entire application to shutdown. Or if you&#x2019;re in the middle of some asynchronous flow you can propagate that error out to the next callback. The choice depends on both the situation and the desired behavior.</p>

<pre class="code-block"><code class="language-javascript">fs.readFile(&apos;/foo.txt&apos;, function(err, data) {
  // If an error occurred, handle it (throw, propagate, etc)
  if(err) {
    console.log(&apos;Unknown Error&apos;);
    return;
  }
  // Otherwise, log the file contents
  console.log(data);
});
</code></pre>

<h3 id="err-ception-propagating-your-errors">Err-ception: Propagating Your Errors</h3>

<p>When a function passes its errors to a callback it no longer has to make assumptions on how that error should be handled. <code>readFile()</code> itself has no idea how severe a file read error is to your specific application. It could be expected, or it could be catastrophic. Instead of having to decide itself, <code>readFile()</code> propagates it back for you to handle.</p>

<p>When you&#x2019;re consistent with this pattern, errors can be propagated up as as many times as you&#x2019;d like. Each callback can choose to ignore, handle, or propagate the error based on the information and context that exist at that level.</p>

<pre class="code-block"><code class="language-javascript">if(err) {
  // Handle &quot;Not Found&quot; by responding with a custom error page
  if(err.fileNotFound) {
    return this.sendErrorMessage(&apos;File Does not Exist&apos;);
  }
  // Ignore &quot;No Permission&quot; errors, this controller knows that we don&apos;t care
  // Propagate all other errors (Express will catch them)
  if(!err.noPermission) {
    return next(err);
  }
}
</code></pre>

<h3 id="slow-your-roll-control-your-flow">Slow Your Roll, Control Your Flow</h3>

<p>With a solid callback protocol in hand, you are no longer limited to using one callback at a time. Callbacks can be called in parallel, in a queue, in serial, or any other combination you can imagine. If you want to read in 10 different files, or make 100 different API calls, there&#x2019;s no reason to make them one-at-a-time.</p>

<p>The <a href="https://github.com/caolan/async">async</a> library is great for advanced callback usage. And because of the error-first callback pattern, it&#x2019;s incredibly easy to hook in to.</p>

<pre class="code-block"><code class="language-javascript">// Example taken from caolan/async README
async.parallel({
    one: function(callback){
        setTimeout(function(){
            callback(null, 1);
        }, 200);
    },
    two: function(callback){
        setTimeout(function(){
            callback(null, 2);
        }, 100);
    }
},
function(err, results) {
    // results is equal to: {one: 1, two: 2}
});
</code></pre>

<h3 id="bringing-it-all-together">Bringing it all Together</h3>

<p>To see all these concepts come together, check out <a href="https://github.com/FredKSchott/the-node-way/blob/master/03-error-first-example.js#">some more examples</a> on Github.  And of course, you can always choose to ignore all of this callback stuff and go fall in love with <a href="https://github.com/kriskowal/q">promises</a>&#x2026; but that&#x2019;s a whole other post entirely :)</p>


    