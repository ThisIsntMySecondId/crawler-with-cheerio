
      <header>
        <h2>The Node.js Way - Testing Essentials</h2>
        
      </header>
        <p class="date">
          May 14, 2014
          <span style="color: #777;">(Updated December 18, 2014)</span>
        </p>
      

<p>Setting up good testing can be tricky no matter what language you&#x2019;re in. JavaScript&#x2019;s flexibility can make your tests powerful in terms of access, but if you&#x2019;re not careful that same flexibility can leave you tearing your hair out the next day. For instance, how do you test API callbacks? How do you deal with require? Without a proper setup, <a href="http://david.heinemeierhansson.com/2014/tdd-is-dead-long-live-testing.html">whether TDD is dead or not</a> will end up meaning very little.</p>

<p>This post will explain the tools needed for efficient testing with Node.js. Together, they form an essential testing suite that will cover almost any project. The setup was designed to cover just the essentials, valuing simplicity over cleverness and advanced features. If that sounds counter-intuitive&#x2026; read on.</p>

<h3 id="introduction-zero-points-for-clever-tests">Introduction: Zero Points for Clever Tests</h3>

<p>Before introducing the tools, it&#x2019;s important to emphasis why you should be writing tests in the first place: confidence. You write tests to inspire confidence that everything is working as expected. If something breaks you want to be sure that you&#x2019;ll catch it, and quickly understand what went wrong. Every line of every test file should be written for this purpose.</p>

<p>The problem is that modern frameworks have gotten incredibly clever. This is ultimately a good thing, advanced tooling and technology can only benefit the developer. But it also means you&#x2019;ll need to be careful: this extra power is easily gained at the expense of clarity. Your tests may run faster or have more reusable code, but does that make you more or less confident in what is actually being tested? Always remember: <strong>There are no points for clever tests.</strong></p>

<p>Test clarity should be valued above all else. If you framework obfuscates this in the name of efficiency or cleverness, then it is doing you a disservice.</p>

<h3 id="the-essential-toolkit">The Essential Toolkit</h3>

<p>With that out of the way, lets introduce the essential Node testing kit. As you may remember, The Node Way values smaller, swappable, single-purpose tools over large do-everything-under-the-sun testing frameworks. In that spirit, the essential toolkit is a collection of smaller tools that each do one thing exceptionally well. They are:</p>

<ul>
<li>A Testing Framework (Mocha, Vows, Intern)</li>
<li>An Assertion Library (Chai, Assert)</li>
<li>Stubs (Sinon)</li>
<li>Module Control (Mockery, Rewire)</li>
</ul>

<h4 id="a-testing-framework">A Testing Framework</h4>

<p>The first and most important thing you&#x2019;ll need is a testing framework. A framework will provide a clear and scalable bedrock for all of our tests. There are a ton of available options in this area, each with a different feature set and design. No matter which framework you go for, just make sure you chose one that supports writing clear, maintainable tests.</p>

<p>For Node.js, <a href="http://mochajs.org/">Mocha</a> is the gold standard. It has been around forever, and is well tested and maintained. Its customization options are extensive, which makes it incredibly flexible as well. While the framework is far from sexy, its setup/teardown pattern encourages explicit, understandable, and easy-to-follow tests.</p>

<pre class="code-block"><code class="language-javascript">describe(&apos;yourModuleName&apos;, function() {
  before(function(){
    // The before() callback gets run before all tests in the suite. Do one-time setup here.
  });
  beforeEach(function(){
    // The beforeEach() callback gets run before each test in the suite.
  });
  it(&apos;does x when y&apos;, function(){
    // Now... Test!
  });
  after(function() {
    // after() is run after all your tests have completed. Do teardown here.
  });
});
</code></pre>

<h4 id="an-assertion-library">An Assertion Library</h4>

<p>With a new testing framework in place, you&#x2019;re ready to write some tests. The easiest way to do that is with an assertion library.</p>

<pre class="code-block"><code class="language-javascript">assert(object.isValid, &apos;tests that this property is true, and throws an error if it is false&apos;);
</code></pre>

<p>There are a ton of different libraries and syntax styles available for you to use. TDD, BDD, assert(), should()&#x2026; the list goes on. BDD has been gaining popularity recently thanks to its natural-language structure, but it should all come down to what feels best to you. <a href="http://chaijs.com/">Chai</a> is a great library for experimentation because it supports most of the popular assertion styles. But if you&#x2019;re a dependency minimalist, Node.js comes bundled with a <a href="http://nodejs.org/api/assert.html">simple assertion library</a> as well.</p>

<pre class="code-block"><code class="language-javascript">var life = 40 + 2;
expect(life).to.equal(42); // BDD Assertion Style
assert.equal(life, 42);    // TDD Assertion Style
</code></pre>

<h4 id="stubs">Stubs</h4>

<p>Unfortunately, assertions alone can only get you so far. When testing more complex functions, you&#x2019;ll need a way to influence the environment and test code under explicit conditions. While it&#x2019;s important for good tests to stay true to the original code, sometimes we need to be certain that some function will return true, or that an API call will yield with an expected value. <a href="http://sinonjs.org">Sinon</a> allows us to do this easily.</p>

<pre class="code-block"><code class="language-javascript">var callback = sinon.stub();
callback.withArgs(42).returns(1);
callback.withArgs(1).throws(&quot;TypeError&quot;);

callback();   // No return value, no exception
callback(42); // Returns 1
callback(1);  // Throws TypeError
</code></pre>

<p>Sinon includes a collection of other useful tools for testing, such as fake timers and argument matchers. In addition to Stubs, there are also <a href="http://sinonjs.org/docs/#spies">Spies</a> for watching functions are called and with what arguments, and <a href="http://sinonjs.org/docs/#mocks">Mocks</a> for setting expectations on some behavior. Start simple and feel free to experiment as you go.</p>

<h4 id="module-control">Module Control</h4>

<p>You&#x2019;re almost ready to start writing tests, but there&#x2019;s still one last problem in your way: <code>require()</code>. Because most calls to <code>require</code> happen privately, you have no way to stub, assert, or otherwise access external modules from your tests. To really control them, you&#x2019;ll need to control <code>require</code>.</p>

<p>There are a few different ways to accomplish this, depending on how much power is needed. <a href="https://github.com/mfncooper/mockery">Mockery</a> lets you populate the internal module cache and replace modules to be loaded with objects of our own. Just be sure to disable &amp; deregister mocks after the tests have run.</p>

<pre class="code-block"><code class="language-javascript">before(function() {
  mockery.enable();
  // Allow some ...
  mockery.registerAllowable(&apos;async&apos;);
  // ... Control others
  mockery.registerMock(&apos;../some-other-module&apos;, stubbedModule);
});
after(function() {
  mockery.deregisterAll();
  mockery.disable();
});
</code></pre>

<p><a href="https://github.com/jhnns/rewire">Rewire</a> is another popular mocking tool that is much more powerful than Mockery. With it, you can get and set private variables within the file, inject new code, replace old functions, and otherwise modify the original file. This may sound like a better deal, but with all this power comes the cliched responsibility. Just because you <em>can</em> check/set a private variable doesn&#x2019;t mean you <em>should</em>. While tempting, rewriting and rewiring the private module scope like that brings your code farther away from the original module behavior, and can easily get in the way of writing good tests.</p>

<h3 id="bringing-it-all-together">Bringing it all Together</h3>

<p>To see these tools all working together <a href="https://github.com/FredKSchott/NodeJS-Handbook/blob/master/04-testing-example.js">check out a working example on GitHub</a>. While I singled out a few favorite libraries above, there are plenty of good alternatives in each of the categories listed. Think I missed something important? Let me know in the comments, or on Twitter at <a href="https://twitter.com/FredKSchott">@FredKSchott</a>.</p>


    