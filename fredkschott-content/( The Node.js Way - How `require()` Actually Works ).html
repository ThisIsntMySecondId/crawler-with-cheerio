
      <header>
        <h2>The Node.js Way - How `require()` Actually Works</h2>
        
      </header>
        <p class="date">
          June 9, 2014
          <span style="color: #777;">(Updated December 18, 2014)</span>
        </p>
      

<blockquote>
<p><strong>Update July. 28, 2014:</strong> I just gave a talk at <a href="http://www.meetup.com/BayNode/">BayNode</a> on this exact subject, which includes a walkthrough of all the code discussed in this post. If talks &amp; slides are more your style, <a href="http://fredkschott.com/post/2014/07/module-js/">check it out</a>.
<!-- --></p>
</blockquote>

<p>Almost any Node.js developer can tell you what the <code>require()</code> function does, but how many of us actually know how it works?  We use it every day to load libraries and modules, but its behavior otherwise is a mystery.</p>

<p>Curious, I dug into Node core to find out what was happening under the hood. But instead of finding a single function, I ended up at the heart of Node&#x2019;s module system: <code>module.js</code>. The file contains a surprisingly powerful yet relatively unknown core module that controls the loading, compiling, and caching of every file used. <code>require()</code>, it turned out, was just the tip of the iceberg.</p>

<h3 id="module-js">module.js</h3>

<pre class="code-block"><code class="language-javascript">function Module(id, parent) {
  this.id = id;
  this.exports = {};
  this.parent = parent;
  // ...
</code></pre>

<p>The Module type found in <code>module.js</code> has two main roles inside of Node.js. First, it provides a foundation for all Node.js modules to build off of. Each file is given a new instance of this base module on load, which persists even after the file has run. This is why we are able attach properties to <code>module.exports</code> and return them later as needed.</p>

<p>The module&#x2019;s second big job is to handle Node&#x2019;s module loading mechanism. The stand-alone <code>require</code> function that we use is actually an abstraction over <code>module.require</code>, which is itself just a simple wrapper around <code>Module._load</code>. This load method handles the actual loading of each file, and is where we&#x2019;ll begin our journey.</p>

<h3 id="module-load">Module._load</h3>

<pre class="code-block"><code class="language-javascript">Module._load = function(request, parent, isMain) {
  // 1. Check Module._cache for the cached module.
  // 2. Create a new Module instance if cache is empty.
  // 3. Save it to the cache.
  // 4. Call module.load() with your the given filename.
  //    This will call module.compile() after reading the file contents.
  // 5. If there was an error loading/parsing the file,
  //    delete the bad module from the cache
  // 6. return module.exports
};
</code></pre>

<p><code>Module._load</code> is responsible for loading new modules and managing the module cache. Caching each module on load reduces the number of redundant file reads and can speed up your application significantly. In addition, sharing module instances allows for singleton-like modules that can keep state across a project.</p>

<p>If a module doesn&#x2019;t exist in the cache, <code>Module._load</code> will create a new base module for that file. It will then tell the module to read in the new file&#x2019;s contents before sending them to <code>module._compile</code>.[1]</p>

<p>If you notice step #6 above, you&#x2019;ll see that <code>module.exports</code> is returned to the user. This is why you use <code>exports</code> and <code>module.exports</code> when defining your public interface, since that&#x2019;s exactly what <code>Module._load</code> and then <code>require</code> will return. I was surprised that there wasn&#x2019;t more magic going on here, but if anything that&#x2019;s for the better.</p>

<h3 id="module-compile">module._compile</h3>

<pre class="code-block"><code class="language-javascript">Module.prototype._compile = function(content, filename) {
  // 1. Create the standalone require function that calls module.require.
  // 2. Attach other helper methods to require.
  // 3. Wraps the JS code in a function that provides our require,
  //    module, etc. variables locally to the module scope.
  // 4. Run that function
};
</code></pre>

<p>This is where the real magic happens. First, a special standalone <code>require</code> function is created for that module. <em>THIS</em> is the require function that we are all familiar with. While the function itself is just a wrapper around <code>Module.require</code>, it also contains some lesser-known helper properties and methods for us to use:</p>

<ul>
<li><code>require()</code>: Loads an external module</li>
<li><code>require.resolve()</code>: Resolves a module name to its absolute path</li>
<li><code>require.main</code>: The main module</li>
<li><code>require.cache</code>: All cached modules</li>
<li><code>require.extensions</code>: Available compilation methods for each valid file type, based on its extension</li>
</ul>

<p>Once <code>require</code> is ready, the entire loaded source code is wrapped in a new function, which takes in  <code>require</code>, <code>module</code>, <code>exports</code>, and all other exposed variables as arguments. This creates a new functional scope just for that module so that there is no pollution of the rest of the Node.js environment.</p>

<pre class="code-block"><code class="language-javascript">(function (exports, require, module, __filename, __dirname) {
  // YOUR CODE INJECTED HERE!
});
</code></pre>

<p>Finally, the function wrapping the module is run. The entire <code>Module._compile</code> method is executed synchronously, so the original call to <code>Module._load</code> just waits for this code to run before finishing up and returning <code>module.exports</code> back to the user.</p>

<h3 id="conclusion">Conclusion</h3>

<p>And so we&#x2019;ve reached the end of the require code path, and in doing so have come full circle by creating the very <code>require</code> function that we had begun investigating in the first place.</p>

<p>If you&#x2019;ve made it all this way, then you&#x2019;re ready for the final secret: <code>require(&apos;module&apos;)</code>. That&#x2019;s right, the module system itself can be loaded VIA the module system. INCEPTION. This may sound strange, but it lets userland modules interact with the loading system without digging into Node.js core. Popular modules like <a href="/posts/testing-essentials/">mockery and rewire</a> are built off of this.<sup>[2]</sup></p>

<p>If you want to learn more, check out the <a href="https://github.com/joyent/node/blob/master/lib/module.js">module.js source code</a> for yourself. There is plenty more there to keep you busy and blow your mind. Bonus points for the first person who can tell me what &#x2018;NODE_MODULE_CONTEXTS&#x2019; is and why it was added.</p>

<hr>

<p>[1] The <code>module._compile</code> method is only used for running JavaScript files. JSON files are simply parsed and returned via <code>JSON.parse()</code></p>

<p>[2] However, both of these modules are built on private Module methods, like <code>Module._resolveLookupPaths</code> and <code>Module._findPath</code>. You could argue that this isn&#x2019;t much better&#x2026;</p>


    