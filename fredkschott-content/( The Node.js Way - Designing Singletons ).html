
      <header>
        <h2>The Node.js Way - Designing Singletons</h2>
        
      </header>
        <p class="date">
          December 20, 2013
          <span style="color: #777;">(Updated December 18, 2014)</span>
        </p>
      

<p>In most languages, sharing an object across your entire application can be a complex process. These <a href="http://en.wikipedia.org/wiki/Singleton_pattern">Singletons</a> are usually quite complex, and require some <a href="http://www.galloway.me.uk/tutorials/singleton-classes/">advanced modifications</a> to get working. In Node, however, this type of object is the default. Every module that you require is shared across your application, so there&#x2019;s no need for any special classes or extra code.</p>

<p>Node doesn&#x2019;t impose much structure on it&#x2019;s developers, so it&#x2019;s up to you to choose the right design for your modules. In this post, I&#x2019;ll share a few simple patterns that will help you get the most out of your singletons by designing them as cleanly and clearly as possible.</p>

<h3 id="private-vs-public">Private vs. Public</h3>

<p>When creating a new module, the variables and functions you define are kept private by default. You&#x2019;ll need to use <a href="http://nodejs.org/api/modules.html#modules_module_exports">module.exports</a> to define your interface and make them public. This is a straight-forward process: whatever you set to <code>module.exports</code> becomes public, and is visible when your module is required.</p>

<pre class="code-block"><code class="language-javascript">// module.js
var a = 1; // Private
module.exports = {
  b: 2 // Public
};
</code></pre>
<pre class="code-block"><code class="language-javascript">var m = require(&apos;module&apos;);
console.log(m.a); // undefined
console.log(m.b); // 2
</code></pre>

<h3 id="exporting-a-single-source-of-truth">Exporting a Single Source of Truth</h3>

<p>There are a million different decisions to make when designing your module. Where should I declare my functions? Where should I export my public interface? Should I declare all my public functions as properties of module.exports directly, or one by one?</p>

<p>While these questions all come down to personal preference, the decisions you make can have major effects on the clarity of your code. Declaring and exporting properties one at a time, for example, leaves you scanning each function declaration to understand the public interface. Exporting all of your functions at the bottom means you have to jump around your file to see what&#x2019;s public. Neither design gives you the full picture, and both can be burdensome to understand.</p>

<p>To get the most out of your code, keep a single source of truth to your design. You should be able to look in one place to see exactly what&#x2019;s public, what&#x2019;s private, and what the hell is going on. Kill two birds with one stone: define AND export your public variables at the same time. You&#x2019;ll have an explicit separation of private and public as well as a complete picture of your public interface, instead of constantly searching your code base to find which functions are being exported.</p>

<pre class="code-block"><code class="language-javascript">// Private
var TWO = 2;
function sum(x, y) {
    return x + y;
}

// Public
module.exports = {
    x: 5,
    add2: function(num) {
        return sum(TWO, num);
    },
    addX: function(num) {
        return sum(module.exports.x, num);
    }
}
</code></pre>

<h3 id="avoiding-the-exports-alias">Avoiding the Exports Alias</h3>

<p>A seasoned Node veteran will know there&#x2019;s a second way to export your code. <code>exports</code> is an alias to <code>module.exports</code>, which you can also use to define your public interface. But be careful: it&#x2019;s just an alias to <code>module.exports</code>. Setting individual properties works fine, but if you set the entire <code>exports</code> or <code>module.exports</code> object (as we do above) you&#x2019;ll break the connection between the two and <code>exports</code> will no longer work.</p>

<p>While there&#x2019;s nothing wrong with using the exports alias instead of <code>module.exports</code>, losing the ability to set more than one property at a time forces you to lose our single point of truth. For that reason, stick with <code>module.exports</code> when defining your public interface.</p>

<p>The one exception where <code>exports</code> may still be valuable is when referencing the public module from private code. Instead of calling <code>module.exports.x</code> every time you want the value of <code>x</code>, you can save yourself some characters by using <code>exports.x</code> instead. Just remember that for this to work properly, you&#x2019;ll need to re-assign the alias yourself:</p>

<pre class="code-block"><code class="language-javascript">// Public
exports = module.exports = {
    /* &#x2026; */
}
</code></pre>

<h3 id="bringing-it-all-together">Bringing it all Together</h3>

<p>To see all these rules come together, check out a <a href="https://github.com/FredKSchott/the-node-way/blob/master/01-singleton-example.js">complete singleton</a>. But remember, rules are meant to be broken. Use what works, discard what doesn&#x2019;t, and you&#x2019;ll be well on your way to Node Nirvana.</p>


    