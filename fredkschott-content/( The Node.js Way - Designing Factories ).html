
      <header>
        <h2>The Node.js Way - Designing Factories</h2>
        
      </header>
        <p class="date">
          January 6, 2015
          
        </p>
      

<p>Previously, we introduced the <a href="/posts/designing-custom-types/">Custom Type Module Pattern</a> and how it can be used to create custom objects in your application. These custom types play a major role in JavaScript development, but creating them directly can quickly become a messy business. In this post I&#x2019;ll introduce a new kind of module that makes working with custom types much easier: The Factory.</p>

<h3 id="what-is-a-factory">What is a Factory?</h3>

<p>To understand what a factory does, you should first know the problem that it tries to solve. Creating a new custom object directly requires a call to the constructor with the <code>new</code> keyword and any required arguments. This offers complete control over creation: Powerful&#x2026; but not always feasible.</p>

<pre class="code-block"><code class="language-javascript">/* Using Custom Types Only */
var Widget = require(&apos;./lib/widget&apos;);

// Somewhere in your application...
var redWidget = new Widget(42, true);
redWidget.paintPartA(&apos;red&apos;);
redWidget.paintPartB(&apos;red&apos;);
redWidget.paintPartC(&apos;red&apos;);

// Somewhere else in your application...
var blueWidget = new Widget(42, true);
blueWidget.paintPartA(&apos;blue&apos;);
blueWidget.paintPartB(&apos;blue&apos;);
blueWidget.paintPartC(&apos;blue&apos;);

// And somewhere else again...
var greenWidget = new Widget(42, true);
greenWidget.paintPartA(&apos;green&apos;);
greenWidget.paintPartB(&apos;green&apos;);
greenWidget.paintPartC(&apos;green&apos;);
</code></pre>

<p>Working with a complex constructor may be overkill in some simple cases, or you may find yourself passing the same arguments to the same constructors over and over and over again. Both situations are at best annoying, and at worst completely unscalable. No one likes copypasta in their code.</p>

<p>Lets introduce a factory to help clean up this mess. A factory&#x2019;s job is to create objects so that you don&#x2019;t have to, which can be advantageous for several reasons. If you are creating a public module to share on npm or even privately within your own team, adding a factory interface can make your module easier to work with. This is especially true if setup requires additional steps after calling the constructor. A factory can consolidate and standardize on all of this custom-creation for both you and your users, dictating exactly how new objects should be created.</p>

<p>Using a factory, the example above becomes:</p>

<pre class="code-block"><code class="language-javascript">/* Refactored to use a Factory */
var widgetFactory = require(&apos;./lib/widget-factory&apos;);

var redWidget = widgetFactory.getRedWidget();
var blueWidget = widgetFactory.getBlueWidget();
var greenWidget = widgetFactory.getGreenWidget();
</code></pre>

<p>If you find yourself struggling with complex object creation across multiple parts of your application, this pattern could be the answer. By centralizing this logic in a single place, you keep it from spreading across the rest of your code base where it shouldn&#x2019;t belong.</p>

<h3 id="separate-the-product">Separate the Product</h3>

<p>Before creating a factory, make sure that any <a href="/posts/designing-custom-types/">custom types</a> that you plan to use are already defined as separate modules. It can be tempting to keep both the factory and the product (the object returned by a factory) in the same file, but remember your training: <a href="/introduction/#build-small-single-purpose-modules">small, single purpose modules</a>. Separating the two modules will reduce confusion between the different responsibilities of each. In practice, testing coverage and code re-use are both improved as well.</p>

<pre class="code-block"><code class="language-javascript">/* Bad! */
function Widget(cogs, bool) {
    // Define widget custom type here
}

module.exports = { // ...
</code></pre>

<pre class="code-block"><code class="language-javascript">/* Good! */
var Widget = require(&apos;./widget&apos;);

module.exports = { // ...
</code></pre>

<h3 id="the-factory-design-pattern">The Factory Design Pattern</h3>

<p>Both singleton and custom type patterns can be used as the base for your new factory. If you know that your entire application can share a single factory object, then building it as a singleton is almost always the right choice. A singleton can be easily required anywhere, and won&#x2019;t need any complex construction or passing around of its own. But remember, your state is limited to the single module.</p>

<pre class="code-block"><code class="language-javascript">/* A Factory Implemented as a Singleton */
var Widget = require(&apos;./widget&apos;);

module.exports = {
    getRedWidget: function getRedWidget() {
        var widget = new Widget(42, true);
        widget.paintPartA(&apos;red&apos;);
        widget.paintPartB(&apos;red&apos;);
        widget.paintPartC(&apos;red&apos;);
        return widget;
    },
    getBlueWidget: function getBlueWidget() {
        // ...
    }
}
</code></pre>

<p>But sharing the same factory across your application won&#x2019;t always cut it. Sometimes, a factory needs some set up and customization of its own. If this is the case, design your factory using the custom type pattern instead. This way your factory can expose some basic level of flexibility with each instance, while still encapsulating the product customization.</p>

<pre class="code-block"><code class="language-javascript">/* A Factory Implemented as a Custom Type */
var Widget = require(&apos;./lib/widget&apos;);

var WidgetFactory = module.exports = function WidgetFactory(options) {
    this.cogs = options.cogs;
    this.bool = options.bool;
}

WidgetFactory.prototype.getRedWidget = function getRedWidget() {
    var widget = new Widget(this.cogs, this.bool);
    widget.paintPartA(&apos;red&apos;);
    widget.paintPartB(&apos;red&apos;);
    widget.paintPartC(&apos;red&apos;);
    return widget;
};

WidgetFactory.prototype.getBlueWidget = function getBlueWidget() {
    // ...
};
</code></pre>
<pre class="code-block"><code class="language-javascript">/* Usage of a Custom-Type Factory */
var WidgetFactory = require(&apos;./lib/widget-factory&apos;);

var widgetFactory42  = new WidgetFactory({cogs: 42,  bool: true});
var widgetFactory101 = new WidgetFactory({cogs: 101, bool: true});
</code></pre>

<h3 id="abstract-factories">Abstract Factories</h3>

<p>Once you are familiar with factories, you can begin to get creative with how you use them. The <strong>Abstract Factory</strong> pattern, for instance, offers even greater flexibility by taking the whole concept one step further. If you have several factories that each create different products from the same parent type, you can create a common interface between them. This lets you to swap between the factories themselves (depending on which type of object you&#x2019;d like to create) without modifying the rest of your code.</p>

<p>Consider the Express application below:</p>

<pre class="code-block"><code class="language-javascript">/* Original Code */

function someMiddleware(req, res, next) {
    if(/* user is logged in */) {
        req.loggedInUserFactory = new LoggedInUserFactory(req.session.userID);
    } else {
        req.loggedOutUserFactory = new LoggedOutUserFactory();
    }
    next(); // next() will call nextMiddleware() with the same req/res objects
}

function nextMiddleware(req, res, next) {
    var user, credentials;
    if(/* user is logged in */) {
        user = req.loggedInUserFactory.createUser();
        credentials = req.loggedInUserFactory.createCredentials();
    } else {
        user = req.loggedOutUserFactory.createAnonymousUser();
        credentials = req.loggedOutUserFactory.createAnonymousCredentials();
    }
    res.send(&apos;Welcome back, &apos; + user.fullName);
}
</code></pre>

<p>Here you&#xA0;have two different factories for logged in and logged out users. They don&#x2019;t share a common interface (<code>createUser()</code> vs. <code>createAnonymousUser()</code>) which means you will always need to check which kind of factory you&#x2019;re working with before using it. This leads to lots of unnecessary <code>/*user is logged in */</code> checks throughout the application, when really all you need is one.</p>

<p>If you can consolidate each interface into something common across both, you will have implemented an abstract factory. Then, each factory can be used without worrying which type of user is being created. That decision is made once, and then carried on by the rest of your application.</p>

<pre class="code-block"><code class="language-javascript">/* Original Code Refactored to Use an Abstract Factory */

function someMiddleware(req, res, next) {
    if(/* user is logged in */) {
        req.userFactory = new LoggedInUserFactory(req.session.userID);
    } else {
        req.userFactory = new LoggedOutUserFactory();
    }
    next(); // This will call nextMiddleware() with the same req/res objects
}

function nextMiddleware(req, res, next) {
    var user = req.userFactory.getUser();
    var credentials = req.userFactory.getCredentials();
    res.send(&apos;Welcome back, &apos; + user.fullName);
}

</code></pre>

<h3 id="conclusion">Conclusion</h3>

<p>A good custom type interface should be flexible enough to support all reasonable use cases. But this flexibility can come at a cost. As the number of supported options grows, the interface and required setup can also grow more complex. Worse, creation complexity can bleed out all over your application, with copy and pasted code blocks for each new object. Factories are a clean solution to return sanity to your application, by making complex and repeated customizations simpler and more maintainable.</p>


    