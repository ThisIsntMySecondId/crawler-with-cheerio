
      <header>
        <h2>Monkey Patches &amp; MP3s</h2>
        <p class="subtitle">Exploring Dangerous Design Patterns in Node.js</p> 
      </header>
        <p class="date">
          September 18, 2014
          
        </p>
      

<p>There are a million different ways to design a JavaScript module. Standard patterns like the <a href="http://fredkschott.com/post/2013/12/node-js-cookbook---designing-singletons/">singleton</a> and <a href="http://fredkschott.com/post/2014/01/node-js-cookbook---constructors-and-custom-types/">custom type</a> are widely adopted, and provide a dependable feature-set. Some other patterns, however, push the limits of what a module can (and should) actually be. The first group is often encouraged, while the second is denounced without further thought. This post will attempt to explore that second group.</p>

<p>Before jumping in, I want to explicitly point out that <strong>almost all of the concepts explained below should be avoided in production.</strong> These patterns have the potential to cause nightmares for you or your team down the road, with hidden bugs and unexpected side-effects. But they exist for a reason, and when used properly (read: <em>very carefully</em>) they can solve real problems that other, safer patterns can&#x2019;t. But just&#x2026; you know&#x2026; with those terrible, dangerous side effects.</p>

<h3 id="monkey-patches">Monkey Patches</h3>

<p>JavaScript is a dynamic language, which - when paired with its prototype-based nature - gives the developer free range to modify objects and classes across entire applications. So when you one day find yourself building a pig-latin generator and wishing that JavaScript strings handled this conversion themselves, you can do something like this:</p>

<pre class="code-block"><code class="language-javascript">String.prototype.pigLatin = function() { /* ... */ }
&apos;Is this actually a good idea?&apos;.pigLatin()  // &apos;Is-ay is-thay actually-ay an ood-gay idea-ay?&apos;
</code></pre>

<p>Modifying already-existing methods can be a little trickier. You can simply overwrite them, but if you want to leverage the original function you&#x2019;ll need to save it first. Using a more practical example than the one above, you may want to attach data to every every template that gets rendered in an Express application:</p>

<pre class="code-block"><code class="language-javascript">// Save the original render function to use later
res._render = res.render;
// Wrap the render function to process args before rendering
res.render = function(view, options, callback) {
  options.global = { /* ... */ };
  this._render(view, options, callback);
}
</code></pre>

<p>This practice is called <strong>monkey patching</strong>, and it is generally considered to be a <a href="http://blog.codinghorror.com/monkeypatching-for-humans/">terrible</a> <a href="http://www.nczonline.net/blog/2010/03/02/maintainable-javascript-dont-modify-objects-you-down-own/">idea</a>. Monkey patches pollute your application&#x2019;s shared environment. They can collide with other patches, and be impossible to debug even when working properly. The pattern is a powerful hack, but luckily its adoption and use is generally limited.</p>

<p>But desperate times can call for desperate measures, and sometimes a monkey patch is necessary. If the situation allows it, building your patch as a separate module will help keep the hack quarantined and decoupled from the rest of your application. Organizing your monkey patches in one place can also make it easier to find when/if debugging is needed.</p>

<p>The first thing you&#x2019;ll want to do is make as many assertions about the environment as possible. Assert that the method you&#x2019;re adding/modifying hasn&#x2019;t been added/modified yet. Check that its version is correct. Check that everything exists exactly as you expect. Check all of this first, and throw an error if any of it doesn&#x2019;t look right. While this might sound over-the-top now, it could save you days of debugging later if you fail to catch some subtle collision.</p>

<p>You should also consider exporting your monkey patch as a singleton, with a single <code>apply()</code> method that executes the code. Applying the patch explicitly (instead of as a side effect of loading it) will make your module&#x2019;s purpose clearer. It will also allow you to pass arguments to your monkey patch, which might be helpful or even necessary depending on your use case.</p>

<pre class="code-block"><code class="language-javascript">// some-monkey-patch/index.js
module.exports = {
  apply: function() {
    /* check environment/arguments &amp; apply patch */
  }
}

// later...
require(&apos;some-monkey-patch&apos;).apply();
</code></pre>

<h3 id="polyfills">Polyfills</h3>

<p>Polyfills are most commonly found on the client-side, where different browsers have different levels of feature support. Instead of forcing your application down to support the lowest-common denominator (looking at you, IE) you can use a polyfill to add new features to old browsers and standardize across platforms.</p>

<p>As a server-side developer, you might think that you&#x2019;re safe from this problem. But with Node&#x2019;s long v0.12 development cycle, even Node.js developers will find new features that aren&#x2019;t fully available to them yet. For example, <a href="http://nodejs.org/docs/v0.11.9/api/process.html#process_async_listeners">async-listeners</a> were added in v0.11.9, but you&#x2019;ll have to wait until v0.12.0 before you&#x2019;ll see them in a stable build.</p>

<p>Or&#x2026; you could consider using an <a href="https://www.npmjs.org/package/async-listener">async-listener</a> polyfill.</p>

<pre class="code-block"><code class="language-javascript">// load polyfill if native support is unavailable
if (!process.addAsyncListener) require(&apos;async-listener&apos;);
</code></pre>

<p><strong>The polyfill</strong> is still a monkey patch at heart, but it can be much safer to apply in practice. Instead of modifying anything and everything, polyfills are limited to implementing an already-defined feature. The presence of a spec makes polyfills easier to accept, but all the same warnings and guidelines for monkey patching still apply. Understand the code you&#x2019;re adding, watch out for collisions (specs can always change), and make sure you assert as much as possible about your environment before applying the patch.</p>

<h3 id="json-modules">JSON Modules</h3>

<p>JSON is the data format of choice for Node.js, and native JSON support makes it easy to load and then interact with static data files as if they were actually JavaScript modules. The original <a href="https://www.npmjs.org/package/http-status-codes-json">http-status-codes-json</a> module, for example, was entirely represented by <a href="https://github.com/andreineculau/http-status-codes-json/blob/7c9660e4a812ec66eafc40ab7400511f85d3b315/http_status_codes.json">a static JSON file</a>. And because of Node&#x2019;s JSON support, the module became an interactive dictionary of HTTP status codes.</p>

<pre class="code-block"><code class="language-javascript">// http_status_codes.json
{
  &quot;100&quot;: &quot;Continue&quot;,
  &quot;200&quot;: &quot;OK&quot;,
  /* ... */

// later...
var httpStatusCodes = require(&apos;http-status-codes-json&apos;);
console.log(httpStatusCodes[res.statusCode]); // &apos;NOT FOUND&apos;
</code></pre>

<p>This feature can be powerful, but don&#x2019;t refactor your code just yet. Modules are loaded synchronously, which means nothing else can run while the file is loaded and parsed. And once parsed, the result is saved and persisted in your module cache for the rest of your applications lifetime. Unless you intend to actually interact with the object as a module, stick to <code>fs.readFile()</code> and/or <code>JSON.parse()</code>, and save yourself the performance hit and added complexity.</p>

<h3 id="compile-to-js-modules">Compile-to-JS Modules</h3>

<p>Node supports JSON right out of the box, but <code>require()</code> will throw an error if you try loading anything else. However, if you roll up your sleeves and start poking around, you&#x2019;ll find that Node can be made to support any number of file types, as long as you provide the parsers.</p>

<p>Here&#x2019;s how it works: Node holds a collection of &#x201C;file extensions&#x201D; internally, which are responsible for loading, parsing, and exporting a valid representation of a given file. The <a href="https://github.com/joyent/node/blob/master/lib/module.js#L483">native JSON extension</a>, for example, reads the file via <code>fs.readFileSync()</code>, parses the results via <code>JSON.parse()</code>, and then attaches the final object to <code>module.exports</code>. While these parsers are private to Node&#x2019;s <a href="fredkschott.com/post/2014/06/require-and-the-module-system/">Module type</a>, they are exposed to developers via the <code>require()</code> function.</p>

<p><a href="http://coffeescript.org/">CoffeeScript</a> is probably the most popular compile-to-js language, but to properly use it with Node you&#x2019;ll need to compile it down to JavaScript after every change. Using the technique described above, fans could instead build CoffeeScript support right into Node.js, handling this extra step automatically:</p>

<pre class="code-block"><code class="language-javascript">module.exports = {
  apply: function() {
    // Load your new CoffeeScript extension into Node.js
    require.extensions[&apos;coffee&apos;] = function coffeescriptLoader(module, filename) {
      // Read the contents from the &apos;.coffee&apos; file
      var fileContent = fs.readFileSync(filename, &apos;utf8&apos;);
      // Compile it into JavaScript so that V8 can understand it
      var jsContent = coffeescript.compile(fileContent);
      // Pass the contents to be compiled like a normal JavaScript module
      module._compile(jsContent, filename);
    };
  }
}

// Later...
require(&apos;require-coffee&apos;).apply();
</code></pre>

<p>Note: <a href="http://nodejs.org/api/globals.html#globals_require_extensions">This feature was deprecated</a> once everyone realized that processing your code into JS and JSON <em>before</em> run-time is almost always the better way to go. Parsing directly during runtime can make bugs harder to find, since you can&#x2019;t see the actual JS/JSON that gets generated.</p>

<h3 id="mp3-modules">MP3&#x2026; Modules?</h3>

<p>CoffeeScript was built with JavaScript in mind, so requiring a CoffeeScript module makes a lot of sense. But since Node.js leaves the file representation up to the developer, you can really require any file type you want. In this last section, lets see how this would work with something completely different, like an MP3.</p>

<p>It would be too easy to just load and return the file contents as an MP3 module, so lets go one step further. In addition to getting the MP3 file contents, the file extension should also generate song metadata (such as title and artist) via the <a href="https://www.npmjs.org/package/audio-metadata">audio-metadata</a> module.</p>

<pre class="code-block"><code class="language-javascript">var audioMetaData = require(&apos;audio-metadata&apos;);

// A custom type to represent the mp3 file and its metadata
function MP3(file) {
  // Attach the file contents
  this.content = file;
  // Process and attach the audio id3 tags
  this.metadata = audioMetaData.id3v2(fileContent);
}

// Attach your new MP3 extension
require.extensions[&apos;mp3&apos;] = function mp3Loader(module, filename) {
  // Read the contents from the &apos;.mp3&apos; file
  var fileContent = fs.readFileSync(filename);
  // Export a new MP3 instance to represent the module
  module.exports = new MP3(fileContents);
};

// Later...
var song = require(&apos;/media/i-believe-in-a-thing-called-love.mp3&apos;);
console.log(song.metadata.artist + &apos;: &apos; + song.metadata.title); // &apos;The Darkness: I Believe in a Thing Called Love&apos;
</code></pre>

<p>Depending on the use case, this extension could be built to add even more functionality like streaming, playing, and otherwise interacting with the song, all automatically supported at load time.</p>

<h3 id="conclusion">Conclusion</h3>

<p>This post isn&#x2019;t meant to endorse or approve of any of the above patterns, but it isn&#x2019;t a blanket denouncement either. What makes these modules dangerous is the same thing that makes them so powerful: they don&#x2019;t follow the normal rules. Polyfills can update your feature set without actually updating the framework, while File Extensions change the idea of what a Node.js module can actually be. Understanding how any of this is possible will help you make smarter decisions when it comes to module design, and allow you to spot potential problems before they happen.</p>

<p>And one day, when you find yourself in a jam, one of these patterns might just help you out.</p>


    