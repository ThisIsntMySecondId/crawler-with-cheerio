
      <header>
        <h2>The Node.js Way - Designing Constructors</h2>
        
      </header>
        <p class="date">
          January 8, 2014
          <span style="color: #777;">(Updated December 18, 2014)</span>
        </p>
      

<p>Node&#x2019;s module system was built to share objects across an application. The <a href="/posts/designing-singletons/">singleton pattern</a> is a natural fit for this, but what if you want something that isn&#x2019;t shared? What if you want to create multiple objects, each with unique properties but shared common behavior? In most languages this would require a new class. In JavaScript, you&#x2019;ll need something a little different.</p>

<h3 id="classes-in-javascript">Classes in JavaScript</h3>

<p>JavaScript has no real idea of classes, at least not in the traditional sense. What it does have are types, which are so commonly used for the same purpose that the two are nearly synonymous. In fact, most developers would call the following definition a class, even thought &#x2018;User&#x2019; is really just a new type of object.</p>

<pre class="code-block"><code class="language-javascript">function User(n) {
    this.name = n;
}
var bob = new User(&apos;Bob&apos;);
</code></pre>

<p>While this pattern enables a lot of classy (ha!) things in JavaScript,  <code>User</code> is still just a function, and <code>bob</code> is still just an object. But because JavaScript objects are so powerful, you can use them to create new custom types with unique methods/properties that make them feel and behave just like traditional classes.</p>

<pre class="code-block"><code class="language-javascript">User.prototype.sayHi = function() {
    console.log(&apos;Hi, My name is &apos; + this.name);
};
bob.sayHi(); // &quot;Hi, My name is Bob&quot;
</code></pre>

<p>So while it&#x2019;s generally safe to go about thinking of these instances as classes, remember that they&#x2019;re still just objects at heart. The constructor simply defines the type while the prototype describes the behavior. For more, Nicholas Zakas wrote <a href="http://www.nczonline.net/blog/2012/10/16/does-javascript-need-classes/">a great post on custom types</a> that goes into this distinction in greater detail.</p>

<h3 id="layout">Layout</h3>

<p>Back to the original question: How do you export a custom type &#x2013; an entirely different paradigm than the singleton &#x2013; while maintaining those same concepts for cleaner code?</p>

<p>When reading a new module, the first thing you need to do is find out whats being exported. In large files this isn&#x2019;t always obvious, so go out of your way to make it explicit. To do this, tie <code>module.exports</code> to the constructor at the very top of your &#x201C;Public&#x201D; section.</p>

<pre class="code-block"><code class="language-javascript">//Private
var privateVariable = true;

//Public
module.exports = User;
function User(n) {
    this.name = n;
}

User.prototype.foobar = // ...
</code></pre>

<p>This gives you an easy separation of private and public, a single point of truth for what is being exported, and a nice name for your type.</p>

<p>Now you can require this constructor anywhere in our application and easily create a new user.</p>

<pre class="code-block"><code class="language-javascript">var User = require(&apos;User&apos;);
var bob = new User(&apos;Bob&apos;);
</code></pre>

<h3 id="private-properties">Private Properties</h3>

<p>All object properties are publicly accessible, so adding them to a new instance is easy (for example: calling <code>bob.name</code> will return &#x2018;Bob&#x2019; in the example above). Private properties, however, aren&#x2019;t so simple. Consider the example below which tries to use private variables:</p>

<pre class="code-block"><code class="language-javascript">var paid = true;

User.prototype.togglePaid = function togglePaid() {
    paid = !paid;
}
</code></pre>

<p>It is true that the &#x2018;paid&#x2019; variable is now private, and checking <code>bob.paid</code> will return undefined. But paid is also no longer unique to each User. Each time <code>togglePaid()</code> is called on <strong>any</strong> user it will toggle the value of &#x2018;paid&#x2019; for <strong>all</strong> users. <code>require()</code> caches each module, so only one of those variables exists in our application and all Users are referencing it.</p>

<p>We need private variables to live as properties of an instance, but all properties are public. Out of options, defer to a semantic separation. Use the following naming convention to distinguish between public and private:</p>

<pre class="code-block"><code class="language-javascript">function User(n) {
    this.name = n;     // public
    this._paid = true; // private
}
User.prototype.togglePaid = function() {
    this._paid = !this._paid;
}
</code></pre>

<p>The dangling underscore tells a developer that this property is private to the implementation, and isn&#x2019;t meant to be referenced directly outside of the object (since you could change them at any time). While they&#x2019;re not truely private, the naming convention will help you distinguish between the two as if they were. This is a common practice in Node, used within the Node code-base and by most other developers.</p>

<h3 id="antipattern-truly-private-properties-at-whatever-cost">Antipattern: Truly Private Properties (At Whatever Cost)</h3>

<p>I need to apologize now, because I lied in the above section: There is <em>one</em> other option for truely private variables. It involves being clever with scope. To define variables and functions that are truely private to each instance, you&#x2019;ll need to define your entire module within the private scope of your constructor:</p>

<pre class="code-block"><code class="language-javascript">function User(n) {
    var paid = true; // private (to each User instance)

    this.name = n;     // public
    this.togglePaid = function togglePaid() {
        paid = !paid;
    }
}
</code></pre>

<p>Now <code>paid</code> is individual to each instance, and correctly referenced each time <code>togglePaid()</code> is called. But since it is never attached to the User object, it remains private within the constructor function.</p>

<p>Doing this way isn&#x2019;t necessarily wrong, but it is slow. The constructor is run every time a new User is created, so the <code>togglePaid()</code> function needs to be recreated for each new user instance. If you modify the prototype instead, <code>togglePaid()</code> would only be defined one time, when the module is loaded. While performance may not always be a concern, this pattern should only be used if truly private variables are absolutely necessary.</p>

<h3 id="bringing-it-all-together">Bringing it all Together</h3>

<p>To see all these concepts come together, check out a <a href="https://github.com/FredKSchott/the-node-way/blob/master/02-custom-type-example.js#">full constructor design</a> and all the other <a href="https://github.com/FredKSchott/the-node-way/">Node.js Cookbook examples</a>. And remember, rules are meant to be broken. Use what works for you, discard what doesn&#x2019;t, and you&#x2019;ll be well on your way to Node Nirvana.</p>


    