const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const config = require('./config3');

console.log(config);

const writeStream = fs.createWriteStream(config.saveLocation);

// Write Headers
writeStream.write(`Sr.No.,${config.csvHeaders()} \n`);

const url = config.startUrl;

const getHtmlData = (url) => {
    return new Promise((resolve, reject) => {
        request(url, (err, response, html) => {
            if (err) {
                console.log("cant access the url: ", url);
                reject(err);
            }
            if (!err && response.statusCode == 200) {
                resolve(html);
            }
        });
    });
};

const parseHtml = (html) => {
    const $ = cheerio.load(html);
    let data = {};
    let posts = [];

    const heading = $(config.post);
    heading.each((i, el) => {
        let post = {};
        
        post.id = i;
        
        Object.entries(config.details).forEach( x => {
            post[x[0]] = $(el).find(x[1]).text();
        })

        posts.push(post);
    });

    // let nextpath = $(config.nextButton).attr('href');
    // if (!!nextpath) data.nextLink = config.mainUrl + nextpath;
    // else data.nextLink = "";
    data.nextLink = $(config.nextButton).attr('href');
    data.posts = posts;
    console.log(data);
    return Promise.resolve(data);
};

function getPostContent(link, title, file_name) {
    request(`https://stackabuse.com${link}`, (err, response, html) => {
        if (!err && response.statusCode == 200) {
            const $ = cheerio.load(html);
            const content = $('section article.post .post-content');
            fs.writeFile(`./content/${file_name}-content.html`, content.html(), (err) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log(`Wrote contents of Post ${title} in file ${file_name} successfully`);
                    return;
                }
            });
        }
    });
}

const getAllData = async (startUrl) => {
    nextLink = startUrl;
    number_of_posts = 0;
    while (nextLink) {
        let ddd = await getHtmlData(nextLink).then(parseHtml);
        nextLink = ddd.nextLink;
        ddd.posts.forEach(post => {
            getPostContent(post.url, post.title, `Post ${++number_of_posts}`);
            //Write data in csv file
            writeStream.write(`${number_of_posts},\"${post.title}\",${post.url},${post.author},${post.date},\"${post.tags}\",./content/${number_of_posts}-content.html \n`);
        });
    }

}

getAllData(config.startUrl);