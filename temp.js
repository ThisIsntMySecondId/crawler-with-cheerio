const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
//The configuration file
let config = null;

const pages = process.argv[3] || 5;

////-DONE-Todo: remember to validate this stuff (the third argument i.e. the config file name)
try {
    config = require(process.argv[2]);
} catch (err) {
    if (err.code == 'MODULE_NOT_FOUND') {
        console.log('\x1b[47m\x1b[31m %s \x1b[0m', 'Config File Not Found Error: Please Make sure that you are entering correct confige file name');
        process.exit();
    }
}

// console.log(config);

if (config.storeContent && !fs.existsSync(config.contentSaveLocation)) {
    fs.mkdirSync(config.contentSaveLocation);
    console.log(` ${config.contentSaveLocation} directory created successfully`);
}

const writeStream = fs.createWriteStream(config.saveLocation);
// Write Headers
writeStream.write(`Sr.No.,${config.csvHeaders}\n`);

const getHtmlData = (url) => {
    return new Promise((resolve, reject) => {
        request(url, (err, response, html) => {
            if (err) {
                console.log("cant access the url: ", url);
                reject(err);
            }

            if (!err && response.statusCode == 200) {
                console.log(html);
                resolve(html);
            }
        });
    });
};

//Todo: Rename post and posts to some meaningfull names
//Todo: Refactor this method as it only gives information residing on only one page
const getAllPostsInformation = (html) => {
    const $ = cheerio.load(html);
    let data = {}, posts = [];

    const postWrapper = $(config.postWrapper);
    // console.log(postWrapper);
    postWrapper.each((i, post) => {
        // console.log(post);
        let postDetail = {};
        Object.entries(config.details).forEach(x => {
            postDetail[x[0]] = getDetail($, post, x[1].type, x[1].location, x[1].postprocessing);
        });
        posts.push(postDetail);
    })


    let nextpath = $(config.nextButton).attr('href');
    if (nextpath) {
        if (nextpath.includes(config.mainUrl)) {
            data.nextLink = nextpath;
        } else {
            data.nextLink = config.mainUrl + nextpath;
        }
    } else {
        data.nextLink = '';
    }
    data.posts = posts;
    return Promise.resolve(data);
}

//Todo: remember to give meaning ful names to certain perameter instead og detail
//Todo: remember to fix image src some times in data-src attribute
const getDetail = ($, post, type, location, postprocessing) => {
    switch (type) {
        case 'text': {
            let detail = $(post).find(location).text();
            if (postprocessing) {
                return postprocessing(detail);
            }
            return detail;
        }
        case 'url': {
            let detail = $(post).find(location).attr('href');
            if (postprocessing) {
                return postprocessing(detail);
            }
            return detail;
        }
        case 'image': {
            let detail = $(post).find(location).attr('href');
            if (postprocessing) {
                return postprocessing(detail);
            }
            return detail;
        }
    }
}

function getAndStorePostContent(link, file_name) {
    request(link, (err, response, html) => {
        if (!err && response.statusCode == 200) {
            const $ = cheerio.load(html);
            const content = $(config.contentLocation);
            // console.log(content.html());
            fs.writeFile(`${config.contentSaveLocation+file_name.replace(/[/\:/"*?<>|]/g, '')}`, content.html(), (err) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log(`Wrote file ${file_name} successfully`);
                    return;
                }
            });
        }
    });
}   


//Note: Title is compulsory and is used as file name for storing post content
//? Go through all blog pages and store data in csv file
//? Default required pages is 5
const getAllPostAndSaveThem = async (url, requiredPages = 5) => {
    let newLink = url;
    let totalPosts = 0;
    let page = 0;
    while (newLink && (page <= requiredPages)) {
        let ddd = await getHtmlData(config.startUrl).then(getAllPostsInformation);
        newLink = ddd.nextLink;
        ddd.posts.forEach(post => {

            //? Get and Store Contents to files
            if (config.storeContent) {
                console.log(post[config.contentLink]);
                if (post[config.contentLink] && post[config.contentLink].includes(config.mainUrl)) {
                    //? write content from post links
                    getAndStorePostContent(post[config.contentLink], `( ${post.title} ).html`);
                } else if (post[config.contentLink] && !post[config.contentLink].includes(config.mainUrl)) {
                    //? post link is relative attach main link to post link
                    getAndStorePostContent(config.mainUrl + post[config.contentLink], `(${post.title}-content).html`);
                } else {
                    console.log(`Post link was not proper could not write to file the contents of post ${post.title}`);
                }
            }

            //? Write to a csv file
            writeStream.write(`${totalPosts++},` + Object.values(post).map(x => `\"${x}\"`).join(',') + '\n');
        })
        console.log(`Written *** `);
        if (++page >= requiredPages) {
            break;
        }
    }
}

getAllPostAndSaveThem(config.startUrl, pages);

////? Go through all blog pages and store data in csv file
////? Default required posts is 100
//// Todo: Fix unable to write exact number of posts in csv file.
//// const getAllPostAndSaveThem = async (url, requiredPosts = 100) => {
////     let newLink = url;
////     let totalPosts = 0;
////     while (newLink && (totalPosts <= requiredPosts)) {
////         let ddd = await getHtmlData(config.startUrl).then(getAllPostsInformation);
////         newLink = ddd.nextLink;
////         ddd.posts.forEach(post => {
////             //? Write to a csv file
////             if (totalPosts > requiredPosts) {
////                 console.log(`Written ${requiredPosts}. `);
////                 process.exit();
////             };
////             console.log(`${totalPosts},\"${post.title}\",${post.url},\"${post.date}\",./content/${totalPosts}-content.html \n`);
////             writeStream.write(`${totalPosts++},\"${post.title}\",${post.url},\"${post.date}\",./content/${totalPosts}-content.html \n`);
////         })
////         console.log(`Written *** `);
////     }
//// }