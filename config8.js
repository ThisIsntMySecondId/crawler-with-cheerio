//https://david-dm.org/matthewmueller/x-ray

//? Info Plz manually preprocess relative url to absolute url and vice versa
//Todo: You need to go inside the blog to get details
const config = {
    name: 'x-ray-dependencies',
    mainUrl: 'https://david-dm.org/',
    startUrl: 'https://david-dm.org/matthewmueller/x-ray',
    postWrapper: 'tbody tr',
    details: {
        dependency: {
            type: 'text',
            location: 'td.dep a',
            postprocessing: null,
            data: null,
        },
        changes: {
            type: 'text',
            location: 'td.changes',
            postprocessing: null,
            data: 'reactid',
        },
        changesTimes2: {
            type: 'text',
            location: 'td.changes',
            postprocessing: x => x*2,
            data: 'reactid',
        },
        required: {
            type: 'text',
            location: 'td.required',
            postprocessing: null,
            data: null,
        },
        stable: {
            type: 'text',
            location: 'td.stable',
            postprocessing: null,
            data: null,
        },
        latest: {
            type: 'text',
            location: 'td.latest',
            postprocessing: null,
            data: null,
        },
        status: {
            type: 'text',
            location: 'td.status span.sqr',
            postprocessing: null,
            data: 'reactid',
        },
    },
    nextButton: '',
    storeContent: false,
    contentLocation: '.blog-post',
    contentLink: 'postLink',
    get contentSaveLocation() {
        return `./${this.name}-content/`;
    },
    saveLocation: 'x-ray-dependencies.csv',
    get csvHeaders() {
        return Object.entries(config.details).map(x => x[0]).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

module.exports = config;

//TOdo: Three Types text, image, url
//Todo: Post Processing
//Todo: work with next button a little bit and add post processing in next button
//Todo: give resonable names to properties of config (not post, details etc.)