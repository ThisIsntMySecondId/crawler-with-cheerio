//? Info Plz manually preprocess relative url to absolute url and vice versa

const config = {
    name: 'stackabuse',
    mainUrl: 'https://stackabuse.com/',
    startUrl: 'https://stackabuse.com/',
    postWrapper: '.post',
    details: {
        title: {
            type: 'text',
            location: '.post-head h2.post-title a',
            postprocessing: null,
        },
        author: {
            type: 'text',
            location: '.post-meta .author a',
            postprocessing: null,
        },
        postLink: {
            type: 'url',
            location: '.post-head .post-title a',
            postprocessing: null,
        },
        date: {
            type: 'text',
            location: '.post-meta .date',
            postprocessing: null,
        },
    },
    nextButton: '.pagination a.older-posts',
    storeContent: true,
    contentLocation: '',
    contentLink: 'postLink',
    get contentSaveLocation() {
        return `./${this.name}-content/`;
    },
    saveLocation: 'post10.csv',
    get csvHeaders() {
        return Object.entries(config.details).map(x => x[0]).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

module.exports = config;

//TOdo: Three Types text, image, url
//Todo: Post Processing
//Todo: work with next button a little bit and add post processing in next button
//Todo: give resonable names to properties of config (not post, details etc.)