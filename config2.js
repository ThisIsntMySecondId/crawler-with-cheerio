const config = {
    mainUrl: 'https://2ality.com/',
    startUrl: 'https://2ality.com/',
    post: '.index-entry',
    details: {
        title: '.index-entry-title',
        postLink: '.index-entry-title a',
        date: '.index-entry-date-tags',
    },
    nextButton: '.index-page-nav a',
    saveLocation: 'post5.csv',
    csvHeaders: function () {
        return Object.keys(this.details).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

// console.log(Object.entries(config.details)[1][1]);

module.exports = config;

// console.log(config.startUrl.toString());