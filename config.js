const config = {
    mainUrl: 'https://stackabuse.com',
    startUrl: 'https://stackabuse.com/tag/node/',
    post: '.post',
    details: {
        title: '.post-head .post-title a',
        postLink: '.post-head .post-title a',
        author: '.post-head .post-meta .author a',
        date: '.post-head .post-meta .date',
        tags: '.post-footer .tag-list',
    },
    nextButton: 'nav.pagination a.older-posts',
    saveLocation: 'post4.csv',
    csvHeaders: function () {
        return Object.keys(this.details).map(x => (x.charAt(0).toUpperCase() + x.slice(1))).join(',');
    }
};

// console.log(Object.entries(config.details)[1][1]);

module.exports = config;

// console.log(config.startUrl.toString());